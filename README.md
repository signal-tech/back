# Amo RnD

Перейти cd /api

Запустить:
1) composer install (возможно придется на локальную машину поставить php8)
2) bin/console assets:install

Вернутся cd ..
Запустить генерацию сикретов для авторизации:
1) make exec service="api" command="bin/console fos:oauth-server:create-client --grant-type='http://grants/sodeCode'"
Данная команда отобразить сикрет и клиент айди

Для авторизации grant-type будет http://grants/sodeCode

Для миграций: (Вряд ли пригодится)
make exec service="api" command="bin/console doctrine:migrations:migrate -n"

Запуск листенера сделок:
1) Сам листенер: make exec service="api" command="bin/console socket:listen-traders-deals-ftx"
2) Запуск очереди сохранения сделок: make exec service="api" command="bin/console  messenger:consume amqp_orders"

Подсчет портфолио:
make exec service="api" command="bin/console  messenger:consume portfolio"

Подсчет статистики (сначала нужно посчитать портфолио):
make exec service="api" command="bin/console  messenger:consume statistics"

Смс:
make exec service="api" command="bin/console  messenger:consume auth_send_code"


ТУДУХИ
2) Сделать руками updatedAt и createdAt либа имеет баг, что бы обычной выгрузке сетиться updatedAt
4) Валидацию доступа по токенам (Юзер хочет сделать автоследование/мануальную сделку, а доступа нет)
6) Положить корректно файл с секретом для шифрования на сервер