APP_ENV=dev

api_post_update: api_cache_clear api_postgres_migrate
api_post_init: api_cache_clear api_postgres_migrate

api_reset: api_cache_remove api_postgres_reset api_post_init

api_postgres_reset: api_postgres_drop api_postgres_create api_postgres_migrate

api_postgres_drop:
	make exec service="api" command="bin/console doctrine:database:drop --force --if-exists"

api_postgres_create:
	make exec service="api" command="bin/console doctrine:database:create --if-not-exists"

api_postgres_migrate:
	make exec service="api" command="bin/console doctrine:migrations:migrate -n --all-or-nothing --allow-no-migration --no-debug -e $(APP_ENV)"

api_elastic_populate:
	make exec service="api" command="bin/console fos:elastica:populate -n --no-debug -e $(APP_ENV)"

api_elastic_reset:
	make exec service="api" command="bin/console fos:elastica:reset -n --force --no-debug -e $(APP_ENV)" \
	&& make exec service="api" command="bin/console fos:elastica:create -n --no-debug -e $(APP_ENV)"

api_test:
	make exec service="api" command="vendor/bin/codecept run --steps -n"

api_permission_fix:
	make exec service="api" command="mkdir -m 0777 -p var/cache"
	make exec service="api" command="mkdir -m 0777 -p var/log"
	make exec service="api" command="chmod -R 0777 var/log var/cache"

api_cache_clear:
	make exec service="api" command="bin/console cache:clear -n -e $(APP_ENV)"

api_cache_remove:
	make exec service="api" command="rm -rf var/cache/*"

# Сгенерирует RSA ключ для JWT
api_generate_jwt:
	echo "Default pass: 123456" \
	&& mkdir -p ./api/var/jwt \
	&& openssl genrsa -out ./api/var/jwt/private.pem -aes256 4096 \
	&& openssl rsa -pubout -in ./api/var/jwt/private.pem -out ./api/var/jwt/public.pem

api_phpcs:
	make exec service="api" command="./vendor/bin/php-cs-fixer --config=.php-cs-fixer.dist.php fix -v --diff --dry-run ./src ./tests"

api_phpcsfix:
	make exec service="api" command="./vendor/bin/php-cs-fixer --config=.php-cs-fixer.dist.php fix ./src ./tests"

api_stan:
	make exec service="api" command="php -d memory_limit=1024m vendor/bin/phpstan analyse"

api_psalm:                                       ## Статический анализ
	make exec service="api" command="php -d memory_limit=1024m vendor/bin/psalm --show-info=false --no-cache"
