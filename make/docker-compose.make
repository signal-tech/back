EXEC_OPTIONS=

start-containers:
	docker-compose -f docker-compose.yml up --build -d

stop:
	docker-compose -f docker-compose.yml stop

remove:
	docker-compose -f docker-compose.yml down --remove-orphans -v

exec:
	docker-compose -f docker-compose.yml exec $(EXEC_OPTIONS) $(service) $(command)

init_docker_compose:
	cp ./docker-compose.yml.dist ./docker-compose.yml