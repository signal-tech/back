<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211230152814 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE copy_trading_logs_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE copy_trading_logs (id INT NOT NULL, order_id INT NOT NULL, copy_trading_id INT NOT NULL, data JSON NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E6587FA88D9F6D38 ON copy_trading_logs (order_id)');
        $this->addSql('CREATE INDEX IDX_E6587FA8C2CFBBE5 ON copy_trading_logs (copy_trading_id)');
        $this->addSql('ALTER TABLE copy_trading_logs ADD CONSTRAINT FK_E6587FA88D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE copy_trading_logs ADD CONSTRAINT FK_E6587FA8C2CFBBE5 FOREIGN KEY (copy_trading_id) REFERENCES copy_tradings (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE copy_trading_logs_id_seq CASCADE');
        $this->addSql('DROP TABLE copy_trading_logs');
    }
}
