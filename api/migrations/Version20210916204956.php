<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210916204956 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders ADD order_id INT NOT NULL');
        $this->addSql('ALTER TABLE orders ADD client_id INT NOT NULL');
        $this->addSql('ALTER TABLE orders ADD market VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE orders ADD type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE orders ADD side VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE orders ADD size VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE orders ADD price VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE orders ADD time_in_force BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE orders ADD status VARCHAR(255) NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders DROP order_id');
        $this->addSql('ALTER TABLE orders DROP client_id');
        $this->addSql('ALTER TABLE orders DROP market');
        $this->addSql('ALTER TABLE orders DROP type');
        $this->addSql('ALTER TABLE orders DROP side');
        $this->addSql('ALTER TABLE orders DROP size');
        $this->addSql('ALTER TABLE orders DROP price');
        $this->addSql('ALTER TABLE orders DROP time_in_force');
        $this->addSql('ALTER TABLE orders DROP status');
    }
}
