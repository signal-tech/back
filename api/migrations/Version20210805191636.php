<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Feature\StockExchange\StockExchange;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210805191636 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('INSERT INTO stock_exchanges (id, name, code, icon_path, link)
VALUES (3, \'bitmex\', \'' . StockExchange::BITMEX_TYPE . '\', \'icons/bitmex_icon.png\', \'https://www.bitmex.com/register/1p2Pe1\')');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
