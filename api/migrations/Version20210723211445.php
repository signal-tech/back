<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Feature\Social\Social;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210723211445 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        $this->addSql('INSERT INTO socials (id, name, code) VALUES (1, \'telegram\', \'' . Social::TELEGRAM_CODE . '\')');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
