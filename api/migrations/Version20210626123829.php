<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210626123829 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders DROP CONSTRAINT fk_e52ffdeea76ed395');
        $this->addSql('ALTER TABLE orders DROP CONSTRAINT fk_e52ffdeeb7617794');
        $this->addSql('DROP INDEX idx_e52ffdeeb7617794');
        $this->addSql('DROP INDEX idx_e52ffdeea76ed395');
        $this->addSql('ALTER TABLE orders ADD stock_exchange_token_id INT NOT NULL');
        $this->addSql('ALTER TABLE orders DROP user_id');
        $this->addSql('ALTER TABLE orders DROP stock_exchange_id');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEEABED5A6C FOREIGN KEY (stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E52FFDEEABED5A6C ON orders (stock_exchange_token_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders DROP CONSTRAINT FK_E52FFDEEABED5A6C');
        $this->addSql('DROP INDEX IDX_E52FFDEEABED5A6C');
        $this->addSql('ALTER TABLE orders ADD stock_exchange_id INT NOT NULL');
        $this->addSql('ALTER TABLE orders RENAME COLUMN stock_exchange_token_id TO user_id');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT fk_e52ffdeea76ed395 FOREIGN KEY (user_id) REFERENCES users (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT fk_e52ffdeeb7617794 FOREIGN KEY (stock_exchange_id) REFERENCES stock_exchanges (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_e52ffdeeb7617794 ON orders (stock_exchange_id)');
        $this->addSql('CREATE INDEX idx_e52ffdeea76ed395 ON orders (user_id)');
    }
}
