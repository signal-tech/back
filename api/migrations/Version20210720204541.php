<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210720204541 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE statistics_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE statistics (id INT NOT NULL, stock_exchange_token_id INT NOT NULL, range_start_date_time BIGINT NOT NULL, range_end_date_time BIGINT NOT NULL, percent INT NOT NULL, type VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_E2D38B22ABED5A6C ON statistics (stock_exchange_token_id)');
        $this->addSql('ALTER TABLE statistics ADD CONSTRAINT FK_E2D38B22ABED5A6C FOREIGN KEY (stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE portfolios DROP last_listening_channel_connect');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE statistics_id_seq CASCADE');
        $this->addSql('DROP TABLE statistics');
        $this->addSql('ALTER TABLE portfolios ADD last_listening_channel_connect TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL');
    }
}
