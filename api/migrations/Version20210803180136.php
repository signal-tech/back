<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Feature\StockExchange\StockExchange;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210803180136 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->addSql('INSERT INTO stock_exchanges (id, name, code, icon_path, link)
VALUES (2, \'binance\', \'' . StockExchange::BINANCE_TYPE . '\', \'icons/binance_icon.png\', \'https://accounts.binance.com/ru/register?ref=197352477\')');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }
}
