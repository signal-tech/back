<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220104204734 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE copy_tradings DROP CONSTRAINT fk_a43ea13285bb829e');
        $this->addSql('ALTER TABLE copy_tradings DROP CONSTRAINT fk_a43ea13297d6a00c');
        $this->addSql('ALTER TABLE order_manual DROP CONSTRAINT fk_d354ae118e5d74ae');
        $this->addSql('ALTER TABLE statistics DROP CONSTRAINT fk_e2d38b228e5d74ae');
        $this->addSql('ALTER TABLE orders DROP CONSTRAINT fk_e52ffdee8e5d74ae');
        $this->addSql('ALTER TABLE portfolios DROP CONSTRAINT fk_b81b226f8e5d74ae');
        $this->addSql('DROP SEQUENCE stock_exchange_token_parts_id_seq CASCADE');
        $this->addSql('DROP TABLE stock_exchange_token_parts');
        $this->addSql('DROP INDEX idx_a43ea13285bb829e');
        $this->addSql('DROP INDEX idx_a43ea13297d6a00c');
        $this->addSql('ALTER TABLE copy_tradings ADD parent_stock_exchange_token_id INT NOT NULL');
        $this->addSql('ALTER TABLE copy_tradings ADD child_stock_exchange_token_id INT NOT NULL');
        $this->addSql('ALTER TABLE copy_tradings DROP parent_stock_exchange_token_part_id');
        $this->addSql('ALTER TABLE copy_tradings DROP child_stock_exchange_token_part_id');
        $this->addSql('ALTER TABLE copy_tradings ADD CONSTRAINT FK_A43EA132F26A5AB5 FOREIGN KEY (parent_stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE copy_tradings ADD CONSTRAINT FK_A43EA1324DC5B3ED FOREIGN KEY (child_stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_A43EA132F26A5AB5 ON copy_tradings (parent_stock_exchange_token_id)');
        $this->addSql('CREATE INDEX IDX_A43EA1324DC5B3ED ON copy_tradings (child_stock_exchange_token_id)');
        $this->addSql('DROP INDEX idx_d354ae118e5d74ae');
        $this->addSql('ALTER TABLE order_manual RENAME COLUMN stock_exchange_token_part_id TO stock_exchange_token_id');
        $this->addSql('ALTER TABLE order_manual ADD CONSTRAINT FK_D354AE11ABED5A6C FOREIGN KEY (stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_D354AE11ABED5A6C ON order_manual (stock_exchange_token_id)');
        $this->addSql('DROP INDEX idx_e52ffdee8e5d74ae');
        $this->addSql('ALTER TABLE orders RENAME COLUMN stock_exchange_token_part_id TO stock_exchange_token_id');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEEABED5A6C FOREIGN KEY (stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E52FFDEEABED5A6C ON orders (stock_exchange_token_id)');
        $this->addSql('DROP INDEX idx_b81b226f8e5d74ae');
        $this->addSql('ALTER TABLE portfolios RENAME COLUMN stock_exchange_token_part_id TO stock_exchange_token_id');
        $this->addSql('ALTER TABLE portfolios ADD CONSTRAINT FK_B81B226FABED5A6C FOREIGN KEY (stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B81B226FABED5A6C ON portfolios (stock_exchange_token_id)');
        $this->addSql('DROP INDEX idx_e2d38b228e5d74ae');
        $this->addSql('ALTER TABLE statistics RENAME COLUMN stock_exchange_token_part_id TO stock_exchange_token_id');
        $this->addSql('ALTER TABLE statistics ADD CONSTRAINT FK_E2D38B22ABED5A6C FOREIGN KEY (stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E2D38B22ABED5A6C ON statistics (stock_exchange_token_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE stock_exchange_token_parts_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE stock_exchange_token_parts (id INT NOT NULL, stock_exchange_token_id INT NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_listening_channel_connect TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, type VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX idx_af67e56dabed5a6c ON stock_exchange_token_parts (stock_exchange_token_id)');
        $this->addSql('ALTER TABLE stock_exchange_token_parts ADD CONSTRAINT fk_af67e56dabed5a6c FOREIGN KEY (stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE copy_tradings DROP CONSTRAINT FK_A43EA132F26A5AB5');
        $this->addSql('ALTER TABLE copy_tradings DROP CONSTRAINT FK_A43EA1324DC5B3ED');
        $this->addSql('DROP INDEX IDX_A43EA132F26A5AB5');
        $this->addSql('DROP INDEX IDX_A43EA1324DC5B3ED');
        $this->addSql('ALTER TABLE copy_tradings ADD parent_stock_exchange_token_part_id INT NOT NULL');
        $this->addSql('ALTER TABLE copy_tradings ADD child_stock_exchange_token_part_id INT NOT NULL');
        $this->addSql('ALTER TABLE copy_tradings DROP parent_stock_exchange_token_id');
        $this->addSql('ALTER TABLE copy_tradings DROP child_stock_exchange_token_id');
        $this->addSql('ALTER TABLE copy_tradings ADD CONSTRAINT fk_a43ea13285bb829e FOREIGN KEY (parent_stock_exchange_token_part_id) REFERENCES stock_exchange_token_parts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE copy_tradings ADD CONSTRAINT fk_a43ea13297d6a00c FOREIGN KEY (child_stock_exchange_token_part_id) REFERENCES stock_exchange_token_parts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_a43ea13285bb829e ON copy_tradings (parent_stock_exchange_token_part_id)');
        $this->addSql('CREATE INDEX idx_a43ea13297d6a00c ON copy_tradings (child_stock_exchange_token_part_id)');
        $this->addSql('ALTER TABLE order_manual DROP CONSTRAINT FK_D354AE11ABED5A6C');
        $this->addSql('DROP INDEX IDX_D354AE11ABED5A6C');
        $this->addSql('ALTER TABLE order_manual RENAME COLUMN stock_exchange_token_id TO stock_exchange_token_part_id');
        $this->addSql('ALTER TABLE order_manual ADD CONSTRAINT fk_d354ae118e5d74ae FOREIGN KEY (stock_exchange_token_part_id) REFERENCES stock_exchange_token_parts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_d354ae118e5d74ae ON order_manual (stock_exchange_token_part_id)');
        $this->addSql('ALTER TABLE statistics DROP CONSTRAINT FK_E2D38B22ABED5A6C');
        $this->addSql('DROP INDEX IDX_E2D38B22ABED5A6C');
        $this->addSql('ALTER TABLE statistics RENAME COLUMN stock_exchange_token_id TO stock_exchange_token_part_id');
        $this->addSql('ALTER TABLE statistics ADD CONSTRAINT fk_e2d38b228e5d74ae FOREIGN KEY (stock_exchange_token_part_id) REFERENCES stock_exchange_token_parts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_e2d38b228e5d74ae ON statistics (stock_exchange_token_part_id)');
        $this->addSql('ALTER TABLE orders DROP CONSTRAINT FK_E52FFDEEABED5A6C');
        $this->addSql('DROP INDEX IDX_E52FFDEEABED5A6C');
        $this->addSql('ALTER TABLE orders RENAME COLUMN stock_exchange_token_id TO stock_exchange_token_part_id');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT fk_e52ffdee8e5d74ae FOREIGN KEY (stock_exchange_token_part_id) REFERENCES stock_exchange_token_parts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_e52ffdee8e5d74ae ON orders (stock_exchange_token_part_id)');
        $this->addSql('ALTER TABLE portfolios DROP CONSTRAINT FK_B81B226FABED5A6C');
        $this->addSql('DROP INDEX IDX_B81B226FABED5A6C');
        $this->addSql('ALTER TABLE portfolios RENAME COLUMN stock_exchange_token_id TO stock_exchange_token_part_id');
        $this->addSql('ALTER TABLE portfolios ADD CONSTRAINT fk_b81b226f8e5d74ae FOREIGN KEY (stock_exchange_token_part_id) REFERENCES stock_exchange_token_parts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_b81b226f8e5d74ae ON portfolios (stock_exchange_token_part_id)');
    }
}
