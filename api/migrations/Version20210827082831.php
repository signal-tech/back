<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210827082831 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders ADD data JSON NOT NULL');
        $this->addSql('ALTER TABLE orders DROP remote_id');
        $this->addSql('ALTER TABLE orders DROP remote_client_id');
        $this->addSql('ALTER TABLE orders DROP market');
        $this->addSql('ALTER TABLE orders DROP type');
        $this->addSql('ALTER TABLE orders DROP side');
        $this->addSql('ALTER TABLE orders DROP size');
        $this->addSql('ALTER TABLE orders DROP price');
        $this->addSql('ALTER TABLE orders DROP reduce_only');
        $this->addSql('ALTER TABLE orders DROP ioc');
        $this->addSql('ALTER TABLE orders DROP post_only');
        $this->addSql('ALTER TABLE orders DROP status');
        $this->addSql('ALTER TABLE orders DROP filled_size');
        $this->addSql('ALTER TABLE orders DROP remaining_size');
        $this->addSql('ALTER TABLE orders DROP avg_fill_price');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders ADD remote_id VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE orders ADD remote_client_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE orders ADD market VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE orders ADD type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE orders ADD side VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE orders ADD size DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE orders ADD price DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE orders ADD reduce_only BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE orders ADD ioc BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE orders ADD post_only BOOLEAN NOT NULL');
        $this->addSql('ALTER TABLE orders ADD status VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE orders ADD filled_size DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE orders ADD remaining_size DOUBLE PRECISION NOT NULL');
        $this->addSql('ALTER TABLE orders ADD avg_fill_price DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE orders DROP data');
    }
}
