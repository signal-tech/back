<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210821193101 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders DROP CONSTRAINT fk_e52ffdeeabed5a6c');
        $this->addSql('DROP INDEX idx_e52ffdeeabed5a6c');
        $this->addSql('ALTER TABLE orders RENAME COLUMN stock_exchange_token_id TO stock_exchange_token_part_id');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT FK_E52FFDEE8E5D74AE FOREIGN KEY (stock_exchange_token_part_id) REFERENCES stock_exchange_token_parts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E52FFDEE8E5D74AE ON orders (stock_exchange_token_part_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders DROP CONSTRAINT FK_E52FFDEE8E5D74AE');
        $this->addSql('DROP INDEX IDX_E52FFDEE8E5D74AE');
        $this->addSql('ALTER TABLE orders RENAME COLUMN stock_exchange_token_part_id TO stock_exchange_token_id');
        $this->addSql('ALTER TABLE orders ADD CONSTRAINT fk_e52ffdeeabed5a6c FOREIGN KEY (stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_e52ffdeeabed5a6c ON orders (stock_exchange_token_id)');
    }
}
