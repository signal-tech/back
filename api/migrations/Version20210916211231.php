<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210916211231 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders DROP "size"');
        $this->addSql('ALTER TABLE orders ADD size  DOUBLE PRECISION  NOT NULL');

        $this->addSql('ALTER TABLE orders DROP "price"');
        $this->addSql('ALTER TABLE orders ADD price  DOUBLE PRECISION  NOT NULL');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE orders ALTER size TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE orders ALTER size DROP DEFAULT');
        $this->addSql('ALTER TABLE orders ALTER price TYPE VARCHAR(255)');
        $this->addSql('ALTER TABLE orders ALTER price DROP DEFAULT');
    }
}
