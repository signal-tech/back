<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210723213720 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE user_socials ADD social_id INT NOT NULL');
        $this->addSql('ALTER TABLE user_socials ADD CONSTRAINT FK_30AEA663FFEB5B27 FOREIGN KEY (social_id) REFERENCES socials (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_30AEA663FFEB5B27 ON user_socials (social_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE user_socials DROP CONSTRAINT FK_30AEA663FFEB5B27');
        $this->addSql('DROP INDEX IDX_30AEA663FFEB5B27');
        $this->addSql('ALTER TABLE user_socials DROP social_id');
    }
}
