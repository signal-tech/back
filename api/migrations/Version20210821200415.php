<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210821200415 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE statistics DROP CONSTRAINT fk_e2d38b22abed5a6c');
        $this->addSql('DROP INDEX idx_e2d38b22abed5a6c');
        $this->addSql('ALTER TABLE statistics RENAME COLUMN stock_exchange_token_id TO stock_exchange_token_part_id');
        $this->addSql('ALTER TABLE statistics ADD CONSTRAINT FK_E2D38B228E5D74AE FOREIGN KEY (stock_exchange_token_part_id) REFERENCES stock_exchange_token_parts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_E2D38B228E5D74AE ON statistics (stock_exchange_token_part_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE statistics DROP CONSTRAINT FK_E2D38B228E5D74AE');
        $this->addSql('DROP INDEX IDX_E2D38B228E5D74AE');
        $this->addSql('ALTER TABLE statistics RENAME COLUMN stock_exchange_token_part_id TO stock_exchange_token_id');
        $this->addSql('ALTER TABLE statistics ADD CONSTRAINT fk_e2d38b22abed5a6c FOREIGN KEY (stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_e2d38b22abed5a6c ON statistics (stock_exchange_token_id)');
    }
}
