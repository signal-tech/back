<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210812171021 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE stock_exchange_token_parts_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE stock_exchange_token_parts (id INT NOT NULL, stock_exchange_token_id INT NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, last_listening_channel_connect TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, name VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_AF67E56DABED5A6C ON stock_exchange_token_parts (stock_exchange_token_id)');
        $this->addSql('ALTER TABLE portfolios DROP CONSTRAINT fk_b81b226fabed5a6c');
        $this->addSql('DROP INDEX idx_b81b226fabed5a6c');
        $this->addSql('ALTER TABLE portfolios RENAME COLUMN stock_exchange_token_id TO stock_exchange_token_part_id');
        $this->addSql('ALTER TABLE portfolios ADD CONSTRAINT FK_B81B226F8E5D74AE FOREIGN KEY (stock_exchange_token_part_id) REFERENCES stock_exchange_token_parts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B81B226F8E5D74AE ON portfolios (stock_exchange_token_part_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE portfolios DROP CONSTRAINT FK_B81B226F8E5D74AE');
        $this->addSql('DROP SEQUENCE stock_exchange_token_parts_id_seq CASCADE');
        $this->addSql('DROP TABLE stock_exchange_token_parts');
        $this->addSql('DROP INDEX IDX_B81B226F8E5D74AE');
        $this->addSql('ALTER TABLE portfolios RENAME COLUMN stock_exchange_token_part_id TO stock_exchange_token_id');
        $this->addSql('ALTER TABLE portfolios ADD CONSTRAINT fk_b81b226fabed5a6c FOREIGN KEY (stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_b81b226fabed5a6c ON portfolios (stock_exchange_token_id)');
    }
}
