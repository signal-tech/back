<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210617170055 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE stock_exchange_tokens ADD stock_exchange_id INT NOT NULL');
        $this->addSql('ALTER TABLE stock_exchange_tokens DROP type');
        $this->addSql('ALTER TABLE stock_exchange_tokens ADD CONSTRAINT FK_33EA94C1B7617794 FOREIGN KEY (stock_exchange_id) REFERENCES stock_exchanges (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_33EA94C1B7617794 ON stock_exchange_tokens (stock_exchange_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE stock_exchange_tokens DROP CONSTRAINT FK_33EA94C1B7617794');
        $this->addSql('DROP INDEX IDX_33EA94C1B7617794');
        $this->addSql('ALTER TABLE stock_exchange_tokens ADD type VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE stock_exchange_tokens DROP stock_exchange_id');
    }
}
