<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210812173203 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE stock_exchange_token_parts DROP name');
        $this->addSql('ALTER TABLE stock_exchange_token_parts ADD CONSTRAINT FK_AF67E56DABED5A6C FOREIGN KEY (stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE stock_exchange_token_parts DROP CONSTRAINT FK_AF67E56DABED5A6C');
        $this->addSql('ALTER TABLE stock_exchange_token_parts ADD name VARCHAR(255) NOT NULL');
    }
}
