<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210709220635 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE portfolios DROP CONSTRAINT fk_b81b226fb7617794');
        $this->addSql('DROP INDEX idx_b81b226fb7617794');
        $this->addSql('ALTER TABLE portfolios RENAME COLUMN stock_exchange_id TO stock_exchange_token_id');
        $this->addSql('ALTER TABLE portfolios ADD CONSTRAINT FK_B81B226FABED5A6C FOREIGN KEY (stock_exchange_token_id) REFERENCES stock_exchange_tokens (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX IDX_B81B226FABED5A6C ON portfolios (stock_exchange_token_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE portfolios DROP CONSTRAINT FK_B81B226FABED5A6C');
        $this->addSql('DROP INDEX IDX_B81B226FABED5A6C');
        $this->addSql('ALTER TABLE portfolios RENAME COLUMN stock_exchange_token_id TO stock_exchange_id');
        $this->addSql('ALTER TABLE portfolios ADD CONSTRAINT fk_b81b226fb7617794 FOREIGN KEY (stock_exchange_id) REFERENCES stock_exchanges (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('CREATE INDEX idx_b81b226fb7617794 ON portfolios (stock_exchange_id)');
    }
}
