<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210709220520 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE portfolios_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE portfolios (id INT NOT NULL, stock_exchange_id INT NOT NULL, last_listening_channel_connect TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, data JSON NOT NULL, range_start_date_time BIGINT NOT NULL, range_end_date_time BIGINT NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_B81B226FB7617794 ON portfolios (stock_exchange_id)');
        $this->addSql('ALTER TABLE portfolios ADD CONSTRAINT FK_B81B226FB7617794 FOREIGN KEY (stock_exchange_id) REFERENCES stock_exchanges (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE portfolios_id_seq CASCADE');
        $this->addSql('DROP TABLE portfolios');
    }
}
