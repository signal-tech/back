<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use App\Feature\Social\Social;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210808181340 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE socials ADD icon_path VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE stock_exchanges ALTER icon_path SET NOT NULL');
        $this->addSql('UPDATE socials SET icon_path = \'icons/telegram_icon.svg\' WHERE id = \'1\'');
        $this->addSql('ALTER TABLE socials ALTER icon_path SET NOT NULL');

        $this->addSql('INSERT INTO socials (id, name, code, icon_path) VALUES (2, \'youtube\', \'' . Social::YOUTUBE_CODE . '\', \'icons/youtube_icon.svg\')');
        $this->addSql('INSERT INTO socials (id, name, code, icon_path) VALUES (3, \'instagram\', \'' . Social::INSTAGRAM_CODE . '\', \'icons/instagram_icon.svg\')');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE socials DROP icon_path');
        $this->addSql('ALTER TABLE stock_exchanges ALTER icon_path DROP NOT NULL');
    }
}
