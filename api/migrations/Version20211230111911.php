<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211230111911 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE copy_tradings_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE copy_tradings (id INT NOT NULL, parent_stock_exchange_token_part_id INT NOT NULL, child_stock_exchange_token_part_id INT NOT NULL, deleted_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
        $this->addSql('CREATE INDEX IDX_A43EA13285BB829E ON copy_tradings (parent_stock_exchange_token_part_id)');
        $this->addSql('CREATE INDEX IDX_A43EA13297D6A00C ON copy_tradings (child_stock_exchange_token_part_id)');
        $this->addSql('ALTER TABLE copy_tradings ADD CONSTRAINT FK_A43EA13285BB829E FOREIGN KEY (parent_stock_exchange_token_part_id) REFERENCES stock_exchange_token_parts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE copy_tradings ADD CONSTRAINT FK_A43EA13297D6A00C FOREIGN KEY (child_stock_exchange_token_part_id) REFERENCES stock_exchange_token_parts (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'postgresql', 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE copy_tradings_id_seq CASCADE');
        $this->addSql('DROP TABLE copy_tradings');
    }
}
