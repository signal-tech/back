<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210911152009 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE market_historical_prices ADD from_coin VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE market_historical_prices ADD to_coin VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE market_historical_prices DROP "from"');
        $this->addSql('ALTER TABLE market_historical_prices DROP "to"');
        $this->addSql('ALTER TABLE market_historical_prices RENAME COLUMN time TO price_time');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('ALTER TABLE market_historical_prices ADD "from" VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE market_historical_prices ADD "to" VARCHAR(255) NOT NULL');
        $this->addSql('ALTER TABLE market_historical_prices DROP from_coin');
        $this->addSql('ALTER TABLE market_historical_prices DROP to_coin');
        $this->addSql('ALTER TABLE market_historical_prices RENAME COLUMN price_time TO "time"');
    }
}
