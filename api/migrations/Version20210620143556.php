<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210620143556 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('CREATE SEQUENCE orders_id_seq INCREMENT BY 1 MINVALUE 1 START 1');
        $this->addSql('CREATE TABLE orders (id INT NOT NULL, remote_id INT NOT NULL, remote_client_id INT DEFAULT NULL, market VARCHAR(255) NOT NULL, type VARCHAR(255) NOT NULL, side VARCHAR(255) NOT NULL, size DOUBLE PRECISION NOT NULL, price DOUBLE PRECISION NOT NULL, reduce_only BOOLEAN NOT NULL, ioc BOOLEAN NOT NULL, post_only BOOLEAN NOT NULL, status VARCHAR(255) NOT NULL, filled_size DOUBLE PRECISION NOT NULL, remaining_size DOUBLE PRECISION NOT NULL, avg_fill_price DOUBLE PRECISION NOT NULL, created_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL, PRIMARY KEY(id))');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf('postgresql' !== $this->connection->getDatabasePlatform()->getName(), 'Migration can only be executed safely on \'postgresql\'.');

        $this->addSql('DROP SEQUENCE orders_id_seq CASCADE');
        $this->addSql('DROP TABLE orders');
    }
}
