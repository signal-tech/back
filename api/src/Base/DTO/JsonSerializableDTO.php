<?php

namespace App\Base\DTO;

use JsonSerializable;
use ReflectionClass;
use function get_class;
use function is_array;
use function is_object;

class JsonSerializableDTO implements JsonSerializable
{
    /**
     * @return mixed[]
     */
    public function jsonSerialize(): array
    {
        return $this->extractProps($this);
    }

    private function extractProps(object $object): array
    {
        $public = [];

        $reflection = new ReflectionClass(get_class($object));

        foreach ($reflection->getProperties() as $property) {
            $property->setAccessible(true);

            $value = $property->getValue($object);
            $name = $property->getName();

            if (is_array($value)) {
                $public[$name] = [];

                foreach ($value as $item) {
                    if (is_object($item)) {
                        $itemArray = $this->extractProps($item);
                        $public[$name][] = $itemArray;
                    } else {
                        $public[$name][] = $item;
                    }
                }
            } elseif (is_object($value)) {
                $public[$name] = $this->extractProps($value);
            } else {
                $public[$name] = $value;
            }
        }

        return $public;
    }
}
