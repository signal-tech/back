<?php

namespace App\Domain\Common;

interface ViewsCountableInterface
{
    public function getViews(): int;

    public function incrementViews(): self;
}
