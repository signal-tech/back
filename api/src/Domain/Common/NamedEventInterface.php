<?php

namespace App\Domain\Common;

interface NamedEventInterface
{
    public static function getName(): string;
}
