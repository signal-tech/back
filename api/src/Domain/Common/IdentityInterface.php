<?php

namespace App\Domain\Common;

interface IdentityInterface
{
    public function getId(): int;

    public function setId(int $id): self;
}
