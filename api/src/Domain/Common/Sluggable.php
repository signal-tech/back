<?php

namespace App\Domain\Common;

interface Sluggable
{
    public function getSlug(): string;
}
