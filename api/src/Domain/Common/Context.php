<?php

namespace App\Domain\Common;

/**
 * Контексты сериализации с описанием
 */
interface Context
{
    /**
     * Обновление своего профиля (для мастера).
     */
    public const USER_PROFILE_UPDATE = 'user_profile_update';
}
