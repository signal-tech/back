<?php

namespace App\Domain\User;

use App\Domain\Common\IdentityInterface;

interface User extends IdentityInterface
{
    public function getName(): string;

    public function setName(string $name): self;

    // todo: добавить домен аватарки юзера

    public function getSurName(): string;

    public function setSurname(string $surname): self;

    public function getPhone(): ?string;

    public function setPhone(?string $phone): self;
}
