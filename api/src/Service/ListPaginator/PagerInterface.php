<?php

namespace App\Service\ListPaginator;

interface PagerInterface
{
    public function getPage(): int;

    public function getPerPage(): int;

    public function getPages(): int;

    public function getCount(): int;

    public function getTotal(): int;
}
