<?php

namespace App\Service\ListPaginator;

use function ceil;

class Pager implements PagerInterface
{
    private int $page;

    private int $perPage;

    private int $pages;

    private int $count;

    private int $total;

    /**
     * Pager constructor.
     */
    public function __construct(int $page, int $perPage, int $count, int $total)
    {
        $this->page = $page;
        $this->perPage = $perPage;
        $this->pages = (int) ceil($total / $perPage);
        $this->count = $count;
        $this->total = $total;
    }

    public function getPage(): int
    {
        return $this->page;
    }

    public function getPerPage(): int
    {
        return $this->perPage;
    }

    public function getPages(): int
    {
        return $this->pages;
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function getTotal(): int
    {
        return $this->total;
    }
}
