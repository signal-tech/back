<?php

namespace App\Service\ListPaginator;

use function count;

class PaginateResponse implements PaginateResponseInterface
{
    private PagerInterface $pager;

    /**
     * @var object[]
     */
    private array $data;

    private ?object $metadata;

    /**
     * PaginateResponse constructor.
     *
     * @param object[] $data
     */
    public function __construct(array $data, int $page, int $perPage, int $total)
    {
        $this->data = $data;
        $this->metadata = null;
        $this->pager = new Pager($page, $perPage, count($data), $total);
    }

    /**
     * @return object[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    public function getMetadata(): ?object
    {
        return $this->metadata;
    }

    /**
     * @param ?object $metadata
     *
     * @return PaginateResponse
     */
    public function setMetadata(?object $metadata): self
    {
        $this->metadata = $metadata;

        return $this;
    }

    public function getPager(): PagerInterface
    {
        return $this->pager;
    }

    public function setPager(PagerInterface $pager): self
    {
        $this->pager = $pager;

        return $this;
    }
}
