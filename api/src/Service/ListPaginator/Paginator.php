<?php

namespace App\Service\ListPaginator;

use AutoMapperPlus\AutoMapperInterface;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\Query\Parameter;
use Doctrine\ORM\Query\Parser;
use Doctrine\ORM\QueryBuilder;
use function array_filter;
use function array_key_exists;
use function count;
use function is_array;
use function is_callable;
use function sprintf;

class Paginator implements PaginatorInterface
{
    private AutoMapperInterface $mapper;

    public function __construct(
        AutoMapperInterface $mapper
    ) {
        $this->mapper = $mapper;
    }

    /**
     * @param ?string   $targetDataClass
     * @param ?callable $dataMutator
     *
     * @throws NoResultException
     * @throws NonUniqueResultException
     *
     * @psalm-suppress MixedArgument
     */
    public function createResponse(
        QueryBuilder $qb,
        int $page,
        int $perPage,
        ?string $targetDataClass = null,
        ?callable $dataMutator = null
    ): PaginateResponseInterface {
        $calculateTotalQb = clone $qb;
        $data = $qb
            ->setFirstResult(($page - 1) * $perPage)
            ->setMaxResults($perPage)
            ->getQuery()
            ->getResult();

        if (null !== $targetDataClass && is_array($data) && count($data) > 0) {
            $data = $this->mapper->mapMultiple($data, $targetDataClass);
        }

        if (is_callable($dataMutator)) {
            /** @var array $data */
            $data = $dataMutator($data);
        }

        return new PaginateResponse($data, $page, $perPage, $this->calculateTotal($calculateTotalQb) ?? 0);
    }

    /**
     * @throws NonUniqueResultException
     * @throws NoResultException
     */
    private function calculateTotal(QueryBuilder $queryBuilder): ?int
    {
        $qb = clone $queryBuilder;
        $qb->setParameters(clone $qb->getParameters());

        $rootAliases = $qb->getRootAliases();

        if (!isset($rootAliases[0])) {
            return null;
        }

        $join = [];

        /**
         * @var string $alias
         * @var array  $part
         */
        foreach ($qb->getDQLPart('join') as $alias => $part) {
            $join[$alias] = array_filter(
                $part,
                static function (Join $join) {
                    return Join::INNER_JOIN === $join->getJoinType();
                }
            );
        }

        /** @var string $alias */
        $alias = $rootAliases[0];

        $qb
            ->select(sprintf('count(%s)', $alias))
            ->resetDQLParts(['orderBy', 'groupBy'])
            ->setFirstResult(0)
            ->setMaxResults(null)
            ->add('join', $join); /** @phpstan-ignore-line */
        $dbQuery = $qb->getQuery();

        $parser = new Parser($dbQuery);
        $parameterMappings = $parser->parse()->getParameterMappings();
        /** @var Parameter[] $parameters */
        $parameters = $qb->getParameters();

        foreach ($parameters as $key => $parameter) {
            $parameterName = $parameter->getName();

            if (!(isset($parameterMappings[$parameterName]) || array_key_exists($parameterName, $parameterMappings))) {
                unset($parameters[$key]);
            }
        }

        $dbQuery->setParameters($parameters);
        $returnData = $dbQuery->getSingleScalarResult();

        return $returnData ? (int) $returnData : null;
    }
}
