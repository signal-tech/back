<?php

namespace App\Service\ListPaginator;

use Doctrine\ORM\QueryBuilder;

interface PaginatorInterface
{
    public function createResponse(
        QueryBuilder $qb,
        int $page,
        int $perPage,
        ?string $targetDataClass = null,
        ?callable $dataMutator = null
    ): PaginateResponseInterface;
}
