<?php

namespace App\Service\ListPaginator;

/**
 * Формат ответа списков.
 */
interface PaginateResponseInterface
{
    /**
     * @return object[]
     */
    public function getData(): array;

    public function getMetadata(): ?object;

    public function getPager(): PagerInterface;
}
