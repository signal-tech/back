<?php

namespace App\Service\ListPaginator;

use App\Service\Request\RequestObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;
use function is_numeric;

class PagerRequest implements RequestObjectInterface
{
    /**
     * @Assert\NotBlank
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    public int $page = 1;

    /**
     * @Assert\NotBlank
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1", max="20")
     */
    public int $perPage = 10;

    /**
     * @param string[] $request
     */
    public static function createFromRequestPayload(array $request): self
    {
        $filter = new self(); //TODO дублирование кода из за этой строки потом поправить
        $filter->page = self::calculatePageValue($request, 'page', 1);
        $filter->perPage = self::calculatePageValue($request, 'perPage', 300); //todo для фронта пока быстрее так, потом уменьшить

        return $filter;
    }

    /**
     * @param string[] $request
     */
    protected static function calculatePageValue(array $request, string $key, int $default = 1): int
    {
        return isset($request[$key]) && is_numeric($request[$key]) ? (int) $request[$key] : $default;
    }
}
