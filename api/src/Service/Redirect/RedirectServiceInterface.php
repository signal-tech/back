<?php

namespace App\Service\Redirect;

interface RedirectServiceInterface
{
    public function getRealUrl(string $type, int $id): string;

    public function createRedirectLink(object $entity): string;
}
