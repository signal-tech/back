<?php

namespace App\Service\Redirect;

use App\Feature\Discount\DiscountEntity;
use App\Feature\Product\ProductEntity;
use App\Feature\Redirect\Message\RedirectInfoMessage;
use App\Feature\Shop\ShopEntity;
use App\Service\Environment\EnvironmentService;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Messenger\MessageBusInterface;

class RedirectService implements RedirectServiceInterface
{
//    private EntityManagerInterface $em;
//
//    private MessageBusInterface $messageBus;
//
//    public function __construct(
//        EntityManagerInterface $em,
//        MessageBusInterface $messageBus
//    ) {
//        $this->em = $em;
//        $this->messageBus = $messageBus;
//    }

    public function getRealUrl(string $type, int $id): string
    {
//        switch ($type) {
//            case 'shop':
//                $shopRepository = $this->em->getRepository(ShopEntity::class);
//
//                /** @var ShopEntity|null $shop */
//                $shop = $shopRepository->find($id);
//
//                if (null === $shop) {
//                    throw new Exception('Данного магазина не существует');
//                }
//
//                $goToLink = $shop->getGoToLink();
//                $redirectedUrl = !empty($goToLink) ? $goToLink : $shop->getSiteUrl();
//
//                $this->messageBus->dispatch(new RedirectInfoMessage($type, $id, $redirectedUrl));
//
//                return $redirectedUrl;
//            case 'discount':
//                $discountRepository = $this->em->getRepository(DiscountEntity::class);
//
//                /** @var DiscountEntity|null $discount */
//                $discount = $discountRepository->find($id);
//
//                if (null === $discount || empty($discount->getGoToLink())) {
//                    throw new Exception('Данной акции не существует');
//                }
//
//                $redirectedUrl = $discount->getGoToLink();
//
//                $this->messageBus->dispatch(new RedirectInfoMessage($type, $id, $redirectedUrl));
//
//                return $redirectedUrl;
//            case 'product':
//                $productRepository = $this->em->getRepository(ProductEntity::class);
//
//                /** @var ProductEntity|null $product */
//                $product = $productRepository->find($id);
//
//                if (null === $product || empty($product->getGoToLink())) {
//                    throw new Exception('Данного товара не существует');
//                }
//
//                $redirectedUrl = $product->getGoToLink();
//
//                $this->messageBus->dispatch(new RedirectInfoMessage($type, $id, $redirectedUrl));
//
//                return $redirectedUrl;
//        }

        return '';
        // throw new Exception('Ошибка передаваемого типа');
    }

    public function createRedirectLink(object $entity): string
    {
//        if ($entity instanceof ShopEntity) {
//            return EnvironmentService::getHost() . '/api/v1/r/shop/' . $entity->getId();
//        }
//
//        if ($entity instanceof DiscountEntity) {
//            return EnvironmentService::getHost() . '/api/v1/r/discount/' . $entity->getId();
//        }
//
//        if ($entity instanceof ProductEntity) {
//            return EnvironmentService::getHost() . '/api/v1/r/product/' . $entity->getId();
//        }
//

        return '';
        // throw new Exception('Ошибка типа сущности');
    }
}
