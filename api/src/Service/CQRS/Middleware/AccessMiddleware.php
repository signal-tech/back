<?php

namespace App\Service\CQRS\Middleware;

use App\Infrastructure\OAuth\Service\TokenService;
use App\Service\CQRS\AccessibleInterface;
use League\Tactician\Handler\Locator\HandlerLocator;
use League\Tactician\Middleware;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Contracts\Translation\TranslatorInterface;
use function get_class;

class AccessMiddleware implements Middleware
{
    private HandlerLocator $handlerLocator;

    private TranslatorInterface $translator;

    private TokenService $tokenService;

    public function __construct(
        HandlerLocator $handlerLocator,
        TranslatorInterface $translator,
        TokenService $tokenService
    ) {
        $this->handlerLocator = $handlerLocator;
        $this->translator = $translator;
        $this->tokenService = $tokenService;
    }

    /**
     * @param object $command
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        $handler = $this->handlerLocator->getHandlerForCommand(get_class($command));

        if ($handler instanceof AccessibleInterface
            && !$handler->hasAccess($command, $this->tokenService->getUserByToken())) {
            throw new AccessDeniedException($this->translator->trans('user.access_denied'));
        }

        return $next($command);
    }
}
