<?php

namespace App\Service\CQRS;

use App\Feature\User\User;

abstract class AbstractAuthorizedAccessible implements AccessibleInterface
{
    public function hasAccess(object $command, ?User $authorizedUser): bool
    {
        return !(null === $authorizedUser);
    }
}
