<?php

namespace App\Service\CQRS;

use App\Feature\User\User;

interface AccessibleInterface
{
    public function hasAccess(object $command, ?User $authorizedUser): bool;
}
