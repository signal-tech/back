<?php

namespace App\Service\Url;

interface UrlServiceInterface
{
    public function getPlatformUrl(string $url): string;
}
