<?php

namespace App\Service\Url;

use App\Service\Environment\EnvironmentService;

class UrlService implements UrlServiceInterface
{
    public function getPlatformUrl(string $url): string
    {
        return EnvironmentService::getHost() . $url;
    }
}
