<?php

namespace App\Service\Crypt;

use App\Service\Environment\EnvironmentService;
use ParagonIE\Halite\KeyFactory;
use ParagonIE\Halite\Symmetric\Crypto as Symmetric;
use ParagonIE\Halite\Symmetric\EncryptionKey;
use ParagonIE\HiddenString\HiddenString;
use Psr\Log\LoggerInterface;
use Throwable;

class CryptService implements CryptServiceInterface
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    public function encrypt(string $value): string
    {
        return Symmetric::encrypt(new HiddenString($value), $this->loadKey());
    }

    public function decrypt(string $value): string
    {
        return Symmetric::decrypt($value, $this->loadKey())->getString();
    }

    private function loadKey(): EncryptionKey
    {
        try {
            return KeyFactory::loadEncryptionKey(EnvironmentService::getEncryptionPath());
        } catch (Throwable $e) {
            $this->logger->emergency(
                'Unable to load the encryption key!',
                [
                    'error' => $e->getMessage(),
                ]
            );

            throw $e;
        }
    }
}
