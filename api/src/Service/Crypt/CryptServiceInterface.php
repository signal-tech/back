<?php

namespace App\Service\Crypt;

interface CryptServiceInterface
{
    public function encrypt(string $value): string;

    public function decrypt(string $value): string;
}
