<?php

namespace App\Service\Mapper;

use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;

class CustomOperation
{
    public static function fromPropertyPath(string $propertyPath): FromPropertyPathOperation
    {
        return new FromPropertyPathOperation($propertyPath);
    }

    /**
     * @param string[] $ignoredAttributes
     */
    public static function beConstructedUsingDenormalizer(
        DenormalizerInterface $denormalizer,
        string $className,
        array $ignoredAttributes = ['id']
    ): BeConstructedUsingDenormalizer {
        return new BeConstructedUsingDenormalizer($denormalizer, $className, $ignoredAttributes);
    }
}
