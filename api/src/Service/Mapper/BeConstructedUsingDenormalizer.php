<?php

namespace App\Service\Mapper;

use RuntimeException;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use function get_class;
use function method_exists;
use function sprintf;

class BeConstructedUsingDenormalizer
{
    /**
     * @var DenormalizerInterface
     */
    private $denormalizer;

    /**
     * @var string[]
     */
    private $ignoredAttributes;

    /**
     * @var string
     */
    private $className;

    /**
     * BeConstructedUsingDenormalizer constructor.
     *
     * @param string[] $ignoredAttributes
     */
    public function __construct(DenormalizerInterface $denormalizer, string $className, $ignoredAttributes = [])
    {
        $this->denormalizer = $denormalizer;
        $this->ignoredAttributes = $ignoredAttributes;
        $this->className = $className;
    }

    /**
     * @throws ExceptionInterface
     */
    public function __invoke(object $entity): object
    {
        if (!method_exists($entity, 'getData')) {
            throw new RuntimeException(sprintf("Method getData does't exists in %s", get_class($entity)));
        }

        /** @var object $denormalizeObject */
        $denormalizeObject = $this->denormalizer->denormalize(
            $entity->getData(),
            $this->className,
            null,
            [
                ObjectNormalizer::IGNORED_ATTRIBUTES => $this->ignoredAttributes,
            ]
        );

        return $denormalizeObject;
    }
}
