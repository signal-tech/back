<?php

namespace App\Service\Mapper;

use AutoMapperPlus\Configuration\Options;
use AutoMapperPlus\MappingOperation\MappingOperationInterface;
use AutoMapperPlus\PropertyAccessor\PropertyAccessorInterface;

class FromPropertyPathOperation implements MappingOperationInterface
{
    /**
     * @var string
     */
    protected $propertyPath;

    /**
     * @var PropertyAccessorInterface
     */
    private $propertyAccessor;

    /**
     * FromPropertyPathOperation constructor.
     */
    public function __construct(string $propertyPath)
    {
        $this->propertyPath = $propertyPath;
    }

    /**
     * @param object $source
     * @param object $destination
     */
    public function mapProperty(string $propertyName, $source, $destination): void
    {
        $this->propertyAccessor->setProperty(
            $destination,
            $propertyName,
            $this->propertyAccessor->getProperty($source, $this->propertyPath)
        );
    }

    public function setOptions(Options $options): void
    {
        $this->propertyAccessor = $options->getPropertyAccessor();
    }
}
