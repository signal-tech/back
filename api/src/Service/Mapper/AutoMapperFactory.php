<?php

namespace App\Service\Mapper;

use AutoMapperPlus\AutoMapper;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;

class AutoMapperFactory
{
    /**
     * @var AutoMapperConfigInterface
     */
    private $config;

    /**
     * AutoMapperFactory constructor.
     */
    public function __construct(AutoMapperConfigInterface $config)
    {
        $this->config = $config;

        $this->config->getOptions()->setPropertyAccessor(new AutoMapperPropertyAccessor());
        $this->config->getOptions()->setPropertyWriter(new AutoMapperPropertyAccessor());
        $this->config->getOptions()->setPropertyReader(new AutoMapperPropertyAccessor());
    }

    public function addConfigureCallback(AutoMapperConfiguratorInterface $configurator): void
    {
        $configurator->configure($this->config);
    }

    public function create(): AutoMapperInterface
    {
        return new AutoMapper($this->config);
    }
}
