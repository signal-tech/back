<?php

namespace App\Service\Mapper;

use AutoMapperPlus\PropertyAccessor\PropertyAccessorInterface as AutoMapperPropertyAccessorInterface;
use Symfony\Component\PropertyAccess\PropertyAccess;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\PropertyInfo\Extractor\ReflectionExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractor;
use Symfony\Component\PropertyInfo\PropertyInfoExtractorInterface;
use function get_class;

class AutoMapperPropertyAccessor implements AutoMapperPropertyAccessorInterface
{
    /**
     * @var PropertyAccessorInterface
     */
    private $accessor;

    /**
     * @var PropertyInfoExtractorInterface
     */
    private $propertyInfo;

    public function __construct()
    {
        $this->accessor = PropertyAccess::createPropertyAccessorBuilder()
            ->enableExceptionOnInvalidIndex()
            ->getPropertyAccessor();

        $extr = new ReflectionExtractor();

        $this->propertyInfo = new PropertyInfoExtractor(
            [$extr],
            [$extr],
            [],
            [$extr]
        );
    }

    /**
     * @param object $object
     */
    public function hasProperty($object, string $propertyName): bool
    {
        return $this->accessor->isReadable($object, $propertyName);
    }

    /**
     * @param object $object
     *
     * @return mixed
     */
    public function getProperty($object, string $propertyName)
    {
        return $this->accessor->getValue($object, $propertyName);
    }

    /**
     * @param object $object
     * @param mixed  $value
     */
    public function setProperty($object, string $propertyName, $value): void
    {
        $this->accessor->setValue($object, $propertyName, $value);
    }

    /**
     * Returns a list of property names available on the object.
     *
     * @param object $object
     *
     * @return string[]
     *
     * @psalm-suppress InvalidNullableReturnType
     * @psalm-suppress NullableReturnStatement
     */
    public function getPropertyNames($object): array
    {
        /** @var string[] $properties */
        $properties = $this->propertyInfo->getProperties(get_class($object));

        return $properties;
    }
}
