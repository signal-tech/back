<?php

namespace App\Service\Environment;

use function getenv;

class EnvironmentService
{
    public static function getHost(): string
    {
        return (string) getenv('HOST');
    }

    public static function getEmailLogoPath(): string
    {
        return self::getHost() . '/assets/email/logo.png';
    }

    public static function getRecaptchaSecret(): string
    {
        return (string) getenv('RECAPTCHA_SECRET');
    }

    public static function getApiVersion(): int
    {
        return (int) getenv('API_VERSION');
    }

    public static function getEncryptionPath(): string
    {
        return (string) getenv('ENCRYPTION_PATH');
    }
}
