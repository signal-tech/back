<?php

namespace App\Service\Verify;

use App\Service\Environment\EnvironmentService;
use Symfony\Component\HttpClient\HttpClient;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class RecaptchaService implements RecaptchaServiceInterface
{
    private HttpClientInterface $client;

    public function __construct()
    {
        $this->client = HttpClient::create();
    }

    public function verify(string $response): array
    {
        $responseRequest = $this->client->request('POST', 'https://www.google.com/recaptcha/api/siteverify', [
            // these values are automatically encoded before including them in the URL
            'query' => [
                'secret' => EnvironmentService::getRecaptchaSecret(),
                'response' => $response,
            ],
        ]);

        return $responseRequest->toArray();
    }
}
