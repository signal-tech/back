<?php

namespace App\Service\Verify;

interface RecaptchaServiceInterface
{
    public function verify(string $response): array;
}
