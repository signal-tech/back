<?php

namespace App\Service;

use League\Tactician\CommandBus;

class CQRS
{
    private CommandBus $command;

    private CommandBus $query;

    public function __construct(
        CommandBus $command,
        CommandBus $query
    ) {
        $this->query = $query;
        $this->command = $command;
    }

    /**
     * @param object $command
     *
     * @return mixed
     */
    public function execute($command)
    {
        return $this->command->handle($command);
    }

    /**
     * @param object $query
     *
     * @return mixed
     */
    public function get($query)
    {
        return $this->query->handle($query);
    }
}
