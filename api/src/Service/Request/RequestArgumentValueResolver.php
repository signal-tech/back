<?php

namespace App\Service\Request;

use App\Service\Validation\ValidationException;
use App\Service\Validation\ValidationService;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Controller\ArgumentValueResolverInterface;
use Symfony\Component\HttpKernel\ControllerMetadata\ArgumentMetadata;
use function array_merge;
use function is_a;

class RequestArgumentValueResolver implements ArgumentValueResolverInterface
{
    private ValidationService $validator;

    public function __construct(
        ValidationService $validator
    ) {
        $this->validator = $validator;
    }

    /**
     * {@inheritdoc}
     */
    public function supports(Request $request, ArgumentMetadata $argument): bool
    {
        $type = $argument->getType();

        if (null === $type) {
            return false;
        }

        return is_a($type, RequestObjectInterface::class, true);
    }

    /**
     * @throws ValidationException
     *
     * @return object[]
     *
     * @psalm-return \Generator<int, RequestObjectInterface, mixed, void>
     */
    public function resolve(Request $request, ArgumentMetadata $argument): iterable
    {
        /** @var RequestObjectInterface $class */
        $class = $argument->getType();

        /** @var string[] $data */
        $data = $request->query->all();

        /** @var string[] $mergedData */
        $mergedData = array_merge($data, $request->request->all());

        $requestObject = $class::createFromRequestPayload($mergedData);
        $this->validator->validateObject($requestObject);

        yield $requestObject;
    }
}
