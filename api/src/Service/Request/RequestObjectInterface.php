<?php

namespace App\Service\Request;

interface RequestObjectInterface
{
    /**
     * @param string[] $request
     *
     * @return RequestObjectInterface
     */
    public static function createFromRequestPayload(array $request): self;
}
