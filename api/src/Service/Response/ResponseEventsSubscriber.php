<?php

namespace App\Service\Response;

use App\Exception\StockExchange\StockExchangeInvalidPermissionException;
use App\Service\Environment\EnvironmentService;
use App\Service\Validation\ValidationException;
use Doctrine\ORM\EntityNotFoundException;
use InvalidArgumentException;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Event\ExceptionEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\HttpKernel\Exception\HttpExceptionInterface;
use Symfony\Component\HttpKernel\KernelEvents;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Validator\ConstraintViolation;
use function array_key_exists;
use function getenv;
use function getmypid;

class ResponseEventsSubscriber implements EventSubscriberInterface
{
    public function __construct(
        private LoggerInterface $logger
    ) {
    }

    /**
     * @throws ExceptionInterface
     */
    public function onKernelException(ExceptionEvent $event): void
    {
        $exception = $event->getThrowable();
        $errors = [];
        $code = $exception instanceof HttpExceptionInterface ? $exception->getStatusCode() : Response::HTTP_INTERNAL_SERVER_ERROR;
        $exceptionMessage = 'Внутренняя ошибка';

        if ($exception instanceof ValidationException) {
            $exceptionMessage = 'Ошибка данных формы';
            /** @var ConstraintViolation $violation */
            foreach ($exception->getViolations() as $violation) {
                if (!array_key_exists($violation->getPropertyPath(), $errors)) {
                    $errors[$violation->getPropertyPath()] = [];
                }

                $errors[$violation->getPropertyPath()][] = $violation->getMessage();
            }

            $code = Response::HTTP_UNPROCESSABLE_ENTITY;
        } elseif ($exception instanceof InvalidArgumentException) {
            $exceptionMessage = $exception->getMessage();
            $code = Response::HTTP_UNPROCESSABLE_ENTITY;
        } elseif ($exception instanceof EntityNotFoundException) {
            $exceptionMessage = $exception->getMessage();
            $code = Response::HTTP_NOT_FOUND;
        } elseif ($exception instanceof StockExchangeInvalidPermissionException) {
            $exceptionMessage = $exception->getMessage();
            $code = Response::HTTP_NOT_FOUND;
        } else {
            $this->logger->error($exception->getMessage(), $exception->getTrace());
        }

        $event->setResponse(
            new JsonResponse(
                [
                    'errors' => $errors,
                    'code' => $code,
                    'message' => $exceptionMessage,
                    'processId' => getmypid(),
                    'errorTittle' => getenv('APP_DEBUG') ? $exception->getMessage() : '',
                    'stackTrace' => getenv('APP_DEBUG') ? $exception->getTrace() : [],
                ],
                $code
            )
        );
    }

    public function onKernelResponse(ResponseEvent $event): void
    {
        $response = $event->getResponse();

        // Set multiple headers simultaneously
        $response->headers->add([
            'Api-Version' => EnvironmentService::getApiVersion(),
        ]);
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents(): array
    {
        return [
            KernelEvents::EXCEPTION => 'onKernelException',
            KernelEvents::RESPONSE => 'onKernelResponse',
        ];
    }
}
