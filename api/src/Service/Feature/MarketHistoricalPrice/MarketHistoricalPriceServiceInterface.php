<?php

namespace App\Service\Feature\MarketHistoricalPrice;

use App\Feature\MarketHistoricalPrice\MarketHistoricalPrice;
use App\Feature\StockExchange\StockExchange;

interface MarketHistoricalPriceServiceInterface
{
    public function createMarketHistoricalPrice(
        StockExchange $stockExchange,
        int $startTimeStamp,
        string $from,
        string $to,
        float $price
    ): MarketHistoricalPrice;
}
