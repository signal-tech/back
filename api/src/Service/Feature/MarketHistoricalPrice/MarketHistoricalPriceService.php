<?php

namespace App\Service\Feature\MarketHistoricalPrice;

use App\Feature\MarketHistoricalPrice\MarketHistoricalPrice;
use App\Feature\MarketHistoricalPrice\MarketHistoricalPriceRepository;
use App\Feature\StockExchange\StockExchange;
use Doctrine\ORM\EntityManagerInterface;

class MarketHistoricalPriceService implements MarketHistoricalPriceServiceInterface
{
    public function __construct(
        private EntityManagerInterface $em
    ) {
    }

    public function createMarketHistoricalPrice(
        StockExchange $stockExchange,
        int $startTimeStamp,
        string $from,
        string $to,
        float $price
    ): MarketHistoricalPrice {
        /**
         * @var MarketHistoricalPriceRepository $marketHistoricalPriceRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $marketHistoricalPriceRepository = $this->em->getRepository(MarketHistoricalPrice::class);

        $marketHistoricalPrice = $marketHistoricalPriceRepository->getMarketHistoricalPrice(
            $stockExchange,
            $startTimeStamp,
            $from,
            $to
        );

        if (null !== $marketHistoricalPrice) {
            return $marketHistoricalPrice;
        }

        $marketHistoricalPrice = new MarketHistoricalPrice();
        $marketHistoricalPrice->setPriceTime($startTimeStamp);
        $marketHistoricalPrice->setPrice($price);
        $marketHistoricalPrice->setFromCoin($from);
        $marketHistoricalPrice->setToCoin($to);
        $marketHistoricalPrice->setStockExchange($stockExchange);

        $this->em->persist($marketHistoricalPrice);
        $this->em->flush();

        return $marketHistoricalPrice;
    }
}
