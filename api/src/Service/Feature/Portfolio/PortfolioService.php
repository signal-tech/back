<?php

namespace App\Service\Feature\Portfolio;

use App\Feature\Portfolio\DTO\PortfolioDTO;
use App\Feature\Portfolio\DTO\Position\PositionDTO;
use App\Feature\Portfolio\Portfolio;
use App\Feature\Portfolio\PortfolioRepository;
use App\Feature\StockExchangeToken\StockExchangeToken;
use Doctrine\ORM\EntityManagerInterface;
use function bcadd;
use function bcmul;
use function bcsub;
use function number_format;

class PortfolioService implements PortfolioServiceInterface
{
    public function __construct(
        private EntityManagerInterface $em
    ) {
    }

    public function createPortfolio(
        StockExchangeToken $token,
        int $periodStart,
        int $periodEnd,
        PortfolioDTO $portfolioDTO
    ): Portfolio {
        /**
         * @var PortfolioRepository $portfolioRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $portfolioRepository = $this->em->getRepository(Portfolio::class);

        $portfolio = $portfolioRepository->findOneBy([
            'stockExchangeToken' => $token,
            'rangeStartDateTime' => $periodStart,
            'rangeEndDateTime' => $periodEnd,
        ]);

        if (null === $portfolio) {
            $portfolio = new Portfolio();
        }

        $portfolio->setData($portfolioDTO);
        $portfolio->setStockExchangeToken($token);
        $portfolio->setRangeStartDateTime($periodStart);
        $portfolio->setRangeEndDateTime($periodEnd);

        $this->em->persist($portfolio);
        $this->em->flush();

        return $portfolio;
    }

    public function recalculateWallet(PortfolioDTO $portfolioDTO, string $walletCoin): void
    {
        foreach ($portfolioDTO->fills as $fill) { //Вычитаем комиссии
            $feePosition = (new PositionDTO())->setSize(0)->setMarket($fill->getFeeCurrency());

            if (isset($portfolioDTO->positions[$fill->getFeeCurrency()])) {
                $feePosition = $portfolioDTO->positions[$fill->getFeeCurrency()];
            }

            $feePosition->setSize(
                (float) bcsub(
                    number_format($feePosition->getSize(), self::SCALE, '.', ''),
                    number_format($fill->getFee(), self::SCALE, '.', ''),
                    self::SCALE
                )
            );

            if (0.0 === $feePosition->getSize()) {
                unset($portfolioDTO->positions[$fill->getFeeCurrency()]);
            } else {
                $portfolioDTO->positions[$fill->getFeeCurrency()] = $feePosition;
            }
        }

        $position = (new PositionDTO())->setSize(0)->setMarket($walletCoin);

        if (isset($portfolioDTO->positions[$walletCoin])) {
            $position = $portfolioDTO->positions[$walletCoin];
        }

        $position->setSize( //добавляем выплаты финансирования
            (float) bcadd(
                number_format($position->getSize(), self::SCALE, '.', ''),
                number_format($portfolioDTO->getFundingPayment(), self::SCALE, '.', ''),
                self::SCALE
            )
        );

        $portfolioDTO->positions[$walletCoin] = $position;

        foreach ($portfolioDTO->getDeposit() as $depositDTO) { //Вводы
            $position = (new PositionDTO())->setSize(0)->setMarket($depositDTO->getCoin());

            if (isset($portfolioDTO->positions[$depositDTO->getCoin()])) {
                $position = $portfolioDTO->positions[$depositDTO->getCoin()];
            }

            $position->setSize(
                (float) bcadd(
                    number_format($position->getSize(), self::SCALE, '.', ''),
                    number_format($depositDTO->getSize(), self::SCALE, '.', ''),
                    self::SCALE
                )
            );

            $portfolioDTO->positions[$depositDTO->getCoin()] = $position;
        }

        foreach ($portfolioDTO->getWithdrawal() as $withdrawalDTO) { //Выводы
            $position = (new PositionDTO())->setSize(0)->setMarket($withdrawalDTO->getCoin());

            if (isset($portfolioDTO->positions[$withdrawalDTO->getCoin()])) {
                $position = $portfolioDTO->positions[$withdrawalDTO->getCoin()];
            }

            $position->setSize(
                (float) bcsub(
                    number_format($position->getSize(), self::SCALE, '.', ''),
                    number_format($withdrawalDTO->getSize(), self::SCALE, '.', ''),
                    self::SCALE
                )
            );

            if (0.0 === $position->getSize()) {
                unset($portfolioDTO->positions[$withdrawalDTO->getCoin()]);
            } else {
                $portfolioDTO->positions[$withdrawalDTO->getCoin()] = $position;
            }
        }

        foreach ($portfolioDTO->getLending() as $lendingDTO) { //Выводы
            $position = (new PositionDTO())->setSize(0)->setMarket($lendingDTO->getCoin());

            if (isset($portfolioDTO->positions[$lendingDTO->getCoin()])) {
                $position = $portfolioDTO->positions[$lendingDTO->getCoin()];
            }

            $position->setSize(
                (float) bcadd(
                    number_format($position->getSize(), self::SCALE, '.', ''),
                    number_format($lendingDTO->getProceeds(), self::SCALE, '.', ''),
                    self::SCALE
                )
            );

            if (0.0 === $position->getSize()) {
                unset($portfolioDTO->positions[$lendingDTO->getCoin()]);
            } else {
                $portfolioDTO->positions[$lendingDTO->getCoin()] = $position;
            }
        }

        foreach ($portfolioDTO->getBorrow() as $borrowDTO) { //Выводы
            $position = (new PositionDTO())->setSize(0)->setMarket($borrowDTO->getCoin());

            if (isset($portfolioDTO->positions[$borrowDTO->getCoin()])) {
                $position = $portfolioDTO->positions[$borrowDTO->getCoin()];
            }

            $position->setSize(
                (float) bcadd(
                    number_format($position->getSize(), self::SCALE, '.', ''),
                    number_format($borrowDTO->getProceeds(), self::SCALE, '.', ''),
                    self::SCALE
                )
            );

            if (0.0 === $position->getSize()) {
                unset($portfolioDTO->positions[$borrowDTO->getCoin()]);
            } else {
                $portfolioDTO->positions[$borrowDTO->getCoin()] = $position;
            }
        }
    }

    public function getPosition(PortfolioDTO $portfolioDTO, string $market): PositionDTO
    {
        $anotherMarketData = (new PositionDTO())->setSize(0)->setMarket($market);

        if (isset($portfolioDTO->positions[$market])) {
            $anotherMarketData = $portfolioDTO->positions[$market];
        }

        return $anotherMarketData;
    }

    public function addToSize(
        float $currentSize,
        float $price,
        float $size
    ): float {
        return (float) bcadd(
            number_format($currentSize, self::SCALE, '.', ''),
            bcmul(number_format($price, self::SCALE, '.', ''), (string) $size, self::SCALE),
            self::SCALE
        );
    }

    public function reduceSize(
        float $currentSize,
        float $price,
        float $size
    ): float {
        return (float) bcsub(
            number_format($currentSize, self::SCALE, '.', ''),
            bcmul(number_format($price, self::SCALE, '.', ''), (string) $size, self::SCALE),
            self::SCALE
        );
    }
}
