<?php

namespace App\Service\Feature\Portfolio;

use App\Feature\Portfolio\DTO\PortfolioDTO;
use App\Feature\Portfolio\DTO\Position\PositionDTO;
use App\Feature\Portfolio\Portfolio;
use App\Feature\StockExchangeToken\StockExchangeToken;

interface PortfolioServiceInterface
{
    public const SCALE = 8;

    public function createPortfolio(
        StockExchangeToken $token,
        int $periodStart,
        int $periodEnd,
        PortfolioDTO $portfolioDTO
    ): Portfolio;

    public function addToSize(
        float $currentSize,
        float $price,
        float $size
    ): float;

    public function reduceSize(
        float $currentSize,
        float $price,
        float $size
    ): float;

    public function recalculateWallet(PortfolioDTO $portfolioDTO, string $walletCoin): void;

    public function getPosition(PortfolioDTO $portfolioDTO, string $market): PositionDTO;
}
