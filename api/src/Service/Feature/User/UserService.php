<?php

namespace App\Service\Feature\User;

use App\Feature\User\User;
use Doctrine\ORM\EntityManagerInterface;

class UserService implements UserServiceInterface
{
    public function __construct(
        private EntityManagerInterface $em
    ) {
    }

    public function reduceSubscribers(User $user): void
    {
        $user->setSubscribersCount($user->getSubscribersCount() - 1);

        $this->em->persist($user);
        $this->em->flush();
    }

    public function addToSubscribers(User $user): void
    {
        $user->setSubscribersCount($user->getSubscribersCount() + 1);

        $this->em->persist($user);
        $this->em->flush();
    }

    public function reduceSubscriptions(User $user): void
    {
        $user->setSubscriptionsCount($user->getSubscriptionsCount() - 1);

        $this->em->persist($user);
        $this->em->flush();
    }

    public function addToSubscriptions(User $user): void
    {
        $user->setSubscriptionsCount($user->getSubscriptionsCount() + 1);

        $this->em->persist($user);
        $this->em->flush();
    }
}
