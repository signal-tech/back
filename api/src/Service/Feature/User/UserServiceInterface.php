<?php

namespace App\Service\Feature\User;

use App\Feature\User\User;

interface UserServiceInterface
{
    public function reduceSubscribers(User $user): void;

    public function addToSubscriptions(User $user): void;

    public function addToSubscribers(User $user): void;

    public function reduceSubscriptions(User $user): void;
}
