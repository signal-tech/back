<?php

namespace App\Service\Feature\Statistics;

use App\Feature\Portfolio\DTO\Deposit\DepositDTO;
use App\Feature\Portfolio\DTO\Withdrawal\WithdrawalDTO;
use App\Feature\Portfolio\Portfolio;
use App\Feature\Portfolio\PortfolioRepository;
use App\Feature\Statistics\Statistics;
use App\Feature\Statistics\StatisticsRepository;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\StockExchange\StockExchangeServiceInterface;
use App\Service\Time\TimeServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Throwable;
use function count;

class StatisticsService implements StatisticsServiceInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private StockExchangeServiceInterface $stockExchangeService,
        private LoggerInterface $logger
    ) {
    }

    public function create(
        StockExchangeToken $token,
        int $startPeriod,
        int $endPeriod,
        string $type,
        float $percent
    ): Statistics {
        /**
         * @var StatisticsRepository $statisticsRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $statisticsRepository = $this->em->getRepository(Statistics::class);

        $statistic = $statisticsRepository->findOneBy([
            'stockExchangeToken' => $token,
            'type' => $type,
            'rangeStartDateTime' => $startPeriod,
            'rangeEndDateTime' => $endPeriod,
        ]);

        if (null === $statistic) {
            $statistic = new Statistics();
        }

        $statistic->setStockExchangeToken($token);
        $statistic->setType($type);
        $statistic->setRangeStartDateTime($startPeriod);
        $statistic->setRangeEndDateTime($endPeriod);
        $statistic->setPercent($percent);

        $this->em->persist($statistic);
        $this->em->flush();

        return $statistic;
    }

    public function calculateStatisticByPeriodAndGetEntity(
        StockExchangeToken $token,
        int $startPeriod,
        int $endPeriod,
        string $type
    ): Statistics {
        try {
            /**
             * @var StatisticsRepository $statisticsRepository
             * @psalm-suppress UnnecessaryVarAnnotation
             */
            $statisticsRepository = $this->em->getRepository(Statistics::class);
            /**
             * @var PortfolioRepository $portfolioRepository
             * @psalm-suppress UnnecessaryVarAnnotation
             */
            $portfolioRepository = $this->em->getRepository(Portfolio::class);

            $statistic = $statisticsRepository->findOneBy([
                'stockExchangeToken' => $token,
                'type' => $type,
                'rangeStartDateTime' => $startPeriod,
                'rangeEndDateTime' => $endPeriod,
            ]);

            if (null !== $statistic) {
                $updatedPortfolios = $portfolioRepository->getPortfolioListByTokenAndPeriodAndUpdatedNotLessDate(
                    $token,
                    $startPeriod,
                    $endPeriod,
                    $statistic->getUpdatedAt()
                );

                if (0 === count($updatedPortfolios)) {
                    return $statistic;
                }
            }

            $percent = $this->calculateStatisticByPeriod(
                $token,
                $startPeriod,
                $endPeriod
            );

            return $this->create(
                $token,
                $startPeriod,
                $endPeriod,
                $type,
                $percent
            );
        } catch (Throwable $throwable) {
            $this->logger->critical(
                'Statistics calculate error'
                . ' Token : ' . $token->getId()
                . ' Start period: ' . $startPeriod
                . ' End period: ' . $endPeriod
                . ' Type:' . $type
                . ' Message: ' . $throwable->getMessage()
            );

            throw $throwable;
        }
    }

    /**
     * @return float
     *
     *  https://help.tinkoff.ru/pulse/profile/yield/
     */
    public function calculateStatisticByPeriod(
        StockExchangeToken $token,
        int $startPeriod,
        int $endPeriod
    ): float {
        /**
         * @var PortfolioRepository $portfolioRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $portfolioRepository = $this->em->getRepository(Portfolio::class);

        $portfoliosList = $portfolioRepository->getPortfolioListByTokenAndPeriod($token, $startPeriod, $endPeriod);

        //TODO нужно будет брать у 1 элемента
        $previousPortfolio = $portfolioRepository->findOneBy([
            'stockExchangeToken' => $token,
            'rangeStartDateTime' => $startPeriod - TimeServiceInterface::DAY_SECONDS,
            'rangeEndDateTime' => $startPeriod,
        ]);

        $portfolioWalletCoin = $this->stockExchangeService->getPortfolioWalletCoin($token);
        $deposit = 0.0;
        $withdrawal = 0.0;
        $countDays = count($portfoliosList);

        if (null === $previousPortfolio && $countDays < 2) {
            return 0;
        } elseif (null === $previousPortfolio) {
            $previousPortfolio = $portfoliosList[0];
            unset($portfoliosList[0]);
        }

        $lastPortfolio = $portfoliosList[$countDays - 1];
        unset($portfoliosList[$countDays - 1]);

        $firstTimeWallet = $this->calculateSumByPortfolio($previousPortfolio);

        if ('USD' !== $portfolioWalletCoin) {
            $firstTimeWallet = $firstTimeWallet * (1 / $this->stockExchangeService->getMarketHistoricalPrice(
                $token,
                $this->stockExchangeService->getUsdCoin($token),
                $previousPortfolio->getRangeEndDateTime()
            ));
        }

        foreach ($portfoliosList as $portfolioItem) {
            $depositPrice = $this->calculateSumByDepositOrWithdrawal($token, $portfolioItem->getData()->getDeposit(), $portfolioItem->getRangeEndDateTime());
            $withdrawalPrice = $this->calculateSumByDepositOrWithdrawal($token, $portfolioItem->getData()->getWithdrawal(), $portfolioItem->getRangeEndDateTime());

            if ('USD' !== $portfolioWalletCoin) {
                $coinPrice = (1 / $this->stockExchangeService->getMarketHistoricalPrice(
                    $token,
                    $this->stockExchangeService->getUsdCoin($token),
                    $portfolioItem->getRangeEndDateTime()
                ));

                $withdrawal += $withdrawalPrice * $coinPrice;
                $deposit += $depositPrice * $coinPrice;
            } else {
                $withdrawal += $withdrawalPrice;
                $deposit += $depositPrice;
            }
        }

        $lastTimeWallet = $this->calculateSumByPortfolio($lastPortfolio);
        $depositPrice = $this->calculateSumByDepositOrWithdrawal($token, $lastPortfolio->getData()->getDeposit(), $lastPortfolio->getRangeEndDateTime());
        $withdrawalPrice = $this->calculateSumByDepositOrWithdrawal($token, $lastPortfolio->getData()->getWithdrawal(), $lastPortfolio->getRangeEndDateTime());

        if ('USD' !== $portfolioWalletCoin) {
            $coinPrice = (1 / $this->stockExchangeService->getMarketHistoricalPrice(
                $token,
                $this->stockExchangeService->getUsdCoin($token),
                $lastPortfolio->getRangeEndDateTime()
            ));

            $withdrawal += $withdrawalPrice * $coinPrice;
            $deposit += $depositPrice * $coinPrice;
            $lastTimeWallet = $lastTimeWallet * $coinPrice;
        } else {
            $withdrawal += $withdrawalPrice;
            $deposit += $depositPrice;
        }

        $divider = $firstTimeWallet;

        if ($withdrawal < $deposit) {
            $divider = $divider - $withdrawal + $deposit;
        }

        if ($lastTimeWallet < 0 || $firstTimeWallet < 0) {
            $this->logger->critical(
                'Статистика: Отрицательный баланс на старте или в конце
                  "Id token:" ' . $token->getId()
                . ' Date start: ' . $startPeriod
                . ' Date end: ' . $endPeriod
                . ' start money: ' . $firstTimeWallet
                . ' end money: ' . $lastTimeWallet
            );

            return 0;
        }

        if (0.0 === $divider) {
            $this->logger->critical(
                'Статистика: делитель равен 0
                  "Id token:" ' . $token->getId()
                . ' Date start: ' . $startPeriod
                . ' Date end: ' . $endPeriod
                . ' start money: ' . $firstTimeWallet
                . ' end money: ' . $lastTimeWallet
            );

            return 0;
        }

        $percent = (($lastTimeWallet + $withdrawal) - ($firstTimeWallet + $deposit)) / $divider;

        return $percent * 100;
    }

    /**
     * @param DepositDTO[]|WithdrawalDTO[] $data
     */
    private function calculateSumByDepositOrWithdrawal(StockExchangeToken $token, array $data, int $time): float
    {
        $sum = 0.0;

        foreach ($data as $dataItem) {
            if ($this->stockExchangeService->getPortfolioWalletCoin($token) === $dataItem->getCoin()) {
                $sum += $dataItem->getSize();
            } else {
                $sum += $dataItem->getSize() * $this->stockExchangeService->getMarketHistoricalPrice($token, $dataItem->getCoin(), $time);
            }
        }

        return $sum;
    }

    private function calculateSumByPortfolio(Portfolio $portfolio): float
    {
        $sumPortfolioItem = 0.0;

        $portfolioData = $portfolio->getData();

        if (null !== $portfolioData->getTotalWallet()) {
            $sumPortfolioItem += (float) $portfolioData->getTotalWallet();
        } else {
            foreach ($portfolioData->getPositions() as $position) {
                if ($position->getMarket() === $this->stockExchangeService->getPortfolioWalletCoin($portfolio->getStockExchangeToken())) {
                    $price = 1;
                } else {
                    $price = $this->stockExchangeService->getMarketHistoricalPrice(
                        $portfolio->getStockExchangeToken(),
                        $position->getMarket(),
                        $portfolio->getRangeEndDateTime()
                    );
                }

                $sumPortfolioItem += $price * $position->getSize();
            }
        }

        return $sumPortfolioItem;
    }
}
