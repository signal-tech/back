<?php

namespace App\Service\Feature\Statistics;

use App\Feature\Statistics\Statistics;
use App\Feature\StockExchangeToken\StockExchangeToken;

interface StatisticsServiceInterface
{
    public function create(
        StockExchangeToken $token,
        int $startPeriod,
        int $endPeriod,
        string $type,
        float $percent
    ): Statistics;

    public function calculateStatisticByPeriodAndGetEntity(
        StockExchangeToken $token,
        int $startPeriod,
        int $endPeriod,
        string $type
    ): Statistics;

    public function calculateStatisticByPeriod(StockExchangeToken $token, int $startPeriod, int $endPeriod): float;
}
