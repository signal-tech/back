<?php

namespace App\Service\Feature\CopyTrading;

use App\Feature\CopyTrading\CopyTrading;
use App\Feature\Order\Order;

interface CopyTradingServiceInterface
{
    public function copyTrade(Order $order, CopyTrading $copyTrading): void;
}
