<?php

namespace App\Service\Feature\CopyTrading;

use App\Feature\CopyTrading\CopyTrading;
use App\Feature\CopyTrading\CopyTradingLog;
use App\Feature\Order\Order;
use App\Service\StockExchange\DTO\AbstractCreateOrderDTO;
use App\Service\StockExchange\StockExchangeServiceInterface;
use Doctrine\ORM\EntityManagerInterface;

class CopyTradingService implements CopyTradingServiceInterface
{
    public function __construct(
        private StockExchangeServiceInterface $stockExchangeService,
        private EntityManagerInterface $em
    ) {
    }

    public function copyTrade(Order $order, CopyTrading $copyTrading): void
    {
        $createOrderDTO = $this->stockExchangeService->getCreateOrderDTO($copyTrading->getChildToken(), $order);

        $this->createCopyTradingLog($createOrderDTO, $order, $copyTrading);

        $this->stockExchangeService->createOrder($createOrderDTO, $copyTrading->getChildToken());
    }

    private function createCopyTradingLog(AbstractCreateOrderDTO $createOrderDTO, Order $order, CopyTrading $copyTrading): void
    {
        $log = new CopyTradingLog();

        $log->setOrder($order);
        $log->setCopyTrading($copyTrading);
        $log->setData($createOrderDTO->jsonSerialize());

        $this->em->persist($log);
        $this->em->flush();
    }
}
