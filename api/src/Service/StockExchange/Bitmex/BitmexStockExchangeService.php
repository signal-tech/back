<?php

namespace App\Service\StockExchange\Bitmex;

use App\Feature\Order\Order;
use App\Feature\Portfolio\DTO\PortfolioDTO;
use App\Feature\StockExchange\StockExchange;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Serializer\SerializerFactory;
use App\Service\StockExchange\Bitmex\DTO\Order\CreateOrderDTO;
use App\Service\StockExchange\Bitmex\DTO\Order\OrderDTO;
use App\Service\StockExchange\Bitmex\DTO\Wallet\WalletDTO;
use App\Service\StockExchange\DTO\AbstractCreateOrderDTO;
use App\Service\StockExchange\DTO\MarketFiltersDataDTO;
use DateTime;
use DateTimeZone;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Throwable;
use function hash_hmac;
use function microtime;
use function round;

class BitmexStockExchangeService implements BitmexStockExchangeServiceInterface
{
    protected const SCALE = 8;

    private Serializer $serializer;

    public function __construct(
        private HttpClientInterface $client
    ) {
        $this->client = $client;
        $this->serializer = SerializerFactory::create();
    }

    public function canProcess(StockExchangeToken $stockExchangeToken): bool
    {
        return StockExchange::BITMEX_TYPE === $stockExchangeToken->getStockExchange()->getCode();
    }

    public function getFirstMoneyDepositDateTime(StockExchangeToken $stockExchangeToken): ?int
    {
        return null;
    }

    public function getFirstOrderDateTime(StockExchangeToken $stockExchangeToken): ?int
    {
        $returnDateTime = null;
        $data = $this->getOrderHistoryList($stockExchangeToken, 10, 0);

        if (!empty($data)) {
            $firstData = $data[0];

            $returnDateTime = new DateTime($firstData->getTransactTime());
        }

        return $returnDateTime ? $returnDateTime->getTimestamp() : null;
    }

    /**
     * @return OrderDTO[]
     */
    public function getData(
        StockExchangeToken $stockExchangeToken
    ): array {
        $url = 'user/walletHistory';

        /** @var OrderDTO[] $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            $url,
            OrderDTO::class
        );

        return $data;
    }

    public function getPortfolioByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime,
        ?PortfolioDTO $previousPortfolioDTO
    ): PortfolioDTO {
        return new PortfolioDTO();
    }

    public function isTokenValid(StockExchangeToken $stockExchangeToken): bool
    {
        try {
            $this->getWallet($stockExchangeToken);

            return true;
        } catch (Throwable $throwable) {
            return false;
        }
    }

    public function getMarketHistoricalPrice(
        StockExchangeToken $stockExchangeToken,
        string $marketCode,
        int $startTimeStamp
    ): float {
        return 0.0; //TODO
    }

    public function getMarketActualPrice(
        StockExchangeToken $stockExchangeToken,
        string $marketCode
    ): float {
        return 0.0; //TODO
    }

    public function getMarketFiltersData(
        StockExchangeToken $stockExchangeToken,
        string $marketCode
    ): MarketFiltersDataDTO {
        return new MarketFiltersDataDTO(); //TODO
    }

    public function getPortfolioWalletCoin(): string
    {
        return '';
    }

    public function getUsdCoin(StockExchangeToken $stockExchangeToken): string
    {
        return 'USD';
    }

    public function getCreateOrderDTO(StockExchangeToken $stockExchangeToken, Order $order): AbstractCreateOrderDTO
    {
        $createOrder = new CreateOrderDTO();

        return $createOrder;
    }

    public function createOrder(AbstractCreateOrderDTO $createOrderDTO, StockExchangeToken $stockExchangeToken): void
    {
    }

    private function getWallet(
        StockExchangeToken $stockExchangeToken
    ): WalletDTO {
        $url = 'user/wallet';

        /** @var WalletDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            $url,
            WalletDTO::class,
            false
        );

        return $data;
    }

    /**
     * @param ?int $startTimeStamp
     * @param ?int $endTimeStamp
     *
     * @return OrderDTO[]
     */
    private function getOrderHistoryList(
        StockExchangeToken $stockExchangeToken,
        int $limit,
        int $offset,
        ?int $startTimeStamp = null,
        ?int $endTimeStamp = null
    ): array {
        $url = 'execution/tradeHistory?reverse=true&count=' . $limit . '&start=' . $offset;

        if (null !== $startTimeStamp) {
            $url .= '&startTime=' . $this->dateTimeFormat($startTimeStamp);
        }

        if (null !== $endTimeStamp) {
            $url .= '&endTime=' . $this->dateTimeFormat($endTimeStamp);
        }

        /** @var OrderDTO[] $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            $url,
            OrderDTO::class
        );

        return $data;
    }

    private function getDataFromStockExchange(
        StockExchangeToken $stockExchangeToken,
        string $url,
        string $dtoType,
        bool $isArray = true
    ): array|object {
        $time = round(microtime(true) * 1000) + 5;

        $response = $this->client->request(
            'GET',
            'https://www.bitmex.com/api/v1/' . $url,
            [
                'headers' => [
                    'api-key' => $stockExchangeToken->getApiKey(),
                    'api-expires' => $time,
                    'api-signature' => hash_hmac(
                        'sha256',
                        'GET/api/v1/' . $url . $time,
                        $stockExchangeToken->getSecret()
                    ),
                ],
            ]
        );
        $response->getContent();

        $content = $response->toArray();

        if ($isArray) {
            $dtoType .= '[]';
        }

        /** @var object|array $dto */
        $dto = $this->serializer->denormalize($content, $dtoType, 'json');

        return $dto;
    }

    private function dateTimeFormat(int $time): string
    {
        return (new DateTime('now', new DateTimeZone('UTC')))
            ->setTimestamp($time)
            ->format('Y-m-d H:i');
    }
}
