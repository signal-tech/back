<?php

namespace App\Service\StockExchange\Bitmex\DTO\Wallet;

class WalletDTO
{
    public int $account;

    public string $currency;

    public int $prevDeposited;

    public int $prevWithdrawn;

    public int $prevTransferIn;

    public int $prevTransferOut;

    public int $prevAmount;

    public string $prevTimestamp;

    public int $deltaDeposited;

    public int $deltaWithdrawn;

    public int $deltaTransferIn;

    public int $deltaTransferOut;

    public int $deltaAmount;

    public int $deposited;

    public int $withdrawn;

    public int $transferIn;

    public int $transferOut;

    public int $amount;

    public int $pendingCredit;

    public int $pendingDebit;

    public int $confirmedDebit;

    public string $timestamp;

    public function getAccount(): int
    {
        return $this->account;
    }

    public function setAccount(int $account): self
    {
        $this->account = $account;

        return $this;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function setCurrency(string $currency): self
    {
        $this->currency = $currency;

        return $this;
    }

    public function getPrevDeposited(): int
    {
        return $this->prevDeposited;
    }

    public function setPrevDeposited(int $prevDeposited): self
    {
        $this->prevDeposited = $prevDeposited;

        return $this;
    }

    public function getPrevWithdrawn(): int
    {
        return $this->prevWithdrawn;
    }

    public function setPrevWithdrawn(int $prevWithdrawn): self
    {
        $this->prevWithdrawn = $prevWithdrawn;

        return $this;
    }

    public function getPrevTransferIn(): int
    {
        return $this->prevTransferIn;
    }

    public function setPrevTransferIn(int $prevTransferIn): self
    {
        $this->prevTransferIn = $prevTransferIn;

        return $this;
    }

    public function getPrevTransferOut(): int
    {
        return $this->prevTransferOut;
    }

    public function setPrevTransferOut(int $prevTransferOut): self
    {
        $this->prevTransferOut = $prevTransferOut;

        return $this;
    }

    public function getPrevAmount(): int
    {
        return $this->prevAmount;
    }

    public function setPrevAmount(int $prevAmount): self
    {
        $this->prevAmount = $prevAmount;

        return $this;
    }

    public function getPrevTimestamp(): string
    {
        return $this->prevTimestamp;
    }

    public function setPrevTimestamp(string $prevTimestamp): self
    {
        $this->prevTimestamp = $prevTimestamp;

        return $this;
    }

    public function getDeltaDeposited(): int
    {
        return $this->deltaDeposited;
    }

    public function setDeltaDeposited(int $deltaDeposited): self
    {
        $this->deltaDeposited = $deltaDeposited;

        return $this;
    }

    public function getDeltaWithdrawn(): int
    {
        return $this->deltaWithdrawn;
    }

    public function setDeltaWithdrawn(int $deltaWithdrawn): self
    {
        $this->deltaWithdrawn = $deltaWithdrawn;

        return $this;
    }

    public function getDeltaTransferIn(): int
    {
        return $this->deltaTransferIn;
    }

    public function setDeltaTransferIn(int $deltaTransferIn): self
    {
        $this->deltaTransferIn = $deltaTransferIn;

        return $this;
    }

    public function getDeltaTransferOut(): int
    {
        return $this->deltaTransferOut;
    }

    public function setDeltaTransferOut(int $deltaTransferOut): self
    {
        $this->deltaTransferOut = $deltaTransferOut;

        return $this;
    }

    public function getDeltaAmount(): int
    {
        return $this->deltaAmount;
    }

    public function setDeltaAmount(int $deltaAmount): self
    {
        $this->deltaAmount = $deltaAmount;

        return $this;
    }

    public function getDeposited(): int
    {
        return $this->deposited;
    }

    public function setDeposited(int $deposited): self
    {
        $this->deposited = $deposited;

        return $this;
    }

    public function getWithdrawn(): int
    {
        return $this->withdrawn;
    }

    public function setWithdrawn(int $withdrawn): self
    {
        $this->withdrawn = $withdrawn;

        return $this;
    }

    public function getTransferIn(): int
    {
        return $this->transferIn;
    }

    public function setTransferIn(int $transferIn): self
    {
        $this->transferIn = $transferIn;

        return $this;
    }

    public function getTransferOut(): int
    {
        return $this->transferOut;
    }

    public function setTransferOut(int $transferOut): self
    {
        $this->transferOut = $transferOut;

        return $this;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getPendingCredit(): int
    {
        return $this->pendingCredit;
    }

    public function setPendingCredit(int $pendingCredit): self
    {
        $this->pendingCredit = $pendingCredit;

        return $this;
    }

    public function getPendingDebit(): int
    {
        return $this->pendingDebit;
    }

    public function setPendingDebit(int $pendingDebit): self
    {
        $this->pendingDebit = $pendingDebit;

        return $this;
    }

    public function getConfirmedDebit(): int
    {
        return $this->confirmedDebit;
    }

    public function setConfirmedDebit(int $confirmedDebit): self
    {
        $this->confirmedDebit = $confirmedDebit;

        return $this;
    }

    public function getTimestamp(): string
    {
        return $this->timestamp;
    }

    public function setTimestamp(string $timestamp): self
    {
        $this->timestamp = $timestamp;

        return $this;
    }
}
