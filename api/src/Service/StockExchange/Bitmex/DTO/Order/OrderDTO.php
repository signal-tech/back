<?php

namespace App\Service\StockExchange\Bitmex\DTO\Order;

class OrderDTO
{
    public string $orderID;

    public string $clOrdID;

    public string $clOrdLinkID;

    public int $account;

    public string $symbol;

    public string $side;

    public ?string $simpleOrderQty;

    public float $orderQty;

    public float $price;

    public ?float $displayQty;

    public ?float $stopPx;

    public ?string $pegOffsetValue;

    public string $pegPriceType;

    public string $currency;

    public string $settlCurrency;

    public string $ordType;

    public string $timeInForce;

    public string $execInst;

    public string $contingencyType;

    public string $exDestination;

    public string $ordStatus;

    public string $triggered;

    public bool $workingIndicator;

    public string $ordRejReason;

    public ?float $simpleLeavesQty;

    public float $leavesQty;

    public ?float $simpleCumQty;

    public float $cumQty;

    public ?float $avgPx;

    public string $multiLegReportingType;

    public string $text;

    public string $transactTime;

    public string $timestamp;

    public function getOrderID(): string
    {
        return $this->orderID;
    }

    public function getClOrdID(): string
    {
        return $this->clOrdID;
    }

    public function getClOrdLinkID(): string
    {
        return $this->clOrdLinkID;
    }

    public function getAccount(): int
    {
        return $this->account;
    }

    public function getSymbol(): string
    {
        return $this->symbol;
    }

    public function getSide(): string
    {
        return $this->side;
    }

    public function getSimpleOrderQty(): ?string
    {
        return $this->simpleOrderQty;
    }

    public function getOrderQty(): float
    {
        return $this->orderQty;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getDisplayQty(): ?float
    {
        return $this->displayQty;
    }

    public function getStopPx(): ?float
    {
        return $this->stopPx;
    }

    public function getPegOffsetValue(): ?string
    {
        return $this->pegOffsetValue;
    }

    public function getPegPriceType(): string
    {
        return $this->pegPriceType;
    }

    public function getCurrency(): string
    {
        return $this->currency;
    }

    public function getSettlCurrency(): string
    {
        return $this->settlCurrency;
    }

    public function getOrdType(): string
    {
        return $this->ordType;
    }

    public function getTimeInForce(): string
    {
        return $this->timeInForce;
    }

    public function getExecInst(): string
    {
        return $this->execInst;
    }

    public function getContingencyType(): string
    {
        return $this->contingencyType;
    }

    public function getExDestination(): string
    {
        return $this->exDestination;
    }

    public function getOrdStatus(): string
    {
        return $this->ordStatus;
    }

    public function getTriggered(): string
    {
        return $this->triggered;
    }

    public function isWorkingIndicator(): bool
    {
        return $this->workingIndicator;
    }

    public function getOrdRejReason(): string
    {
        return $this->ordRejReason;
    }

    public function getSimpleLeavesQty(): ?float
    {
        return $this->simpleLeavesQty;
    }

    public function getLeavesQty(): float
    {
        return $this->leavesQty;
    }

    public function getSimpleCumQty(): ?float
    {
        return $this->simpleCumQty;
    }

    public function getCumQty(): float
    {
        return $this->cumQty;
    }

    public function getAvgPx(): ?float
    {
        return $this->avgPx;
    }

    public function getMultiLegReportingType(): string
    {
        return $this->multiLegReportingType;
    }

    public function getText(): string
    {
        return $this->text;
    }

    public function getTransactTime(): string
    {
        return $this->transactTime;
    }

    public function getTimestamp(): string
    {
        return $this->timestamp;
    }
}
