<?php

namespace App\Service\StockExchange\Bitmex;

use App\Service\StockExchange\StockExchangeStrategyServiceInterface;

interface BitmexStockExchangeServiceInterface extends StockExchangeStrategyServiceInterface
{
}
