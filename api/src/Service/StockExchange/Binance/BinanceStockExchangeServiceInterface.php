<?php

namespace App\Service\StockExchange\Binance;

use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\StockExchange\StockExchangeStrategyServiceInterface;

interface BinanceStockExchangeServiceInterface extends StockExchangeStrategyServiceInterface
{
    public const SPOT = 'spot';

    public function getSocketListenToken(
        StockExchangeToken $stockExchangeToken
    ): string;

    public function updateSocketListenToken(
        StockExchangeToken $stockExchangeToken,
        string $listenKey
    ): void;
}
