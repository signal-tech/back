<?php

namespace App\Service\StockExchange\Binance\DTO\Order;

class NewOrderResponseDTO
{
    private string $symbol;

    private int $orderId;

    private int $orderListId;

    private string $clientOrderId;

    private int $transactTime;

    private string $price;

    private string $origQty;

    private string $executedQty;

    private string $cummulativeQuoteQty;

    private string $status;

    private string $timeInForce;

    private string $type;

    private string $side;

    private array $fills;

    public function getSymbol(): string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function setOrderId(int $orderId): self
    {
        $this->orderId = $orderId;

        return $this;
    }

    public function getOrderListId(): int
    {
        return $this->orderListId;
    }

    public function setOrderListId(int $orderListId): self
    {
        $this->orderListId = $orderListId;

        return $this;
    }

    public function getClientOrderId(): string
    {
        return $this->clientOrderId;
    }

    public function setClientOrderId(string $clientOrderId): self
    {
        $this->clientOrderId = $clientOrderId;

        return $this;
    }

    public function getTransactTime(): int
    {
        return $this->transactTime;
    }

    public function setTransactTime(int $transactTime): self
    {
        $this->transactTime = $transactTime;

        return $this;
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getOrigQty(): string
    {
        return $this->origQty;
    }

    public function setOrigQty(string $origQty): self
    {
        $this->origQty = $origQty;

        return $this;
    }

    public function getExecutedQty(): string
    {
        return $this->executedQty;
    }

    public function setExecutedQty(string $executedQty): self
    {
        $this->executedQty = $executedQty;

        return $this;
    }

    public function getCummulativeQuoteQty(): string
    {
        return $this->cummulativeQuoteQty;
    }

    public function setCummulativeQuoteQty(string $cummulativeQuoteQty): self
    {
        $this->cummulativeQuoteQty = $cummulativeQuoteQty;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTimeInForce(): string
    {
        return $this->timeInForce;
    }

    public function setTimeInForce(string $timeInForce): self
    {
        $this->timeInForce = $timeInForce;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSide(): string
    {
        return $this->side;
    }

    public function setSide(string $side): self
    {
        $this->side = $side;

        return $this;
    }

    public function getFills(): array
    {
        return $this->fills;
    }

    public function setFills(array $fills): self
    {
        $this->fills = $fills;

        return $this;
    }
}
