<?php

namespace App\Service\StockExchange\Binance\DTO\Order;

use App\Service\StockExchange\DTO\AbstractCreateOrderDTO;

class CreateOrderDTO extends AbstractCreateOrderDTO
{
    private string $symbol;

    private string $side;

    private string $type;

    private string $timeInForce;

    private float $quantity;

    private float $price;

    public function getSymbol(): string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getSide(): string
    {
        return $this->side;
    }

    public function setSide(string $side): self
    {
        $this->side = $side;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getTimeInForce(): string
    {
        return $this->timeInForce;
    }

    public function setTimeInForce(string $timeInForce): self
    {
        $this->timeInForce = $timeInForce;

        return $this;
    }

    public function getQuantity(): float
    {
        return $this->quantity;
    }

    public function setQuantity(float $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }
}
