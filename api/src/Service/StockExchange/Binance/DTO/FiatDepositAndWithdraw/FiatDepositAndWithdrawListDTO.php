<?php

namespace App\Service\StockExchange\Binance\DTO\FiatDepositAndWithdraw;

class FiatDepositAndWithdrawListDTO
{
    public string $code;

    public string $message;

    /**
     * @var FiatDepositAndWithdrawDTO[]
     */
    public array $data;

    public int $total;

    public bool $success;

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getMessage(): string
    {
        return $this->message;
    }

    public function setMessage(string $message): self
    {
        $this->message = $message;

        return $this;
    }

    /**
     * @return FiatDepositAndWithdrawDTO[]
     */
    public function getData(): array
    {
        return $this->data;
    }

    /**
     * @param FiatDepositAndWithdrawDTO[] $data
     */
    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getTotal(): int
    {
        return $this->total;
    }

    public function setTotal(int $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function isSuccess(): bool
    {
        return $this->success;
    }

    public function setSuccess(bool $success): self
    {
        $this->success = $success;

        return $this;
    }
}
