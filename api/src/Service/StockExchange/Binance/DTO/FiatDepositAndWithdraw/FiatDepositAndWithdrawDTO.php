<?php

namespace App\Service\StockExchange\Binance\DTO\FiatDepositAndWithdraw;

class FiatDepositAndWithdrawDTO
{
    public string $orderNo;

    public string $fiatCurrency;

    public string $indicatedAmount;

    public string $amount;

    public string $totalFee;

    public string $method;

    public string $status;

    public int $createTime;

    public int $updateTime;

    public function getOrderNo(): string
    {
        return $this->orderNo;
    }

    public function setOrderNo(string $orderNo): self
    {
        $this->orderNo = $orderNo;

        return $this;
    }

    public function getFiatCurrency(): string
    {
        return $this->fiatCurrency;
    }

    public function setFiatCurrency(string $fiatCurrency): self
    {
        $this->fiatCurrency = $fiatCurrency;

        return $this;
    }

    public function getIndicatedAmount(): string
    {
        return $this->indicatedAmount;
    }

    public function setIndicatedAmount(string $indicatedAmount): self
    {
        $this->indicatedAmount = $indicatedAmount;

        return $this;
    }

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function setAmount(string $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getTotalFee(): string
    {
        return $this->totalFee;
    }

    public function setTotalFee(string $totalFee): self
    {
        $this->totalFee = $totalFee;

        return $this;
    }

    public function getMethod(): string
    {
        return $this->method;
    }

    public function setMethod(string $method): self
    {
        $this->method = $method;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getCreateTime(): int
    {
        return $this->createTime;
    }

    public function setCreateTime(int $createTime): self
    {
        $this->createTime = $createTime;

        return $this;
    }

    public function getUpdateTime(): int
    {
        return $this->updateTime;
    }

    public function setUpdateTime(int $updateTime): self
    {
        $this->updateTime = $updateTime;

        return $this;
    }
}
