<?php

namespace App\Service\StockExchange\Binance\DTO\AccountSnapshot;

class AccountSnapshotListDTO
{
    public int $code;

    public string $msg;

    /**
     * @var AccountSnapshotDataDTO[]
     */
    public array $snapshotVos;

    public function getCode(): int
    {
        return $this->code;
    }

    public function setCode(int $code): self
    {
        $this->code = $code;

        return $this;
    }

    public function getMsg(): string
    {
        return $this->msg;
    }

    public function setMsg(string $msg): self
    {
        $this->msg = $msg;

        return $this;
    }

    /**
     * @return AccountSnapshotDataDTO[]
     */
    public function getSnapshotVos(): array
    {
        return $this->snapshotVos;
    }

    /**
     * @param AccountSnapshotDataDTO[] $snapshotVos
     */
    public function setSnapshotVos(array $snapshotVos): self
    {
        $this->snapshotVos = $snapshotVos;

        return $this;
    }
}
