<?php

namespace App\Service\StockExchange\Binance\DTO\AccountSnapshot;

class AccountSnapshotDataDTO
{
    public AccountSnapshotBalancesListDTO $data;

    public string $type;

    public int $updateTime;

    public function getData(): AccountSnapshotBalancesListDTO
    {
        return $this->data;
    }

    public function setData(AccountSnapshotBalancesListDTO $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getUpdateTime(): int
    {
        return $this->updateTime;
    }

    public function setUpdateTime(int $updateTime): self
    {
        $this->updateTime = $updateTime;

        return $this;
    }
}
