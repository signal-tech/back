<?php

namespace App\Service\StockExchange\Binance\DTO\AccountSnapshot;

class AccountSnapshotBalancesListDTO
{
    public string $totalAssetOfBtc;

    /**
     * @var AccountSnapshotBalanceDTO[]
     */
    public array $balances;

    /**
     * @return AccountSnapshotBalanceDTO[]
     */
    public function getBalances(): array
    {
        return $this->balances;
    }

    /**
     * @param AccountSnapshotBalanceDTO[] $balances
     */
    public function setBalances(array $balances): self
    {
        $this->balances = $balances;

        return $this;
    }

    public function getTotalAssetOfBtc(): string
    {
        return $this->totalAssetOfBtc;
    }

    public function setTotalAssetOfBtc(string $totalAssetOfBtc): self
    {
        $this->totalAssetOfBtc = $totalAssetOfBtc;

        return $this;
    }
}
