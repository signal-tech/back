<?php

namespace App\Service\StockExchange\Binance\DTO\AccountSnapshot;

class AccountSnapshotBalanceDTO
{
    public string $asset;

    public string $free;

    public string $locked;

    public function getAsset(): string
    {
        return $this->asset;
    }

    public function setAsset(string $asset): self
    {
        $this->asset = $asset;

        return $this;
    }

    public function getFree(): string
    {
        return $this->free;
    }

    public function setFree(string $free): self
    {
        $this->free = $free;

        return $this;
    }

    public function getLocked(): string
    {
        return $this->locked;
    }

    public function setLocked(string $locked): self
    {
        $this->locked = $locked;

        return $this;
    }
}
