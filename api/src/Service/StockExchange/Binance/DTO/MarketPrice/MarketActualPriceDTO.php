<?php

namespace App\Service\StockExchange\Binance\DTO\MarketPrice;

use App\Base\DTO\JsonSerializableDTO;

class MarketActualPriceDTO extends JsonSerializableDTO
{
    private string $symbol;

    private string $price;

    public function getSymbol(): string
    {
        return $this->symbol;
    }

    public function setSymbol(string $symbol): self
    {
        $this->symbol = $symbol;

        return $this;
    }

    public function getPrice(): string
    {
        return $this->price;
    }

    public function setPrice(string $price): self
    {
        $this->price = $price;

        return $this;
    }
}
