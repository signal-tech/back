<?php

namespace App\Service\StockExchange\Binance\DTO\MoneyDeposit;

class MoneyDepositDTO
{
    public string $amount;

    public string $coin;

    public string $network;

    public int $status;

    public string $address;

    public string $addressTag;

    public string $txId;

    public int $insertTime;

    public int $transferType;

    public string $confirmTimes;

    public function getAmount(): string
    {
        return $this->amount;
    }

    public function getCoin(): string
    {
        return $this->coin;
    }

    public function getNetwork(): string
    {
        return $this->network;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getAddressTag(): string
    {
        return $this->addressTag;
    }

    public function getTxId(): string
    {
        return $this->txId;
    }

    public function getInsertTime(): int
    {
        return $this->insertTime;
    }

    public function getTransferType(): int
    {
        return $this->transferType;
    }

    public function getConfirmTimes(): string
    {
        return $this->confirmTimes;
    }
}
