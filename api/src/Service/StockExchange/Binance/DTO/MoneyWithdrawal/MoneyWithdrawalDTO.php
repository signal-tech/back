<?php

namespace App\Service\StockExchange\Binance\DTO\MoneyWithdrawal;

class MoneyWithdrawalDTO
{
    public string $address;

    public float $amount;

    public string $applyTime;

    public string $coin;

    public string $id;

    public string $withdrawOrderId;

    public string $network;

    public int $transferType;

    public int $status;

    public float $transactionFee;

    public string $txId;

    public function getAddress(): string
    {
        return $this->address;
    }

    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    public function getAmount(): float
    {
        return $this->amount;
    }

    public function setAmount(float $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function getApplyTime(): string
    {
        return $this->applyTime;
    }

    public function setApplyTime(string $applyTime): self
    {
        $this->applyTime = $applyTime;

        return $this;
    }

    public function getCoin(): string
    {
        return $this->coin;
    }

    public function setCoin(string $coin): self
    {
        $this->coin = $coin;

        return $this;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function setId(string $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getWithdrawOrderId(): string
    {
        return $this->withdrawOrderId;
    }

    public function setWithdrawOrderId(string $withdrawOrderId): self
    {
        $this->withdrawOrderId = $withdrawOrderId;

        return $this;
    }

    public function getNetwork(): string
    {
        return $this->network;
    }

    public function setNetwork(string $network): self
    {
        $this->network = $network;

        return $this;
    }

    public function getTransferType(): int
    {
        return $this->transferType;
    }

    public function setTransferType(int $transferType): self
    {
        $this->transferType = $transferType;

        return $this;
    }

    public function getStatus(): int
    {
        return $this->status;
    }

    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    public function getTransactionFee(): float
    {
        return $this->transactionFee;
    }

    public function setTransactionFee(float $transactionFee): self
    {
        $this->transactionFee = $transactionFee;

        return $this;
    }

    public function getTxId(): string
    {
        return $this->txId;
    }

    public function setTxId(string $txId): self
    {
        $this->txId = $txId;

        return $this;
    }
}
