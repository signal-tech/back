<?php

namespace App\Service\StockExchange\Binance\DTO\ApiKeyPermission;

class ApiKeyPermissionDTO
{
    public bool $ipRestrict;

    public int $createTime;

    public bool $enableWithdrawals;

    public bool $enableInternalTransfer;

    public bool $permitsUniversalTransfer;

    public bool $enableVanillaOptions;

    public bool $enableReading;

    public bool $enableFutures;

    public bool $enableMargin;

    public bool $enableSpotAndMarginTrading;

    public int $tradingAuthorityExpirationTime;

    public function isIpRestrict(): bool
    {
        return $this->ipRestrict;
    }

    public function setIpRestrict(bool $ipRestrict): self
    {
        $this->ipRestrict = $ipRestrict;

        return $this;
    }

    public function getCreateTime(): int
    {
        return $this->createTime;
    }

    public function setCreateTime(int $createTime): self
    {
        $this->createTime = $createTime;

        return $this;
    }

    public function isEnableWithdrawals(): bool
    {
        return $this->enableWithdrawals;
    }

    public function setEnableWithdrawals(bool $enableWithdrawals): self
    {
        $this->enableWithdrawals = $enableWithdrawals;

        return $this;
    }

    public function isEnableInternalTransfer(): bool
    {
        return $this->enableInternalTransfer;
    }

    public function setEnableInternalTransfer(bool $enableInternalTransfer): self
    {
        $this->enableInternalTransfer = $enableInternalTransfer;

        return $this;
    }

    public function isPermitsUniversalTransfer(): bool
    {
        return $this->permitsUniversalTransfer;
    }

    public function setPermitsUniversalTransfer(bool $permitsUniversalTransfer): self
    {
        $this->permitsUniversalTransfer = $permitsUniversalTransfer;

        return $this;
    }

    public function isEnableVanillaOptions(): bool
    {
        return $this->enableVanillaOptions;
    }

    public function setEnableVanillaOptions(bool $enableVanillaOptions): self
    {
        $this->enableVanillaOptions = $enableVanillaOptions;

        return $this;
    }

    public function isEnableReading(): bool
    {
        return $this->enableReading;
    }

    public function setEnableReading(bool $enableReading): self
    {
        $this->enableReading = $enableReading;

        return $this;
    }

    public function isEnableFutures(): bool
    {
        return $this->enableFutures;
    }

    public function setEnableFutures(bool $enableFutures): self
    {
        $this->enableFutures = $enableFutures;

        return $this;
    }

    public function isEnableMargin(): bool
    {
        return $this->enableMargin;
    }

    public function setEnableMargin(bool $enableMargin): self
    {
        $this->enableMargin = $enableMargin;

        return $this;
    }

    public function isEnableSpotAndMarginTrading(): bool
    {
        return $this->enableSpotAndMarginTrading;
    }

    public function setEnableSpotAndMarginTrading(bool $enableSpotAndMarginTrading): self
    {
        $this->enableSpotAndMarginTrading = $enableSpotAndMarginTrading;

        return $this;
    }

    public function getTradingAuthorityExpirationTime(): int
    {
        return $this->tradingAuthorityExpirationTime;
    }

    public function setTradingAuthorityExpirationTime(int $tradingAuthorityExpirationTime): self
    {
        $this->tradingAuthorityExpirationTime = $tradingAuthorityExpirationTime;

        return $this;
    }
}
