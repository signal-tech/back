<?php

namespace App\Service\StockExchange\Binance;

use App\Exception\StockExchange\StockExchangeInvalidPermissionException;
use App\Exception\StockExchange\TokenIsNotValidException;
use App\Feature\Order\Order;
use App\Feature\Portfolio\DTO\Deposit\DepositDTO;
use App\Feature\Portfolio\DTO\PortfolioDTO;
use App\Feature\Portfolio\DTO\Position\PositionDTO;
use App\Feature\Portfolio\DTO\Withdrawal\WithdrawalDTO;
use App\Feature\StockExchange\StockExchange;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Serializer\SerializerFactory;
use App\Service\StockExchange\Binance\DTO\AccountSnapshot\AccountSnapshotListDTO;
use App\Service\StockExchange\Binance\DTO\ApiKeyPermission\ApiKeyPermissionDTO;
use App\Service\StockExchange\Binance\DTO\FiatDepositAndWithdraw\FiatDepositAndWithdrawListDTO;
use App\Service\StockExchange\Binance\DTO\MarketPrice\MarketActualPriceDTO;
use App\Service\StockExchange\Binance\DTO\MoneyDeposit\MoneyDepositDTO;
use App\Service\StockExchange\Binance\DTO\MoneyWithdrawal\MoneyWithdrawalDTO;
use App\Service\StockExchange\Binance\DTO\Order\CreateOrderDTO;
use App\Service\StockExchange\Binance\DTO\Order\NewOrderResponseDTO;
use App\Service\StockExchange\DTO\AbstractCreateOrderDTO;
use App\Service\StockExchange\DTO\MarketFilterDTO;
use App\Service\StockExchange\DTO\MarketFiltersDataDTO;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Validator\Constraints\Date;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Throwable;
use function array_merge;
use function count;
use function explode;
use function hash_hmac;
use function microtime;
use function round;
use function strripos;
use function time;

class BinanceStockExchangeService implements BinanceStockExchangeServiceInterface
{
    protected const SCALE = 8;

    private const URL = 'https://api.binance.com/'; //https://testnet.binance.vision https://api.binance.com

    private Serializer $serializer;

    public function __construct(
        private HttpClientInterface $client,
        private LoggerInterface $logger,
        private EntityManagerInterface $em
    ) {
        $this->serializer = SerializerFactory::create();
    }

    public function canProcess(StockExchangeToken $stockExchangeToken): bool
    {
        return StockExchange::BINANCE_TYPE === $stockExchangeToken->getStockExchange()->getCode();
    }

    public function getSocketListenToken(
        StockExchangeToken $stockExchangeToken
    ): string {
        $response = $this->client->request(
            'POST',
            self::URL . 'api/v3/userDataStream',
            [
                'headers' => [
                    'X-MBX-APIKEY' => $stockExchangeToken->getApiKey(),
                ],
            ]
        );
        $response->getContent();
        $content = $response->toArray();

        /** @var string|null $key */
        $key = $content['listenKey'] ?? null;

        if (null === $key) {
            throw new Exception('Ошибка получение ключа прослушивания канала');
        }

        return $key;
    }

    public function updateSocketListenToken(
        StockExchangeToken $stockExchangeToken,
        string $listenKey
    ): void {
        $this->client->request(
            'PUT',
            self::URL . 'api/v3/userDataStream?listenKey=' . $listenKey,
            [
                'headers' => [
                    'X-MBX-APIKEY' => $stockExchangeToken->getApiKey(),
                ],
            ]
        );
    }

    public function getMarketHistoricalPrice(
        StockExchangeToken $stockExchangeToken,
        string $marketCode,
        int $startTimeStamp
    ): float {
        $startTimeStamp = $startTimeStamp * 1000;
        $prices = $this->getMarketHistoricalPriceList('BTC' . $marketCode, $startTimeStamp);

        if (empty($prices)) {
            throw new RuntimeException('Отсутствует актуальная цена для одного из элементов портфеля');
        }

        /** @var float $price */
        $price = $prices[0][1];

        return 1 / $price; //Возвращает 1 бтс к чему то, а нужно обратное число
    }

    public function getMarketActualPrice(
        StockExchangeToken $stockExchangeToken,
        string $marketCode
    ): float {
        $url = 'api/v3/ticker/price?symbol=' . $marketCode;

        $response = $this->client->request(
            'GET',
            self::URL . $url
        );
        $response->getContent();
        $content = $response->toArray();

        /**
         * @var MarketActualPriceDTO $data
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $data = $this->serializer->denormalize($content, MarketActualPriceDTO::class);

        return (float) $data->getPrice();
    }

    public function getPortfolioWalletCoin(): string
    {
        return 'BTC';
    }

    public function getUsdCoin(StockExchangeToken $stockExchangeToken): string
    {
        return 'BUSD';
    }

    public function getMarketFiltersData(
        StockExchangeToken $stockExchangeToken,
        string $marketCode
    ): MarketFiltersDataDTO {
        $url = 'api/v3/exchangeInfo?symbol=' . $marketCode;

        $response = $this->client->request(
            'GET',
            self::URL . $url
        );
        $response->getContent();
        $content = $response->toArray();

        $filtersDataDTO = new MarketFiltersDataDTO();

        if (!isset($content['symbols'][0]['filters'])) {
            return $filtersDataDTO;
        }

        foreach ($content['symbols'][0]['filters'] as $filter) {
            if ('LOT_SIZE' === $filter['filterType']) {
                $filterDTO = new MarketFilterDTO();
                $filterDTO->setMin((float) $filter['minQty']);
                $filterDTO->setMax((float) $filter['maxQty']);
                $filterDTO->setStep((float) $filter['stepSize']);

                $filtersDataDTO->setSize($filterDTO);
            } elseif ('PRICE_FILTER' === $filter['filterType']) {
                $filterDTO = new MarketFilterDTO();
                $filterDTO->setMin((float) $filter['minPrice']);
                $filterDTO->setMax((float) $filter['maxPrice']);
                $filterDTO->setStep((float) $filter['tickSize']);

                $filtersDataDTO->setPrice($filterDTO);
            }
        }

        return $filtersDataDTO;
    }

    public function getCoinsData(StockExchangeToken $stockExchangeToken): ApiKeyPermissionDTO
    {
        /** @var ApiKeyPermissionDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            'sapi/v1/capital/config/getall',
            ApiKeyPermissionDTO::class,
            false
        );

        return $data;
    }

    public function isTokenValid(StockExchangeToken $stockExchangeToken): bool
    {
        try {
            $this->getApiKeyPermissions($stockExchangeToken);

            return true;
        } catch (Throwable $throwable) {
            return false;
        }
    }

    /**
     * @psalm-suppress MixedArgument
     * @psalm-suppress MixedOperand
     * @psalm-suppress PossiblyNullOperand
     */
    public function getFirstMoneyDepositDateTime(StockExchangeToken $stockExchangeToken): ?int
    {
        $time = 1483228800000; // Стартуем с 2017 года - это дата основания бинанса
        $defaultDiff = 7776000000; // 90 дней - такой может быть макс разница между start и end date

        $limit = 1000;

        $firstDepositTime = null;
        $firstFiatDepositTime = null;

        $requestTime = $time;

        while ($requestTime < (time() * 1000)) {
            $offset = 0;

            $data = $this->getDepositHistoryList($stockExchangeToken, $limit, $offset, $requestTime, $requestTime + $defaultDiff);
            $offset += $limit;

            while (count($data) === $limit) {
                $data = $this->getDepositHistoryList($stockExchangeToken, $limit, $offset, $requestTime, $requestTime + $defaultDiff);
                $offset += $limit;
            }

            if (!empty($data)) {
                $firstDeposit = $data[count($data) - 1];

                $firstDepositTime = $firstDeposit->getInsertTime();

                break;
            }

            $requestTime += $defaultDiff;
        }

        $finishTime = time() * 1000;

        if (null !== $firstDepositTime) {
            $finishTime = $firstDepositTime;
        }

        $limit = 500;
        $requestTime = $time;

        while ($requestTime < $finishTime) {
            $page = 1;

            $data = $this->getFiatDepositAndWithdrawalHistoryList($stockExchangeToken, $limit, $page, $requestTime, $requestTime + $defaultDiff, 0);
            ++$page;

            while (count($data->getData()) === $limit) {
                $data = $this->getFiatDepositAndWithdrawalHistoryList($stockExchangeToken, $limit, $page, $requestTime, $requestTime + $defaultDiff, 0);
                ++$page;
            }

            if (!empty($data->getData())) {
                $firstDeposit = $data->getData()[count($data->getData()) - 1];

                $firstFiatDepositTime = $firstDeposit->getCreateTime();

                break;
            }

            $requestTime += $defaultDiff;
        }

        if (null === $firstDepositTime) {
            return (int) round($firstFiatDepositTime / 1000);
        } elseif (null === $firstFiatDepositTime) {
            return (int) round($firstDepositTime / 1000);
        }

        if ($firstDepositTime < $firstFiatDepositTime) {
            return (int) round($firstDepositTime / 1000);
        }

        return (int) round($firstFiatDepositTime / 1000);
    }

    public function getFirstOrderDateTime(StockExchangeToken $stockExchangeToken): ?int
    {
        return null;
    }

    public function getPortfolioByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime,
        ?PortfolioDTO $previousPortfolioDTO
    ): PortfolioDTO {
        $startPeriodDateTime = $startPeriodDateTime * 1000;
        $endPeriodDateTime = $endPeriodDateTime * 1000;

        $portfolioDTO = new PortfolioDTO();

        $portfolio = $this->getAccountSnapshotByPeriod($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);

        if (isset($portfolio->getSnapshotVos()[0])) {
            $portfolioData = $portfolio->getSnapshotVos()[0];

            $positions = [];

            foreach ($portfolioData->getData()->getBalances() as $balanceData) {
                if (0.0 === (float) $balanceData->getFree() + (float) $balanceData->getLocked()) {
                    continue;
                }

                $positions[$balanceData->getAsset()] = (new PositionDTO())
                    ->setSize((float) $balanceData->getFree() + (float) $balanceData->getLocked())
                    ->setMarket($balanceData->getAsset());
            }

            $portfolioDTO->setPositions($positions);
            $portfolioDTO->setTotalWallet((float) $portfolioData->getData()->getTotalAssetOfBtc());
        } else {
            $portfolioDTO->setPositions([]);
            $portfolioDTO->setTotalWallet(0.0);
        }

        $dataDeposit = $this->getDepositByPeriod($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        //TODO а если что то совпадет?
        $dataDeposit = array_merge($dataDeposit, $this->getFiatDepositByPeriod($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime));
        $portfolioDTO->setDeposit($dataDeposit);

        $dataWithdrawal = $this->getWithdrawalByPeriod($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        //TODO а если что то совпадет?
        $dataWithdrawal = array_merge($dataWithdrawal, $this->getFiatWithdrawalByPeriod($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime));
        $portfolioDTO->setWithdrawal($dataWithdrawal);

        return $portfolioDTO;
    }

    public function getCreateOrderDTO(StockExchangeToken $stockExchangeToken, Order $order): AbstractCreateOrderDTO
    {
        $orderData = $order->getData();

        $createOrder = new CreateOrderDTO();

        $createOrder->setSymbol((string) $orderData['s']);
        $createOrder->setSide((string) $orderData['S']);
        $createOrder->setType((string) $orderData['o']);
        $createOrder->setTimeInForce((string) $orderData['f']);
        $createOrder->setQuantity((float) $orderData['q']);
        $createOrder->setPrice((float) $orderData['p']);

        return $createOrder;
    }

    /**
     * @param CreateOrderDTO $createOrderDTO
     */
    public function createOrder(AbstractCreateOrderDTO $createOrderDTO, StockExchangeToken $stockExchangeToken): void
    {
        $url = 'api/v3/order?symbol=' . $createOrderDTO->getSymbol()
        . '&side=' . $createOrderDTO->getSide()
        . '&type=limit' //. $createOrderDTO->getType()
        . '&timeInForce=' . $createOrderDTO->getTimeInForce()
        . '&quantity=' . $createOrderDTO->getQuantity()
        . '&price=' . $createOrderDTO->getPrice();

        $this->getDataFromStockExchange(
            $stockExchangeToken,
            $url,
            NewOrderResponseDTO::class,
            false,
            'POST'
        );
    }

    private function getMarketHistoricalPriceList(
        string $marketCode,
        int $startTimeStamp
    ): array {
        $url = 'api/v3/klines?symbol=' . $marketCode . '&interval=1m&startTime=' . ((string) $startTimeStamp) . '&endTime=' . ((string) ($startTimeStamp + 10000));

        $response = $this->client->request(
            'GET',
            self::URL . $url
        );
        $response->getContent();
        $content = $response->toArray();

        return $content;
    }

    /**
     * @param DepositDTO[]      $moneyDepositList
     * @param MoneyDepositDTO[] $moneyDepositListDTO
     *
     * @return DepositDTO[]
     */
    private function calculateMoneyDeposit(array $moneyDepositList, array $moneyDepositListDTO): array
    {
        foreach ($moneyDepositListDTO as $depositDTO) {
            $moneyDeposit = new DepositDTO();

            if (isset($moneyDepositList[$depositDTO->getCoin()])) {
                $moneyDeposit = $moneyDepositList[$depositDTO->getCoin()];
            }
            $moneyDeposit->setCoin($depositDTO->getCoin());
            $moneyDeposit->setSize($moneyDeposit->getSize() + ((float) $depositDTO->getAmount()));

            $moneyDepositList[$depositDTO->getCoin()] = $moneyDeposit;
        }

        return $moneyDepositList;
    }

    /**
     * @param WithdrawalDTO[]      $moneyWithdrawalList
     * @param MoneyWithdrawalDTO[] $moneyWithdrawalListDTO
     *
     * @return WithdrawalDTO[]
     */
    private function calculateMoneyWithdrawal(array $moneyWithdrawalList, array $moneyWithdrawalListDTO): array
    {
        foreach ($moneyWithdrawalListDTO as $withdrawalDTO) {
            $moneyWithdrawal = new WithdrawalDTO();

            if (isset($moneyWithdrawalList[$withdrawalDTO->getCoin()])) {
                $moneyWithdrawal = $moneyWithdrawalList[$withdrawalDTO->getCoin()];
            }
            $moneyWithdrawal->setCoin($withdrawalDTO->getCoin());
            $moneyWithdrawal->setSize($moneyWithdrawal->getSize() + $withdrawalDTO->getAmount());

            $moneyWithdrawalList[$withdrawalDTO->getCoin()] = $moneyWithdrawal;
        }

        return $moneyWithdrawalList;
    }

    private function getApiKeyPermissions(StockExchangeToken $stockExchangeToken): ApiKeyPermissionDTO
    {
        /** @var ApiKeyPermissionDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            'sapi/v1/account/apiRestrictions',
            ApiKeyPermissionDTO::class,
            false
        );

        return $data;
    }

    /**
     * @return WithdrawalDTO[]
     */
    private function getWithdrawalByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime
    ): array {
        $limit = 1000;
        $offset = 0;
        $returnData = [];

        $data = $this->getWithdrawalHistoryList($stockExchangeToken, $limit, $offset, $startPeriodDateTime, $endPeriodDateTime);
        $returnData = $this->calculateMoneyWithdrawal($returnData, $data);

        $offset += $limit;

        while (count($data) === $limit) {
            $data = $this->getWithdrawalHistoryList($stockExchangeToken, $limit, $offset, $startPeriodDateTime, $endPeriodDateTime);
            $returnData = $this->calculateMoneyWithdrawal($returnData, $data);
            $offset += $limit;
        }

        return $returnData;
    }

    /**
     * @return DepositDTO[]
     */
    private function getDepositByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime
    ): array {
        $limit = 1000;
        $offset = 0;
        $returnData = [];

        $data = $this->getDepositHistoryList($stockExchangeToken, $limit, $offset, $startPeriodDateTime, $endPeriodDateTime);
        $returnData = $this->calculateMoneyDeposit($returnData, $data);

        $offset += $limit;

        while (count($data) === $limit) {
            $data = $this->getDepositHistoryList($stockExchangeToken, $limit, $offset, $startPeriodDateTime, $endPeriodDateTime);
            $returnData = $this->calculateMoneyDeposit($returnData, $data);
            $offset += $limit;
        }

        return $returnData;
    }

    /**
     * @return MoneyDepositDTO[]
     */
    private function getDepositHistoryList(
        StockExchangeToken $stockExchangeToken,
        int $limit,
        int $offset,
        ?int $startTimeStamp = null,
        ?int $endTimeStamp = null
    ): array {
        $url = 'sapi/v1/capital/deposit/hisrec?limit=' . $limit . '&offset=' . $offset;

        if (null !== $startTimeStamp) {
            $url .= '&startTime=' . $startTimeStamp;
        }

        if (null !== $endTimeStamp) {
            $url .= '&endTime=' . $endTimeStamp;
        }

        /** @var MoneyDepositDTO[] $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            $url,
            MoneyDepositDTO::class
        );

        return $data;
    }

    /**
     * @return DepositDTO[]
     */
    private function getFiatDepositByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime
    ): array {
        $limit = 500;
        $page = 1;
        $returnData = [];

        $data = $this->getFiatDepositAndWithdrawalHistoryList($stockExchangeToken, $limit, $page, $startPeriodDateTime, $endPeriodDateTime, 0);

        $returnData = $this->calculateMoneyFiatDeposit($returnData, $data);

        ++$page;

        while (count($data->getData()) === $limit) {
            $data = $this->getFiatDepositAndWithdrawalHistoryList($stockExchangeToken, $limit, $page, $startPeriodDateTime, $endPeriodDateTime, 0);
            $returnData = $this->calculateMoneyFiatDeposit($returnData, $data);
            ++$page;
        }

        return $returnData;
    }

    /**
     * @return WithdrawalDTO[]
     */
    private function getFiatWithdrawalByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime
    ): array {
        $limit = 500;
        $page = 1;
        $returnData = [];

        $data = $this->getFiatDepositAndWithdrawalHistoryList($stockExchangeToken, $limit, $page, $startPeriodDateTime, $endPeriodDateTime, 1);

        $returnData = $this->calculateMoneyFiatWithdrawal($returnData, $data);

        ++$page;

        while (count($data->getData()) === $limit) {
            $data = $this->getFiatDepositAndWithdrawalHistoryList($stockExchangeToken, $limit, $page, $startPeriodDateTime, $endPeriodDateTime, 1);
            $returnData = $this->calculateMoneyFiatWithdrawal($returnData, $data);
            ++$page;
        }

        return $returnData;
    }

    /**
     * @param DepositDTO[] $moneyDepositList
     *
     * @return DepositDTO[]
     */
    private function calculateMoneyFiatDeposit(array $moneyDepositList, FiatDepositAndWithdrawListDTO $moneyDepositListDTO): array
    {
        foreach ($moneyDepositListDTO->getData() as $depositDTO) {
            if ('Successful' !== $depositDTO->getStatus()) {
                continue;
            }

            $moneyDeposit = new DepositDTO();

            if (isset($moneyDepositList[$depositDTO->getFiatCurrency()])) {
                $moneyDeposit = $moneyDepositList[$depositDTO->getFiatCurrency()];
            }
            $moneyDeposit->setCoin($depositDTO->getFiatCurrency());
            $moneyDeposit->setSize($moneyDeposit->getSize() + (float) $depositDTO->getAmount());

            $moneyDepositList[$depositDTO->getFiatCurrency()] = $moneyDeposit;
        }

        return $moneyDepositList;
    }

    /**
     * @param WithdrawalDTO[] $moneyWithdrawalList
     *
     * @return WithdrawalDTO[]
     */
    private function calculateMoneyFiatWithdrawal(array $moneyWithdrawalList, FiatDepositAndWithdrawListDTO $moneyWithdrawalListDTO): array
    {
        foreach ($moneyWithdrawalListDTO->getData() as $withdrawalDTO) {
            if ('Successful' !== $withdrawalDTO->getStatus()) {
                continue;
            }

            $moneyWithdrawal = new WithdrawalDTO();

            if (isset($moneyWithdrawalList[$withdrawalDTO->getFiatCurrency()])) {
                $moneyWithdrawal = $moneyWithdrawalList[$withdrawalDTO->getFiatCurrency()];
            }
            $moneyWithdrawal->setCoin($withdrawalDTO->getFiatCurrency());
            $moneyWithdrawal->setSize($moneyWithdrawal->getSize() + (float) $withdrawalDTO->getAmount());

            $moneyWithdrawalList[$withdrawalDTO->getFiatCurrency()] = $moneyWithdrawal;
        }

        return $moneyWithdrawalList;
    }

    private function getFiatDepositAndWithdrawalHistoryList(
        StockExchangeToken $stockExchangeToken,
        int $rows,
        int $page,
        int $startTimeStamp,
        int $endTimeStamp,
        int $transactionType //0-deposit,1-withdraw
    ): FiatDepositAndWithdrawListDTO {
        $url = 'sapi/v1/fiat/orders?rows=' . $rows . '&page=' . $page;

        $url .= '&beginTime=' . $startTimeStamp;
        $url .= '&endTime=' . $endTimeStamp;
        $url .= '&transactionType=' . $transactionType;

        /** @var FiatDepositAndWithdrawListDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            $url,
            FiatDepositAndWithdrawListDTO::class,
            false
        );

        return $data;
    }

    private function getAccountSnapshotByPeriod(
        StockExchangeToken $stockExchangeToken,
        ?int $startTimeStamp = null,
        ?int $endTimeStamp = null
    ): AccountSnapshotListDTO {
        $url = 'sapi/v1/accountSnapshot?type=SPOT&startTime=' . ((string) $startTimeStamp) . '&endTime=' . ((string) $endTimeStamp);

        /** @var AccountSnapshotListDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            $url,
            AccountSnapshotListDTO::class,
            false
        );

        return $data;
    }

    private function getWithdrawalHistoryList(
        StockExchangeToken $stockExchangeToken,
        int $limit,
        int $offset,
        ?int $startTimeStamp = null,
        ?int $endTimeStamp = null
    ): array {
        $url = 'sapi/v1/capital/withdraw/history?limit=' . $limit . '&offset=' . $offset;

        if (null !== $startTimeStamp) {
            $url .= '&startTime=' . $startTimeStamp;
        }

        if (null !== $endTimeStamp) {
            $url .= '&endTime=' . $endTimeStamp;
        }

        /** @var MoneyWithdrawalDTO[] $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            $url,
            MoneyWithdrawalDTO::class
        );

        return $data;
    }

    private function getDataFromStockExchange(
        StockExchangeToken $stockExchangeToken,
        string $url,
        string $dtoType,
        bool $isArray = true,
        string $method = 'GET'
    ): array|object {
        $time = round(microtime(true) * 1000);

        if (false === strripos($url, '?')) {
            $url .= '?';
        } else {
            $url .= '&';
        }

        //TODO recvWindow - убрать или уменьшить после тестов
        $url .= 'recvWindow=60000&timestamp=' . $time;

        $params = explode('?', $url);

        $signature = hash_hmac(
            'sha256',
            $params[1],
            $stockExchangeToken->getSecret()
        );

        $response = $this->client->request(
            $method,
            self::URL . $url . '&signature=' . $signature,
            [
                'headers' => [
                    'X-MBX-APIKEY' => $stockExchangeToken->getApiKey(),
                ],
            ]
        );

        $data = $response->getContent(false);

        if (Response::HTTP_OK !== $response->getStatusCode()) { //TODO сделать нормальную стратегию
            $this->logger->critical(
                'Binance request FATAL code: ' . $response->getStatusCode() . ' content: ' . $data . ' stockExchangeTokenId: ' . $stockExchangeToken->getId()
            );

            $errorData = json_decode($data, true);

            if ('Invalid API-key, IP, or permissions for action.' === $errorData['msg'] && 'POST' === $method) {
                throw new StockExchangeInvalidPermissionException('Отсутствует доступ для данного действия на стороне биржи.');
            }

            if (
                'Invalid Api-Key ID.' === $errorData['msg']
                || 'Invalid API-key, IP, or permissions for action.' === $errorData['msg'] && 'GET' === $method
            ) {
                $stockExchangeToken->setDeletedAtFromStockExchange(new DateTime());
                $this->em->persist($stockExchangeToken);
                $this->em->flush();
            }

            throw new TokenIsNotValidException('Binance request FATAL code: ' . $response->getStatusCode() . ' content: ' . $data . 'stockExchangeTokenId: ' . $stockExchangeToken->getId());
        }

        $content = $response->toArray();

        if ($isArray) {
            $dtoType .= '[]';
        }

        /** @var array|object $dto */
        $dto = $this->serializer->denormalize($content, $dtoType);

        return $dto;
    }
}
