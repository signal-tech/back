<?php

namespace App\Service\StockExchange;

use App\Feature\MarketHistoricalPrice\MarketHistoricalPrice;
use App\Feature\MarketHistoricalPrice\MarketHistoricalPriceRepository;
use App\Feature\Order\Order;
use App\Feature\Portfolio\DTO\PortfolioDTO;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Feature\MarketHistoricalPrice\MarketHistoricalPriceServiceInterface;
use App\Service\StockExchange\DTO\AbstractCreateOrderDTO;
use App\Service\StockExchange\DTO\MarketFiltersDataDTO;
use Doctrine\ORM\EntityManagerInterface;
use RuntimeException;

class StockExchangeService implements StockExchangeServiceInterface
{
    /**
     * @var StockExchangeStrategyServiceInterface[]
     */
    private $stockExchangeStrategies = [];

    public function __construct(
        private MarketHistoricalPriceServiceInterface $marketHistoricalPriceService,
        private EntityManagerInterface $em
    ) {
    }

    public function addElement(StockExchangeStrategyServiceInterface $service): void
    {
        $this->stockExchangeStrategies[] = $service;
    }

    public function getFirstMoneyDepositDateTime(StockExchangeToken $stockExchangeToken): ?int
    {
        foreach ($this->stockExchangeStrategies as $service) {
            if ($service->canProcess($stockExchangeToken)) {
                return $service->getFirstMoneyDepositDateTime($stockExchangeToken);
            }
        }

        return null;
    }

    public function getFirstOrderDateTime(StockExchangeToken $stockExchangeToken): ?int
    {
        foreach ($this->stockExchangeStrategies as $service) {
            if ($service->canProcess($stockExchangeToken)) {
                return $service->getFirstOrderDateTime($stockExchangeToken);
            }
        }

        return null;
    }

    public function isTokenValid(StockExchangeToken $stockExchangeToken): bool
    {
        foreach ($this->stockExchangeStrategies as $service) {
            if ($service->canProcess($stockExchangeToken)) {
                return $service->isTokenValid($stockExchangeToken);
            }
        }

        return false;
    }

    public function getPortfolioByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime,
        ?PortfolioDTO $previousPortfolioDTO
    ): PortfolioDTO {
        foreach ($this->stockExchangeStrategies as $service) {
            if ($service->canProcess($stockExchangeToken)) {
                return $service->getPortfolioByPeriod(
                    $stockExchangeToken,
                    $startPeriodDateTime,
                    $endPeriodDateTime,
                    $previousPortfolioDTO
                );
            }
        }

        throw new RuntimeException('Ошибка выборка стратегии биржи');
    }

    public function getPortfolioWalletCoin(StockExchangeToken $stockExchangeToken): string
    {
        foreach ($this->stockExchangeStrategies as $service) {
            if ($service->canProcess($stockExchangeToken)) {
                return $service->getPortfolioWalletCoin();
            }
        }

        throw new RuntimeException('Ошибка выборка стратегии биржи');
    }

    public function getMarketHistoricalPrice(
        StockExchangeToken $stockExchangeToken,
        string $marketCode,
        int $startTimeStamp
    ): float {
        /**
         * @var MarketHistoricalPriceRepository $marketHistoricalPriceRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $marketHistoricalPriceRepository = $this->em->getRepository(MarketHistoricalPrice::class);

        $marketHistoricalPrice = $marketHistoricalPriceRepository->getMarketHistoricalPrice(
            $stockExchangeToken->getStockExchange(),
            $startTimeStamp,
            $marketCode,
            $this->getPortfolioWalletCoin($stockExchangeToken)
        );

        if (null !== $marketHistoricalPrice) {
            return $marketHistoricalPrice->getPrice();
        }

        foreach ($this->stockExchangeStrategies as $service) {
            if ($service->canProcess($stockExchangeToken)) {
                $price = $service->getMarketHistoricalPrice(
                    $stockExchangeToken,
                    $marketCode,
                    $startTimeStamp
                );

                $this->marketHistoricalPriceService->createMarketHistoricalPrice(
                    $stockExchangeToken->getStockExchange(),
                    $startTimeStamp,
                    $marketCode,
                    $this->getPortfolioWalletCoin($stockExchangeToken),
                    $price
                );

                return $price;
            }
        }

        throw new RuntimeException('Ошибка выборка стратегии биржи');
    }

    public function getMarketActualPrice(
        StockExchangeToken $stockExchangeToken,
        string $marketCode
    ): float {
        foreach ($this->stockExchangeStrategies as $service) {
            if ($service->canProcess($stockExchangeToken)) {
                return $service->getMarketActualPrice(
                    $stockExchangeToken,
                    $marketCode
                );
            }
        }

        throw new RuntimeException('Ошибка выборка стратегии биржи');
    }

    public function getMarketFiltersData(
        StockExchangeToken $stockExchangeToken,
        string $marketCode
    ): MarketFiltersDataDTO {
        foreach ($this->stockExchangeStrategies as $service) {
            if ($service->canProcess($stockExchangeToken)) {
                return $service->getMarketFiltersData(
                    $stockExchangeToken,
                    $marketCode
                );
            }
        }

        throw new RuntimeException('Ошибка выборка стратегии биржи');
    }

    public function getUsdCoin(StockExchangeToken $stockExchangeToken): string
    {
        foreach ($this->stockExchangeStrategies as $service) {
            if ($service->canProcess($stockExchangeToken)) {
                return $service->getUsdCoin(
                    $stockExchangeToken
                );
            }
        }

        throw new RuntimeException('Ошибка выборка стратегии биржи');
    }

    public function createOrder(AbstractCreateOrderDTO $createOrderDTO, StockExchangeToken $stockExchangeToken): void
    {
        foreach ($this->stockExchangeStrategies as $service) {
            if ($service->canProcess($stockExchangeToken)) {
                $service->createOrder($createOrderDTO, $stockExchangeToken);

                return;
            }
        }

        throw new RuntimeException('Ошибка выборка стратегии биржи');
    }

    public function getCreateOrderDTO(StockExchangeToken $stockExchangeToken, Order $order): AbstractCreateOrderDTO
    {
        foreach ($this->stockExchangeStrategies as $service) {
            if ($service->canProcess($stockExchangeToken)) {
                return $service->getCreateOrderDTO($stockExchangeToken, $order);
            }
        }

        throw new RuntimeException('Ошибка выборка стратегии биржи');
    }
}
