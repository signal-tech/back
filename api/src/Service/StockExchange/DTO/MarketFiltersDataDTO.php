<?php

namespace App\Service\StockExchange\DTO;

class MarketFiltersDataDTO
{
    private ?MarketFilterDTO $price;

    private ?MarketFilterDTO $size;

    public function getPrice(): ?MarketFilterDTO
    {
        return $this->price;
    }

    public function setPrice(?MarketFilterDTO $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getSize(): ?MarketFilterDTO
    {
        return $this->size;
    }

    public function setSize(?MarketFilterDTO $size): self
    {
        $this->size = $size;

        return $this;
    }
}
