<?php

namespace App\Service\StockExchange\DTO;

use App\Base\DTO\JsonSerializableDTO;

abstract class AbstractCreateOrderDTO extends JsonSerializableDTO
{
}
