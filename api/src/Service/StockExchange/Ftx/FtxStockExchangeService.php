<?php

namespace App\Service\StockExchange\Ftx;

use App\Feature\Order\Order;
use App\Feature\Portfolio\DTO\Borrow\BorrowDTO;
use App\Feature\Portfolio\DTO\Deposit\DepositDTO;
use App\Feature\Portfolio\DTO\Fills\FillsDTO;
use App\Feature\Portfolio\DTO\Lending\LendingDTO;
use App\Feature\Portfolio\DTO\PortfolioDTO;
use App\Feature\Portfolio\DTO\Withdrawal\WithdrawalDTO;
use App\Feature\StockExchange\StockExchange;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Feature\Portfolio\PortfolioServiceInterface;
use App\Service\Serializer\SerializerFactory;
use App\Service\StockExchange\DTO\AbstractCreateOrderDTO;
use App\Service\StockExchange\DTO\MarketFiltersDataDTO;
use App\Service\StockExchange\Ftx\DTO\Balance\BalanceListDTO;
use App\Service\StockExchange\Ftx\DTO\BorrowHistory\BorrowHistoryListDTO;
use App\Service\StockExchange\Ftx\DTO\Fills\FillsListDTO;
use App\Service\StockExchange\Ftx\DTO\FundingPayment\FundingPaymentListDTO;
use App\Service\StockExchange\Ftx\DTO\LendingHistory\LendingHistoryListDTO;
use App\Service\StockExchange\Ftx\DTO\MarketHistoricalPrices\MarketHistoricalPricesListDTO;
use App\Service\StockExchange\Ftx\DTO\MoneyDeposit\MoneyDepositListDTO;
use App\Service\StockExchange\Ftx\DTO\MoneyWithdrawal\MoneyWithdrawalListDTO;
use App\Service\StockExchange\Ftx\DTO\Order\CreateOrderDTO;
use App\Service\StockExchange\Ftx\DTO\Order\NewOrderListResponseDTO;
use App\Service\StockExchange\Ftx\DTO\Order\OrderListDTO;
use App\Service\StockExchange\Ftx\DTO\StakingRewards\StakingRewardsListDTO;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use RuntimeException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use Symfony\Contracts\HttpClient\HttpClientInterface;
use Throwable;
use function bcadd;
use function bcsub;
use function count;
use function hash_hmac;
use function number_format;
use function time;

/**
 * Оссобенности апи биржи:
 * 1) Пагинация по дате, но отдают всего 100 элементов.
 */
class FtxStockExchangeService implements FtxStockExchangeServiceInterface
{
    protected const SCALE = 8;

    private Serializer $serializer;

    public function __construct(
        private HttpClientInterface $client,
        private PortfolioServiceInterface $portfolioService,
        private LoggerInterface $logger,
        private EntityManagerInterface $em
    ) {
        $this->serializer = SerializerFactory::create();
    }

    public function canProcess(StockExchangeToken $stockExchangeToken): bool
    {
        return StockExchange::FTX_TYPE === $stockExchangeToken->getStockExchange()->getCode();
    }

    public function getMarketHistoricalPrice(
        StockExchangeToken $stockExchangeToken,
        string $marketCode,
        int $startTimeStamp
    ): float {
        $prices = $this->getMarketHistoricalPriceList($stockExchangeToken, $marketCode, $startTimeStamp)->getResult();

        if (empty($prices)) {
            throw new RuntimeException('Отсутствует актуальная цена для одного из элементов портфеля');
        }

        $price = $prices[0]->getOpen();

        return $price;
    }

    public function getMarketActualPrice(
        StockExchangeToken $stockExchangeToken,
        string $marketCode
    ): float {
        return 0.0;
    }

    public function getFirstMoneyDepositDateTime(StockExchangeToken $stockExchangeToken): ?int
    {
        $searchTime = 1417392000; //Пока хардкодом 2015 год
        $diffTime = 2419200; //месяц

        $returnDateTime = null;
        $firstData = null;

        while ($searchTime < time()) {
            $data = $this->getDepositHistoryList($stockExchangeToken, $searchTime, $searchTime + $diffTime);
            $dataDeposit = $data->getResult();

            $searchTime += $diffTime;

            if (!empty($dataDeposit)) {
                $firstData = $dataDeposit[count($dataDeposit) - 1];

                break;
            }
        }

        if (null !== $firstData) {
            $returnDateTime = new DateTime($firstData->getTime());
            $finishTime = $returnDateTime->getTimestamp();

            while (!empty($dataDeposit)) {
                $data = $this->getDepositHistoryList($stockExchangeToken, $searchTime, $finishTime);
                $dataDeposit = $data->getResult();

                if (!empty($dataDeposit)) {
                    $firstData = $dataDeposit[count($dataDeposit) - 1];
                }

                $returnDateTime = new DateTime($firstData->getTime());
                $finishTime = $returnDateTime->getTimestamp();
            }
        }

        return $returnDateTime ? $returnDateTime->getTimestamp() : null;
    }

    public function getFirstOrderDateTime(StockExchangeToken $stockExchangeToken): ?int
    {
        $searchTime = 1417392000; //Пока хардкодом 2015 год
        $diffTime = 2419200; //месяц

        $returnDateTime = null;

        $firstData = null;
        $data = null;

        while ($searchTime < time()) {
            $data = $this->getOrderHistoryList($stockExchangeToken, $searchTime, $searchTime + $diffTime);
            $dataOrder = $data->getResult();

            if (!empty($dataOrder)) {
                $firstData = $dataOrder[count($dataOrder) - 1];

                break;
            }
            $searchTime += $diffTime;
        }

        if (null !== $firstData) {
            $returnDateTime = new DateTime($firstData->getCreatedAt());
            $finishTime = $returnDateTime->getTimestamp();

            while ($data && $data->isHasMoreData()) {
                $data = $this->getOrderHistoryList($stockExchangeToken, $searchTime, $finishTime);
                $dataOrder = $data->getResult();

                if (empty($dataOrder)) {
                    break;
                }
                $firstData = $dataOrder[count($dataOrder) - 1];

                $returnDateTime = new DateTime($firstData->getCreatedAt());
                $finishTime = $returnDateTime->getTimestamp();
            }
        }

        return $returnDateTime ? $returnDateTime->getTimestamp() : null;
    }

    public function getPortfolioByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime,
        ?PortfolioDTO $previousPortfolioDTO
    ): PortfolioDTO {
        $portfolioDTO = new PortfolioDTO();
        $portfolioDTO->setPositions($previousPortfolioDTO ? $previousPortfolioDTO->getPositions() : []);

        $fundingPayment = $this->getFundingPaymentByPeriod($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        $portfolioDTO->setFundingPayment($fundingPayment);

        $this->getPositionsByPeriod($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime, $portfolioDTO);

        $deposit = $this->getDepositByPeriod($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        $portfolioDTO->setDeposit($deposit);

        $withdrawal = $this->getWithdrawalByPeriod($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        $portfolioDTO->setWithdrawal($withdrawal);

        $borrow = $this->getBorrowByPeriod($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        $portfolioDTO->setBorrow($borrow);

        $lending = $this->getLendingByPeriod($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        $portfolioDTO->setLending($lending);

        $this->portfolioService->recalculateWallet($portfolioDTO, $this->getPortfolioWalletCoin());

        return $portfolioDTO;
    }

    public function isTokenValid(StockExchangeToken $stockExchangeToken): bool
    {
        try {
            $this->getBalance($stockExchangeToken);

            return true;
        } catch (Throwable $throwable) {
            return false;
        }
    }

    public function getPortfolioWalletCoin(): string
    {
        return 'USD';
    }

    public function getUsdCoin(StockExchangeToken $stockExchangeToken): string
    {
        return 'USD';
    }

    public function getMarketFiltersData(
        StockExchangeToken $stockExchangeToken,
        string $marketCode
    ): MarketFiltersDataDTO {
        return new MarketFiltersDataDTO(); //TODO
    }

    public function getCreateOrderDTO(StockExchangeToken $stockExchangeToken, Order $order): AbstractCreateOrderDTO
    {
        $orderData = $order->getData();

        $createOrder = new CreateOrderDTO();

        $createOrder->setMarket((string) $orderData['market']);
        $createOrder->setSide((string) $orderData['side']);
        $createOrder->setPrice((float) $orderData['price']);
        $createOrder->setType((string) $orderData['type']);
        $createOrder->setSize((float) $orderData['size']);
        $createOrder->setReduceOnly((bool) $orderData['reduceOnly']);
        $createOrder->setIoc((bool) $orderData['ioc']);
        $createOrder->setPostOnly((bool) $orderData['postOnly']);

        return $createOrder;
    }

    /**
     * @param CreateOrderDTO $createOrderDTO
     */
    public function createOrder(AbstractCreateOrderDTO $createOrderDTO, StockExchangeToken $stockExchangeToken): void
    {
        throw new RuntimeException('Создание сделок FTX недоступно.');
//
//        $this->getDataFromStockExchange(
//            $stockExchangeTokenPart->getStockExchangeToken(),
//            'orders',
//            NewOrderListResponseDTO::class,
//            'POST',
//            [
//                'market' => $createOrderDTO->getMarket(),
//                'side' => $createOrderDTO->getSide(),
//                'type' => $createOrderDTO->getType(),
//                'size' => $createOrderDTO->getSize(),
//                'price' => $createOrderDTO->getPrice(),
//                'reduceOnly' => $createOrderDTO->isReduceOnly(),
//                'ioc' => $createOrderDTO->isIoc(),
//                'postOnly' => $createOrderDTO->isPostOnly(),
//            ]
//        );
    }

    private function getBalance(StockExchangeToken $stockExchangeToken): BalanceListDTO
    {
        /** @var BalanceListDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            'wallet/balances',
            BalanceListDTO::class
        );

        return $data;
    }

    private function getDataFromStockExchange(
        StockExchangeToken $stockExchangeToken,
        string $url,
        string $dtoType,
        string $method = 'GET',
        ?array $body = null
    ): object {
        $time = time() * 1000;

        $toSignatureString = $time . $method . '/api/' . $url;

        $requestData = [
            'headers' => [
                'FTX-KEY' => $stockExchangeToken->getApiKey(),
                'FTX-TS' => $time,
            ],
        ];

        if ($body) {
            $toSignatureString .= json_encode($body);
            $requestData['body'] = json_encode($body);
            $requestData['headers']['Content-Type'] = 'application/json';
        }
        //echo $toSignatureString;die;
        $signature = hash_hmac(
            'sha256',
            $toSignatureString,
            $stockExchangeToken->getSecret()
        );

        $requestData['headers']['FTX-SIGN'] = $signature;

        $response = $this->client->request(
            $method,
            'https://ftx.com/api/' . $url,
            $requestData
        );

        $data = $response->getContent(false);

        if (Response::HTTP_OK !== $response->getStatusCode()) { //TODO сделать нормальную стратегию
            $this->logger->critical(
                'FTX request FATAL code: ' . $response->getStatusCode() . ' content: ' . $data . ' stockExchangeTokenId: ' . $stockExchangeToken->getId()
            );

            $errorData = json_decode($data, true);

            if ('Not logged in: Invalid API key' === $errorData['error'] || 'Not logged in: IP not whitelisted' === $errorData['error']) {
                $stockExchangeToken->setDeletedAtFromStockExchange(new DateTime());
                $this->em->persist($stockExchangeToken);
                $this->em->flush();
            }

            throw new RuntimeException('FTX request FATAL code: ' . $response->getStatusCode() . ' content: ' . $data . 'stockExchangeTokenId: ' . $stockExchangeToken->getId());
        }

        $content = $response->toArray();

        /** @var object $dto */
        $dto = $this->serializer->denormalize($content, $dtoType);

        return $dto;
    }

    private function getLendingHistoryList(
        StockExchangeToken $stockExchangeToken,
        int $startTimeStamp,
        int $endTimeStamp
    ): LendingHistoryListDTO {
        /** @var LendingHistoryListDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            'spot_margin/lending_history?start_time=' . $startTimeStamp . '&end_time=' . $endTimeStamp,
            LendingHistoryListDTO::class
        );

        return $data;
    }

    private function getBorrowHistoryList(
        StockExchangeToken $stockExchangeToken,
        int $startTimeStamp,
        int $endTimeStamp
    ): BorrowHistoryListDTO {
        /** @var BorrowHistoryListDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            'spot_margin/borrow_history?start_time=' . $startTimeStamp . '&end_time=' . $endTimeStamp,
            BorrowHistoryListDTO::class
        );

        return $data;
    }

//    private function getStakingRewardsList(
//        StockExchangeToken $stockExchangeToken,
//        int $startTimeStamp,
//        int $endTimeStamp
//    ): StakingRewardsListDTO {
//        /** @var StakingRewardsListDTO $data */
//        $data = $this->getDataFromStockExchange(
//            $stockExchangeToken,
//            'staking/staking_rewards?start_time=' . $startTimeStamp . '&end_time=' . $endTimeStamp,
//            StakingRewardsListDTO::class
//        );
//
//        return $data;
//    }

    private function getFillsList(
        StockExchangeToken $stockExchangeToken,
        int $startTimeStamp,
        int $endTimeStamp
    ): FillsListDTO {
        /** @var FillsListDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            'fills?start_time=' . $startTimeStamp . '&end_time=' . $endTimeStamp,
            FillsListDTO::class
        );

        return $data;
    }

    private function getFundingPaymentList(
        StockExchangeToken $stockExchangeToken,
        int $startTimeStamp,
        int $endTimeStamp
    ): FundingPaymentListDTO {
        /** @var FundingPaymentListDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            'funding_payments?start_time=' . $startTimeStamp . '&end_time=' . $endTimeStamp,
            FundingPaymentListDTO::class
        );

        return $data;
    }

    private function getMarketHistoricalPriceList(
        StockExchangeToken $stockExchangeToken,
        string $marketCode,
        int $startTimeStamp
    ): MarketHistoricalPricesListDTO {
        try {
            /** @var MarketHistoricalPricesListDTO $data */
            $data = $this->getDataFromStockExchange(
                $stockExchangeToken,
                'markets/' . $marketCode . '/candles?resolution=15&start_time=' . $startTimeStamp . '&end_time=' . ((string) ($startTimeStamp + 10)),
                MarketHistoricalPricesListDTO::class
            );
        } catch (Throwable $throwable) {
            /** @var MarketHistoricalPricesListDTO $data */
            $data = $this->getDataFromStockExchange(
                $stockExchangeToken,
                'markets/' . $marketCode . '/USD/candles?resolution=15&start_time=' . $startTimeStamp . '&end_time=' . ((string) ($startTimeStamp + 10)),
                MarketHistoricalPricesListDTO::class
            );
        }

        return $data;
    }

    private function getDepositHistoryList(
        StockExchangeToken $stockExchangeToken,
        int $startTimeStamp,
        int $endTimeStamp
    ): MoneyDepositListDTO {
        /** @var MoneyDepositListDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            'wallet/deposits?start_time=' . $startTimeStamp . '&end_time=' . $endTimeStamp,
            MoneyDepositListDTO::class
        );

        return $data;
    }

    private function getOrderHistoryList(
        StockExchangeToken $stockExchangeToken,
        int $startTimeStamp,
        int $endTimeStamp
    ): OrderListDTO {
        /** @var OrderListDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            'orders/history?start_time=' . $startTimeStamp . '&end_time=' . $endTimeStamp,
            OrderListDTO::class
        );

        return $data;
    }

    private function getWithdrawalHistoryList(
        StockExchangeToken $stockExchangeToken,
        int $startTimeStamp,
        int $endTimeStamp
    ): MoneyWithdrawalListDTO {
        /** @var MoneyWithdrawalListDTO $data */
        $data = $this->getDataFromStockExchange(
            $stockExchangeToken,
            'wallet/withdrawals?start_time=' . $startTimeStamp . '&end_time=' . $endTimeStamp,
            MoneyWithdrawalListDTO::class
        );

        return $data;
    }

//    private function getFillsByPeriod(//Возможно не только USD поэтому пока вернем массив
//        StockExchangeToken $stockExchangeToken,
//        int $startPeriodDateTime,
//        int $endPeriodDateTime
//    ): array {
//        $returnData = [];
//        $data = $this->getFillsList($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
//        $dataFills = $data->getResult();
//
//        $returnData = $this->calculateFillsDeposit($returnData, $data);
//
//        if (!empty($dataFills)) {
//            $firstData = $dataFills[count($dataFills) - 1];
//
//            $returnDateTime = new DateTime($firstData->getTime());
//            $finishTime = $returnDateTime->getTimestamp();
//
//            while (!empty($dataFills)) {
//                $data = $this->getFillsList($stockExchangeToken, $startPeriodDateTime, $finishTime);
//
//                $dataFills = $data->getResult();
//                if (empty($dataFills)) {
//                    break;
//                }
//
//                $returnData = $this->calculateFillsDeposit($returnData, $data);
//
//                $firstData = $dataFills[count($dataFills) - 1];
//
//                $returnDateTime = new DateTime($firstData->getTime());
//                $finishTime = $returnDateTime->getTimestamp();
//            }
//        }
//
//        return $returnData;
//    }

    private function getFundingPaymentByPeriod(//Возможно не только USD поэтому пока вернем массив
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime
    ): float {
        $returnData = 0.0;
        $data = $this->getFundingPaymentList($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        $dataFills = $data->getResult();

        $returnData = $this->calculateFundingPayments($returnData, $data);

        if (!empty($dataFills)) {
            $firstData = $dataFills[count($dataFills) - 1];

            $returnDateTime = new DateTime($firstData->getTime());
            $finishTime = $returnDateTime->getTimestamp() - 1; //TODO по правильному обработать везде

            while (!empty($dataFills)) {/** @phpstan-ignore-line */
                $data = $this->getFundingPaymentList($stockExchangeToken, $startPeriodDateTime, $finishTime);

                $dataFills = $data->getResult();

                if (empty($dataFills)) {
                    break;
                }

                $returnData = $this->calculateFundingPayments($returnData, $data);

                $firstData = $dataFills[count($dataFills) - 1];

                $returnDateTime = new DateTime($firstData->getTime());
                $finishTime = $returnDateTime->getTimestamp();
            }
        }

        return -1 * $returnData;
    }

    private function calculateFundingPayments(float $payment, FundingPaymentListDTO $fundingPaymentListDTO): float
    {
        foreach ($fundingPaymentListDTO->getResult() as $fillDTO) {
            $payment = (float) bcadd(
                (string) $payment,
                number_format($fillDTO->getPayment(), self::SCALE, '.', ''),
                self::SCALE
            );
        }

        return $payment;
    }

//    /**
//     * @return FillsDTO[]
//     */
//    private function calculateFillsDeposit(array $fillsList, FillsListDTO $fillsListDTO): array
//    {
//        foreach ($fillsListDTO->getResult() as $fillDTO) {
//            $fill = new FillsDTO();
//
//            if (isset($fillsList[$fillDTO->getFeeCurrency()])) {
//                $fill = $fillsList[$fillDTO->getFeeCurrency()];
//            }
//            $fill->setFeeCurrency($fillDTO->getFeeCurrency());
//            $fill->setFee(
//                bcadd(
//                    number_format($fill->getFee(), self::SCALE),
//                    number_format($fillDTO->getFee(), self::SCALE),
//                    self::SCALE
//                )
//            );
//
//            $fillsList[$fill->getFeeCurrency()] = $fill;
//        }
//
//        return $fillsList;
//    }

    private function getLendingByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime
    ): array {
        $returnData = [];
        $data = $this->getLendingHistoryList($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        //$dataLending = $data->getResult();

        $returnData = $this->calculateLending($returnData, $data);

//        if (!empty($dataLending)) {
//            $firstData = $dataLending[count($dataLending) - 1];
//
//            $returnDateTime = new DateTime($firstData->getTime());
//            $finishTime = $returnDateTime->getTimestamp()  - 1;
//
//            while (!empty($dataLending)) {
//                $data = $this->getLendingHistoryList($stockExchangeToken, $startPeriodDateTime, $finishTime);
//                $dataLending = $data->getResult();
//                if (empty($dataLending)) {
//                    break;
//                }
//                $returnData = $this->calculateLending($returnData, $data);
//
//                $firstData = $dataLending[count($dataLending) - 1];
//
//                $returnDateTime = new DateTime($firstData->getTime());
//                $finishTime = $returnDateTime->getTimestamp();
//            }
//        }

        return $returnData;
    }

    private function getBorrowByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime
    ): array {
        $returnData = [];
        $data = $this->getBorrowHistoryList($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        //$dataBorrow = $data->getResult();

        $returnData = $this->calculateBorrow($returnData, $data);

//        if (!empty($dataBorrow)) {
//            $firstData = $dataBorrow[count($dataBorrow) - 1];
//
//            $returnDateTime = new DateTime($firstData->getTime());
//            $finishTime = $returnDateTime->getTimestamp() - 1;
//
//            while (!empty($dataBorrow)) {
//                $data = $this->getBorrowHistoryList($stockExchangeToken, $startPeriodDateTime, $finishTime);
//                $dataBorrow = $data->getResult();
//                if (empty($dataBorrow)) {
//                    break;
//                }
//                $returnData = $this->calculateBorrow($returnData, $data);
//
//                $firstData = $dataBorrow[count($dataBorrow) - 1];
//
//                $returnDateTime = new DateTime($firstData->getTime());
//                $finishTime = $returnDateTime->getTimestamp();
//            }
//        }

        return $returnData;
    }

    private function getDepositByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime
    ): array {
        $returnData = [];
        $data = $this->getDepositHistoryList($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        $dataDeposit = $data->getResult();

        $returnData = $this->calculateMoneyDeposit($returnData, $data);

        if (!empty($dataDeposit)) {
            $firstData = $dataDeposit[count($dataDeposit) - 1];

            $returnDateTime = new DateTime($firstData->getTime());
            $finishTime = $returnDateTime->getTimestamp();

            while (!empty($dataDeposit)) {/** @phpstan-ignore-line */
                $data = $this->getDepositHistoryList($stockExchangeToken, $startPeriodDateTime, $finishTime);
                $dataDeposit = $data->getResult();

                if (empty($dataDeposit)) {
                    break;
                }
                $returnData = $this->calculateMoneyDeposit($returnData, $data);

                $firstData = $dataDeposit[count($dataDeposit) - 1];

                $returnDateTime = new DateTime($firstData->getTime());
                $finishTime = $returnDateTime->getTimestamp();
            }
        }

        return $returnData;
    }

    private function getWithdrawalByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime
    ): array {
        $returnData = [];
        $data = $this->getWithdrawalHistoryList($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        $dataDeposit = $data->getResult();

        $returnData = $this->calculateMoneyWithdrawal($returnData, $data);

        if (!empty($dataDeposit)) {
            $firstData = $dataDeposit[count($dataDeposit) - 1];

            $returnDateTime = new DateTime($firstData->getTime());
            $finishTime = $returnDateTime->getTimestamp();

            while (!empty($dataDeposit)) {/** @phpstan-ignore-line */
                $data = $this->getWithdrawalHistoryList($stockExchangeToken, $startPeriodDateTime, $finishTime);
                $dataDeposit = $data->getResult();

                if (empty($dataDeposit)) {
                    break;
                }
                $returnData = $this->calculateMoneyWithdrawal($returnData, $data);

                $firstData = $dataDeposit[count($dataDeposit) - 1];

                $returnDateTime = new DateTime($firstData->getTime());
                $finishTime = $returnDateTime->getTimestamp();
            }
        }

        return $returnData;
    }

    /**
     * @param WithdrawalDTO[] $moneyWithdrawalList
     *
     * @return WithdrawalDTO[]
     */
    private function calculateMoneyWithdrawal(array $moneyWithdrawalList, MoneyWithdrawalListDTO $moneyWithdrawalListDTO): array
    {
        foreach ($moneyWithdrawalListDTO->getResult() as $withdrawalDTO) {
            $moneyWithdrawal = new WithdrawalDTO();

            if (isset($moneyWithdrawalList[$withdrawalDTO->getCoin()])) {
                $moneyWithdrawal = $moneyWithdrawalList[$withdrawalDTO->getCoin()];
            }
            $moneyWithdrawal->setCoin($withdrawalDTO->getCoin());
            $moneyWithdrawal->setSize($moneyWithdrawal->getSize() + $withdrawalDTO->getSize());

            $moneyWithdrawalList[$withdrawalDTO->getCoin()] = $moneyWithdrawal;
        }

        return $moneyWithdrawalList;
    }

    /**
     * @param BorrowDTO[] $borrowList
     *
     * @return BorrowDTO[]
     */
    private function calculateBorrow(array $borrowList, BorrowHistoryListDTO $borrowHistoryListDTO): array
    {
        foreach ($borrowHistoryListDTO->getResult() as $borrowHistoryDTO) {
            $borrow = new BorrowDTO();

            if (isset($borrowList[$borrowHistoryDTO->getCoin()])) {
                $borrow = $borrowList[$borrowHistoryDTO->getCoin()];
            }
            $borrow->setCoin($borrowHistoryDTO->getCoin());
            // $borrow->setSize($borrow->getSize() + $borrowHistoryDTO->getSize());
            $borrow->setProceeds($borrow->getProceeds() + $borrowHistoryDTO->getProceeds());

            $borrowList[$borrowHistoryDTO->getCoin()] = $borrow;
        }

        return $borrowList;
    }

    /**
     * @param LendingDTO[] $lendingList
     *
     * @return LendingDTO[]
     */
    private function calculateLending(array $lendingList, LendingHistoryListDTO $lendingHistoryListDTO): array
    {
        foreach ($lendingHistoryListDTO->getResult() as $lendingHistoryDTO) {
            $lending = new LendingDTO();

            if (isset($lendingList[$lendingHistoryDTO->getCoin()])) {
                $lending = $lendingList[$lendingHistoryDTO->getCoin()];
            }
            $lending->setCoin($lendingHistoryDTO->getCoin());
            //  $lending->setSize($lending->getSize() + $lendingHistoryDTO->getSize());
            $lending->setProceeds($lending->getProceeds() + $lendingHistoryDTO->getProceeds());

            $lendingList[$lendingHistoryDTO->getCoin()] = $lending;
        }

        return $lendingList;
    }

    /**
     * @param DepositDTO[] $moneyDepositList
     *
     * @return DepositDTO[]
     */
    private function calculateMoneyDeposit(array $moneyDepositList, MoneyDepositListDTO $moneyDepositListDTO): array
    {
        foreach ($moneyDepositListDTO->getResult() as $depositDTO) {
            $moneyDeposit = new DepositDTO();

            if (isset($moneyDepositList[$depositDTO->getCoin()])) {
                $moneyDeposit = $moneyDepositList[$depositDTO->getCoin()];
            }
            $moneyDeposit->setCoin($depositDTO->getCoin());
            $moneyDeposit->setSize($moneyDeposit->getSize() + $depositDTO->getSize());

            $moneyDepositList[$depositDTO->getCoin()] = $moneyDeposit;
        }

        return $moneyDepositList;
    }

    private function getPositionsByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime,
        PortfolioDTO $portfolioDTO
    ): PortfolioDTO {
        $data = $this->getFillsList($stockExchangeToken, $startPeriodDateTime, $endPeriodDateTime);
        $dataFills = $data->getResult();

        $portfolioDTO = $this->calculatePositionsByFillsList($portfolioDTO, $data);

        if (!empty($dataFills)) {
            $firstData = $dataFills[count($dataFills) - 1];

            $returnDateTime = new DateTime($firstData->getTime());
            $finishTime = $returnDateTime->getTimestamp() - 1;

            while (!empty($dataFills)) {/** @phpstan-ignore-line */
                $data = $this->getFillsList($stockExchangeToken, $startPeriodDateTime, $finishTime);

                $dataFills = $data->getResult();

                if (empty($dataFills)) {
                    break;
                }

                $portfolioDTO = $this->calculatePositionsByFillsList($portfolioDTO, $data);

                $firstData = $dataFills[count($dataFills) - 1];

                $returnDateTime = new DateTime($firstData->getTime());
                $finishTime = $returnDateTime->getTimestamp() - 1;
            }
        }

        return $portfolioDTO;
    }

    private function calculatePositionsByFillsList(PortfolioDTO $portfolioDTO, FillsListDTO $fillsList): PortfolioDTO
    {
        foreach ($fillsList->getResult() as $fillsDTO) {
            /** @var string $market */
            $market = $fillsDTO->getMarket();

            if (null !== $fillsDTO->getBaseCurrency()) {
                /** @var string $market */
                $market = $fillsDTO->getBaseCurrency();
            }

            $currentMarketData = $this->portfolioService->getPosition($portfolioDTO, $market);

            // or buy
            if ('sell' === $fillsDTO->getSide()) {
                $currentMarketData->setSize(
                    (float) bcsub((string) $currentMarketData->getSize(), (string) $fillsDTO->getSize(), self::SCALE)
                );

                if ('otc' === $fillsDTO->getType()) {
                    /** @var string $marketAnother */
                    $marketAnother = $fillsDTO->getQuoteCurrency();
                } else {
                    $marketAnother = $this->getPortfolioWalletCoin();
                }

                $anotherMarketData = $this->portfolioService->getPosition($portfolioDTO, $marketAnother);

                $anotherMarketData->setSize(
                    $this->portfolioService->addToSize($anotherMarketData->getSize(), $fillsDTO->getPrice(), $fillsDTO->getSize())
                );

                $portfolioDTO->positions[$marketAnother] = $anotherMarketData;
            } else {
                $currentMarketData->setSize(
                    (float) bcadd((string) $currentMarketData->getSize(), (string) $fillsDTO->getSize(), self::SCALE)
                );

                $marketAnother = $this->getPortfolioWalletCoin();

                $anotherMarketData = $this->portfolioService->getPosition($portfolioDTO, $marketAnother);

                $anotherMarketData->setSize(
                    $this->portfolioService->reduceSize($anotherMarketData->getSize(), $fillsDTO->getPrice(), $fillsDTO->getSize())
                );

                $portfolioDTO->positions[$marketAnother] = $anotherMarketData;
            }

            $fill = new FillsDTO();

            if (isset($portfolioDTO->fills[$fillsDTO->getFeeCurrency()])) {
                $fill = $portfolioDTO->fills[$fillsDTO->getFeeCurrency()];
            }
            $fill->setFeeCurrency($fillsDTO->getFeeCurrency());
            $fill->setFee(
                (float) bcadd(
                    number_format($fill->getFee(), self::SCALE, '.', ''),
                    number_format($fillsDTO->getFee(), self::SCALE, '.', ''),
                    self::SCALE
                )
            );

            if (0.0 === $fill->getFee()) {
                unset($portfolioDTO->fills[$fill->getFeeCurrency()]);
            } else {
                $portfolioDTO->fills[$fill->getFeeCurrency()] = $fill;
            }

            if (0.0 === $currentMarketData->getSize()) {
                unset($portfolioDTO->positions[$market]);
            } else {
                $portfolioDTO->positions[$market] = $currentMarketData;
            }
        }

        return $portfolioDTO;
    }
}
