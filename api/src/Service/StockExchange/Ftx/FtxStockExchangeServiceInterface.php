<?php

namespace App\Service\StockExchange\Ftx;

use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\StockExchange\StockExchangeStrategyServiceInterface;

interface FtxStockExchangeServiceInterface extends StockExchangeStrategyServiceInterface
{
    public function getMarketHistoricalPrice(
        StockExchangeToken $stockExchangeToken,
        string $marketCode,
        int $startTimeStamp
    ): float;
}
