<?php

namespace App\Service\StockExchange\Ftx\DTO\MarketHistoricalPrices;

class MarketHistoricalPricesListDTO
{
    public bool $success;

    /**
     * @var MarketHistoricalPricesDTO[]
     */
    public array $result;

    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return MarketHistoricalPricesDTO[]
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
