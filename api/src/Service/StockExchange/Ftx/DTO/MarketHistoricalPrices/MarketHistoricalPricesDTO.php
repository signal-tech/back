<?php

namespace App\Service\StockExchange\Ftx\DTO\MarketHistoricalPrices;

class MarketHistoricalPricesDTO
{
    public float $close;

    public float $high;

    public float $low;

    public float $open;

    public string $startTime;

    public float $volume;

    public function getClose(): float
    {
        return $this->close;
    }

    public function getHigh(): float
    {
        return $this->high;
    }

    public function getLow(): float
    {
        return $this->low;
    }

    public function getOpen(): float
    {
        return $this->open;
    }

    public function getStartTime(): string
    {
        return $this->startTime;
    }

    public function getVolume(): float
    {
        return $this->volume;
    }
}
