<?php

namespace App\Service\StockExchange\Ftx\DTO\FundingPayment;

class FundingPaymentListDTO
{
    public bool $success;

    /**
     * @var FundingPaymentDTO[]
     */
    public array $result;

    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return FundingPaymentDTO[]
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
