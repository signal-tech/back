<?php

namespace App\Service\StockExchange\Ftx\DTO\FundingPayment;

class FundingPaymentDTO
{
    public string $future;

    public int $id;

    public float $payment;

    public string $time;

    public float $rate;

    public function getFuture(): string
    {
        return $this->future;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getPayment(): float
    {
        return $this->payment;
    }

    public function getTime(): string
    {
        return $this->time;
    }

    public function getRate(): float
    {
        return $this->rate;
    }
}
