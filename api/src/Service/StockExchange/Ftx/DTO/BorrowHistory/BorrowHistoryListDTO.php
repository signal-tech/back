<?php

namespace App\Service\StockExchange\Ftx\DTO\BorrowHistory;

class BorrowHistoryListDTO
{
    public bool $success;

    /**
     * @var BorrowHistoryDTO[]
     */
    public array $result;

    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return BorrowHistoryDTO[]
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
