<?php

namespace App\Service\StockExchange\Ftx\DTO\BorrowHistory;

class BorrowHistoryDTO
{
    public string $coin;

    public float $proceeds;

    public float $rate;

    public float $size;

    public string $time;

    public function getCoin(): string
    {
        return $this->coin;
    }

    public function setCoin(string $coin): self
    {
        $this->coin = $coin;

        return $this;
    }

    public function getProceeds(): float
    {
        return $this->proceeds;
    }

    public function setProceeds(float $proceeds): self
    {
        $this->proceeds = $proceeds;

        return $this;
    }

    public function getRate(): float
    {
        return $this->rate;
    }

    public function setRate(float $rate): self
    {
        $this->rate = $rate;

        return $this;
    }

    public function getSize(): float
    {
        return $this->size;
    }

    public function setSize(float $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getTime(): string
    {
        return $this->time;
    }

    public function setTime(string $time): self
    {
        $this->time = $time;

        return $this;
    }
}
