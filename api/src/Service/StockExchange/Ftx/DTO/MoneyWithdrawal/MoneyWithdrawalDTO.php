<?php

namespace App\Service\StockExchange\Ftx\DTO\MoneyWithdrawal;

class MoneyWithdrawalDTO
{
    public string $coin;

    public string $address;

    public ?string $tag;

    public float $fee;

    public int $id;

    public float $size;

    public string $status;

    public string $time;

    public string $txid;

    public function getCoin(): string
    {
        return $this->coin;
    }

    public function getAddress(): string
    {
        return $this->address;
    }

    public function getTag(): ?string
    {
        return $this->tag;
    }

    public function getFee(): float
    {
        return $this->fee;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getSize(): float
    {
        return $this->size;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getTime(): string
    {
        return $this->time;
    }

    public function getTxid(): string
    {
        return $this->txid;
    }
}
