<?php

namespace App\Service\StockExchange\Ftx\DTO\MoneyWithdrawal;

class MoneyWithdrawalListDTO
{
    public bool $success;

    /**
     * @var MoneyWithdrawalDTO[]
     */
    public array $result;

    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return MoneyWithdrawalDTO[]
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
