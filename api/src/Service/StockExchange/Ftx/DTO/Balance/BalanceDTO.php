<?php

namespace App\Service\StockExchange\Ftx\DTO\Balance;

class BalanceDTO
{
    public string $coin;

    public float $free;

    public float $spotBorrow;

    public float $total;

    public float $usdValue;

    public float $availableWithoutBorrow;

    public function getCoin(): string
    {
        return $this->coin;
    }

    public function setCoin(string $coin): self
    {
        $this->coin = $coin;

        return $this;
    }

    public function getFree(): float
    {
        return $this->free;
    }

    public function setFree(float $free): self
    {
        $this->free = $free;

        return $this;
    }

    public function getSpotBorrow(): float
    {
        return $this->spotBorrow;
    }

    public function setSpotBorrow(float $spotBorrow): self
    {
        $this->spotBorrow = $spotBorrow;

        return $this;
    }

    public function getTotal(): float
    {
        return $this->total;
    }

    public function setTotal(float $total): self
    {
        $this->total = $total;

        return $this;
    }

    public function getUsdValue(): float
    {
        return $this->usdValue;
    }

    public function setUsdValue(float $usdValue): self
    {
        $this->usdValue = $usdValue;

        return $this;
    }

    public function getAvailableWithoutBorrow(): float
    {
        return $this->availableWithoutBorrow;
    }

    public function setAvailableWithoutBorrow(float $availableWithoutBorrow): self
    {
        $this->availableWithoutBorrow = $availableWithoutBorrow;

        return $this;
    }
}
