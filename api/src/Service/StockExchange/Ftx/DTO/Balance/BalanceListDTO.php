<?php

namespace App\Service\StockExchange\Ftx\DTO\Balance;

class BalanceListDTO
{
    public bool $success;

    /**
     * @var BalanceDTO[]
     */
    public array $result;

    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return BalanceDTO[]
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
