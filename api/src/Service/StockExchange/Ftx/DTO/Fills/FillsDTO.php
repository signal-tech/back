<?php

namespace App\Service\StockExchange\Ftx\DTO\Fills;

class FillsDTO
{
    public float $fee;

    public string $feeCurrency;

    public float $feeRate;

    public ?string $future;

    public int $id;

    public string $liquidity;

    public ?string $market;

    public ?string $baseCurrency;

    public ?string $quoteCurrency;

    public ?int $orderId;

    public ?int $tradeId;

    public float $price;

    public string $side;

    public float $size;

    public string $time;

    public string $type;

    public function getFee(): float
    {
        return $this->fee;
    }

    public function getFeeCurrency(): string
    {
        return $this->feeCurrency;
    }

    public function getFeeRate(): float
    {
        return $this->feeRate;
    }

    public function getFuture(): ?string
    {
        return $this->future;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getLiquidity(): string
    {
        return $this->liquidity;
    }

    public function getMarket(): ?string
    {
        return $this->market;
    }

    public function getBaseCurrency(): ?string
    {
        return $this->baseCurrency;
    }

    public function getQuoteCurrency(): ?string
    {
        return $this->quoteCurrency;
    }

    public function getOrderId(): ?int
    {
        return $this->orderId;
    }

    public function getTradeId(): ?int
    {
        return $this->tradeId;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getSide(): string
    {
        return $this->side;
    }

    public function getSize(): float
    {
        return $this->size;
    }

    public function getTime(): string
    {
        return $this->time;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
