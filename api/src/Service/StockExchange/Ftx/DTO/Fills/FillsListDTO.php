<?php

namespace App\Service\StockExchange\Ftx\DTO\Fills;

class FillsListDTO
{
    public bool $success;

    /**
     * @var FillsDTO[]
     */
    public array $result;

    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return FillsDTO[]
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
