<?php

namespace App\Service\StockExchange\Ftx\DTO\MoneyDeposit;

class MoneyDepositListDTO
{
    public bool $success;

    /**
     * @var MoneyDepositDTO[]
     */
    public array $result;

    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return MoneyDepositDTO[]
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
