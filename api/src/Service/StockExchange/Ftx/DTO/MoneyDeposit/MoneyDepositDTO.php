<?php

namespace App\Service\StockExchange\Ftx\DTO\MoneyDeposit;

class MoneyDepositDTO
{
    public int $id;

    public ?int $achAccountId;

    public string $coin;

    public string $time;

    public string $status;

    public float $fee;

    public float $requestedSize;

    public float $size;

    public bool $ach;

    public string $type;

    public bool $fiat;

    public string $creditedAt;

    public bool $credited;

    public bool $wasEarlyCredited;

    public bool $earlyCredited;

    public ?string $errorCode;

    public function getId(): int
    {
        return $this->id;
    }

    public function getAchAccountId(): ?int
    {
        return $this->achAccountId;
    }

    public function getCoin(): string
    {
        return $this->coin;
    }

    public function getTime(): string
    {
        return $this->time;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getFee(): float
    {
        return $this->fee;
    }

    public function getRequestedSize(): float
    {
        return $this->requestedSize;
    }

    public function getSize(): float
    {
        return $this->size;
    }

    public function isAch(): bool
    {
        return $this->ach;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function isFiat(): bool
    {
        return $this->fiat;
    }

    public function getCreditedAt(): string
    {
        return $this->creditedAt;
    }

    public function isCredited(): bool
    {
        return $this->credited;
    }

    public function isWasEarlyCredited(): bool
    {
        return $this->wasEarlyCredited;
    }

    public function isEarlyCredited(): bool
    {
        return $this->earlyCredited;
    }

    public function getErrorCode(): ?string
    {
        return $this->errorCode;
    }
}
