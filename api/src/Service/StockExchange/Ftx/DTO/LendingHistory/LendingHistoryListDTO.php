<?php

namespace App\Service\StockExchange\Ftx\DTO\LendingHistory;

class LendingHistoryListDTO
{
    public bool $success;

    /**
     * @var LendingHistoryDTO[]
     */
    public array $result;

    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return LendingHistoryDTO[]
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
