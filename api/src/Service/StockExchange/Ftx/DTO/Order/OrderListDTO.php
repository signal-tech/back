<?php

namespace App\Service\StockExchange\Ftx\DTO\Order;

class OrderListDTO
{
    public bool $success;

    /**
     * @var OrderDTO[]
     */
    public array $result;

    public bool $hasMoreData;

    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return OrderDTO[]
     */
    public function getResult(): array
    {
        return $this->result;
    }

    public function isHasMoreData(): bool
    {
        return $this->hasMoreData;
    }
}
