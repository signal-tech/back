<?php

namespace App\Service\StockExchange\Ftx\DTO\Order;

class OrderDTO
{
    public int $id;

    public ?int $clientId;

    public string $market;

    public string $type;

    public string $side;

    public ?float $price;

    public ?float $size;

    public string $status;

    public float $filledSize;

    public float $remainingSize;

    public bool $reduceOnly;

    public bool $liquidation;

    public ?float $avgFillPrice;

    public bool $postOnly;

    public bool $ioc;

    public string $createdAt;

    public ?string $future;

    public function getId(): int
    {
        return $this->id;
    }

    public function getClientId(): ?int
    {
        return $this->clientId;
    }

    public function getMarket(): string
    {
        return $this->market;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getSide(): string
    {
        return $this->side;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function getSize(): ?float
    {
        return $this->size;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function getFilledSize(): float
    {
        return $this->filledSize;
    }

    public function getRemainingSize(): float
    {
        return $this->remainingSize;
    }

    public function isReduceOnly(): bool
    {
        return $this->reduceOnly;
    }

    public function isLiquidation(): bool
    {
        return $this->liquidation;
    }

    public function getAvgFillPrice(): ?float
    {
        return $this->avgFillPrice;
    }

    public function isPostOnly(): bool
    {
        return $this->postOnly;
    }

    public function isIoc(): bool
    {
        return $this->ioc;
    }

    public function getCreatedAt(): string
    {
        return $this->createdAt;
    }

    public function getFuture(): ?string
    {
        return $this->future;
    }
}
