<?php

namespace App\Service\StockExchange\Ftx\DTO\Order;

use App\Service\StockExchange\DTO\AbstractCreateOrderDTO;

class CreateOrderDTO extends AbstractCreateOrderDTO
{
    private string $market;

    private string $side;

    private float $price;

    private string $type;

    private float $size;

    private bool $reduceOnly;

    private bool $ioc;

    private bool $postOnly;

    public function getMarket(): string
    {
        return $this->market;
    }

    public function setMarket(string $market): self
    {
        $this->market = $market;

        return $this;
    }

    public function getSide(): string
    {
        return $this->side;
    }

    public function setSide(string $side): self
    {
        $this->side = $side;

        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSize(): float
    {
        return $this->size;
    }

    public function setSize(float $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function isReduceOnly(): bool
    {
        return $this->reduceOnly;
    }

    public function setReduceOnly(bool $reduceOnly): self
    {
        $this->reduceOnly = $reduceOnly;

        return $this;
    }

    public function isIoc(): bool
    {
        return $this->ioc;
    }

    public function setIoc(bool $ioc): self
    {
        $this->ioc = $ioc;

        return $this;
    }

    public function isPostOnly(): bool
    {
        return $this->postOnly;
    }

    public function setPostOnly(bool $postOnly): self
    {
        $this->postOnly = $postOnly;

        return $this;
    }
}
