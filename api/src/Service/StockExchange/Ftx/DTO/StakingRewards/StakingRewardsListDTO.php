<?php

namespace App\Service\StockExchange\Ftx\DTO\StakingRewards;

class StakingRewardsListDTO
{
    public bool $success;

    /**
     * @var StakingRewardsDTO[]
     */
    public array $result;

    public function isSuccess(): bool
    {
        return $this->success;
    }

    /**
     * @return StakingRewardsDTO[]
     */
    public function getResult(): array
    {
        return $this->result;
    }
}
