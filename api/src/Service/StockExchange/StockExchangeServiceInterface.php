<?php

namespace App\Service\StockExchange;

use App\Feature\Order\Order;
use App\Feature\Portfolio\DTO\PortfolioDTO;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\StockExchange\DTO\AbstractCreateOrderDTO;
use App\Service\StockExchange\DTO\MarketFiltersDataDTO;

interface StockExchangeServiceInterface
{
    public function getFirstMoneyDepositDateTime(StockExchangeToken $stockExchangeToken): ?int;

    public function getFirstOrderDateTime(StockExchangeToken $stockExchangeToken): ?int;

    public function getPortfolioByPeriod(
        StockExchangeToken $stockExchangeToken,
        int $startPeriodDateTime,
        int $endPeriodDateTime,
        ?PortfolioDTO $previousPortfolioDTO
    ): PortfolioDTO;

    public function isTokenValid(StockExchangeToken $stockExchangeToken): bool;

    public function getPortfolioWalletCoin(StockExchangeToken $stockExchangeToken): string;

    public function getUsdCoin(StockExchangeToken $stockExchangeToken): string;

    public function getMarketHistoricalPrice(
        StockExchangeToken $stockExchangeToken,
        string $marketCode,
        int $startTimeStamp
    ): float;

    public function getMarketActualPrice(
        StockExchangeToken $stockExchangeToken,
        string $marketCode
    ): float;

    public function getMarketFiltersData(
        StockExchangeToken $stockExchangeToken,
        string $marketCode
    ): MarketFiltersDataDTO;

    public function createOrder(AbstractCreateOrderDTO $createOrderDTO, StockExchangeToken $stockExchangeToken): void;

    public function getCreateOrderDTO(StockExchangeToken $stockExchangeToken, Order $order): AbstractCreateOrderDTO;
}
