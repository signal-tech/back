<?php

namespace App\Service\ObjectPatcher;

use Symfony\Component\Validator\Constraint;

interface ObjectPatcherInterface
{
    public function patch(object $object, array $patchData, array $context = [Constraint::DEFAULT_GROUP]): void;
}
