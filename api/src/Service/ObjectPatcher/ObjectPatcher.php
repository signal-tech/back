<?php

namespace App\Service\ObjectPatcher;

use Doctrine\Common\Annotations\Reader;
use ReflectionException;
use ReflectionProperty;
use Symfony\Component\PropertyAccess\PropertyAccessorInterface;
use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Normalizer\DenormalizerInterface;
use Symfony\Component\Validator\Constraint;
use function array_intersect;
use function array_key_exists;
use function array_keys;
use function count;
use function get_class;
use function is_array;
use function is_object;

class ObjectPatcher implements ObjectPatcherInterface
{
    protected PropertyAccessorInterface $accessor;

    protected DenormalizerInterface $denormalizer;

    private Reader $reader;

    public function __construct(DenormalizerInterface $denormalizer, PropertyAccessorInterface $accessor, Reader $reader)
    {
        $this->denormalizer = $denormalizer;
        $this->accessor = $accessor;
        $this->reader = $reader;
    }

    /**
     * @throws ExceptionInterface
     */
    public function patch(object $object, array $patchData, array $context = [Constraint::DEFAULT_GROUP]): void
    {
        $this->patchRecursive($object, $patchData, $context);
    }

    /**
     * @throws ExceptionInterface
     * @throws ReflectionException
     *
     * @psalm-suppress PossiblyInvalidArgument
     */
    private function patchRecursive(object $object, array $patchData, array $context): void
    {
        $patchDataObject = $this->denormalizer->denormalize($patchData, get_class($object), null, $context);

        foreach (array_keys($patchData) as $property) {
            if (!$this->accessor->isReadable($patchDataObject, $property)) {
                continue;
            }
            /**
             * @psalm-suppress RedundantCast
             */
            $className = (string) get_class($object);

            $groups = $this->reader
                ->getPropertyAnnotation(new ReflectionProperty($className, $property), Groups::class);

            if (!$groups instanceof Groups) {
                continue;
            }

            if (0 === count(array_intersect($context, $groups->getGroups()))) {
                continue;
            }

            $value = $this->accessor->getValue($patchDataObject, $property);

            if (is_object($value)) {
                /** @var object|null $nextObject */
                $nextObject = $this->accessor->getValue($object, $property);

                if (null !== $nextObject) {
                    /** @var array $patchDataArray */
                    $patchDataArray = $patchData[$property];

                    $this->patchRecursive($nextObject, $patchDataArray, $context);

                    continue;
                }

                if ($this->accessor->isWritable($object, $property)) {
                    $this->accessor->setValue($object, $property, $value);
                }

                continue;
            }

            if (is_array($value) && array_key_exists(0, $value)) {
                continue;
            }

            $this->accessor->setValue($object, $property, $value);
        }
    }
}
