<?php

namespace App\Service\Validation;

use Symfony\Component\Validator\ConstraintViolationListInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class ValidationService implements ObjectValidatorInterface
{
    private ValidatorInterface $validator;

    public function __construct(
        ValidatorInterface $validator
    ) {
        $this->validator = $validator;
    }

    /**
     * @throws ValidationException
     */
    public function validateObject(object $object, ?array $groups = null, bool $throwException = true): ConstraintViolationListInterface
    {
        $violations = $this->validator->validate($object, null, $groups);

        if (true === $throwException && 0 !== $violations->count()) {
            throw ValidationException::create($violations, $object);
        }

        return $violations;
    }
}
