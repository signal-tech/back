<?php

namespace App\Service\Validation;

use Symfony\Component\Validator\ConstraintViolationListInterface;

interface ObjectValidatorInterface
{
    /**
     * @param string[]|null $groups
     */
    public function validateObject(object $object, ?array $groups = null, bool $throwException = true): ConstraintViolationListInterface;
}
