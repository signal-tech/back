<?php

namespace App\Service\Validation\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 * Class EntityExist.
 *
 * @Annotation
 */
class EntityExist extends Constraint
{
    public string $message = 'Данная сущность не найдена';

    public ?string $targetEntity = null;
}
