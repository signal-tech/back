<?php

namespace App\Service\Validation\Constraints;

use App\Feature\User\User;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;

class UserRolesCheckValidator extends ConstraintValidator
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $value
     *
     * @throws EntityNotFoundException
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof UserRolesCheck) {
            throw new UnexpectedTypeException($constraint, UserRolesCheck::class);
        }
        $portfolio = $this->em->getRepository(User::class);
        /** @var User|null $user */
        $user = $portfolio->find($value);

        if (null === $user) {
            throw new EntityNotFoundException('Данного пользователя не существует');
        }
    }
}
