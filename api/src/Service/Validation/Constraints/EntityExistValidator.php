<?php

namespace App\Service\Validation\Constraints;

use App\Domain\Common\Sluggable;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;
use Symfony\Component\Validator\Exception\UnexpectedTypeException;
use function class_implements;
use function in_array;
use function is_numeric;

class EntityExistValidator extends ConstraintValidator
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param mixed $value
     *
     * @psalm-suppress RepositoryStringShortcut
     */
    public function validate($value, Constraint $constraint): void
    {
        if (!$constraint instanceof EntityExist) {
            throw new UnexpectedTypeException($constraint, EntityExist::class);
        }

        if (null === $value) {
            return;
        }

        /** @var class-string|null $targetEntity */
        $targetEntity = $constraint->targetEntity;

        if (null === $targetEntity) {
            throw new EntityNotFoundException('Не передан тип сущности');
        }

        $repository = $this->em->getRepository($targetEntity);
        $interfaces = class_implements($targetEntity);

        $entityBySlug = null;
        $entityById = null;

        if ($interfaces && !is_numeric($value) && in_array(Sluggable::class, $interfaces, true)) {
            $entityBySlug = $repository->findOneBy(['slug' => (string) $value]);
        }

        if (null !== $entityBySlug) {
            return;
        }

        if (is_numeric($value)) {
            $entityById = $repository->find((int) $value);
        }

        if (null === $entityById) {
            throw new EntityNotFoundException($constraint->message);
        }
    }
}
