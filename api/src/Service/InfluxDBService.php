<?php

namespace App\Service;

use InfluxDB\Database;

class InfluxDBService
{
    /**
     * @var Database
     */
    private $influxdb;

    public function __construct(
        Database $influxdb
    ) {
        $this->influxdb = $influxdb;
    }

    public function getDataBase(): Database
    {
        return $this->influxdb;
    }
}
