<?php

namespace App\Service\Time;

use App\Service\Time\DTO\StartAndEndDay;
use App\Service\Time\DTO\StartAndEndMonth;
use App\Service\Time\DTO\StartAndEndYear;
use DateTime;
use DateTimeZone;
use function strtotime;

class TimeService implements TimeServiceInterface
{
    public function getStartAndEndDayByTime(int $time): StartAndEndDay
    {
        $day = new DateTime('now', new DateTimeZone('UTC'));
        $day->setTimestamp($time);
        $day->setTime(0, 0);

        $dto = new StartAndEndDay();

        $dto->setStartTime($day->getTimestamp());
        $dto->setEndTime($day->getTimestamp() + self::DAY_SECONDS);

        return $dto;
    }

    public function getStartAndEndMonthByTime(int $time): StartAndEndMonth
    {
        $day = new DateTime('now', new DateTimeZone('UTC'));
        $day->setTimestamp($time);

        $month_start = strtotime('first day of this month', $day->getTimestamp());
        $month_end = strtotime('last day of this month', $day->getTimestamp());

        $dto = new StartAndEndMonth();

        $dto->setStartTime($month_start);
        $dto->setEndTime($month_end + self::DAY_SECONDS);

        return $dto;
    }

    public function getStartAndEndYearByTime(int $time): StartAndEndYear
    {
        $day = new DateTime('now', new DateTimeZone('UTC'));
        $day->setTimestamp($time);

        $year_start = strtotime('first day of January', $day->getTimestamp());
        $year_end = strtotime('last day of December', $day->getTimestamp());
        $dto = new StartAndEndYear();

        $dto->setStartTime($year_start + 60 * 60 * 3); //Почему то возвращает не UTC0
        $dto->setEndTime($year_end + self::DAY_SECONDS + 60 * 60 * 3);

        return $dto;
    }

    public function getStartAndEndYearByYearNumber(int $year): StartAndEndYear
    {
        $day = new DateTime('now', new DateTimeZone('UTC'));
        $day->setDate($year, 1, 1);

        $year_start = strtotime('first day of January', $day->getTimestamp());
        $year_end = strtotime('last day of December', $day->getTimestamp());
        $dto = new StartAndEndYear();

        $dto->setStartTime($year_start + 60 * 60 * 3); //Почему то возвращает не UTC0
        $dto->setEndTime($year_end + self::DAY_SECONDS + 60 * 60 * 3);

        return $dto;
    }

    public function getStartAndEndMonthByYearAndMonthNumber(int $year, int $month): StartAndEndMonth
    {
        $day = new DateTime($year . '-' . $month . '-01', new DateTimeZone('UTC'));

        $month_start = strtotime('first day of this month', $day->getTimestamp());
        $month_end = strtotime('last day of this month', $day->getTimestamp());
        $dto = new StartAndEndMonth();

        $dto->setStartTime($month_start);
        $dto->setEndTime($month_end + self::DAY_SECONDS);

        return $dto;
    }

    public function getStartAndEndMonthByYearAndMonthAndDayNumber(int $year, int $month, int $day): StartAndEndDay
    {
        $day = new DateTime($year . '-' . $month . '-' . $day, new DateTimeZone('UTC'));
        $day->setTime(0, 0);

        $dto = new StartAndEndDay();

        $dto->setStartTime($day->getTimestamp());
        $dto->setEndTime($day->getTimestamp() + self::DAY_SECONDS);

        return $dto;
    }
}
