<?php

namespace App\Service\Time;

use App\Service\Time\DTO\StartAndEndDay;
use App\Service\Time\DTO\StartAndEndMonth;
use App\Service\Time\DTO\StartAndEndYear;

interface TimeServiceInterface
{
    public const DAY_SECONDS = 60 * 60 * 24;

    public const MONTHS = [
        'Январь',
        'Февраль',
        'Март',
        'Апрель',
        'Май',
        'Июнь',
        'Июль',
        'Август',
        'Сентябрь',
        'Октябрь',
        'Ноябрь',
        'Декабрь',
    ];

    public function getStartAndEndDayByTime(int $time): StartAndEndDay;

    public function getStartAndEndMonthByTime(int $time): StartAndEndMonth;

    public function getStartAndEndYearByTime(int $time): StartAndEndYear;

    public function getStartAndEndYearByYearNumber(int $year): StartAndEndYear;

    public function getStartAndEndMonthByYearAndMonthNumber(int $year, int $month): StartAndEndMonth;

    public function getStartAndEndMonthByYearAndMonthAndDayNumber(int $year, int $month, int $day): StartAndEndDay;
}
