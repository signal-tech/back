<?php

namespace App\Service\Notification;

use Exception;
use Psr\Log\LoggerInterface;
use Twilio\Rest\Client;

class SmsService implements SmsServiceInterface
{
    private LoggerInterface $logger;

    private string $accountSid;

    private string $authToken;

    private string $twilioNumber;

    public function __construct(
        LoggerInterface $logger,
        string $accountSid,
        string $authToken,
        string $twilioNumber,
    ) {
        $this->logger = $logger;
        $this->accountSid = $accountSid;
        $this->authToken = $authToken;
        $this->twilioNumber = $twilioNumber;
    }

    public function send(string $phone, string $code): void
    {
        try {
//            $account_sid = '';
//            $auth_token = '';
//            $twilio_number = '';

            $client = new Client($this->accountSid, $this->authToken);
            $client->messages->create(
            // Where to send a text message (your cell phone?)
                $phone,
                [
                    'from' => $this->twilioNumber,
                    'body' => 'Security code for Market bite: ' . $code,
                ]
            );
        } catch (Exception $e) {
            $this->logger->critical($e->getMessage());
        }
    }
}
