<?php

namespace App\Service\Notification;

interface SmsServiceInterface
{
    public function send(string $phone, string $code): void;
}
