<?php

namespace App\Service\SiteMap\DTO;

class SiteMapItemDTO
{
    private string $loc;

    private ?string $lastmod;

    private ?string $changefreq;

    private ?float $priority;

    public function __construct(
        string $loc,
        ?string $lastmod = null,
        ?string $changefreq = null,
        ?float $priority = null
    ) {
        $this->loc = $loc;
        $this->lastmod = $lastmod;
        $this->changefreq = $changefreq;
        $this->priority = $priority;
    }

    public function getLoc(): string
    {
        return $this->loc;
    }

    public function getLastmod(): ?string
    {
        return $this->lastmod;
    }

    public function getChangefreq(): ?string
    {
        return $this->changefreq;
    }

    public function getPriority(): ?float
    {
        return $this->priority;
    }
}
