<?php

namespace App\Service\SiteMap;

use App\DBAL\Types\ShopStatusType;
use App\Feature\Category\CategoryEntity;
use App\Feature\Discount\DiscountEntity;
use App\Feature\Product\ProductEntity;
use App\Feature\Shop\ShopEntity;
use App\Service\Url\UrlServiceInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Presta\SitemapBundle\Event\SitemapPopulateEvent;
use Presta\SitemapBundle\Service\UrlContainerInterface;
use Presta\SitemapBundle\Sitemap\Url\UrlConcrete;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use function count;

class SitemapListener implements EventSubscriberInterface
{
//    /**
//     * @var UrlGeneratorInterface
//     */
//    private $urlGenerator;
//
//    /**
//     * @var EntityManagerInterface
//     */
//    private $em;
//
//    /**
//     * @var UrlServiceInterface
//     */
//    private $urlService;
//
//    public function __construct(
//        UrlGeneratorInterface $urlGenerator,
//        EntityManagerInterface $em,
//        UrlServiceInterface $urlService
//    ) {
//        $this->urlGenerator = $urlGenerator;
//        $this->em = $em;
//        $this->urlService = $urlService;
//    }

    public function populate(SitemapPopulateEvent $event): void
    {
        $this->registerBlogPostsUrls($event->getUrlContainer());
    }

    public function registerBlogPostsUrls(UrlContainerInterface $urlsContainer): void
    {
//        $count = 0;
//
//        while (true) {
//            /** @var ShopEntity[] $shops */
//            $shops = $this->em->getRepository(ShopEntity::class)->findBy(
//                ['status' => ShopStatusType::ADMITAD_APPROVED],
//                ['id' => 'ASC'],
//                50,
//                $count
//            );
//
//            foreach ($shops as $shop) {
//                $urlsContainer->addUrl(
//                    new UrlConcrete(
//                        $this->getPlatformUrl('/shop' . '/' . $shop->getSlug()),
//                        $shop->getUpdatedAt(),
//                        'daily',
//                        0.8
//                    ),
//                    'shop'
//                );
//            }
//
//            if (count($shops) < 50) {
//                break;
//            }
//
//            $count = $count + 50;
//        }
//
//        $count = 0;
//
//        while (true) {
//            /** @var DiscountEntity[] $discounts */
//            $discounts = $this->em->getRepository(DiscountEntity::class)->findBy(
//                [],
//                ['id' => 'ASC'],
//                50,
//                $count
//            );
//
//            foreach ($discounts as $discount) {
//                $urlsContainer->addUrl(
//                    new UrlConcrete(
//                        $this->getPlatformUrl('/discount' . '/' . $discount->getSlug()),
//                        $discount->getUpdatedAt(),
//                        'daily',
//                        0.8
//                    ),
//                    'discount'
//                );
//            }
//
//            if (count($discounts) < 50) {
//                break;
//            }
//
//            $count = $count + 50;
//        }
//
//        /** @var CategoryEntity[] $categories */
//        $categories = $this->em->getRepository(CategoryEntity::class)->findAll();
//
//        foreach ($categories as $category) {
//            if (null !== $category->getRemoteDiscountId()) {
//                $urlsContainer->addUrl(
//                    new UrlConcrete(
//                        $this->getPlatformUrl('/discounts' . '/' . $category->getSlug()),
//                        (new DateTime())->setTime(4, 30),
//                        'always',
//                        1
//                    ),
//                    'discounts'
//                );
//
//                $urlsContainer->addUrl(
//                    new UrlConcrete(
//                        $this->getPlatformUrl('/shops' . '/' . $category->getSlug()),
//                        (new DateTime())->setTime(4, 30),
//                        'always',
//                        1
//                    ),
//                    'shops'
//                );
//            }
//
//            if (!empty($category->getRemoteProductIds())) {
//                $urlsContainer->addUrl(
//                    new UrlConcrete(
//                        $this->getPlatformUrl('/products' . '/' . $category->getSlug()),
//                        (new DateTime())->setTime(4, 30),
//                        'always',
//                        1
//                    ),
//                    'products'
//                );
//            }
//        }
//
//        $count = 0;
//
//        while (true) {
//            /** @var ProductEntity[] $products */
//            $products = $this->em->getRepository(ProductEntity::class)->findBy(
//                [],
//                ['id' => 'ASC'],
//                50,
//                $count
//            );
//
//            foreach ($products as $product) {
//                $urlsContainer->addUrl(
//                    new UrlConcrete(
//                        $this->getPlatformUrl('/product' . '/' . $product->getSlug()),
//                        $product->getUpdatedAt(),
//                        'daily',
//                        0.8
//                    ),
//                    'product'
//                );
//            }
//
//            if (count($products) < 50) {
//                break;
//            }
//
//            $count = $count + 50;
//        }
    }

//    private function getPlatformUrl(string $string): string
//    {
//        return 'https://couponhub.ru' . $string; //ПОХУЙ
//    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            SitemapPopulateEvent::ON_SITEMAP_POPULATE => 'populate',
        ];
    }
}
