<?php

namespace App\Feature\User\Command;

use App\Feature\User\DTO\Request\UpdateProfileRequest;

/**
 * @see UpdateProfileCommandHandler
 */
class UpdateProfileCommand
{
    private UpdateProfileRequest $updateProfileRequest;

    public function __construct(UpdateProfileRequest $updateProfileRequest)
    {
        $this->updateProfileRequest = $updateProfileRequest;
    }

    public function getUpdateProfileRequest(): UpdateProfileRequest
    {
        return $this->updateProfileRequest;
    }
}
