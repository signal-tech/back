<?php

namespace App\Feature\User\Command;

use App\Feature\User\DTO\Request\UserRequest;

/**
 * @see SendAuthCodeCommandHandler
 */
class SendAuthCodeCommand
{
    private UserRequest $userRequest;

    public function __construct(UserRequest $userRequest)
    {
        $this->userRequest = $userRequest;
    }

    public function getUserRequest(): UserRequest
    {
        return $this->userRequest;
    }
}
