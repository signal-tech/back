<?php

namespace App\Feature\User\Command;

use App\Feature\File\FileEntity;
use App\Feature\User\DTO\ProfileDTO;
use App\Infrastructure\OAuth\Service\TokenService;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\Validation\ValidationException;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @see UploadUserAvatarCommandCommand
 */
class UploadUserAvatarCommandHandler extends AbstractAuthorizedAccessible
{
    private EntityManagerInterface $em;

    private AutoMapperInterface $mapper;

    private TokenService $tokenService;

    public function __construct(
        EntityManagerInterface $em,
        AutoMapperInterface $mapper,
        TokenService $tokenService
    ) {
        $this->tokenService = $tokenService;
        $this->em = $em;
        $this->mapper = $mapper;
    }

    /**
     * @throws UnregisteredMappingException
     * @throws ValidationException
     */
    public function __invoke(UploadUserAvatarCommand $command): ProfileDTO
    {
        $user = $this->tokenService->getUserByToken();

        $avatar = $user->getAvatar();

        if (!$avatar) {
            $avatar = FileEntity::create($command->getAvatar());
        }

        /** @var UploadedFile $commandFile */
        $commandFile = $command->getAvatar();

        $avatar->setRealName($commandFile->getClientOriginalName());
        $avatar->setFile($commandFile);
        $user->setAvatar($avatar);

        $this->em->persist($user);
        $this->em->flush();

        $this->em->refresh($user);

        /** @var ProfileDTO $profileDTO */
        $profileDTO = $this->mapper->map($user, ProfileDTO::class);

        return $profileDTO;
    }
}
