<?php

namespace App\Feature\User\Command;

use Symfony\Component\HttpFoundation\File\File;

/**
 * @see UploadUserAvatarCommandHandler
 */
class UploadUserAvatarCommand
{
    private File $avatar;

    public function __construct(File $avatar)
    {
        $this->avatar = $avatar;
    }

    public function getAvatar(): File
    {
        return $this->avatar;
    }
}
