<?php

namespace App\Feature\User\Command;

use App\Feature\User\DTO\Request\UserRequest;
use App\Feature\User\Message\AuthMessage;
use App\Feature\User\User;
use App\Service\Validation\ValidationException;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use function random_int;

/**
 * @see SendAuthCodeCommand
 */
class SendAuthCodeCommandHandler
{
    private EntityManagerInterface $em;

    private ValidationService $validator;

    private MessageBusInterface $messageBus;

    public function __construct(
        EntityManagerInterface $em,
        ValidationService $validator,
        MessageBusInterface $messageBus
    ) {
        $this->messageBus = $messageBus;
        $this->em = $em;
        $this->validator = $validator;
    }

    /**
     * @throws UnregisteredMappingException
     * @throws ValidationException
     */
    public function __invoke(SendAuthCodeCommand $command): void
    {
        $userRequest = $command->getUserRequest();
        $this->validator->validateObject($userRequest, [UserRequest::GET_CODE]);
        $userRepository = $this->em->getRepository(User::class);

        $phone = '+' . $userRequest->phone;

        /** @var User|null $user */
        $user = $userRepository->findOneBy(['phone' => $phone]);

        if (!$user) {
            $user = new User();
            $user->setPhone($phone);
        }

        $userConfirmationCode = random_int(1000, 9999);

        if ('+79999999999' === $phone) {
            $userConfirmationCode = 1111;
        }

        // $userConfirmationCode = $user->getVerificationCode();
        // if (!$userConfirmationCode) {

        // }
        $user->setVerificationCode((string) $userConfirmationCode);
        $this->em->persist($user);
        $this->em->flush();

        $this->em->refresh($user);

        if ('+79999999999' !== $phone) {
            $this->messageBus->dispatch(new AuthMessage($user->getPhone(), (string) $userConfirmationCode));
        }
    }
}
