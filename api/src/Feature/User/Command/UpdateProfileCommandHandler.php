<?php

namespace App\Feature\User\Command;

use App\Feature\User\User;
use App\Infrastructure\OAuth\Service\TokenService;
use App\Service\Validation\ValidationException;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @see UpdateProfileCommand
 */
class UpdateProfileCommandHandler
{
    private EntityManagerInterface $em;

    private ValidationService $validator;

    private TokenService $tokenService;

    private AutoMapperInterface $mapper;

    public function __construct(
        EntityManagerInterface $em,
        ValidationService $validator,
        AutoMapperInterface $mapper,
        TokenService $tokenService
    ) {
        $this->em = $em;
        $this->validator = $validator;
        $this->tokenService = $tokenService;
        $this->mapper = $mapper;
    }

    /**
     * @throws UnregisteredMappingException
     * @throws ValidationException
     */
    public function __invoke(UpdateProfileCommand $command): void
    {
        $updateRequest = $command->getUpdateProfileRequest();
        $this->validator->validateObject($updateRequest);

        $user = $this->tokenService->getUserByToken();

        /** @var User $user */
        $user = $this->mapper->mapToObject($updateRequest, $user);

        $user->setFilled(true);

        $this->em->persist($user);
        $this->em->flush();

        $this->em->refresh($user);
    }
}
