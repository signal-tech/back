<?php

namespace App\Feature\User;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class UserRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, User::class);
    }

    public function getUserByPhone(string $phone): ?User
    {
        /** @var User $user */
        $user = $this->findOneBy(['phone' => $phone]);

        return $user;
    }
}
