<?php

namespace App\Feature\User\DTO;

class ProfileDTO
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $surname;

    /**
     * @var string|null
     */
    public $description;

    public ?string $avatarPath;

    public bool $isPro;

    public bool $subscribed;

    public int $subscriptionsCount;

    public int $subscribersCount;

    public bool $isMe;

    public bool $filled;
}
