<?php

namespace App\Feature\User\DTO;

class RecommendedUsersListItemDTO
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string|null
     */
    public $name;

    /**
     * @var string|null
     */
    public $surname;

    public ?string $avatarPath;

    public bool $isPro;

    public bool $subscribed;

    public int $subscriptionsCount;

    public int $subscribersCount;

    /**
     * @var string|null
     */
    public $description;

    public bool $isMe;
}
