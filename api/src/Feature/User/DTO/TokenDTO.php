<?php

namespace App\Feature\User\DTO;

class TokenDTO
{
    public string $access_token;

    public int $expires_in;

    public string $refresh_token;

    /**
     * @var string
     */
    public $scope;

    public string $token_type;
}
