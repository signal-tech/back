<?php

namespace App\Feature\User\DTO\Request;

use Symfony\Component\Serializer\Annotation\Groups;
use Symfony\Component\Validator\Constraints as Assert;

class UserRequest
{
    public const GET_CODE = 'getCode';

    public const AUTH = 'auth';

    /**
     * Номер телефона.
     *
     * @var string
     *
     * @Assert\NotBlank(groups={UserRequest::GET_CODE, UserRequest::AUTH})
     *
     * @Groups({UserRequest::GET_CODE, UserRequest::AUTH})
     */
    public $phone;

    /**
     * Высланный проверочный код.
     *
     * @var string
     *
     * @Assert\NotBlank(groups={UserRequest::AUTH})
     *
     * @Groups({UserRequest::AUTH})
     */
    public $code;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={UserRequest::AUTH})
     *
     * @Groups({UserRequest::AUTH})
     */
    public $client_id;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={UserRequest::AUTH})
     *
     * @Groups({UserRequest::AUTH})
     */
    public $client_secret;

    /**
     * @var string
     *
     * @Assert\NotBlank(groups={UserRequest::AUTH})
     *
     * @Groups({UserRequest::AUTH})
     */
    public $grant_type;
}
