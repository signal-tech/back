<?php

namespace App\Feature\User\DTO\Request;

use Symfony\Component\Validator\Constraints as Assert;

class UpdateProfileRequest
{
    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    public $name;

    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    public $surname;

    /**
     * @var string
     */
    public $description;

    /**
     * @Assert\Email(
     *     message="Данный емеил '{{ value }}' некорректный."
     * )
     *
     * @var string
     */
    public $email;
}
