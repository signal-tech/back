<?php

namespace App\Feature\User\Query;

use App\Feature\User\DTO\ProfileDTO;
use App\Feature\User\User;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @see GetProfileQuery
 */
class GetProfileQueryHandler
{
    private EntityManagerInterface $entityManager;

    private AutoMapperInterface $mapper;

    private ValidationService $validator;

    public function __construct(
        EntityManagerInterface $entityManager,
        AutoMapperInterface $mapper,
        ValidationService $validator
    ) {
        $this->validator = $validator;
        $this->entityManager = $entityManager;
        $this->mapper = $mapper;
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     */
    public function __invoke(GetProfileQuery $query): ProfileDTO
    {
        $this->validator->validateObject($query);

        /** @var User $user */
        $user = $this->entityManager->find(User::class, $query->getUserId());

        /** @var ProfileDTO $profileDTO */
        $profileDTO = $this->mapper->map($user, ProfileDTO::class);

        return $profileDTO;
    }
}
