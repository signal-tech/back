<?php

namespace App\Feature\User\Query;

use App\Feature\User\DTO\Request\GetRecommendedUsersListRequestFilter;

/**
 * @see GetRecommendedUsersListQueryHandler
 */
class GetRecommendedUsersListQuery
{
    private GetRecommendedUsersListRequestFilter $filter;

    public function __construct(GetRecommendedUsersListRequestFilter $filter)
    {
        $this->filter = $filter;
    }

    public function getFilter(): GetRecommendedUsersListRequestFilter
    {
        return $this->filter;
    }
}
