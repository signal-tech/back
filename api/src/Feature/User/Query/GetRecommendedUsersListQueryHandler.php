<?php

namespace App\Feature\User\Query;

use App\Feature\User\DTO\RecommendedUsersListItemDTO;
use App\Feature\User\DTO\Request\GetRecommendedUsersListRequestFilter;
use App\Feature\User\User;
use App\Service\ListPaginator\PaginateResponseInterface;
use App\Service\ListPaginator\PaginatorInterface;
use App\Service\Validation\ObjectValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

/**
 * @see GetRecommendedUsersListQuery
 */
class GetRecommendedUsersListQueryHandler
{
    private EntityManagerInterface $em;

    private ObjectValidatorInterface $validator;

    private PaginatorInterface $paginator;

    public function __construct(
        EntityManagerInterface $em,
        ObjectValidatorInterface $validator,
        PaginatorInterface $paginator
    ) {
        $this->em = $em;
        $this->validator = $validator;
        $this->paginator = $paginator;
    }

    public function __invoke(GetRecommendedUsersListQuery $query): PaginateResponseInterface
    {
        $this->validator->validateObject($query->getFilter());

        $queryBuilder = $this->createQuery($query->getFilter());
        // $this->applyQueryFilters($queryBuilder, $query->getFilter());

        return $this->paginator->createResponse(
            $queryBuilder,
            $query->getFilter()->page,
            $query->getFilter()->perPage,
            RecommendedUsersListItemDTO::class
        );
    }

    private function createQuery(GetRecommendedUsersListRequestFilter $filter): QueryBuilder
    {
        $qb = $this->em->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->andWhere('u.filled = true');

        switch ($filter->sort) {
            case 'expired':
                $qb->addOrderBy('u.createdAt', 'ASC')
                    ->addOrderBy('u.id', 'ASC');

                break;
            case 'new':
            default:
                $qb->addOrderBy('u.createdAt', 'DESC')
                    ->addOrderBy('u.id', 'DESC');

                break;
        }

        $qb->groupBy('u.id');

        return $qb;
    }

//    private function applyQueryFilters(QueryBuilder $qb, GetRecommendedUsersListRequestFilter $filter): void
//    {
//    }
}
