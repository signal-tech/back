<?php

namespace App\Feature\User\Query;

use App\Service\Validation\Constraints\UserRolesCheck;

/**
 * @see GetProfileQueryHandler
 */
class GetProfileQuery
{
    /**
     * @UserRolesCheck
     */
    private int $userId;

    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
}
