<?php

namespace App\Feature\User;

use App\Feature\File\FileEntity;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Serializable;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;
use function serialize;
use function unserialize;

/**
 * @ORM\Entity(repositoryClass=UserRepository::class)
 *
 * @ORM\Table(name="users")
 * @ORM\HasLifecycleCallbacks
 */
class User implements UserInterface, Serializable
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private ?string $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private ?string $surname;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private string $nickname;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private string $phone;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private ?string $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     * @Assert\NotBlank
     */
    private ?string $email;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $verificationCode = null;

    /**
     * @ORM\Column(type="boolean")
     */
    private bool $verified = false;

    /**
     * @ORM\Column(type="bigint", options={"default":0})
     */
    private int $subscriptionsCount = 0;

    /**
     * @ORM\Column(type="bigint", options={"default":0})
     */
    private int $subscribersCount = 0;

    /**
     * @var FileEntity
     *
     * @ORM\OneToOne(
     *     targetEntity="App\Feature\File\FileEntity",
     *     orphanRemoval=true,
     *     cascade={"persist", "remove"},
     * )
     * @ORM\JoinColumn(name="avatar_id", referencedColumnName="id", onDelete="SET NULL")
     */
    private ?FileEntity $avatar;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private bool $isPro = false;

    /**
     * @ORM\Column(type="boolean", options={"default":false})
     */
    private bool $filled = false;

    /**
     * @param mixed $name
     * @param mixed $arguments
     */
    public function __call($name, $arguments): void
    {
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getSurname(): ?string
    {
        return $this->surname;
    }

    public function setSurname(string $surname): self
    {
        $this->surname = $surname;

        return $this;
    }

    public function getPassword(): ?string
    {
        return null;
    }

    public function getVerificationCode(): ?string
    {
        return $this->verificationCode;
    }

    /**
     * @param ?string $verificationCode
     */
    public function setVerificationCode(?string $verificationCode): self
    {
        $this->verificationCode = $verificationCode;

        return $this;
    }

    public function isVerified(): bool
    {
        return $this->verified;
    }

    public function setVerified(bool $verified): self
    {
        $this->verified = $verified;

        return $this;
    }

    /**
     * String representation of object.
     *
     * @see http://php.net/manual/en/serializable.serialize.php
     *
     * @return string the string representation of the object or null
     *
     * @since 5.1.0
     */
    public function serialize()
    {
        return serialize([
            $this->id,
            $this->phone,
        ]);
    }

    /**
     * Constructs the object.
     *
     * @see http://php.net/manual/en/serializable.unserialize.php
     *
     * @param string $serialized <p>
     *                           The string representation of the object.
     *                           </p>
     *
     * @since 5.1.0
     */
    public function unserialize($serialized): void
    {
        [
            $this->id,
            $this->username,
            $this->password] = unserialize($serialized);
    }

    /**
     * Returns the roles granted to the user.
     *
     *     public function getRoles()
     *     {
     *         return ['ROLE_USER'];
     *     }
     *
     * Alternatively, the roles might be stored on a ``roles`` property,
     * and populated in any number of different ways when the user object
     * is created.
     *
     * @return string[] The user roles
     */
    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt()
    {
        return null;
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials(): void
    {
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function setPhone(string $phone): void
    {
        $this->phone = $phone;
    }

    public function getNickname(): string
    {
        return $this->nickname;
    }

    public function setNickname(string $nickname): void
    {
        $this->nickname = $nickname;
    }

    public function getUsername(): string
    {
        return '';
    }

    public function getAvatar(): ?FileEntity
    {
        return $this->avatar;
    }

    public function setAvatar(FileEntity $avatar): void
    {
        $this->avatar = $avatar;
    }

    public function getUserIdentifier(): string
    {
        return $this->phone;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): void
    {
        $this->description = $description;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(?string $email): void
    {
        $this->email = $email;
    }

    public function isPro(): bool
    {
        return $this->isPro;
    }

    public function setIsPro(bool $isPro): void
    {
        $this->isPro = $isPro;
    }

    public function getSubscriptionsCount(): int
    {
        return $this->subscriptionsCount;
    }

    public function setSubscriptionsCount(int $subscriptionsCount): self
    {
        $this->subscriptionsCount = $subscriptionsCount;

        return $this;
    }

    public function getSubscribersCount(): int
    {
        return $this->subscribersCount;
    }

    public function setSubscribersCount(int $subscribersCount): self
    {
        $this->subscribersCount = $subscribersCount;

        return $this;
    }

    public function isFilled(): bool
    {
        return $this->filled;
    }

    public function setFilled(bool $filled): self
    {
        $this->filled = $filled;

        return $this;
    }
}
