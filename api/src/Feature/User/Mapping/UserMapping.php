<?php

namespace App\Feature\User\Mapping;

use App\Feature\User\DTO\ProfileDTO;
use App\Feature\User\DTO\RecommendedUsersListItemDTO;
use App\Feature\User\DTO\Request\UpdateProfileRequest;
use App\Feature\User\DTO\UserBaseInfoDTO;
use App\Feature\User\User;
use App\Feature\UserSubscription\UserSubscription;
use App\Feature\UserSubscription\UserSubscriptionRepository;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\UrlHelper;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

class UserMapping implements AutoMapperConfiguratorInterface
{
    private UrlHelper $urlHelper;

    public function __construct(
        UrlHelper $urlHelper,
        private EntityManagerInterface $em,
        private TokenStorageInterface $tokenStorage
    ) {
        $this->urlHelper = $urlHelper;
    }

    /**
     * @throws UnregisteredMappingException
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        /**
         * @var UserSubscriptionRepository $userSubscriptionRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $userSubscriptionRepository = $this->em->getRepository(UserSubscription::class);
        $token = $this->tokenStorage->getToken();

        if (null === $token) {
            return;
        }

        /** @var User $authorizedUser */
        $authorizedUser = $token->getUser();
        $urlHelper = $this->urlHelper;

        $config->registerMapping(UpdateProfileRequest::class, User::class);

        $config->registerMapping(User::class, ProfileDTO::class)
            ->forMember('avatarPath', static function (User $user) use ($urlHelper) {
                $avatar = $user->getAvatar();

                if (!$avatar) {
                    return;
                }

                return $urlHelper->getAbsoluteUrl('/public/images/' . ((string) $avatar->getName()));
            })
            ->forMember('subscribed', static function (User $user) use ($userSubscriptionRepository, $authorizedUser) {
                return $userSubscriptionRepository->isSubscriber($user, $authorizedUser);
            })
            ->forMember('isMe', static function (User $user) use ($authorizedUser) {
                return $authorizedUser->getId() === $user->getId();
            });

        $config->registerMapping(User::class, RecommendedUsersListItemDTO::class)
            ->forMember('avatarPath', static function (User $user) use ($urlHelper) {
                $avatar = $user->getAvatar();

                if (!$avatar) {
                    return;
                }

                return $urlHelper->getAbsoluteUrl('/public/images/' . ((string) $avatar->getName()));
            })
            ->forMember('subscribed', static function (User $user) use ($userSubscriptionRepository, $authorizedUser) {
                return $userSubscriptionRepository->isSubscriber($user, $authorizedUser);
            })
            ->forMember('isMe', static function (User $user) use ($authorizedUser) {
                return $authorizedUser->getId() === $user->getId();
            });

        $config->registerMapping(User::class, UserBaseInfoDTO::class)
            ->forMember('avatarPath', static function (User $user) use ($urlHelper) {
                $avatar = $user->getAvatar();

                if (!$avatar) {
                    return;
                }

                return $urlHelper->getAbsoluteUrl('/public/images/' . ((string) $avatar->getName()));
            });
    }
}
