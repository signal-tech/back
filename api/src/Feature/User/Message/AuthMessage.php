<?php

namespace App\Feature\User\Message;

class AuthMessage
{
    private string $phone;

    private string $confirmCode;

    public function __construct(string $phone, string $confirmCode)
    {
        $this->phone = $phone;
        $this->confirmCode = $confirmCode;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }

    public function getConfirmCode(): string
    {
        return $this->confirmCode;
    }
}
