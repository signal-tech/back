<?php

namespace App\Feature\User\Handler;

use App\Feature\User\Message\AuthMessage;
use App\Service\Notification\SmsServiceInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AuthMessageHandler implements MessageHandlerInterface
{
    private SmsServiceInterface $smsService;

    public function __construct(SmsServiceInterface $smsService)
    {
        $this->smsService = $smsService;
    }

    /**
     * @throws \Symfony\Component\Mailer\Exception\TransportExceptionInterface
     */
    public function __invoke(AuthMessage $message): void
    {
        $this->smsService->send($message->getPhone(), $message->getConfirmCode());
    }
}
