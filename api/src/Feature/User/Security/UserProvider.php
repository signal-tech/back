<?php

namespace App\Feature\User\Security;

use App\Feature\User\User;
use App\Feature\User\UserRepository;
use Exception;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\Exception\UsernameNotFoundException;
use Symfony\Component\Security\Core\Exception\UserNotFoundException;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use function get_class;
use function sprintf;

class UserProvider implements UserProviderInterface
{
    private UserRepository $userRepository;

    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    /**
     * @psalm-suppress ImplementedReturnTypeMismatch
     */
    public function loadUserByIdentifier(string $identifier): User
    {
        $user = $this->userRepository->getUserByPhone($identifier);

        if (null === $user) {
            throw new UserNotFoundException(sprintf('Пользователь "%s" не найден.', $identifier));
        }

        return $user;
    }

    public function loadUserByUsername(string $username): User
    {
        $user = $this->userRepository->getUserByPhone($username);

        if (null === $user) {
            throw new UserNotFoundException(sprintf('Пользователь "%s" не найден.', $username));
        }

        return $user;
    }

    public function refreshUser(UserInterface $user): void /* @phpstan-ignore-line */
    {
        if (!$user instanceof User) {
            throw new UnsupportedUserException(sprintf('Invalid user class "%s".', get_class($user)));
        }

        // Return a User object after making sure its data is "fresh".
        // Or throw a UsernameNotFoundException if the user no longer exists.
        throw new Exception('TODO: fill in refreshUser() inside ' . __FILE__);
    }

    /**
     * Tells Symfony to use this provider for this User class.
     *
     * @param mixed $class
     */
    public function supportsClass($class)
    {
        return User::class === $class;
    }
}
