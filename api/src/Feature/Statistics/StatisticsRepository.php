<?php

namespace App\Feature\Statistics;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Statistics|null find($id, $lockMode = null, $lockVersion = null)
 * @method Statistics|null findOneBy(array $criteria, array $orderBy = null)
 * @method Statistics[]    findAll()
 * @method Statistics[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StatisticsRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Statistics::class);
    }

    /**
     * @return Statistics[]
     */
    public function getStatisticsListByPeriod(
        int $tokenId,
        int $startPeriod,
        int $endPeriod,
        string $type
    ): array {
        /** @var Statistics[] $returnData */
        $returnData = $this->createQueryBuilder('s')
            ->andWhere('s.rangeStartDateTime >= :rangeStartDateTime')
            ->andWhere('s.rangeEndDateTime <= :rangeEndDateTime')
            ->andWhere('s.stockExchangeToken = :token')
            ->andWhere('s.type = :type')
            ->setParameter('rangeStartDateTime', $startPeriod)
            ->setParameter('rangeEndDateTime', $endPeriod)
            ->setParameter('token', $tokenId)
            ->setParameter('type', $type)
            ->orderBy('s.rangeStartDateTime', 'ASC')
            ->getQuery()
            ->getResult();

        return $returnData;
    }
}
