<?php

namespace App\Feature\Statistics;

use App\Feature\StockExchangeToken\StockExchangeToken;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=StatisticsRepository::class)
 *
 * @ORM\Table(name="statistics")
 *
 * @ORM\HasLifecycleCallbacks
 */
class Statistics
{
    use TimestampableEntity;

    public const TYPE_DAY = 'DAY';

    public const TYPE_MONTH = 'Month';

    public const TYPE_YEAR = 'Year';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Feature\StockExchangeToken\StockExchangeToken")
     * @ORM\JoinColumn(name="stock_exchange_token_id", referencedColumnName="id", nullable=false)
     */
    private StockExchangeToken $stockExchangeToken;

    /**
     * @ORM\Column(type="bigint")
     */
    private int $rangeStartDateTime;

    /**
     * @ORM\Column(type="bigint")
     */
    private int $rangeEndDateTime;

    /**
     * @ORM\Column(type="float")
     */
    private float $percent;

    /**
     * @ORM\Column(type="string")
     */
    private string $type;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStockExchangeToken(): StockExchangeToken
    {
        return $this->stockExchangeToken;
    }

    public function setStockExchangeToken(StockExchangeToken $stockExchangeToken): self
    {
        $this->stockExchangeToken = $stockExchangeToken;

        return $this;
    }

    public function getRangeStartDateTime(): int
    {
        return $this->rangeStartDateTime;
    }

    public function setRangeStartDateTime(int $rangeStartDateTime): self
    {
        $this->rangeStartDateTime = $rangeStartDateTime;

        return $this;
    }

    public function getRangeEndDateTime(): int
    {
        return $this->rangeEndDateTime;
    }

    public function setRangeEndDateTime(int $rangeEndDateTime): self
    {
        $this->rangeEndDateTime = $rangeEndDateTime;

        return $this;
    }

    public function getPercent(): float
    {
        return $this->percent;
    }

    public function setPercent(float $percent): self
    {
        $this->percent = $percent;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }
}
