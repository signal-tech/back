<?php

namespace App\Feature\Statistics\Mapping;

use App\Feature\Statistics\DTO\StatisticsDayDTO;
use App\Feature\Statistics\DTO\StatisticsMonthDTO;
use App\Feature\Statistics\DTO\StatisticsYearDTO;
use App\Feature\Statistics\Statistics;
use App\Service\Time\TimeServiceInterface;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use DateTime;
use function round;

class StatisticsMapping implements AutoMapperConfiguratorInterface
{
    public function configure(AutoMapperConfigInterface $config): void
    {
        $config->registerMapping(Statistics::class, StatisticsYearDTO::class)
            ->forMember('year', static function (Statistics $statistics) {
                return (new DateTime())->setTimestamp($statistics->getRangeStartDateTime())->format('Y');
            })
            ->forMember('percent', static function (Statistics $statistics) {
                return round($statistics->getPercent(), 2);
            });

        $config->registerMapping(Statistics::class, StatisticsMonthDTO::class)
            ->forMember('monthNumber', static function (Statistics $statistics) {
                return (new DateTime())->setTimestamp($statistics->getRangeStartDateTime())->format('n');
            })
            ->forMember('monthName', static function (Statistics $statistics) {
                return TimeServiceInterface::MONTHS[((int) (new DateTime())->setTimestamp($statistics->getRangeStartDateTime())->format('n')) - 1];
            })
            ->forMember('percent', static function (Statistics $statistics) {
                return round($statistics->getPercent(), 2);
            });

        $config->registerMapping(Statistics::class, StatisticsDayDTO::class)
            ->forMember('dayNumber', static function (Statistics $statistics) {
                return (new DateTime())->setTimestamp($statistics->getRangeStartDateTime())->format('j');
            })
            ->forMember('percent', static function (Statistics $statistics) {
                return round($statistics->getPercent(), 2);
            });
    }
}
