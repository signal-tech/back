<?php

namespace App\Feature\Statistics\Handler;

use App\Exception\StockExchange\TokenIsNotValidException;
use App\Feature\Portfolio\Portfolio;
use App\Feature\Statistics\Message\CalculateStatisticsYearMessage;
use App\Feature\Statistics\Statistics;
use App\Service\Feature\Statistics\StatisticsServiceInterface;
use App\Service\Time\TimeServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CalculateStatisticsYearMessageHandler implements MessageHandlerInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private StatisticsServiceInterface $statisticsService,
        private TimeServiceInterface $timeService
    ) {
    }

    public function __invoke(CalculateStatisticsYearMessage $message): void
    {
        $portfolioRepository = $this->em->getRepository(Portfolio::class);

        /** @var Portfolio $portfolio */
        $portfolio = $portfolioRepository->find($message->getPortfolioId());

        $token = $portfolio->getStockExchangeToken();

        if (null !== $token->getDeletedAtFromStockExchange()) {
            return;
        }

        $yearTime = $this->timeService->getStartAndEndYearByTime($portfolio->getRangeStartDateTime());

        try {
            $this->statisticsService->calculateStatisticByPeriodAndGetEntity($token, $yearTime->getStartTime(), $yearTime->getEndTime(), Statistics::TYPE_YEAR);
        } catch (TokenIsNotValidException $exception) {
            return;
        }
    }
}
