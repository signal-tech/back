<?php

namespace App\Feature\Statistics\Handler;

use App\Exception\StockExchange\TokenIsNotValidException;
use App\Feature\Portfolio\Portfolio;
use App\Feature\Statistics\Message\CalculateStatisticsMonthMessage;
use App\Feature\Statistics\Message\CalculateStatisticsYearMessage;
use App\Feature\Statistics\Statistics;
use App\Service\Feature\Statistics\StatisticsServiceInterface;
use App\Service\Time\TimeServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class CalculateStatisticsMonthMessageHandler implements MessageHandlerInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private MessageBusInterface $messageBus,
        private StatisticsServiceInterface $statisticsService,
        private TimeServiceInterface $timeService
    ) {
    }

    public function __invoke(CalculateStatisticsMonthMessage $message): void
    {
        $portfolioRepository = $this->em->getRepository(Portfolio::class);

        /** @var Portfolio $portfolio */
        $portfolio = $portfolioRepository->find($message->getPortfolioId());

        $token = $portfolio->getStockExchangeToken();

        if (null !== $token->getDeletedAtFromStockExchange()) {
            return;
        }

        $monthTime = $this->timeService->getStartAndEndMonthByTime($portfolio->getRangeStartDateTime());

        try {
            $this->statisticsService->calculateStatisticByPeriodAndGetEntity($token, $monthTime->getStartTime(), $monthTime->getEndTime(), Statistics::TYPE_MONTH);
        } catch (TokenIsNotValidException $exception) {
            return;
        }

        $this->messageBus->dispatch(new CalculateStatisticsYearMessage($message->getPortfolioId()));
    }
}
