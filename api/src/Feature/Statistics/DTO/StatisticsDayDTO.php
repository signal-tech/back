<?php

namespace App\Feature\Statistics\DTO;

class StatisticsDayDTO
{
    /**
     * @var float
     */
    public $percent;

    /**
     * @var int
     */
    public $dayNumber;

    /**
     * @var bool
     */
    public $disabled = false;

    public function getPercent(): float
    {
        return $this->percent;
    }

    public function setPercent(float $percent): self
    {
        $this->percent = $percent;

        return $this;
    }

    public function getDayNumber(): int
    {
        return $this->dayNumber;
    }

    public function setDayNumber(int $dayNumber): self
    {
        $this->dayNumber = $dayNumber;

        return $this;
    }

    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function setDisabled(bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }
}
