<?php

namespace App\Feature\Statistics\DTO;

class StatisticsYearDTO
{
    /**
     * @var float
     */
    public $percent;

    /**
     * @var int
     */
    public $year;
}
