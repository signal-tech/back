<?php

namespace App\Feature\Statistics\DTO;

class StatisticsMonthDTO
{
    /**
     * @var float
     */
    public $percent;

    /**
     * @var int
     */
    public $monthNumber;

    /**
     * @var bool
     */
    public $disabled = false;

    /**
     * @var string
     */
    public $monthName;

    public function getPercent(): float
    {
        return $this->percent;
    }

    public function setPercent(float $percent): self
    {
        $this->percent = $percent;

        return $this;
    }

    public function getMonthNumber(): int
    {
        return $this->monthNumber;
    }

    public function setMonthNumber(int $monthNumber): self
    {
        $this->monthNumber = $monthNumber;

        return $this;
    }

    public function getMonthName(): string
    {
        return $this->monthName;
    }

    public function setMonthName(string $monthName): self
    {
        $this->monthName = $monthName;

        return $this;
    }

    public function isDisabled(): bool
    {
        return $this->disabled;
    }

    public function setDisabled(bool $disabled): self
    {
        $this->disabled = $disabled;

        return $this;
    }
}
