<?php

namespace App\Feature\Statistics\Query;

use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Validation\Constraints\EntityExist;

/**
 * @see GetStatisticsMonthsListQueryHandler
 */
class GetStatisticsMonthsListQuery
{
    /**
     * @EntityExist(targetEntity=StockExchangeToken::class, message="Данного токена не существует")
     */
    private int $tokenId;

    public function __construct(
        int $tokenId,
        private int $year
    ) {
        $this->tokenId = $tokenId;
    }

    public function getTokenId(): int
    {
        return $this->tokenId;
    }

    public function getYear(): int
    {
        return $this->year;
    }
}
