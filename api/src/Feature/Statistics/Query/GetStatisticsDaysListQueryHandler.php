<?php

namespace App\Feature\Statistics\Query;

use App\Feature\Statistics\DTO\StatisticsDayDTO;
use App\Feature\Statistics\Statistics;
use App\Feature\Statistics\StatisticsRepository;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\Time\TimeServiceInterface;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use function array_merge;
use function usort;

/**
 * @see GetStatisticsDayListQuery
 */
class GetStatisticsDaysListQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private AutoMapperInterface $mapper,
        private ValidationService $validator,
        private TimeServiceInterface $timeService
    ) {
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     *
     * @return mixed
     */
    public function __invoke(GetStatisticsDaysListQuery $query)
    {
        $this->validator->validateObject($query);

        /**
         * @var StatisticsRepository $statisticsRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $statisticsRepository = $this->em->getRepository(Statistics::class);

        $yearTimeDTO = $this->timeService->getStartAndEndMonthByYearAndMonthNumber($query->getYear(), $query->getMonth());

        $array = $statisticsRepository->getStatisticsListByPeriod(
            $query->getTokenId(),
            $yearTimeDTO->getStartTime(),
            $yearTimeDTO->getEndTime(),
            Statistics::TYPE_DAY
        );

        /** @var StatisticsDayDTO[] $statisticsDayDTOList */
        $statisticsDayDTOList = $this->mapper->mapMultiple($array, StatisticsDayDTO::class);

        $addedList = [];

        $daysInMonth = (new DateTime())->setTimestamp($yearTimeDTO->getStartTime())->format('t');

        for ($i = 0; $i < $daysInMonth; ++$i) {
            $monthExist = false;

            foreach ($statisticsDayDTOList as $statisticsDayDTO) {
                if ($statisticsDayDTO->getDayNumber() === ($i + 1)) {
                    $monthExist = true;
                }
            }

            if (!$monthExist) {
                $addedList[] = (new StatisticsDayDTO())
                    ->setDisabled(true)
                    ->setPercent(0)
                    ->setDayNumber($i + 1);
            }
        }

        $returnData = array_merge($statisticsDayDTOList, $addedList);

        usort($returnData, static function (StatisticsDayDTO $first, StatisticsDayDTO $second) {
            if ($first->getDayNumber() === $second->getDayNumber()) {
                return 0;
            }

            return ($first->getDayNumber() > $second->getDayNumber()) ? +1 : -1;
        });

        return $returnData;
    }
}
