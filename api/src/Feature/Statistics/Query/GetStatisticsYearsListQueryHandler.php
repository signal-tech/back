<?php

namespace App\Feature\Statistics\Query;

use App\Feature\Statistics\DTO\StatisticsYearDTO;
use App\Feature\Statistics\Statistics;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @see GetStatisticsYearsListQuery
 */
class GetStatisticsYearsListQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private AutoMapperInterface $mapper,
        private ValidationService $validator
    ) {
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     *
     * @return mixed
     */
    public function __invoke(GetStatisticsYearsListQuery $query)
    {
        $this->validator->validateObject($query);

        $statisticsRepository = $this->em->getRepository(Statistics::class);

        $array = $statisticsRepository->findBy(
            [
                'stockExchangeToken' => $query->getTokenId(),
                'type' => Statistics::TYPE_YEAR,
            ]
        );

        return $this->mapper->mapMultiple($array, StatisticsYearDTO::class);
    }
}
