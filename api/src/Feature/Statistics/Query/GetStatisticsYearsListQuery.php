<?php

namespace App\Feature\Statistics\Query;

use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Validation\Constraints\EntityExist;

/**
 * @see GetStatisticsYearsListQueryHandler
 */
class GetStatisticsYearsListQuery
{
    /**
     * @EntityExist(targetEntity=StockExchangeToken::class, message="Данного токена не существует")
     */
    private int $tokenId;

    public function __construct(
        int $tokenId
    ) {
        $this->tokenId = $tokenId;
    }

    public function getTokenId(): int
    {
        return $this->tokenId;
    }
}
