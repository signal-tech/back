<?php

namespace App\Feature\Statistics\Query;

use App\Feature\Statistics\DTO\StatisticsMonthDTO;
use App\Feature\Statistics\Statistics;
use App\Feature\Statistics\StatisticsRepository;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\Time\TimeServiceInterface;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use function array_merge;
use function usort;

/**
 * @see GetStatisticsMonthsListQuery
 */
class GetStatisticsMonthsListQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private AutoMapperInterface $mapper,
        private ValidationService $validator,
        private TimeServiceInterface $timeService
    ) {
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     *
     * @return mixed
     */
    public function __invoke(GetStatisticsMonthsListQuery $query)
    {
        $this->validator->validateObject($query);

        /**
         * @var StatisticsRepository $statisticsRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $statisticsRepository = $this->em->getRepository(Statistics::class);

        $yearTimeDTO = $this->timeService->getStartAndEndYearByYearNumber($query->getYear());

        $array = $statisticsRepository->getStatisticsListByPeriod(
            $query->getTokenId(),
            $yearTimeDTO->getStartTime(),
            $yearTimeDTO->getEndTime(),
            Statistics::TYPE_MONTH
        );

        /** @var StatisticsMonthDTO[] $statisticsMonthDTOList */
        $statisticsMonthDTOList = $this->mapper->mapMultiple($array, StatisticsMonthDTO::class);

        $addedList = [];

        foreach (TimeServiceInterface::MONTHS as $numberMonth => $monthName) {
            $monthExist = false;

            foreach ($statisticsMonthDTOList as $statisticsMonthDTO) {
                if ($statisticsMonthDTO->getMonthNumber() === ($numberMonth + 1)) {
                    $monthExist = true;
                }
            }

            if (!$monthExist) {
                $addedList[] = (new StatisticsMonthDTO())
                    ->setDisabled(true)
                    ->setPercent(0)
                    ->setMonthNumber($numberMonth + 1)
                    ->setMonthName($monthName);
            }
        }

        $returnData = array_merge($statisticsMonthDTOList, $addedList);

        usort($returnData, static function (StatisticsMonthDTO $first, StatisticsMonthDTO $second) {
            if ($first->getMonthNumber() === $second->getMonthNumber()) {
                return 0;
            }

            return ($first->getMonthNumber() > $second->getMonthNumber()) ? +1 : -1;
        });

        return $returnData;
    }
}
