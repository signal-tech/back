<?php

namespace App\Feature\Statistics\Message;

class CalculateStatisticsDayMessage
{
    public function __construct(
        private int $portfolioId
    ) {
    }

    public function getPortfolioId(): int
    {
        return $this->portfolioId;
    }
}
