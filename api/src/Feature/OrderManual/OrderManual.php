<?php

namespace App\Feature\OrderManual;

use App\Feature\Order\Order;
use App\Feature\StockExchangeToken\StockExchangeToken;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=OrderManualRepository::class)
 *
 * @ORM\Table(name="order_manual")
 * @ORM\HasLifecycleCallbacks
 */
class OrderManual
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Feature\Order\Order")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private Order $order;

    /**
     * @ORM\ManyToOne(targetEntity="App\Feature\StockExchangeToken\StockExchangeToken")
     * @ORM\JoinColumn(name="stock_exchange_token_id", referencedColumnName="id", nullable=false)
     */
    private StockExchangeToken $stockExchangeToken;

    /**
     * @ORM\Column(type="json")
     */
    private array $data;

    /**
     * @ORM\Column(type="json")
     */
    private array $manualData;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getStockExchangeToken(): StockExchangeToken
    {
        return $this->stockExchangeToken;
    }

    public function setStockExchangeToken(StockExchangeToken $stockExchangeToken): self
    {
        $this->stockExchangeToken = $stockExchangeToken;

        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getManualData(): array
    {
        return $this->manualData;
    }

    public function setManualData(array $manualData): self
    {
        $this->manualData = $manualData;

        return $this;
    }
}
