<?php

namespace App\Feature\OrderManual\Command;

use App\Feature\OrderManual\OrderManual;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\StockExchangeToken\StockExchangeTokenRepository;
use App\Infrastructure\OAuth\Service\TokenService;
use App\Infrastructure\OAuth\Service\TokenServiceInterface;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\StockExchange\StockExchangeServiceInterface;
use App\Service\Validation\ValidationException;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;

/**
 * @see CreateOrderManualCommand
 */
class CreateOrderManualCommandHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private ValidationService $validator,
        private AutoMapperInterface $mapper,
//        private TokenService $tokenService,
        private StockExchangeServiceInterface $stockExchangeService,
        private StockExchangeTokenRepository $stockExchangeTokenRepository,
        private TokenServiceInterface $tokenService
    ) {
    }

    /**
     * @throws UnregisteredMappingException
     * @throws ValidationException
     */
    public function __invoke(CreateOrderManualCommand $command): int
    {
        $createRequestDTO = $command->getCreateOrderManualRequest();
        $manualDataDTO = $createRequestDTO->getManualData();

        $this->validator->validateObject($command->getCreateOrderManualRequest());

        /** @var StockExchangeToken $stockExchangeToken */
        $stockExchangeToken = $this->stockExchangeTokenRepository->find($createRequestDTO->getStockExchangeTokenId());

        $order = $command->getOrder();

        $user = $this->tokenService->getUserByToken();

        if ($stockExchangeToken->getUser()->getId() !== $user->getId()) {
            throw new InvalidArgumentException('Данный токен вам не принадлежит.');
        }

        if ($stockExchangeToken->getStockExchange()->getCode() !== $order->getStockExchangeToken()->getStockExchange()->getCode()) {
            throw new InvalidArgumentException('Ошибка токена.');
        }

        $createOrderDTO = $this->stockExchangeService->getCreateOrderDTO($stockExchangeToken, $order);

        $orderManual = new OrderManual();

        $orderManual->setStockExchangeToken($stockExchangeToken);
        $orderManual->setOrder($order);
        $this->mapper->mapToObject($manualDataDTO, $createOrderDTO);

        $orderManual->setData($createOrderDTO->jsonSerialize());
        $orderManual->setManualData($manualDataDTO->jsonSerialize());

        $this->stockExchangeService->createOrder($createOrderDTO, $stockExchangeToken);

        $this->em->persist($orderManual);
        $this->em->flush();

        $this->em->refresh($orderManual);

        return $orderManual->getId();
    }
}
