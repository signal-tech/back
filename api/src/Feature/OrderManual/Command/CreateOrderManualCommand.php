<?php

namespace App\Feature\OrderManual\Command;

use App\Feature\Order\Order;
use App\Feature\OrderManual\DTO\Request\CreateOrderManualRequest;

/**
 * @see CreateOrderManualCommandHandler
 */
class CreateOrderManualCommand
{
    public function __construct(
        private CreateOrderManualRequest $createOrderManualRequest,
        private Order $order
    ) {
    }

    public function getCreateOrderManualRequest(): CreateOrderManualRequest
    {
        return $this->createOrderManualRequest;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }
}
