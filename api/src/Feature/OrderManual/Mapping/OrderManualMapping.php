<?php

namespace App\Feature\OrderManual\Mapping;

use App\Feature\OrderManual\DTO\Request\ManualDataOnCreateOrderRequest;
use App\Service\StockExchange\Binance\DTO\Order\CreateOrderDTO;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;

class OrderManualMapping implements AutoMapperConfiguratorInterface
{
    public function __construct(
    ) {
    }

    public function configure(AutoMapperConfigInterface $config): void
    {
        //TODO делать на каждую ДТО
        $config->registerMapping(ManualDataOnCreateOrderRequest::class, CreateOrderDTO::class);
    }
}
