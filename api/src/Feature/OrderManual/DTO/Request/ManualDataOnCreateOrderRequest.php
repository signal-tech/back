<?php

namespace App\Feature\OrderManual\DTO\Request;

use App\Base\DTO\JsonSerializableDTO;
use Symfony\Component\Validator\Constraints as Assert;

class ManualDataOnCreateOrderRequest extends JsonSerializableDTO
{
    /**
     * @Assert\NotBlank
     * @Assert\GreaterThan(0)
     */
    public float $price;

    /**
     * @Assert\NotBlank
     * @Assert\GreaterThan(0)
     */
    public float $quantity;

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getQuantity(): float
    {
        return $this->quantity;
    }
}
