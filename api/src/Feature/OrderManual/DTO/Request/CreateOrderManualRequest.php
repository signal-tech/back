<?php

namespace App\Feature\OrderManual\DTO\Request;

use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Validation\Constraints\EntityExist;
use Symfony\Component\Validator\Constraints as Assert;

class CreateOrderManualRequest
{
    /**
     * @var int
     *
     * @Assert\NotBlank
     * @EntityExist(targetEntity=StockExchangeToken::class, message="Данный токен не найден")
     */
    public $stockExchangeTokenId;

    /**
     * @Assert\Valid
     */
    public ManualDataOnCreateOrderRequest $manualData;

    public function getStockExchangeTokenId(): int
    {
        return $this->stockExchangeTokenId;
    }

    public function getManualData(): ManualDataOnCreateOrderRequest
    {
        return $this->manualData;
    }
}
