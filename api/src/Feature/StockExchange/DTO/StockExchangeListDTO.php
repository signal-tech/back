<?php

namespace App\Feature\StockExchange\DTO;

class StockExchangeListDTO
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $link;

    /**
     * @var string
     */
    public $iconPath;
}
