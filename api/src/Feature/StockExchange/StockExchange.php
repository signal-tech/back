<?php

namespace App\Feature\StockExchange;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=StockExchangeRepository::class)
 *
 * @ORM\Table(name="stock_exchanges")
 */
class StockExchange
{
    public const FTX_TYPE = 'ftx';

    public const BINANCE_TYPE = 'binance';

    public const BITMEX_TYPE = 'bitmex';

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $name;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $code;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $link;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $iconPath;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): void
    {
        $this->name = $name;
    }

    public function getCode(): string
    {
        return $this->code;
    }

    public function setCode(string $code): void
    {
        $this->code = $code;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getIconPath(): string
    {
        return $this->iconPath;
    }

    public function setIconPath(string $iconPath): self
    {
        $this->iconPath = $iconPath;

        return $this;
    }
}
