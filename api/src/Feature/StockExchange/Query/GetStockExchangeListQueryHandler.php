<?php

namespace App\Feature\StockExchange\Query;

use App\Feature\StockExchange\DTO\StockExchangeListDTO;
use App\Feature\StockExchange\StockExchange;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @see GetStockExchangeListQuery
 */
class GetStockExchangeListQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private AutoMapperInterface $mapper
    ) {
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     *
     * @return mixed
     */
    public function __invoke(GetStockExchangeListQuery $query)
    {
        $stockExchangeRepository = $this->em->getRepository(StockExchange::class);

        $array = $stockExchangeRepository->findAll();

        return $this->mapper->mapMultiple($array, StockExchangeListDTO::class);
    }
}
