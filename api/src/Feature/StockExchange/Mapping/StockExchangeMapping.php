<?php

namespace App\Feature\StockExchange\Mapping;

use App\Feature\StockExchange\DTO\StockExchangeBaseInfoDTO;
use App\Feature\StockExchange\DTO\StockExchangeListDTO;
use App\Feature\StockExchange\StockExchange;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Symfony\Component\HttpFoundation\UrlHelper;

class StockExchangeMapping implements AutoMapperConfiguratorInterface
{
    public function __construct(
        private UrlHelper $urlHelper
    ) {
    }

    /**
     * @throws UnregisteredMappingException
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        $urlHelper = $this->urlHelper;

        $config->registerMapping(StockExchange::class, StockExchangeListDTO::class)
            ->forMember('iconPath', static function (StockExchange $stockExchange) use ($urlHelper) {
                return $urlHelper->getAbsoluteUrl('/public/' . $stockExchange->getIconPath());
            });

        $config->registerMapping(StockExchange::class, StockExchangeBaseInfoDTO::class)
            ->forMember('iconPath', static function (StockExchange $stockExchange) use ($urlHelper) {
                return $urlHelper->getAbsoluteUrl('/public/' . $stockExchange->getIconPath());
            });
    }
}
