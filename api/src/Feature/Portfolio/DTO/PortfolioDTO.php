<?php

namespace App\Feature\Portfolio\DTO;

use App\Feature\Portfolio\DTO\Borrow\BorrowDTO;
use App\Feature\Portfolio\DTO\Deposit\DepositDTO;
use App\Feature\Portfolio\DTO\Fills\FillsDTO;
use App\Feature\Portfolio\DTO\Lending\LendingDTO;
use App\Feature\Portfolio\DTO\Position\PositionDTO;
use App\Feature\Portfolio\DTO\Withdrawal\WithdrawalDTO;

class PortfolioDTO
{
    /**
     * Баланс пользователя с учетом портфеля. (В валюте биржи).
     */
    public ?float $totalWallet = null;

    /**
     * Портфель.
     *
     * @var array<string, PositionDTO>
     */
    public array $positions = [];

    /**
     * Уплаченная комиссия.
     *
     * @var array<string, FillsDTO>
     */
    public array $fills = [];

    /**
     * Вводы.
     *
     * @var array<string, DepositDTO>
     */
    public array $deposit = [];

    /**
     * Выводы.
     *
     * @var array<string, WithdrawalDTO>
     */
    public array $withdrawal = [];

    /**
     * Выплаты финансирования.
     */
    public float $fundingPayment = 0.0;

    /**
     * Что взяли в заем.
     *
     * @var array<string, BorrowDTO>
     */
    public array $borrow = [];

    /**
     * Что дали в заем.
     *
     * @var array<string, LendingDTO>
     */
    public array $lending = [];

    public function getTotalWallet(): ?float
    {
        return $this->totalWallet;
    }

    public function setTotalWallet(?float $totalWallet): self
    {
        $this->totalWallet = $totalWallet;

        return $this;
    }

    /**
     * @return array<string, PositionDTO>
     */
    public function getPositions(): array
    {
        return $this->positions;
    }

    /**
     * @param array<string, PositionDTO> $positions
     */
    public function setPositions(array $positions): self
    {
        $this->positions = $positions;

        return $this;
    }

    /**
     * @return array<string, FillsDTO>
     */
    public function getFills(): array
    {
        return $this->fills;
    }

    /**
     * @param array<string, FillsDTO> $fills
     */
    public function setFills(array $fills): self
    {
        $this->fills = $fills;

        return $this;
    }

    /**
     * @return array<string, DepositDTO>
     */
    public function getDeposit(): array
    {
        return $this->deposit;
    }

    /**
     * @param array<string, DepositDTO> $deposit
     */
    public function setDeposit(array $deposit): self
    {
        $this->deposit = $deposit;

        return $this;
    }

    /**
     * @return array<string, WithdrawalDTO>
     */
    public function getWithdrawal(): array
    {
        return $this->withdrawal;
    }

    /**
     * @param array<string, WithdrawalDTO> $withdrawal
     */
    public function setWithdrawal(array $withdrawal): self
    {
        $this->withdrawal = $withdrawal;

        return $this;
    }

    public function getFundingPayment(): float
    {
        return $this->fundingPayment;
    }

    public function setFundingPayment(float $fundingPayment): self
    {
        $this->fundingPayment = $fundingPayment;

        return $this;
    }

    /**
     * @return array<string, BorrowDTO>
     */
    public function getBorrow(): array
    {
        return $this->borrow;
    }

    /**
     * @param array<string, BorrowDTO> $borrow
     */
    public function setBorrow(array $borrow): self
    {
        $this->borrow = $borrow;

        return $this;
    }

    /**
     * @return array<string, LendingDTO>
     */
    public function getLending(): array
    {
        return $this->lending;
    }

    /**
     * @param array<string, LendingDTO> $lending
     */
    public function setLending(array $lending): self
    {
        $this->lending = $lending;

        return $this;
    }
}
