<?php

namespace App\Feature\Portfolio\DTO\Fills;

class FillsDTO
{
    /**
     * Валюта комиссии (Может быть как доллар, так и любая другая валюта биткоин и тд).
     */
    public string $feeCurrency;

    /**
     * Сумма уплаченной комисии.
     */
    public float $fee = 0.0;

    public function getFeeCurrency(): string
    {
        return $this->feeCurrency;
    }

    public function setFeeCurrency(string $feeCurrency): self
    {
        $this->feeCurrency = $feeCurrency;

        return $this;
    }

    public function getFee(): float
    {
        return $this->fee;
    }

    public function setFee(float $fee): self
    {
        $this->fee = $fee;

        return $this;
    }
}
