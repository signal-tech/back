<?php

namespace App\Feature\Portfolio\DTO\Deposit;

class DepositDTO
{
    /**
     * Валюта ввода.
     */
    public string $coin;

    /**
     * Кол-во.
     */
    public float $size = 0.0;

    public function getCoin(): string
    {
        return $this->coin;
    }

    public function setCoin(string $coin): self
    {
        $this->coin = $coin;

        return $this;
    }

    public function getSize(): float
    {
        return $this->size;
    }

    public function setSize(float $size): self
    {
        $this->size = $size;

        return $this;
    }
}
