<?php

namespace App\Feature\Portfolio\DTO\Position;

class PositionDTO
{
    /**
     * Кол-во.
     */
    public float $size;

    /**
     * Наименование рынка (элемента портфолио).
     */
    public string $market;

    public function getSize(): float
    {
        return $this->size;
    }

    public function setSize(float $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getMarket(): string
    {
        return $this->market;
    }

    public function setMarket(string $market): self
    {
        $this->market = $market;

        return $this;
    }
}
