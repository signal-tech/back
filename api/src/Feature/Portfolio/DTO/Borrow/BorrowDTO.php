<?php

namespace App\Feature\Portfolio\DTO\Borrow;

class BorrowDTO
{
    public string $coin;

    public float $proceeds = 0.0;

    public float $size = 0.0;

    public function getCoin(): string
    {
        return $this->coin;
    }

    public function setCoin(string $coin): self
    {
        $this->coin = $coin;

        return $this;
    }

    public function getProceeds(): float
    {
        return $this->proceeds;
    }

    public function setProceeds(float $proceeds): self
    {
        $this->proceeds = $proceeds;

        return $this;
    }

    public function getSize(): float
    {
        return $this->size;
    }

    public function setSize(float $size): self
    {
        $this->size = $size;

        return $this;
    }
}
