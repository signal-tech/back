<?php

namespace App\Feature\Portfolio\Handler;

use App\Exception\StockExchange\TokenIsNotValidException;
use App\Feature\Portfolio\Message\CalculatePortfolioMessage;
use App\Feature\Portfolio\Portfolio;
use App\Feature\Statistics\Message\CalculateStatisticsDayMessage;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\StockExchangeToken\StockExchangeTokenRepository;
use App\Service\Feature\Portfolio\PortfolioServiceInterface;
use App\Service\StockExchange\StockExchangeServiceInterface;
use App\Service\Time\TimeServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Throwable;
use function time;

class CalculatePortfolioMessageHandler implements MessageHandlerInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private StockExchangeServiceInterface $stockExchangeService,
        private PortfolioServiceInterface $portfolioService,
        private MessageBusInterface $messageBus,
        private LoggerInterface $logger
    ) {
    }

    public function __invoke(CalculatePortfolioMessage $message): void
    {
        try {
            /**
             * @var StockExchangeTokenRepository $tokenRepository
             * @psalm-suppress UnnecessaryVarAnnotation
             */
            $tokenRepository = $this->em->getRepository(StockExchangeToken::class);
            $portfolioRepository = $this->em->getRepository(Portfolio::class);

            $token = $tokenRepository->find($message->getTokenId());

            if (null === $token || null !== $token->getDeletedAtFromStockExchange()) {
                return;
            }

            $previousPortfolio = $portfolioRepository->findOneBy([
                'stockExchangeToken' => $token,
                'rangeStartDateTime' => $message->getPeriodStart() - TimeServiceInterface::DAY_SECONDS,
                'rangeEndDateTime' => $message->getPeriodEnd() - TimeServiceInterface::DAY_SECONDS,
            ]);

            $previousPortfolioDTO = $previousPortfolio ? $previousPortfolio->getData() : null;

            $portfolioDTO = $this->stockExchangeService->getPortfolioByPeriod($token, $message->getPeriodStart(), $message->getPeriodEnd(), $previousPortfolioDTO);

            $portfolio = $this->portfolioService->createPortfolio($token, $message->getPeriodStart(), $message->getPeriodEnd(), $portfolioDTO);

            $this->messageBus->dispatch(
                new CalculateStatisticsDayMessage(
                    $portfolio->getId()
                )
            );

            if ($message->getPeriodEnd() + (2 * TimeServiceInterface::DAY_SECONDS) < time()) { //Лаг 2 дня
                $this->messageBus->dispatch(
                    new CalculatePortfolioMessage(
                        $token->getId(),
                        $message->getPeriodStart() + TimeServiceInterface::DAY_SECONDS,
                        $message->getPeriodEnd() + TimeServiceInterface::DAY_SECONDS
                    )
                );
            }
        } catch (Throwable $throwable) {
            $this->logger->critical(
                'Portfolio calculate error'
                . ' Token : ' . $message->getTokenId()
                . ' Start period: ' . $message->getPeriodStart()
                . ' End period: ' . $message->getPeriodEnd()
                . ' Message: ' . $throwable->getMessage()
            );

            if ($throwable instanceof TokenIsNotValidException) {
                return;
            }

            throw $throwable;
        }
    }
}
