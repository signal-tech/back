<?php

namespace App\Feature\Portfolio;

use App\Feature\Portfolio\DTO\PortfolioDTO;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Serializer\SerializerFactory;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Serializer\Normalizer\AbstractObjectNormalizer;

/**
 * @ORM\Entity(repositoryClass=PortfolioRepository::class)
 *
 * @ORM\Table(name="portfolios")
 *
 * @ORM\HasLifecycleCallbacks
 */
class Portfolio
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Feature\StockExchangeToken\StockExchangeToken")
     * @ORM\JoinColumn(name="stock_exchange_token_id", referencedColumnName="id", nullable=false)
     */
    private StockExchangeToken $stockExchangeToken;

    /**
     * @ORM\Column(type="json")
     */
    private array $data;

    /**
     * @ORM\Column(type="bigint")
     */
    private int $rangeStartDateTime;

    /**
     * @ORM\Column(type="bigint")
     */
    private int $rangeEndDateTime;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getStockExchangeToken(): StockExchangeToken
    {
        return $this->stockExchangeToken;
    }

    public function setStockExchangeToken(StockExchangeToken $stockExchangeToken): self
    {
        $this->stockExchangeToken = $stockExchangeToken;

        return $this;
    }

    public function getData(): PortfolioDTO
    {
        $serializer = SerializerFactory::create();

        $data = $this->data;

        $dto = $serializer->denormalize(
            $data,
            PortfolioDTO::class,
            'array',
            [AbstractObjectNormalizer::DISABLE_TYPE_ENFORCEMENT => true]
        );

        return $dto;
    }

    public function setData(PortfolioDTO $data): void
    {
        $serializer = SerializerFactory::create();

        /** @var mixed[] $data */
        $data = $serializer->normalize($data);

        $this->data = $data;
    }

    public function getRangeStartDateTime(): int
    {
        return $this->rangeStartDateTime;
    }

    public function setRangeStartDateTime(int $rangeStartDateTime): void
    {
        $this->rangeStartDateTime = $rangeStartDateTime;
    }

    public function getRangeEndDateTime(): int
    {
        return $this->rangeEndDateTime;
    }

    public function setRangeEndDateTime(int $rangeEndDateTime): void
    {
        $this->rangeEndDateTime = $rangeEndDateTime;
    }
}
