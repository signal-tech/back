<?php

namespace App\Feature\Portfolio\Query;

use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Validation\Constraints\EntityExist;

/**
 * @see GetPortfolioQuery
 */
class GetPortfolioQuery
{
    /**
     * @EntityExist(targetEntity=StockExchangeToken::class, message="Данного токена не существует")
     */
    private int $tokenId;

    public function __construct(
        int $tokenId,
        private int $year,
        private int $month,
        private int $day
    ) {
        $this->tokenId = $tokenId;
    }

    public function getTokenId(): int
    {
        return $this->tokenId;
    }

    public function getYear(): int
    {
        return $this->year;
    }

    public function getMonth(): int
    {
        return $this->month;
    }

    public function getDay(): int
    {
        return $this->day;
    }
}
