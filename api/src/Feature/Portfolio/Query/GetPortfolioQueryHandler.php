<?php

namespace App\Feature\Portfolio\Query;

use App\Feature\Portfolio\Portfolio;
use App\Feature\Portfolio\PortfolioRepository;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\Time\TimeServiceInterface;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use RuntimeException;

/**
 * @see GetPortfolioQuery
 */
class GetPortfolioQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private ValidationService $validator,
        private TimeServiceInterface $timeService
    ) {
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     *
     * @return mixed
     */
    public function __invoke(GetPortfolioQuery $query)
    {
        $this->validator->validateObject($query);

        /**
         * @var PortfolioRepository $portfolioRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $portfolioRepository = $this->em->getRepository(Portfolio::class);

        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);

        /** @var StockExchangeToken $token */
        $token = $stockExchangeTokenRepository->find($query->getTokenId());

        $dayTimeDTO = $this->timeService->getStartAndEndMonthByYearAndMonthAndDayNumber(
            $query->getYear(),
            $query->getMonth(),
            $query->getDay()
        );

        $array = $portfolioRepository->getPortfolioListByTokenAndPeriod(
            $token,
            $dayTimeDTO->getStartTime(),
            $dayTimeDTO->getEndTime()
        );

        $data = $array[0] ?? null;

        if (null === $data) {
            throw new RuntimeException('Не найдено портфолио');
        }

        return $data->getData();
    }
}
