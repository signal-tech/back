<?php

namespace App\Feature\Portfolio;

use App\Feature\StockExchangeToken\StockExchangeToken;
use DateTime;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Portfolio|null find($id, $lockMode = null, $lockVersion = null)
 * @method Portfolio|null findOneBy(array $criteria, array $orderBy = null)
 * @method Portfolio[]    findAll()
 * @method Portfolio[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @psalm-suppress QueryBuilderSetParameter
 */
class PortfolioRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Portfolio::class);
    }

    //TODO отдавать порциями

    /**
     * @return Portfolio[]
     */
    public function getPortfolioListByTokenAndPeriod(
        StockExchangeToken $token,
        int $startPeriod,
        int $endPeriod
    ): array {
        /** @var Portfolio[] $returnData */
        $returnData = $this->createPortfolioListByTokenAndPeriodQuery($token, $startPeriod, $endPeriod)
            ->orderBy('p.rangeStartDateTime', 'ASC')
            ->getQuery()
            ->getResult();

        return $returnData;
    }

    /**
     * @return Portfolio[]
     */
    public function getPortfolioListByTokenAndPeriodAndUpdatedNotLessDate(
        StockExchangeToken $token,
        int $startPeriod,
        int $endPeriod,
        DateTime $date
    ): array {
        /** @var Portfolio[] $returnData */
        $returnData = $this->createPortfolioListByTokenAndPeriodQuery($token, $startPeriod, $endPeriod)
            ->andWhere('p.updatedAt > :updatedAt')
            ->setParameter('updatedAt', $date)
            ->orderBy('p.rangeStartDateTime', 'ASC')
            ->getQuery()
            ->getResult();

        return $returnData;
    }

    private function createPortfolioListByTokenAndPeriodQuery(
        StockExchangeToken $token,
        int $startPeriod,
        int $endPeriod
    ): QueryBuilder {
        return $this->createQueryBuilder('p')
            ->andWhere('p.rangeStartDateTime >= :rangeStartDateTime')
            ->andWhere('p.rangeEndDateTime <= :rangeEndDateTime')
            ->andWhere('p.stockExchangeToken = :token')
            ->setParameter('rangeStartDateTime', $startPeriod)
            ->setParameter('rangeEndDateTime', $endPeriod)
            ->setParameter('token', $token);
    }
}
