<?php

namespace App\Feature\Portfolio\Message;

class CalculatePortfolioMessage
{
    public function __construct(
        private int $tokenId,
        private int $periodStart,
        private int $periodEnd
    ) {
    }

    public function getTokenId(): int
    {
        return $this->tokenId;
    }

    public function getPeriodStart(): int
    {
        return $this->periodStart;
    }

    public function getPeriodEnd(): int
    {
        return $this->periodEnd;
    }
}
