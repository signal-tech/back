<?php

namespace App\Feature\Redirect;

use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="redirects")
 */
class RedirectEntity
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer", nullable=false)
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private int $id;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $type;

    /**
     * @ORM\Column(type="integer", nullable=false)
     */
    private int $entityId;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    private string $redirectedUrl;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    private DateTimeInterface $redirectedDateTime;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getEntityId(): int
    {
        return $this->entityId;
    }

    public function setEntityId(int $entityId): self
    {
        $this->entityId = $entityId;

        return $this;
    }

    public function getRedirectedUrl(): string
    {
        return $this->redirectedUrl;
    }

    public function setRedirectedUrl(string $redirectedUrl): self
    {
        $this->redirectedUrl = $redirectedUrl;

        return $this;
    }

    public function getRedirectedDateTime(): DateTimeInterface
    {
        return $this->redirectedDateTime;
    }

    public function setRedirectedDateTime(DateTimeInterface $redirectedDateTime): self
    {
        $this->redirectedDateTime = $redirectedDateTime;

        return $this;
    }
}
