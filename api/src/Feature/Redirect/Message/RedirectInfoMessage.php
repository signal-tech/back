<?php

namespace App\Feature\Redirect\Message;

use DateTime;
use DateTimeInterface;

class RedirectInfoMessage
{
    private string $type;

    private int $id;

    private string $redirectedUrl;

    private DateTimeInterface $dateTime;

    public function __construct(
        string $type,
        int $id,
        string $redirectedUrl
    ) {
        $this->type = $type;
        $this->id = $id;
        $this->redirectedUrl = $redirectedUrl;
        $this->dateTime = new DateTime();
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getId(): int
    {
        return $this->id;
    }

    public function getRedirectedUrl(): string
    {
        return $this->redirectedUrl;
    }

    /**
     * @return DateTimeInterface
     */
    public function getDateTime()
    {
        return $this->dateTime;
    }
}
