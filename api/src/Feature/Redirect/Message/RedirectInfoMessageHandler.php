<?php

namespace App\Feature\Redirect\Message;

use App\Feature\Redirect\RedirectEntity;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class RedirectInfoMessageHandler implements MessageHandlerInterface
{
    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function __invoke(RedirectInfoMessage $message): void
    {
        $redirectEntity = new RedirectEntity();

        $redirectEntity->setEntityId($message->getId());
        $redirectEntity->setType($message->getType());
        $redirectEntity->setRedirectedUrl($message->getRedirectedUrl());
        $redirectEntity->setRedirectedDateTime($message->getDateTime());

        $this->em->persist($redirectEntity);
        $this->em->flush();
        $this->em->clear();
    }
}
