<?php

namespace App\Feature\UserSubscription;

use App\Feature\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @psalm-suppress QueryBuilderSetParameter
 */
class UserSubscriptionRepository extends ServiceEntityRepository
{
    /**
     * @param class-string $entityClass
     */
    public function __construct(ManagerRegistry $registry, $entityClass = UserSubscription::class)
    {
        parent::__construct($registry, $entityClass);
    }

    public function isSubscriber(User $user, User $subscriber): bool
    {
        $subscription = $this->createQueryBuilder('s')
            ->andWhere('s.user = :user')
            ->andWhere('s.subscriber = :subscriber')
            ->setParameter('user', $user)
            ->setParameter('subscriber', $subscriber)
            ->getQuery()
            ->getOneOrNullResult();

        return null !== $subscription;
    }
}
