<?php

namespace App\Feature\UserSubscription\Command;

use App\Service\Validation\Constraints\UserRolesCheck;

/**
 * @see SubscribeCommandHandler
 */
class SubscribeCommand
{
    /**
     * @UserRolesCheck
     */
    private int $userId;

    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
}
