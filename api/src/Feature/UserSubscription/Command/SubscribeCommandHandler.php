<?php

namespace App\Feature\UserSubscription\Command;

use App\Feature\User\User;
use App\Feature\UserSubscription\UserSubscription;
use App\Infrastructure\OAuth\Service\TokenService;
use App\Service\Feature\User\UserServiceInterface;
use App\Service\Validation\ValidationException;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @see SubscribeCommand
 */
class SubscribeCommandHandler
{
    private EntityManagerInterface $em;

    private ValidationService $validator;

    private TokenService $tokenService;

    public function __construct(
        EntityManagerInterface $em,
        ValidationService $validator,
        TokenService $tokenService,
        private UserServiceInterface $userService
    ) {
        $this->em = $em;
        $this->validator = $validator;
        $this->tokenService = $tokenService;
    }

    /**
     * @throws UnregisteredMappingException
     * @throws ValidationException
     */
    public function __invoke(SubscribeCommand $command): void
    {
        $this->validator->validateObject($command);

        $userId = $command->getUserId();

        $userRepository = $this->em->getRepository(User::class);

        /** @var User $user */
        $user = $userRepository->find($userId);

        $userSubscriptionRepository = $this->em->getRepository(UserSubscription::class);

        $userSubscriber = $this->tokenService->getUserByToken();

        $subscription = $userSubscriptionRepository->findOneBy(
            [
                'user' => $user,
                'subscriber' => $userSubscriber,
            ]
        );

        if (null === $subscription) {
            $subscription = new UserSubscription();
            $subscription->setUser($user);
            $subscription->setSubscriber($userSubscriber);
            $this->em->persist($subscription);
            $this->em->flush();

            $this->userService->addToSubscribers($user);
            $this->userService->addToSubscriptions($userSubscriber);
        }
    }
}
