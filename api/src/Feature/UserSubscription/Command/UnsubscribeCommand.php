<?php

namespace App\Feature\UserSubscription\Command;

use App\Service\Validation\Constraints\UserRolesCheck;

/**
 * @see UnsubscribeCommandHandler
 */
class UnsubscribeCommand
{
    /**
     * @UserRolesCheck
     */
    private int $userId;

    public function __construct(int $userId)
    {
        $this->userId = $userId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
}
