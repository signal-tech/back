<?php

namespace App\Feature\UserSubscription\Query;

use App\Feature\User\DTO\RecommendedUsersListItemDTO;
use App\Feature\User\User;
use App\Feature\UserSubscription\DTO\Request\GetSubscribersListRequestFilter;
use App\Feature\UserSubscription\UserSubscription;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\ListPaginator\PaginateResponseInterface;
use App\Service\ListPaginator\PaginatorInterface;
use App\Service\Validation\ObjectValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * @see GetSubscribersListQuery
 */
class GetSubscribersListQueryHandler extends AbstractAuthorizedAccessible
{
    private EntityManagerInterface $em;

    private ObjectValidatorInterface $validator;

    private PaginatorInterface $paginator;

    public function __construct(
        EntityManagerInterface $em,
        ObjectValidatorInterface $validator,
        PaginatorInterface $paginator
    ) {
        $this->em = $em;
        $this->validator = $validator;
        $this->paginator = $paginator;
    }

    public function __invoke(GetSubscribersListQuery $query): PaginateResponseInterface
    {
        $this->validator->validateObject($query->getFilter());

        $queryBuilder = $this->createQuery($query->getFilter(), $query->getUserId());
        //$this->applyQueryFilters($queryBuilder, $query->getFilter());

        return $this->paginator->createResponse(
            $queryBuilder,
            $query->getFilter()->page,
            $query->getFilter()->perPage,
            RecommendedUsersListItemDTO::class
        );
    }

    private function createQuery(GetSubscribersListRequestFilter $filter, int $userId): QueryBuilder
    {
        $qb = $this->em->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->innerJoin(UserSubscription::class, 'us', Join::WITH, 'u.id = IDENTITY(us.subscriber)')
            ->where('IDENTITY(us.user) = :userId')
            ->setParameter('userId', $userId);

        switch ($filter->sort) {
            case 'expired':
                $qb->addOrderBy('u.createdAt', 'ASC')
                    ->addOrderBy('u.id', 'ASC');

                break;
            case 'new':
            default:
                $qb->addOrderBy('u.createdAt', 'DESC')
                    ->addOrderBy('u.id', 'DESC');

                break;
        }

        return $qb;
    }

//    private function applyQueryFilters(QueryBuilder $qb, GetSubscribersListRequestFilter $filter): void
//    {
//    }
}
