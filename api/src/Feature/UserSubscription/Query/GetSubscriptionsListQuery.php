<?php

namespace App\Feature\UserSubscription\Query;

use App\Feature\UserSubscription\DTO\Request\GetSubscriptionsListRequestFilter;

/**
 * @see GetSubscriptionsListQueryHandler
 */
class GetSubscriptionsListQuery
{
    public int $subscriberId;

    private GetSubscriptionsListRequestFilter $filter;

    public function __construct(GetSubscriptionsListRequestFilter $filter, int $subscriberId)
    {
        $this->filter = $filter;
        $this->subscriberId = $subscriberId;
    }

    public function getFilter(): GetSubscriptionsListRequestFilter
    {
        return $this->filter;
    }

    public function getSubscriberId(): int
    {
        return $this->subscriberId;
    }
}
