<?php

namespace App\Feature\UserSubscription\Query;

use App\Feature\User\DTO\RecommendedUsersListItemDTO;
use App\Feature\User\User;
use App\Feature\UserSubscription\DTO\Request\GetSubscriptionsListRequestFilter;
use App\Feature\UserSubscription\UserSubscription;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\ListPaginator\PaginateResponseInterface;
use App\Service\ListPaginator\PaginatorInterface;
use App\Service\Validation\ObjectValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * @see GetSubscriptionsListQuery
 */
class GetSubscriptionsListQueryHandler extends AbstractAuthorizedAccessible
{
    private EntityManagerInterface $em;

    private ObjectValidatorInterface $validator;

    private PaginatorInterface $paginator;

    public function __construct(
        EntityManagerInterface $em,
        ObjectValidatorInterface $validator,
        PaginatorInterface $paginator
    ) {
        $this->em = $em;
        $this->validator = $validator;
        $this->paginator = $paginator;
    }

    public function __invoke(GetSubscriptionsListQuery $query): PaginateResponseInterface
    {
        $this->validator->validateObject($query->getFilter());

        $queryBuilder = $this->createQuery($query->getFilter(), $query->getSubscriberId());
        //$this->applyQueryFilters($queryBuilder, $query->getFilter());

        return $this->paginator->createResponse(
            $queryBuilder,
            $query->getFilter()->page,
            $query->getFilter()->perPage,
            RecommendedUsersListItemDTO::class
        );
    }

    private function createQuery(GetSubscriptionsListRequestFilter $filter, int $subscriberId): QueryBuilder
    {
        $qb = $this->em->createQueryBuilder()
            ->select('u')
            ->from(User::class, 'u')
            ->innerJoin(UserSubscription::class, 'us', Join::WITH, 'u.id = IDENTITY(us.user)')
            ->where('IDENTITY(us.subscriber) = :subscriberId')
            ->setParameter('subscriberId', $subscriberId);

        switch ($filter->sort) {
            case 'expired':
                $qb->addOrderBy('u.createdAt', 'ASC')
                    ->addOrderBy('u.id', 'ASC');

                break;
            case 'new':
            default:
                $qb->addOrderBy('u.createdAt', 'DESC')
                    ->addOrderBy('u.id', 'DESC');

                break;
        }

        return $qb;
    }

//    private function applyQueryFilters(QueryBuilder $qb, GetSubscriptionsListRequestFilter $filter): void
//    {
//    }
}
