<?php

namespace App\Feature\UserSubscription\Query;

use App\Feature\UserSubscription\DTO\Request\GetSubscribersListRequestFilter;

/**
 * @see GetSubscribersListQueryHandler
 */
class GetSubscribersListQuery
{
    public int $userId;

    private GetSubscribersListRequestFilter $filter;

    public function __construct(GetSubscribersListRequestFilter $filter, int $userId)
    {
        $this->filter = $filter;
        $this->userId = $userId;
    }

    public function getFilter(): GetSubscribersListRequestFilter
    {
        return $this->filter;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
}
