<?php

namespace App\Feature\Order\Mapping;

use App\Feature\Order\DTO\OrderDTO;
use App\Feature\Order\DTO\OrderListDTO;
use App\Feature\Order\DTO\Request\OrderRequest;
use App\Feature\Order\Order;
use App\Feature\StockExchange\DTO\StockExchangeBaseInfoDTO;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\User\DTO\UserBaseInfoDTO;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use Doctrine\ORM\EntityManagerInterface;

class OrderMapping implements AutoMapperConfiguratorInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private AutoMapperInterface $mapper
    ) {
    }

    public function configure(AutoMapperConfigInterface $config): void
    {
        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);
        $mapper = $this->mapper;

        $config->registerMapping(OrderRequest::class, Order::class)
            ->forMember('stockExchangeToken', static function (OrderRequest $orderRequest) use ($stockExchangeTokenRepository): StockExchangeToken {
                /** @var StockExchangeToken $stockExchangeToken */
                $stockExchangeToken = $stockExchangeTokenRepository->find($orderRequest->stockExchangeTokenId);

                return $stockExchangeToken;
            });

        $config->registerMapping(Order::class, OrderListDTO::class)
            ->forMember('user', static function (Order $order) use ($mapper): UserBaseInfoDTO {
                /** @var UserBaseInfoDTO $baseInfoDTO */
                $baseInfoDTO = $mapper->map($order->getStockExchangeToken()->getUser(), UserBaseInfoDTO::class);

                return $baseInfoDTO;
            })
            ->forMember('stockExchange', static function (Order $order) use ($mapper): StockExchangeBaseInfoDTO {
                /** @var StockExchangeBaseInfoDTO $baseInfoDTO */
                $baseInfoDTO = $mapper->map($order->getStockExchangeToken()->getStockExchange(), StockExchangeBaseInfoDTO::class);

                return $baseInfoDTO;
            });

        $config->registerMapping(Order::class, OrderDTO::class)
            ->forMember('user', static function (Order $order) use ($mapper): UserBaseInfoDTO {
                /** @var UserBaseInfoDTO $baseInfoDTO */
                $baseInfoDTO = $mapper->map($order->getStockExchangeToken()->getUser(), UserBaseInfoDTO::class);

                return $baseInfoDTO;
            })
            ->forMember('stockExchange', static function (Order $order) use ($mapper): StockExchangeBaseInfoDTO {
                /** @var StockExchangeBaseInfoDTO $baseInfoDTO */
                $baseInfoDTO = $mapper->map($order->getStockExchangeToken()->getStockExchange(), StockExchangeBaseInfoDTO::class);

                return $baseInfoDTO;
            });
    }
}
