<?php

namespace App\Feature\Order\Handler;

use App\Feature\Order\Message\CreateOrderMessage;
use App\Feature\Order\Service\OrderServiceInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CreateOrderMessageHandler implements MessageHandlerInterface
{
    public function __construct(
        private OrderServiceInterface $orderService
    ) {
    }

    public function __invoke(CreateOrderMessage $orderMessage): void
    {
        $this->orderService->create($orderMessage->getOrderRequest());
    }
}
