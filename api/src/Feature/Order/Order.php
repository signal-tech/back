<?php

namespace App\Feature\Order;

use App\Feature\StockExchangeToken\StockExchangeToken;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=OrderRepository::class)
 *
 * @ORM\Table(name="orders")
 * @ORM\HasLifecycleCallbacks
 */
class Order
{
    use TimestampableEntity;

    /**
     * @ORM\Column(type="bigint", nullable=false)
     */
    protected int $time;

    /**
     * @ORM\Column(type="bigint", nullable=false)
     */
    protected int $orderId;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected ?string $clientId;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected string $market;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected string $type;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected string $side;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected float $size;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    protected float $price;

    /**
     * @ORM\Column(type="string", nullable=true)
     */
    protected ?string $timeInForce;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected string $status;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\Column(type="json")
     */
    private array $data;

    /**
     * @ORM\ManyToOne(targetEntity="App\Feature\StockExchangeToken\StockExchangeToken")
     * @ORM\JoinColumn(name="stock_exchange_token_id", referencedColumnName="id", nullable=false)
     */
    private StockExchangeToken $stockExchangeToken;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getTime(): int
    {
        return $this->time;
    }

    public function setTime(int $time): self
    {
        $this->time = $time;

        return $this;
    }

    public function getStockExchangeToken(): StockExchangeToken
    {
        return $this->stockExchangeToken;
    }

    public function setStockExchangeToken(StockExchangeToken $stockExchangeToken): self
    {
        $this->stockExchangeToken = $stockExchangeToken;

        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function setOrderId(int $orderId): self
    {
        $this->orderId = $orderId;

        return $this;
    }

    public function getClientId(): ?string
    {
        return $this->clientId;
    }

    public function setClientId(?string $clientId): self
    {
        $this->clientId = $clientId;

        return $this;
    }

    public function getMarket(): string
    {
        return $this->market;
    }

    public function setMarket(string $market): self
    {
        $this->market = $market;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function getSide(): string
    {
        return $this->side;
    }

    public function setSide(string $side): self
    {
        $this->side = $side;

        return $this;
    }

    public function getSize(): float
    {
        return $this->size;
    }

    public function setSize(float $size): self
    {
        $this->size = $size;

        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getTimeInForce(): ?string
    {
        return $this->timeInForce;
    }

    public function setTimeInForce(?string $timeInForce): self
    {
        $this->timeInForce = $timeInForce;

        return $this;
    }

    public function getStatus(): string
    {
        return $this->status;
    }

    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }
}
