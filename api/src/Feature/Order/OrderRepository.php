<?php

namespace App\Feature\Order;

use App\Feature\StockExchange\StockExchange;
use App\Feature\StockExchangeToken\StockExchangeToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Order|null find($id, $lockMode = null, $lockVersion = null)
 * @method Order|null findOneBy(array $criteria, array $orderBy = null)
 * @method Order[]    findAll()
 * @method Order[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @psalm-suppress QueryBuilderSetParameter
 */
class OrderRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Order::class);
    }

    public function getOrderByRemoteIdAndStockExchange(int $orderId, StockExchange $stockExchange): ?Order
    {
        /** @var Order|null $order */
        $order = $this->createQueryBuilder('o')
            ->innerJoin(StockExchangeToken::class, 'se', Join::WITH, 'IDENTITY(o.stockExchangeToken) = se.id')
            ->andWhere('o.remoteId = :order')
            ->andWhere('se.stockExchange = :stockExchange')
            ->setParameter('order', $orderId)
            ->setParameter('stockExchange', $stockExchange)
            ->getQuery()
            ->getResult();

        return $order;
    }
}
