<?php

namespace App\Feature\Order\Service;

use App\Feature\CopyTrading\CopyTrading;
use App\Feature\CopyTrading\CopyTradingRepository;
use App\Feature\CopyTrading\Message\CopyTradingMessage;
use App\Feature\Order\DTO\Request\OrderRequest;
use App\Feature\Order\Order;
use App\Feature\Order\OrderRepository;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\StockExchangeToken\StockExchangeTokenRepository;
use AutoMapperPlus\AutoMapperInterface;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use Symfony\Component\Messenger\MessageBusInterface;

class OrderService implements OrderServiceInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private AutoMapperInterface $mapper
//        private MessageBusInterface $messageBus
    ) {
    }

    public function create(OrderRequest $orderRequest): Order
    {
        /**
         * @var OrderRepository $orderRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $orderRepository = $this->em->getRepository(Order::class);

        /**
         * @var StockExchangeTokenRepository $stockExchangeTokenRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);
        $stockExchangeToken = $stockExchangeTokenRepository->find($orderRequest->getStockExchangeTokenId());

        if (null === $stockExchangeToken) {
            throw new Exception('Не найден токен при получении сделок');
        }

        $order = $orderRepository->getOrderByRemoteIdAndStockExchange($orderRequest->getOrderId(), $stockExchangeToken->getStockExchange());

        if (null !== $order) {
            /** @var Order $entity */
            $entity = $this->mapper->mapToObject($orderRequest, $order);
        } else {
            /** @var Order $entity */
            $entity = $this->mapper->map($orderRequest, Order::class);
        }

        $this->em->persist($entity);
        $this->em->flush();

        //$this->em->refresh($entity);

//        /**
//         * @var CopyTradingRepository $copyTradingRepository
//         * @psalm-suppress UnnecessaryVarAnnotation
//         */
//        $copyTradingRepository = $this->em->getRepository(CopyTrading::class);

//        foreach ($copyTradingRepository->getCopyTradingsListByParent($entity->getStockExchangeToken()) as $copyTradings) {
//            $this->messageBus->dispatch(new CopyTradingMessage($entity->getId(), $copyTradings->getId()));
//        }

        return $entity;
    }
}
