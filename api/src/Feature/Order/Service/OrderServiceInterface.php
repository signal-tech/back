<?php

namespace App\Feature\Order\Service;

use App\Feature\Order\DTO\Request\OrderRequest;
use App\Feature\Order\Order;

interface OrderServiceInterface
{
    public function create(OrderRequest $orderRequest): Order;
}
