<?php

namespace App\Feature\Order\DTO;

use App\Feature\StockExchange\DTO\StockExchangeBaseInfoDTO;
use App\Feature\User\DTO\UserBaseInfoDTO;

class OrderListDTO
{
    public int $id;

    public int $remoteId;

    public ?int $remoteClientId;

    public int $time;

    public string $market;

    public string $type;

    public string $side;

    public float $size;

    public float $price;

    public ?string $timeInForce;

    public string $status;

    public StockExchangeBaseInfoDTO $stockExchange;

    public UserBaseInfoDTO $user;
}
