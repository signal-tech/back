<?php

namespace App\Feature\Order\DTO\Request;

class OrderRequest
{
    public array $data;

    public int $stockExchangeTokenId;

    public int $time;

    public int $orderId;

    public ?string $clientId;

    public string $market;

    public string $type;

    public string $side;

    public float $size;

    public float $price;

    public ?string $timeInForce;

    public string $status;

    public function getData(): array
    {
        return $this->data;
    }

    public function getStockExchangeTokenId(): int
    {
        return $this->stockExchangeTokenId;
    }

    public function getTime(): int
    {
        return $this->time;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function getClientId(): ?string
    {
        return $this->clientId;
    }

    public function getMarket(): string
    {
        return $this->market;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function getSide(): string
    {
        return $this->side;
    }

    public function getSize(): float
    {
        return $this->size;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function getTimeInForce(): ?string
    {
        return $this->timeInForce;
    }

    public function getStatus(): string
    {
        return $this->status;
    }
}
