<?php

namespace App\Feature\Order\DTO\Request;

use Symfony\Component\Validator\Constraints as Assert;
use function array_key_exists;

class GetOrdersByUserListRequestFilter extends GetOrdersBySubscriptionsListRequestFilter
{
    /**
     * @Assert\Type(type="integer")
     */
    public ?int $stockExchangeTokenId;

    public function getStockExchangeTokenId(): ?int
    {
        return $this->stockExchangeTokenId;
    }

    public static function createFromRequestPayload(array $request): self
    {
        $filter = new self();

        $filter->page = array_key_exists('page', $request) && !empty($request['page']) ? (int) $request['page'] : 1;
        $filter->perPage = array_key_exists('perPage', $request) && !empty($request['perPage']) ? (int) $request['perPage'] : 20;
        $filter->stockExchangeTokenId = array_key_exists('stockExchangeTokenId', $request) && !empty($request['stockExchangeTokenId']) ? (int) $request['stockExchangeTokenId'] : null;

        return $filter;
    }
}
