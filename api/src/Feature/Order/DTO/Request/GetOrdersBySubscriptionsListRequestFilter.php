<?php

namespace App\Feature\Order\DTO\Request;

use App\Service\Request\RequestObjectInterface;
use Symfony\Component\Validator\Constraints as Assert;
use function array_key_exists;

class GetOrdersBySubscriptionsListRequestFilter implements RequestObjectInterface
{
    /**
     * @Assert\NotBlank
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1")
     */
    public int $page;

    /**
     * @Assert\NotBlank
     * @Assert\Type(type="integer")
     * @Assert\Range(min="1", max="50")
     */
    public int $perPage;

    public static function createFromRequestPayload(array $request): self
    {
        $filter = new self();

        $filter->page = array_key_exists('page', $request) && !empty($request['page']) ? (int) $request['page'] : 1;
        $filter->perPage = array_key_exists('perPage', $request) && !empty($request['perPage']) ? (int) $request['perPage'] : 20;

        return $filter;
    }
}
