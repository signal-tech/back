<?php

namespace App\Feature\Order\DTO;

use App\Service\StockExchange\DTO\MarketFiltersDataDTO;

class MarketActualDataDTO
{
    private float $actualPrice;

    private MarketFiltersDataDTO $filters;

    public function getActualPrice(): float
    {
        return $this->actualPrice;
    }

    public function setActualPrice(float $actualPrice): self
    {
        $this->actualPrice = $actualPrice;

        return $this;
    }

    public function getFilters(): MarketFiltersDataDTO
    {
        return $this->filters;
    }

    public function setFilters(MarketFiltersDataDTO $filters): self
    {
        $this->filters = $filters;

        return $this;
    }
}
