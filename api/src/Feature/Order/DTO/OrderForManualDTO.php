<?php

namespace App\Feature\Order\DTO;

use App\Feature\StockExchangeToken\DTO\StockExchangeTokenPublicListDTO;

class OrderForManualDTO
{
    public OrderDTO $order;

    /**
     * @var StockExchangeTokenPublicListDTO[]
     */
    public array $tokens;

    public MarketActualDataDTO $marketActualData;

    public function getOrder(): OrderDTO
    {
        return $this->order;
    }

    public function setOrder(OrderDTO $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return StockExchangeTokenPublicListDTO[]
     */
    public function getTokens(): array
    {
        return $this->tokens;
    }

    /**
     * @param StockExchangeTokenPublicListDTO[] $tokens
     *
     * @return $this
     */
    public function setTokens(array $tokens): self
    {
        $this->tokens = $tokens;

        return $this;
    }

    public function getMarketActualData(): MarketActualDataDTO
    {
        return $this->marketActualData;
    }

    public function setMarketActualData(MarketActualDataDTO $marketActualData): self
    {
        $this->marketActualData = $marketActualData;

        return $this;
    }
}
