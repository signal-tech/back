<?php

namespace App\Feature\Order\Message;

use App\Feature\Order\DTO\Request\OrderRequest;

class CreateOrderMessage
{
    private OrderRequest $orderRequest;

    public function __construct(OrderRequest $orderRequest)
    {
        $this->orderRequest = $orderRequest;
    }

    public function getOrderRequest(): OrderRequest
    {
        return $this->orderRequest;
    }
}
