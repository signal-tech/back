<?php

namespace App\Feature\Order\Query;

use App\Feature\Order\DTO\Request\GetOrdersBySubscriptionsListRequestFilter;

/**
 * @see GetOrdersBySubscriptionsListQueryHandler
 */
class GetOrdersBySubscriptionsListQuery
{
    private GetOrdersBySubscriptionsListRequestFilter $filter;

    public function __construct(GetOrdersBySubscriptionsListRequestFilter $filter)
    {
        $this->filter = $filter;
    }

    public function getFilter(): GetOrdersBySubscriptionsListRequestFilter
    {
        return $this->filter;
    }
}
