<?php

namespace App\Feature\Order\Query;

use App\Feature\Order\DTO\MarketActualDataDTO;
use App\Feature\Order\DTO\OrderDTO;
use App\Feature\Order\DTO\OrderForManualDTO;
use App\Feature\Order\Order;
use App\Feature\StockExchangeToken\DTO\StockExchangeTokenPublicListDTO;
use App\Feature\StockExchangeToken\StockExchangeTokenRepository;
use App\Infrastructure\OAuth\Service\TokenServiceInterface;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\StockExchange\StockExchangeServiceInterface;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\AutoMapperInterface;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @see GetOrderForManualQuery
 */
class GetOrderForManualQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private AutoMapperInterface $mapper,
        private ValidationService $validator,
        private StockExchangeTokenRepository $stockExchangeTokenRepository,
        private TokenServiceInterface $tokenService,
        private StockExchangeServiceInterface $stockExchangeService
    ) {
    }

    public function __invoke(GetOrderForManualQuery $query): OrderForManualDTO
    {
        $this->validator->validateObject($query);

        /** @var Order $entity */
        $entity = $this->entityManager->find(Order::class, $query->getOrderId());

        /** @var OrderDTO $orderDTO */
        $orderDTO = $this->mapper->map($entity, OrderDTO::class);

        $stockExchangeCode = $entity->getStockExchangeToken()->getStockExchange()->getCode();
        $user = $this->tokenService->getUserByToken();
        $tokens = $this->stockExchangeTokenRepository->getTokensByStockExchangeTypeAndUser($stockExchangeCode, $user);

        /** @var StockExchangeTokenPublicListDTO[] $tokensListDTO */
        $tokensListDTO = $this->mapper->mapMultiple($tokens, StockExchangeTokenPublicListDTO::class);

        $orderForManualDTO = new OrderForManualDTO();
        $orderForManualDTO->setOrder($orderDTO);
        $orderForManualDTO->setTokens($tokensListDTO);

        $actualData = new MarketActualDataDTO();
        $filterData = $this->stockExchangeService->getMarketFiltersData($entity->getStockExchangeToken(), $entity->getMarket());
        $actualData->setFilters($filterData);
        $actualPrice = $this->stockExchangeService->getMarketActualPrice($entity->getStockExchangeToken(), $entity->getMarket());
        $actualData->setActualPrice($actualPrice);
        $orderForManualDTO->setMarketActualData($actualData);

        return $orderForManualDTO;
    }
}
