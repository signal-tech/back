<?php

namespace App\Feature\Order\Query;

use App\Feature\Order\DTO\OrderListDTO;
use App\Feature\Order\DTO\Request\GetOrdersByUserListRequestFilter;
use App\Feature\Order\Order;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Infrastructure\OAuth\Service\TokenServiceInterface;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\ListPaginator\PaginateResponseInterface;
use App\Service\ListPaginator\PaginatorInterface;
use App\Service\Validation\ObjectValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * @see GetOrdersByUserListQuery
 */
class GetOrdersByUserListQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private ObjectValidatorInterface $validator,
        private PaginatorInterface $paginator,
        private TokenServiceInterface $tokenService
    ) {
    }

    public function __invoke(GetOrdersByUserListQuery $query): PaginateResponseInterface
    {
        $this->validator->validateObject($query->getFilter());

        $this->tokenService->getUserByToken();

        $queryBuilder = $this->createQuery($query->getUserId());
        $this->applyQueryFilters($queryBuilder, $query->getFilter());

        return $this->paginator->createResponse(
            $queryBuilder,
            $query->getFilter()->page,
            $query->getFilter()->perPage,
            OrderListDTO::class
        );
    }

    private function createQuery(int $userId): QueryBuilder
    {
        $qb = $this->em->createQueryBuilder()
            ->select('o')
            ->from(Order::class, 'o')
            ->innerJoin(StockExchangeToken::class, 'se', Join::WITH, 'IDENTITY(o.stockExchangeToken) = se.id')
            ->where('IDENTITY(se.user) = :userId')
            ->andWhere('se.deletedAtFromStockExchange IS null')
            ->setParameter('userId', $userId);

        $qb->addOrderBy('o.createdAt', 'DESC')
            ->addOrderBy('o.id', 'DESC');

        return $qb;
    }

    private function applyQueryFilters(QueryBuilder $qb, GetOrdersByUserListRequestFilter $filter): void
    {
        if ($filter->getStockExchangeTokenId()) {
            $qb->andWhere('se.id = :stockExchangeTokenId')
                ->setParameter('stockExchangeTokenId', $filter->getStockExchangeTokenId());
        }
    }
}
