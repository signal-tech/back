<?php

namespace App\Feature\Order\Query;

use App\Feature\Order\DTO\OrderListDTO;
use App\Feature\Order\DTO\Request\GetOrdersBySubscriptionsListRequestFilter;
use App\Feature\Order\Order;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\User\User;
use App\Feature\UserSubscription\UserSubscription;
use App\Infrastructure\OAuth\Service\TokenServiceInterface;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\ListPaginator\PaginateResponseInterface;
use App\Service\ListPaginator\PaginatorInterface;
use App\Service\Validation\ObjectValidatorInterface;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\ORM\QueryBuilder;

/**
 * @see GetOrdersBySubscriptionsListQuery
 */
class GetOrdersBySubscriptionsListQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private ObjectValidatorInterface $validator,
        private PaginatorInterface $paginator,
        private TokenServiceInterface $tokenService
    ) {
    }

    public function __invoke(GetOrdersBySubscriptionsListQuery $query): PaginateResponseInterface
    {
        $this->validator->validateObject($query->getFilter());

        $user = $this->tokenService->getUserByToken();

        $queryBuilder = $this->createQuery($user);
        //$this->applyQueryFilters($queryBuilder, $query->getFilter());

        return $this->paginator->createResponse(
            $queryBuilder,
            $query->getFilter()->page,
            $query->getFilter()->perPage,
            OrderListDTO::class
        );
    }

    private function createQuery(User $user): QueryBuilder
    {
        $qb = $this->em->createQueryBuilder()
            ->select('o')
            ->from(Order::class, 'o')
            ->innerJoin(StockExchangeToken::class, 'se', Join::WITH, 'IDENTITY(o.stockExchangeToken) = se.id')
            ->innerJoin(UserSubscription::class, 'us', Join::WITH, 'se.user = us.user')
            ->andWhere('us.subscriber = :subscriberId')
            ->andWhere('se.deletedAtFromStockExchange IS null')
            ->setParameter('subscriberId', $user->getId());

        $qb->addOrderBy('o.createdAt', 'DESC')
            ->addOrderBy('o.id', 'DESC');

        return $qb;
    }

//    private function applyQueryFilters(QueryBuilder $qb, GetOrdersBySubscriptionsListRequestFilter $filter): void
//    {
//    }
}
