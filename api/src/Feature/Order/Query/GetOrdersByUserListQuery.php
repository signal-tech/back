<?php

namespace App\Feature\Order\Query;

use App\Feature\Order\DTO\Request\GetOrdersByUserListRequestFilter;

/**
 * @see GetOrdersByUserListQueryHandler
 */
class GetOrdersByUserListQuery
{
    private GetOrdersByUserListRequestFilter $filter;

    private int $userId;

    public function __construct(GetOrdersByUserListRequestFilter $filter, int $userId)
    {
        $this->filter = $filter;
        $this->userId = $userId;
    }

    public function getFilter(): GetOrdersByUserListRequestFilter
    {
        return $this->filter;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
}
