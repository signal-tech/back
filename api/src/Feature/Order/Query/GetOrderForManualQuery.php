<?php

namespace App\Feature\Order\Query;

use App\Feature\Order\Order;
use App\Service\Validation\Constraints\EntityExist;

/**
 * @see GetOrderForManualQueryHandler
 */
class GetOrderForManualQuery
{
    /**
     * @EntityExist(targetEntity=Order::class, message="Данная сделка не найдена")
     */
    private int $orderId;

    public function __construct(int $orderId)
    {
        $this->orderId = $orderId;
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }
}
