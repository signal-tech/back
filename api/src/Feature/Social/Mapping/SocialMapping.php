<?php

namespace App\Feature\Social\Mapping;

use App\Feature\Social\DTO\SocialBaseInfoDTO;
use App\Feature\Social\DTO\SocialListDTO;
use App\Feature\Social\Social;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Symfony\Component\HttpFoundation\UrlHelper;

class SocialMapping implements AutoMapperConfiguratorInterface
{
    public function __construct(
        private UrlHelper $urlHelper
    ) {
    }

    /**
     * @throws UnregisteredMappingException
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        $urlHelper = $this->urlHelper;

        $config->registerMapping(Social::class, SocialListDTO::class)
            ->forMember('iconPath', static function (Social $social) use ($urlHelper) {
                return $urlHelper->getAbsoluteUrl('/public/' . $social->getIconPath());
            });
        $config->registerMapping(Social::class, SocialBaseInfoDTO::class)
            ->forMember('iconPath', static function (Social $social) use ($urlHelper) {
                return $urlHelper->getAbsoluteUrl('/public/' . $social->getIconPath());
            });
    }
}
