<?php

namespace App\Feature\Social\Query;

use App\Feature\Social\DTO\SocialListDTO;
use App\Feature\Social\Social;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @see GetSocialListQuery
 */
class GetSocialListQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private AutoMapperInterface $mapper
    ) {
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     *
     * @return mixed
     */
    public function __invoke(GetSocialListQuery $query)
    {
        $repository = $this->em->getRepository(Social::class);

        $array = $repository->findAll();

        return $this->mapper->mapMultiple($array, SocialListDTO::class);
    }
}
