<?php

namespace App\Feature\Social\DTO;

class SocialBaseInfoDTO
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var string
     */
    public $code;

    /**
     * @var string
     */
    public $iconPath;
}
