<?php

namespace App\Feature\MarketHistoricalPrice;

use App\Feature\StockExchange\StockExchange;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

class MarketHistoricalPriceRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, MarketHistoricalPrice::class);
    }

    public function getMarketHistoricalPrice(
        StockExchange $stockExchange,
        int $startTimeStamp,
        string $from,
        string $to
    ): ?MarketHistoricalPrice {
        /** @var MarketHistoricalPrice|null $marketHistoricalPrice */
        $marketHistoricalPrice = $this->findOneBy([
            'fromCoin' => $from,
            'toCoin' => $to,
            'stockExchange' => $stockExchange,
            'priceTime' => $startTimeStamp,
        ]);

        return $marketHistoricalPrice;
    }
}
