<?php

namespace App\Feature\MarketHistoricalPrice;

use App\Feature\StockExchange\StockExchange;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=MarketHistoricalPriceRepository::class)
 *
 * @ORM\Table(name="market_historical_prices")
 * @ORM\HasLifecycleCallbacks
 */
class MarketHistoricalPrice
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     * @ORM\ManyToOne(targetEntity="App\Feature\StockExchange\StockExchange")
     * @ORM\JoinColumn(name="stock_exchange_id", referencedColumnName="id", nullable=false)
     */
    private StockExchange $stockExchange;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $fromCoin;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $toCoin;

    /**
     * @ORM\Column(type="float", nullable=false)
     */
    private float $price;

    /**
     * @ORM\Column(type="bigint", nullable=false)
     */
    private int $priceTime;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getStockExchange(): StockExchange
    {
        return $this->stockExchange;
    }

    public function setStockExchange(StockExchange $stockExchange): self
    {
        $this->stockExchange = $stockExchange;

        return $this;
    }

    public function getFromCoin(): string
    {
        return $this->fromCoin;
    }

    public function setFromCoin(string $fromCoin): self
    {
        $this->fromCoin = $fromCoin;

        return $this;
    }

    public function getToCoin(): string
    {
        return $this->toCoin;
    }

    public function setToCoin(string $toCoin): self
    {
        $this->toCoin = $toCoin;

        return $this;
    }

    public function getPrice(): float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getPriceTime(): int
    {
        return $this->priceTime;
    }

    public function setPriceTime(int $priceTime): self
    {
        $this->priceTime = $priceTime;

        return $this;
    }
}
