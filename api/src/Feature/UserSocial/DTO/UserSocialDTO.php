<?php

namespace App\Feature\UserSocial\DTO;

use App\Feature\Social\DTO\SocialBaseInfoDTO;

class UserSocialDTO
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $link;

    /**
     * @var string
     */
    public $description;

    /**
     * @var SocialBaseInfoDTO
     */
    public $social;
}
