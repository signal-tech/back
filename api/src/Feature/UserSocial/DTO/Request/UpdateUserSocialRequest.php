<?php

namespace App\Feature\UserSocial\DTO\Request;

use App\Feature\Social\Social;
use App\Service\Validation\Constraints\EntityExist;
use Symfony\Component\Validator\Constraints as Assert;

class UpdateUserSocialRequest
{
    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    public $link;

    /**
     * @var string|null
     */
    public $description;

    /**
     * @var int
     *
     * @Assert\NotBlank
     * @EntityExist(targetEntity=Social::class, message="Данная социальная сеть не найдена")
     */
    public $socialId;
}
