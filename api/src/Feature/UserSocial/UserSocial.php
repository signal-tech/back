<?php

namespace App\Feature\UserSocial;

use App\Feature\Social\Social;
use App\Feature\User\User;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=UserSocialRepository::class)
 *
 * @ORM\Table(name="user_socials")
 *
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class UserSocial
{
    use SoftDeleteable;
    use TimestampableEntity;

    /**
     * @var DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     *  Владелец токена.
     *
     * @ORM\ManyToOne(targetEntity="App\Feature\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private User $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    private ?string $description;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $link;

    /**
     *  Владелец токена.
     *
     * @ORM\ManyToOne(targetEntity="App\Feature\Social\Social")
     * @ORM\JoinColumn(name="social_id", referencedColumnName="id", nullable=false)
     */
    private Social $social;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getLink(): string
    {
        return $this->link;
    }

    public function setLink(string $link): self
    {
        $this->link = $link;

        return $this;
    }

    public function getSocial(): Social
    {
        return $this->social;
    }

    public function setSocial(Social $social): self
    {
        $this->social = $social;

        return $this;
    }
}
