<?php

namespace App\Feature\UserSocial\Command;

use App\Feature\UserSocial\DTO\Request\UpdateUserSocialRequest;
use App\Feature\UserSocial\UserSocial;
use App\Service\Validation\Constraints\EntityExist;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @see UpdateUserSocialCommandHandler
 */
class UpdateUserSocialCommand
{
    /**
     * @EntityExist(targetEntity=UserSocial::class, message="Данной сущности не найдено")
     */
    private int $userSocialId;

    /**
     * @Assert\Valid
     */
    private UpdateUserSocialRequest $updateUserSocialRequest;

    public function __construct(
        UpdateUserSocialRequest $updateUserSocialRequest,
        int $userSocialId
    ) {
        $this->userSocialId = $userSocialId;
        $this->updateUserSocialRequest = $updateUserSocialRequest;
    }

    public function getUpdateUserSocialRequest(): UpdateUserSocialRequest
    {
        return $this->updateUserSocialRequest;
    }

    public function getUserSocialId(): int
    {
        return $this->userSocialId;
    }
}
