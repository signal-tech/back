<?php

namespace App\Feature\UserSocial\Command;

use App\Feature\UserSocial\DTO\Request\CreateUserSocialRequest;

/**
 * @see CreateUserSocialCommandHandler
 */
class CreateUserSocialCommand
{
    public function __construct(
        private CreateUserSocialRequest $createUserSocialRequest
    ) {
    }

    public function getCreateUserSocialRequest(): CreateUserSocialRequest
    {
        return $this->createUserSocialRequest;
    }
}
