<?php

namespace App\Feature\UserSocial\Command;

use App\Feature\UserSocial\UserSocial;
use App\Service\Validation\Constraints\EntityExist;

/**
 * @see DeleteUserSocialCommandHandler
 */
class DeleteUserSocialCommand
{
    /**
     * @EntityExist(targetEntity=UserSocial::class, message="Данной сущности не найдено")
     */
    private int $userSocial;

    public function __construct(
        int $userSocial
    ) {
        $this->userSocial = $userSocial;
    }

    public function getUserSocial(): int
    {
        return $this->userSocial;
    }
}
