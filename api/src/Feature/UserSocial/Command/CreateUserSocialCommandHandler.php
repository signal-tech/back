<?php

namespace App\Feature\UserSocial\Command;

use App\Feature\UserSocial\UserSocial;
use App\Infrastructure\OAuth\Service\TokenService;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\Validation\ValidationException;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @see CreateUserSocialCommand
 */
class CreateUserSocialCommandHandler extends AbstractAuthorizedAccessible
{
    private EntityManagerInterface $em;

    private ValidationService $validator;

    private TokenService $tokenService;

    private AutoMapperInterface $mapper;

    public function __construct(
        EntityManagerInterface $em,
        ValidationService $validator,
        AutoMapperInterface $mapper,
        TokenService $tokenService
    ) {
        $this->em = $em;
        $this->validator = $validator;
        $this->tokenService = $tokenService;
        $this->mapper = $mapper;
    }

    /**
     * @throws UnregisteredMappingException
     * @throws ValidationException
     */
    public function __invoke(CreateUserSocialCommand $command): int
    {
        $this->validator->validateObject($command->getCreateUserSocialRequest());

        /** @var UserSocial $entity */
        $entity = $this->mapper->map($command->getCreateUserSocialRequest(), UserSocial::class);

        $user = $this->tokenService->getUserByToken();

        $entity->setUser($user);

        $this->em->persist($entity);
        $this->em->flush();

        $this->em->refresh($entity);

        return $entity->getId();
    }
}
