<?php

namespace App\Feature\UserSocial\Command;

use App\Feature\UserSocial\UserSocial;
use App\Infrastructure\OAuth\Service\TokenServiceInterface;
use App\Service\Validation\ValidationException;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @see DeleteUserSocialCommand
 */
class DeleteUserSocialCommandHandler
{
    private EntityManagerInterface $em;

    private ValidationService $validator;

    private TokenServiceInterface $tokenService;

    public function __construct(
        EntityManagerInterface $em,
        ValidationService $validator,
        TokenServiceInterface $tokenService
    ) {
        $this->em = $em;
        $this->validator = $validator;
        $this->tokenService = $tokenService;
    }

    /**
     * @throws UnregisteredMappingException
     * @throws ValidationException
     */
    public function __invoke(DeleteUserSocialCommand $command): int
    {
        $this->validator->validateObject($command);

        $repository = $this->em->getRepository(UserSocial::class);

        $user = $this->tokenService->getUserByToken();

        /** @var UserSocial|null $entity */
        $entity = $repository->findOneBy(['id' => $command->getUserSocial(), 'user' => $user]);

        if (null !== $entity) {
            $id = $entity->getId();
            $this->em->remove($entity);

            $this->em->flush();

            return $id;
        }

        return 0;
    }
}
