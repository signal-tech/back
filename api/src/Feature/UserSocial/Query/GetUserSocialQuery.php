<?php

namespace App\Feature\UserSocial\Query;

use App\Feature\UserSocial\UserSocial;
use App\Service\Validation\Constraints\EntityExist;

/**
 * @see GetUserSocialQueryHandler
 */
class GetUserSocialQuery
{
    /**
     * @EntityExist(targetEntity=UserSocial::class, message="Данной сущности не найдено")
     */
    private int $userSocialId;

    public function __construct(int $userSocialId)
    {
        $this->userSocialId = $userSocialId;
    }

    public function getUserSocialId(): int
    {
        return $this->userSocialId;
    }
}
