<?php

namespace App\Feature\UserSocial\Query;

use App\Feature\UserSocial\DTO\UserSocialListDTO;
use App\Feature\UserSocial\UserSocial;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\Validation\ObjectValidatorInterface;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @see GetUserSocialListQuery
 */
class GetUserSocialListQueryHandler extends AbstractAuthorizedAccessible
{
    private EntityManagerInterface $em;

    private AutoMapperInterface $mapper;

    public function __construct(
        EntityManagerInterface $entityManager,
        AutoMapperInterface $mapper,
        private ObjectValidatorInterface $validator,
    ) {
        $this->em = $entityManager;
        $this->mapper = $mapper;
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     *
     * @return mixed
     */
    public function __invoke(GetUserSocialListQuery $query)
    {
        $this->validator->validateObject($query);

        $userSocialRepository = $this->em->getRepository(UserSocial::class);

        $array = $userSocialRepository->findBy(['user' => $query->getUserId()]);

        return $this->mapper->mapMultiple($array, UserSocialListDTO::class);
    }
}
