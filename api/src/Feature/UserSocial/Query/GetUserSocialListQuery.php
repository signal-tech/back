<?php

namespace App\Feature\UserSocial\Query;

use App\Service\Validation\Constraints\UserRolesCheck;

/**
 * @see GetUserSocialListQueryHandler
 */
class GetUserSocialListQuery
{
    /**
     * @UserRolesCheck
     */
    private int $userId;

    /**
     * GetUserSocialListQuery constructor.
     */
    public function __construct(
        int $userId
    ) {
        $this->userId = $userId;
    }

    public function getUserId(): int
    {
        return $this->userId;
    }
}
