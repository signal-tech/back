<?php

namespace App\Feature\UserSocial\Query;

use App\Feature\UserSocial\DTO\UserSocialDTO;
use App\Feature\UserSocial\UserSocial;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @see GetUserSocialQuery
 */
class GetUserSocialQueryHandler extends AbstractAuthorizedAccessible
{
    private EntityManagerInterface $entityManager;

    private AutoMapperInterface $mapper;

    private ValidationService $validator;

    public function __construct(
        EntityManagerInterface $entityManager,
        AutoMapperInterface $mapper,
        ValidationService $validator
    ) {
        $this->validator = $validator;
        $this->entityManager = $entityManager;
        $this->mapper = $mapper;
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     *
     * @return mixed
     */
    public function __invoke(GetUserSocialQuery $query)
    {
        $this->validator->validateObject($query);

        /** @var UserSocial $entity */
        $entity = $this->entityManager->find(UserSocial::class, $query->getUserSocialId());

        return $this->mapper->map($entity, UserSocialDTO::class);
    }
}
