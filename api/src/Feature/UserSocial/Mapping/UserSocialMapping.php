<?php

namespace App\Feature\UserSocial\Mapping;

use App\Feature\Social\DTO\SocialBaseInfoDTO;
use App\Feature\Social\Social;
use App\Feature\Social\SocialRepository;
use App\Feature\UserSocial\DTO\Request\CreateUserSocialRequest;
use App\Feature\UserSocial\DTO\Request\UpdateUserSocialRequest;
use App\Feature\UserSocial\DTO\UserSocialDTO;
use App\Feature\UserSocial\DTO\UserSocialListDTO;
use App\Feature\UserSocial\UserSocial;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;

class UserSocialMapping implements AutoMapperConfiguratorInterface
{
    private EntityManagerInterface $em;

    public function __construct(
        EntityManagerInterface $em,
        private AutoMapperInterface $mapper
    ) {
        $this->em = $em;
    }

    /**
     * @throws UnregisteredMappingException
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        /**
         * @var SocialRepository $socialRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $socialRepository = $this->em->getRepository(Social::class);
        $mapper = $this->mapper;

        $config->registerMapping(CreateUserSocialRequest::class, UserSocial::class)
            ->forMember('social', static function (CreateUserSocialRequest $request) use ($socialRepository): UserSocial {
                /** @var UserSocial $social */
                $social = $socialRepository->find($request->socialId);

                return $social;
            });

        $config->registerMapping(UpdateUserSocialRequest::class, UserSocial::class)
            ->forMember('social', static function (UpdateUserSocialRequest $request) use ($socialRepository): UserSocial {
                /** @var UserSocial $social */
                $social = $socialRepository->find($request->socialId);

                return $social;
            });

        $config->registerMapping(UserSocial::class, UserSocialDTO::class)
            ->forMember('social', static function (UserSocial $userSocial) use ($mapper) {
                /** @var SocialBaseInfoDTO $social */
                $social = $mapper->map($userSocial->getSocial(), SocialBaseInfoDTO::class);

                return $social;
            });
        $config->registerMapping(UserSocial::class, UserSocialListDTO::class)
            ->forMember('social', static function (UserSocial $userSocial) use ($mapper) {
                /** @var SocialBaseInfoDTO $social */
                $social = $mapper->map($userSocial->getSocial(), SocialBaseInfoDTO::class);

                return $social;
            });
    }
}
