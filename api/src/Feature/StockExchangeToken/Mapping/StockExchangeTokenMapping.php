<?php

namespace App\Feature\StockExchangeToken\Mapping;

use App\Feature\StockExchange\DTO\StockExchangeBaseInfoDTO;
use App\Feature\StockExchange\StockExchange;
use App\Feature\StockExchangeToken\DTO\Request\CreateStockExchangeTokenRequest;
use App\Feature\StockExchangeToken\DTO\StockExchangeTokenDTO;
use App\Feature\StockExchangeToken\DTO\StockExchangeTokenListDTO;
use App\Feature\StockExchangeToken\DTO\StockExchangeTokenPublicListDTO;
use App\Feature\StockExchangeToken\StockExchangeToken;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;

class StockExchangeTokenMapping implements AutoMapperConfiguratorInterface
{
    private EntityManagerInterface $em;

    public function __construct(
        EntityManagerInterface $em,
        private AutoMapperInterface $mapper
    ) {
        $this->em = $em;
    }

    /**
     * @throws UnregisteredMappingException
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        $stockExchangeRepository = $this->em->getRepository(StockExchange::class);
        $mapper = $this->mapper;

        $config->registerMapping(CreateStockExchangeTokenRequest::class, StockExchangeToken::class)
            ->forMember('stockExchange', static function (CreateStockExchangeTokenRequest $request) use ($stockExchangeRepository): StockExchange {
                /** @var StockExchange $stockExchange */
                $stockExchange = $stockExchangeRepository->find($request->stockExchangeId);

                return $stockExchange;
            });

        $config->registerMapping(StockExchangeToken::class, StockExchangeTokenDTO::class)
            ->forMember('stockExchange', static function (StockExchangeToken $stockExchangeToken) use ($mapper): StockExchangeBaseInfoDTO {
                /** @var StockExchangeBaseInfoDTO $stockEchange */
                $stockEchange = $mapper->map($stockExchangeToken->getStockExchange(), StockExchangeBaseInfoDTO::class);

                return $stockEchange;
            });
        $config->registerMapping(StockExchangeToken::class, StockExchangeTokenListDTO::class)
            ->forMember('stockExchange', static function (StockExchangeToken $stockExchangeToken) use ($mapper): StockExchangeBaseInfoDTO {
                /** @var StockExchangeBaseInfoDTO $stockEchange */
                $stockEchange = $mapper->map($stockExchangeToken->getStockExchange(), StockExchangeBaseInfoDTO::class);

                return $stockEchange;
            })
            ->forMember('deletedFromStockExchange', static function (StockExchangeToken $stockExchangeToken): bool {
                return null !== $stockExchangeToken->getDeletedAtFromStockExchange();
            });
        $config->registerMapping(StockExchangeToken::class, StockExchangeTokenPublicListDTO::class)
            ->forMember('stockExchange', static function (StockExchangeToken $stockExchangeToken) use ($mapper): StockExchangeBaseInfoDTO {
                /** @var StockExchangeBaseInfoDTO $stockEchange */
                $stockEchange = $mapper->map($stockExchangeToken->getStockExchange(), StockExchangeBaseInfoDTO::class);

                return $stockEchange;
            });
    }
}
