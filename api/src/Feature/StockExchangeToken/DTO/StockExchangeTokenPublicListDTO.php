<?php

namespace App\Feature\StockExchangeToken\DTO;

use App\Feature\StockExchange\DTO\StockExchangeBaseInfoDTO;

class StockExchangeTokenPublicListDTO
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $name;

    /**
     * @var StockExchangeBaseInfoDTO
     */
    public $stockExchange;
}
