<?php

namespace App\Feature\StockExchangeToken\DTO;

use App\Feature\StockExchange\DTO\StockExchangeBaseInfoDTO;

class StockExchangeTokenDTO
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var string
     */
    public $apiKey;

    /**
     * @var string
     */
    public $name;

    /**
     * @var StockExchangeBaseInfoDTO
     */
    public $stockExchange;
}
