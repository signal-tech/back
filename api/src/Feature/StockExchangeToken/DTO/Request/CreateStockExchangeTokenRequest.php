<?php

namespace App\Feature\StockExchangeToken\DTO\Request;

use App\Feature\StockExchange\StockExchange;
use App\Service\Validation\Constraints\EntityExist;
use Symfony\Component\Validator\Constraints as Assert;

class CreateStockExchangeTokenRequest
{
    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    public $apiKey;

    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    public $secret;

    /**
     * @var string
     *
     * @Assert\NotBlank
     */
    public $name;

    /**
     * @var int
     *
     * @Assert\NotBlank
     * @EntityExist(targetEntity=StockExchange::class, message="Данная биржа не найдена")
     */
    public $stockExchangeId;
}
