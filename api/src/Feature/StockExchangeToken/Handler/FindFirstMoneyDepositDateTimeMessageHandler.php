<?php

namespace App\Feature\StockExchangeToken\Handler;

use App\Feature\Portfolio\Message\CalculatePortfolioMessage;
use App\Feature\StockExchangeToken\Message\FindFirstMoneyDepositDateTimeMessage;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\StockExchangeToken\StockExchangeTokenRepository;
use App\Service\StockExchange\StockExchangeServiceInterface;
use App\Service\Time\TimeServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class FindFirstMoneyDepositDateTimeMessageHandler implements MessageHandlerInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private StockExchangeServiceInterface $stockExchangeService,
        private TimeServiceInterface $timeService,
        private MessageBusInterface $messageBus
    ) {
    }

    public function __invoke(FindFirstMoneyDepositDateTimeMessage $message): void
    {
        /**
         * @var StockExchangeTokenRepository $stockExchangeTokenRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);

        $token = $stockExchangeTokenRepository->find($message->getTokenId());

        if (null === $token) {
            return;
        }

        $time = $this->stockExchangeService->getFirstMoneyDepositDateTime($token);

        if (null !== $time) {
            $token->setFirstMoneyDepositDateTime($time);

            $this->em->persist($token);
            $this->em->flush();

            $startAndTimeDTO = $this->timeService->getStartAndEndDayByTime($time);

            $this->messageBus->dispatch(new CalculatePortfolioMessage($token->getId(), $startAndTimeDTO->getStartTime(), $startAndTimeDTO->getEndTime()));
        }
    }
}
