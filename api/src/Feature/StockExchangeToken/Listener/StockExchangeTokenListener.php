<?php

namespace App\Feature\StockExchangeToken\Listener;

use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Crypt\CryptServiceInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Psr\Log\LoggerInterface;
use Throwable;

class StockExchangeTokenListener
{
    public function __construct(
        private LoggerInterface $logger,
        private CryptServiceInterface $cryptService
    ) {
    }

    public function prePersist(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof StockExchangeToken) {
            $this->encryptFields($entity);
        }
    }

    public function preUpdate(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof StockExchangeToken) {
            $this->encryptFields($entity);
        }
    }

    public function postLoad(LifecycleEventArgs $args): void
    {
        $entity = $args->getObject();

        if ($entity instanceof StockExchangeToken) {
            $this->decryptFields($entity);
        }
    }

    public function decryptFields(StockExchangeToken $token): void
    {
        try {
            $apiKey = $this->cryptService->decrypt($token->getApiKey());
            $secretKey = $this->cryptService->decrypt($token->getSecret());
        } catch (Throwable $e) {
            $this->logger->critical(
                'Unable to decrypt field "token data" in Token entity for ID: ' . $token->getId(),
                [
                    'error' => $e->getMessage(),
                ]
            );

            throw $e;
        }

        $token->setApiKey($apiKey);
        $token->setSecret($secretKey);
    }

    private function encryptFields(StockExchangeToken $token): void
    {
        try {
            $apiKey = $this->cryptService->encrypt($token->getApiKey());
            $secretKey = $this->cryptService->encrypt($token->getSecret());
        } catch (Throwable $e) {
            $this->logger->critical(
                'Unable to encrypt field "token data"',
                [
                    'error' => $e->getMessage(),
                ]
            );

            throw $e;
        }

        // Set the entity variables
        $token->setApiKey($apiKey);
        $token->setSecret($secretKey);
    }
}
