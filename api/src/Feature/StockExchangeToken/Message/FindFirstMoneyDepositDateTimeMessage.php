<?php

namespace App\Feature\StockExchangeToken\Message;

class FindFirstMoneyDepositDateTimeMessage
{
    private int $tokenId;

    public function __construct(int $tokenId)
    {
        $this->tokenId = $tokenId;
    }

    public function getTokenId(): int
    {
        return $this->tokenId;
    }
}
