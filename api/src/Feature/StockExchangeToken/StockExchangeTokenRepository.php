<?php

namespace App\Feature\StockExchangeToken;

use App\Feature\StockExchange\StockExchange;
use App\Feature\User\User;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\Query\Expr\Join;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method StockExchangeToken|null find($id, $lockMode = null, $lockVersion = null)
 * @method StockExchangeToken|null findOneBy(array $criteria, array $orderBy = null)
 * @method StockExchangeToken[]    findAll()
 * @method StockExchangeToken[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @psalm-suppress QueryBuilderSetParameter
 */
class StockExchangeTokenRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, StockExchangeToken::class);
    }

    /**
     * @return StockExchangeToken[]
     */
    public function getTokensByStockExchangeType(
        array $typesList,
        bool $withoutConnect = false,
        bool $firstActionsDatesFound = false
    ): array {
        $qb = $this->createQueryBuilder('t')
            ->innerJoin(StockExchange::class, 'se', Join::WITH, 'se.id = t.stockExchange')
            ->andWhere('se.code IN (:typesList)')
            ->andWhere('t.deletedAtFromStockExchange IS null')
            ->setParameter('typesList', $typesList);

        if ($withoutConnect) {
            $qb->andWhere('t.lastListeningChannelConnect IS null');
        }

        if ($firstActionsDatesFound) {
            $qb->andWhere('t.firstMoneyDepositDateTime IS NOT null');
        }

        /** @var StockExchangeToken[] $returnData */
        $returnData = $qb
            ->getQuery()
            ->getResult();

        return $returnData;
    }

    public function getTokensByStockExchangeTypeAndUser(
        string $stockExchangeCode,
        User $user
    ): array {
        $qb = $this->createQueryBuilder('t')
            ->innerJoin(StockExchange::class, 'se', Join::WITH, 'se.id = t.stockExchange')
            ->andWhere('se.code = :stockExchangeType')
            ->andWhere('t.user = :user')
            ->andWhere('t.deletedAtFromStockExchange IS null')
            ->setParameter('stockExchangeType', $stockExchangeCode)
            ->setParameter('user', $user);

        /** @var StockExchangeToken[] $returnData */
        $returnData = $qb
            ->getQuery()
            ->getResult();

        return $returnData;
    }

    public function getTokensByUser(
        User $user
    ): array {
        $qb = $this->createQueryBuilder('t')
            ->andWhere('t.user = :user')
            ->andWhere('t.deletedAtFromStockExchange IS null')
            ->setParameter('user', $user);

        /** @var StockExchangeToken[] $returnData */
        $returnData = $qb
            ->getQuery()
            ->getResult();

        return $returnData;
    }
}
