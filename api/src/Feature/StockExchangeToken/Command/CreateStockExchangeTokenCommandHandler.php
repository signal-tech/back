<?php

namespace App\Feature\StockExchangeToken\Command;

use App\Feature\StockExchangeToken\Message\FindFirstMoneyDepositDateTimeMessage;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Infrastructure\OAuth\Service\TokenService;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\StockExchange\StockExchangeServiceInterface;
use App\Service\Validation\ValidationException;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;
use Symfony\Component\Messenger\MessageBusInterface;
use Throwable;

/**
 * @see CreateStockExchangeTokenCommand
 */
class CreateStockExchangeTokenCommandHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private ValidationService $validator,
        private AutoMapperInterface $mapper,
        private TokenService $tokenService,
        private MessageBusInterface $messageBus,
        private StockExchangeServiceInterface $stockExchangeService
    ) {
    }

    /**
     * @throws UnregisteredMappingException
     * @throws ValidationException
     */
    public function __invoke(CreateStockExchangeTokenCommand $command): int
    {
        $this->validator->validateObject($command->getCreateStockExchangeTokenRequest());

        try {
            $this->em->beginTransaction();

            /** @var StockExchangeToken $entity */
            $entity = $this->mapper->map($command->getCreateStockExchangeTokenRequest(), StockExchangeToken::class);

            $user = $this->tokenService->getUserByToken();

            $entity->setUser($user);

            $this->em->persist($entity);
            $this->em->flush();
            $this->em->refresh($entity);

            $tokenIsValid = $this->stockExchangeService->isTokenValid($entity);

            if (!$tokenIsValid) {
                throw new InvalidArgumentException('Токен не действителен для данной биржи');
            }

            $this->messageBus->dispatch(new FindFirstMoneyDepositDateTimeMessage($entity->getId()));

            $this->em->commit();
        } catch (Throwable $throwable) {
            $this->em->rollback();

            throw $throwable;
        }

        return $entity->getId();
    }
}
