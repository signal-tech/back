<?php

namespace App\Feature\StockExchangeToken\Command;

use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\StockExchangeToken\StockExchangeTokenRepository;
use App\Infrastructure\OAuth\Service\TokenServiceInterface;
use App\Service\Validation\ValidationException;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;

/**
 * @see DeleteStockExchangeTokenCommand
 */
class DeleteStockExchangeTokenCommandHandler
{
    public function __construct(
        private EntityManagerInterface $em,
        private ValidationService $validator,
        private TokenServiceInterface $tokenService
    ) {
    }

    /**
     * @throws UnregisteredMappingException
     * @throws ValidationException
     */
    public function __invoke(DeleteStockExchangeTokenCommand $command): int
    {
        $this->validator->validateObject($command);

        /**
         * @var StockExchangeTokenRepository $tokenRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $tokenRepository = $this->em->getRepository(StockExchangeToken::class);

        $user = $this->tokenService->getUserByToken();

        $token = $tokenRepository->findOneBy(['id' => $command->getTokenId(), 'user' => $user]);

        if (null !== $token) {
            $id = $token->getId();
            $this->em->remove($token);

            $this->em->flush();

            return $id;
        }

        return 0;
    }
}
