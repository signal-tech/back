<?php

namespace App\Feature\StockExchangeToken\Command;

use App\Feature\StockExchangeToken\DTO\Request\CreateStockExchangeTokenRequest;

/**
 * @see CreateStockExchangeTokenCommandHandler
 */
class CreateStockExchangeTokenCommand
{
    private CreateStockExchangeTokenRequest $createStockExchangeTokenRequest;

    public function __construct(CreateStockExchangeTokenRequest $createStockExchangeTokenRequest)
    {
        $this->createStockExchangeTokenRequest = $createStockExchangeTokenRequest;
    }

    public function getCreateStockExchangeTokenRequest(): CreateStockExchangeTokenRequest
    {
        return $this->createStockExchangeTokenRequest;
    }
}
