<?php

namespace App\Feature\StockExchangeToken\Command;

use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Validation\Constraints\EntityExist;

/**
 * @see DeleteStockExchangeTokenCommandHandler
 */
class DeleteStockExchangeTokenCommand
{
    /**
     * @EntityExist(targetEntity=StockExchangeToken::class, message="Данного токена не существует")
     */
    private int $tokenId;

    public function __construct(int $tokenId)
    {
        $this->tokenId = $tokenId;
    }

    public function getTokenId(): int
    {
        return $this->tokenId;
    }
}
