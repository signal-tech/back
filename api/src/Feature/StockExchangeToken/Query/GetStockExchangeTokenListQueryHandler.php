<?php

namespace App\Feature\StockExchangeToken\Query;

use App\Feature\StockExchangeToken\DTO\StockExchangeTokenListDTO;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Infrastructure\OAuth\Service\TokenService;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @see GetStockExchangeTokenListQuery
 */
class GetStockExchangeTokenListQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private AutoMapperInterface $mapper,
        private TokenService $tokenService
    ) {
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     *
     * @return mixed
     */
    public function __invoke(GetStockExchangeTokenListQuery $query)
    {
        $stockExchangeRepository = $this->em->getRepository(StockExchangeToken::class);

        $user = $this->tokenService->getUserByToken();

        $array = $stockExchangeRepository->findBy(['user' => $user]);

        return $this->mapper->mapMultiple($array, StockExchangeTokenListDTO::class);
    }
}
