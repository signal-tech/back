<?php

namespace App\Feature\StockExchangeToken\Query;

/**
 * @see GetStockExchangeTokenPublicListQueryHandler
 */
class GetStockExchangeTokenPublicListQuery
{
    public function __construct(
        private ?int $userId = null
    ) {
    }

    public function getUserId(): ?int
    {
        return $this->userId;
    }
}
