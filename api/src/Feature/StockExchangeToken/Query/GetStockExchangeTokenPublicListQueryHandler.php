<?php

namespace App\Feature\StockExchangeToken\Query;

use App\Feature\StockExchangeToken\DTO\StockExchangeTokenPublicListDTO;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\StockExchangeToken\StockExchangeTokenRepository;
use App\Feature\User\User;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @see GetStockExchangeTokenPublicListQuery
 */
class GetStockExchangeTokenPublicListQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private AutoMapperInterface $mapper
    ) {
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     *
     * @return mixed
     */
    public function __invoke(GetStockExchangeTokenPublicListQuery $query)
    {
        /**
         * @var StockExchangeTokenRepository $stockExchangeTokenRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);

        $userRepository = $this->em->getRepository(User::class);

        /** @var User $user */
        $user = $userRepository->find($query->getUserId());

        $array = $stockExchangeTokenRepository->getTokensByUser($user);

        return $this->mapper->mapMultiple($array, StockExchangeTokenPublicListDTO::class);
    }
}
