<?php

namespace App\Feature\StockExchangeToken\Query;

use App\Feature\StockExchangeToken\DTO\StockExchangeTokenDTO;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\User\User;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;
use RuntimeException;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;

/**
 * @see GetPriceListElementQuery
 */
class GetStockExchangeTokenQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private AutoMapperInterface $mapper,
        private ValidationService $validator,
        private TokenStorageInterface $tokenStorage
    ) {
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     *
     * @return mixed
     */
    public function __invoke(GetStockExchangeTokenQuery $query)
    {
        $this->validator->validateObject($query);

        /** @var TokenInterface $token */
        $token = $this->tokenStorage->getToken();

        /** @var User $user */
        $user = $token->getUser();

        /** @var StockExchangeToken $entity */
        $entity = $this->entityManager->find(StockExchangeToken::class, $query->getStockExchangeTokenId());

        if ($entity->getUser()->getId() !== $user->getId()) {
            throw new RuntimeException('Токен вам не принадлежит');
        }

        return $this->mapper->map($entity, StockExchangeTokenDTO::class);
    }
}
