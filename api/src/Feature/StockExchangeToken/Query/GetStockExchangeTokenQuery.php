<?php

namespace App\Feature\StockExchangeToken\Query;

use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Validation\Constraints\EntityExist;

/**
 * @see GetStockExchangeTokenQueryHandler
 */
class GetStockExchangeTokenQuery
{
    /**
     * @EntityExist(targetEntity=StockExchangeToken::class, message="Данный токен не найден")
     */
    private int $stockExchangeTokenId;

    public function __construct(int $stockExchangeTokenId)
    {
        $this->stockExchangeTokenId = $stockExchangeTokenId;
    }

    public function getStockExchangeTokenId(): int
    {
        return $this->stockExchangeTokenId;
    }
}
