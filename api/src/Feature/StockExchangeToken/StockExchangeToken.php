<?php

namespace App\Feature\StockExchangeToken;

use App\Feature\StockExchange\StockExchange;
use App\Feature\User\User;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=StockExchangeTokenRepository::class)
 *
 * @ORM\Table(name="stock_exchange_tokens")
 *
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class StockExchangeToken
{
    use SoftDeleteable;
    use TimestampableEntity;

    /**
     * @var DateTime
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected ?DateTime $lastListeningChannelConnect;

    /**
     * @ORM\Column(type="bigint", nullable=true)
     */
    protected int $firstMoneyDepositDateTime;

    /**
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected ?DateTime $deletedAtFromStockExchange;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     *  Владелец токена.
     *
     * @ORM\ManyToOne(targetEntity="App\Feature\User\User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    private User $user;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $apiKey;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $secret;

    /**
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    private string $name;

    /**
     * @ORM\ManyToOne(targetEntity="App\Feature\StockExchange\StockExchange")
     * @ORM\JoinColumn(name="stock_exchange_id", referencedColumnName="id", nullable=false)
     */
    private StockExchange $stockExchange;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): void
    {
        $this->id = $id;
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function setUser(User $user): void
    {
        $this->user = $user;
    }

    public function getApiKey(): string
    {
        return $this->apiKey;
    }

    public function setApiKey(string $apiKey): void
    {
        $this->apiKey = $apiKey;
    }

    public function getSecret(): string
    {
        return $this->secret;
    }

    public function setSecret(string $secret): void
    {
        $this->secret = $secret;
    }

    public function getStockExchange(): StockExchange
    {
        return $this->stockExchange;
    }

    public function setStockExchange(StockExchange $stockExchange): void
    {
        $this->stockExchange = $stockExchange;
    }

    /**
     * @return DateTime
     */
    public function getLastListeningChannelConnect(): ?DateTime
    {
        return $this->lastListeningChannelConnect;
    }

    /**
     * @param DateTime $lastListeningChannelConnect
     */
    public function setLastListeningChannelConnect(?DateTime $lastListeningChannelConnect): void
    {
        $this->lastListeningChannelConnect = $lastListeningChannelConnect;
    }

    public function getFirstMoneyDepositDateTime(): int
    {
        return $this->firstMoneyDepositDateTime;
    }

    public function setFirstMoneyDepositDateTime(int $firstMoneyDepositDateTime): void
    {
        $this->firstMoneyDepositDateTime = $firstMoneyDepositDateTime;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getDeletedAtFromStockExchange(): ?DateTime
    {
        return $this->deletedAtFromStockExchange;
    }

    public function setDeletedAtFromStockExchange(DateTime $deletedAtFromStockExchange): self
    {
        $this->deletedAtFromStockExchange = $deletedAtFromStockExchange;

        return $this;
    }
}
