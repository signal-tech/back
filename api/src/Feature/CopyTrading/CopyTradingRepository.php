<?php

namespace App\Feature\CopyTrading;

use App\Feature\StockExchangeToken\StockExchangeToken;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CopyTrading|null find($id, $lockMode = null, $lockVersion = null)
 * @method CopyTrading|null findOneBy(array $criteria, array $orderBy = null)
 * @method CopyTrading[]    findAll()
 * @method CopyTrading[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 *
 * @psalm-suppress QueryBuilderSetParameter
 */
class CopyTradingRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CopyTrading::class);
    }

    /**
     * @return iterable<mixed, CopyTrading>
     */
    public function getCopyTradingsListByParent(StockExchangeToken $stockExchangeToken): iterable
    {
        $qb = $this->createQueryBuilder('c')
            ->where('c.parentToken = :parentToken')
            ->setParameter('parentToken', $stockExchangeToken)
            ->orderBy('c.id', 'ASC');

        /** @var iterable<mixed, CopyTrading> $returnData */
        $returnData = $qb->getQuery()->toIterable();

        return $returnData;
    }
}
