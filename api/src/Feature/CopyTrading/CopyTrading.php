<?php

namespace App\Feature\CopyTrading;

use App\Feature\StockExchangeToken\StockExchangeToken;
use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Mapping\Annotation as Gedmo;
use Gedmo\SoftDeleteable\Traits\SoftDeleteable;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=CopyTradingRepository::class)
 *
 * @ORM\Table(name="copy_tradings")
 *
 * @ORM\HasLifecycleCallbacks
 * @Gedmo\SoftDeleteable(fieldName="deletedAt", timeAware=false)
 */
class CopyTrading
{
    use SoftDeleteable;
    use TimestampableEntity;

    /**
     * @var DateTime|null
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $deletedAt;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     *  Родительский токен за которым следуем
     *
     * @ORM\ManyToOne(targetEntity="App\Feature\StockExchangeToken\StockExchangeToken")
     * @ORM\JoinColumn(name="parent_stock_exchange_token_id", referencedColumnName="id", nullable=false)
     */
    private StockExchangeToken $parentToken;

    /**
     *  Токен который повторяет сделки за родительским
     *
     * @ORM\ManyToOne(targetEntity="App\Feature\StockExchangeToken\StockExchangeToken")
     * @ORM\JoinColumn(name="child_stock_exchange_token_id", referencedColumnName="id", nullable=false)
     */
    private StockExchangeToken $childToken;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getParentToken(): StockExchangeToken
    {
        return $this->parentToken;
    }

    public function setParentToken(StockExchangeToken $parentToken): self
    {
        $this->parentToken = $parentToken;

        return $this;
    }

    public function getChildToken(): StockExchangeToken
    {
        return $this->childToken;
    }

    public function setChildToken(StockExchangeToken $childToken): self
    {
        $this->childToken = $childToken;

        return $this;
    }
}
