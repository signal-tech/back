<?php

namespace App\Feature\CopyTrading\Handler;

use App\Feature\CopyTrading\CopyTradingRepository;
use App\Feature\CopyTrading\Message\CopyTradingMessage;
use App\Feature\Order\OrderRepository;
use App\Service\Feature\CopyTrading\CopyTradingServiceInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class CopyTradingMessageHandler implements MessageHandlerInterface
{
    public function __construct(
        private OrderRepository $orderRepository,
        private CopyTradingRepository $copyTradingRepository,
        private CopyTradingServiceInterface $copyTradingService
    ) {
    }

    public function __invoke(CopyTradingMessage $copyTradingMessage): void
    {
        $order = $this->orderRepository->find($copyTradingMessage->getOrderId());

        if (null === $order) {
            return;
        }

        $copyTrading = $this->copyTradingRepository->find($copyTradingMessage->getCopyTradingId());

        if (null === $copyTrading) {
            return;
        }

        $this->copyTradingService->copyTrade($order, $copyTrading);
    }
}
