<?php

namespace App\Feature\CopyTrading\Query;

use App\Feature\CopyTrading\CopyTrading;
use App\Service\Validation\Constraints\EntityExist;

/**
 * @see GetCopyTradingQueryHandler
 */
class GetCopyTradingQuery
{
    /**
     * @EntityExist(targetEntity=CopyTrading::class, message="Копирование сделок не найдено")
     */
    private int $copyTradingId;

    public function __construct(int $copyTradingId)
    {
        $this->copyTradingId = $copyTradingId;
    }

    public function getCopyTradingId(): int
    {
        return $this->copyTradingId;
    }
}
