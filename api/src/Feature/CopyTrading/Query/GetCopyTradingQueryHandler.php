<?php

namespace App\Feature\CopyTrading\Query;

use App\Feature\CopyTrading\CopyTrading;
use App\Feature\CopyTrading\DTO\CopyTradingDTO;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityNotFoundException;

/**
 * @see GetCopyTradingQuery
 */
class GetCopyTradingQueryHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $entityManager,
        private AutoMapperInterface $mapper,
        private ValidationService $validator
    ) {
    }

    /**
     * @throws EntityNotFoundException
     * @throws UnregisteredMappingException
     *
     * @return mixed
     */
    public function __invoke(GetCopyTradingQuery $query)
    {
        $this->validator->validateObject($query);

        /** @var CopyTrading $entity */
        $entity = $this->entityManager->find(CopyTrading::class, $query->getCopyTradingId());

        return $this->mapper->map($entity, CopyTradingDTO::class);
    }
}
