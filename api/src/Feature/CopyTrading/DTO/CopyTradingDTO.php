<?php

namespace App\Feature\CopyTrading\DTO;

use App\Feature\StockExchangeToken\DTO\StockExchangeTokenPublicListDTO;

class CopyTradingDTO
{
    /**
     * @var int
     */
    public $id;

    /**
     * @var StockExchangeTokenPublicListDTO
     */
    public $childStockExchangeToken;

    /**
     * @var StockExchangeTokenPublicListDTO
     */
    public $parentStockExchangeToken;
}
