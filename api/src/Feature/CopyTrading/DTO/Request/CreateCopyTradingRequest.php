<?php

namespace App\Feature\CopyTrading\DTO\Request;

use App\Feature\StockExchangeTokenPart\StockExchangeTokenPart;
use App\Service\Validation\Constraints\EntityExist;
use Symfony\Component\Validator\Constraints as Assert;

class CreateCopyTradingRequest
{
    /**
     * @var int
     *
     * @Assert\NotBlank
     * @EntityExist(targetEntity=StockExchangeTokenPart::class, message="Токен для автонаследования не найден")
     */
    public $stockExchangeTokenPartId;

    public function getStockExchangeTokenPartId(): int
    {
        return $this->stockExchangeTokenPartId;
    }
}
