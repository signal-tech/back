<?php

namespace App\Feature\CopyTrading\Message;

class CopyTradingMessage
{
    /**
     * CopyTradeMessage constructor.
     */
    public function __construct(
        private int $orderId,
        private int $copyTradingId
    ) {
    }

    public function getOrderId(): int
    {
        return $this->orderId;
    }

    public function getCopyTradingId(): int
    {
        return $this->copyTradingId;
    }
}
