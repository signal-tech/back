<?php

namespace App\Feature\CopyTrading\Command;

use App\Feature\CopyTrading\CopyTrading;
use App\Service\Validation\Constraints\EntityExist;

/**
 * @see DeleteCopyTradingCommandHandler
 */
class DeleteCopyTradingCommand
{
    /**
     * @EntityExist(targetEntity=CopyTrading::class, message="Копирование сделок не найдено")
     */
    private int $copyTradingId;

    public function __construct(int $copyTradingId)
    {
        $this->copyTradingId = $copyTradingId;
    }

    public function getCopyTradingId(): int
    {
        return $this->copyTradingId;
    }
}
