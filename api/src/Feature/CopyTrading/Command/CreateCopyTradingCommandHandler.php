<?php

namespace App\Feature\CopyTrading\Command;

use App\Feature\CopyTrading\CopyTrading;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Infrastructure\OAuth\Service\TokenService;
use App\Service\CQRS\AbstractAuthorizedAccessible;
use App\Service\Validation\ValidationException;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;

/**
 * @see CreateCopyTradingCommand
 */
class CreateCopyTradingCommandHandler extends AbstractAuthorizedAccessible
{
    public function __construct(
        private EntityManagerInterface $em,
        private ValidationService $validator,
        private TokenService $tokenService
    ) {
    }

    /**
     * @throws UnregisteredMappingException
     * @throws ValidationException
     */
    public function __invoke(CreateCopyTradingCommand $command): int
    {
        $this->validator->validateObject($command);

        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);
        $copyTradingRepository = $this->em->getRepository(CopyTrading::class);

        $entity = new CopyTrading();

        /** @var StockExchangeToken $parentToken */
        $parentToken = $stockExchangeTokenRepository->find($command->getParentStockExchangeTokenId());
        $entity->setParentToken($parentToken);

        /** @var StockExchangeToken $childToken */
        $childToken = $stockExchangeTokenRepository->find($command->getCreateCopyTradingRequest()->getStockExchangeTokenPartId());
        $entity->setChildToken($childToken);

        $copyTrading = $copyTradingRepository->findOneBy([
            'childToken' => $childToken,
        ]);

        if (null !== $copyTrading) {
            throw new InvalidArgumentException('У вас уже существует автоследование по данному токену.');
        }

        $user = $this->tokenService->getUserByToken();

        if ($childToken->getUser()->getId() !== $user->getId()) {
            throw new InvalidArgumentException('Данный токен вам не принадлежит.');
        }

        if ($childToken->getUser()->getId() !== $parentToken->getUser()->getId()) {
            throw new InvalidArgumentException('Токены не могут принадлежать вам.');
        }

        if ($childToken->getStockExchange()->getCode() !== $parentToken->getStockExchange()->getCode()) {
            throw new InvalidArgumentException('Ошибка типов платформ токенов.');
        }

        $this->em->persist($entity);
        $this->em->flush();

        $this->em->refresh($entity);

        return $entity->getId();
    }
}
