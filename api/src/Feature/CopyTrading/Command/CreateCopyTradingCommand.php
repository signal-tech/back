<?php

namespace App\Feature\CopyTrading\Command;

use App\Feature\CopyTrading\DTO\Request\CreateCopyTradingRequest;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Validation\Constraints\EntityExist;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @see CreateCopyTradingCommandHandler
 */
class CreateCopyTradingCommand
{
    /**
     * @EntityExist(targetEntity=StockExchangeToken::class, message="Родительский токен для автонаследования не найден")
     */
    private int $parentStockExchangeTokenId;

    /**
     * @Assert\Valid
     */
    private CreateCopyTradingRequest $createCopyTradingRequest;

    /**
     * GetUserSocialListQuery constructor.
     */
    public function __construct(
        CreateCopyTradingRequest $createCopyTradingRequest,
        int $parentStockExchangeTokenId
    ) {
        $this->parentStockExchangeTokenId = $parentStockExchangeTokenId;
        $this->createCopyTradingRequest = $createCopyTradingRequest;
    }

    public function getParentStockExchangeTokenId(): int
    {
        return $this->parentStockExchangeTokenId;
    }

    public function getCreateCopyTradingRequest(): CreateCopyTradingRequest
    {
        return $this->createCopyTradingRequest;
    }
}
