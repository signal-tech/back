<?php

namespace App\Feature\CopyTrading\Command;

use App\Feature\CopyTrading\CopyTrading;
use App\Feature\CopyTrading\CopyTradingRepository;
use App\Infrastructure\OAuth\Service\TokenServiceInterface;
use App\Service\Validation\ValidationException;
use App\Service\Validation\ValidationService;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;
use InvalidArgumentException;

/**
 * @see DeleteCopyTradingCommand
 */
class DeleteCopyTradingCommandHandler
{
    public function __construct(
        private EntityManagerInterface $em,
        private ValidationService $validator,
        private TokenServiceInterface $tokenService
    ) {
    }

    /**
     * @throws UnregisteredMappingException
     * @throws ValidationException
     */
    public function __invoke(DeleteCopyTradingCommand $command): int
    {
        $this->validator->validateObject($command);

        /**
         * @var CopyTradingRepository $repository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $repository = $this->em->getRepository(CopyTrading::class);

        $user = $this->tokenService->getUserByToken();

        $entity = $repository->findOneBy(['id' => $command->getCopyTradingId()]);

        if (null !== $entity) {
            if ($entity->getChildToken()->getUser()->getId() !== $user->getId()) {
                throw new InvalidArgumentException('Отказано в доступе');
            }

            $id = $entity->getId();
            $this->em->remove($entity);

            $this->em->flush();

            return $id;
        }

        return 0;
    }
}
