<?php

namespace App\Feature\CopyTrading;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method CopyTradingLog|null find($id, $lockMode = null, $lockVersion = null)
 * @method CopyTradingLog|null findOneBy(array $criteria, array $orderBy = null)
 * @method CopyTradingLog[]    findAll()
 * @method CopyTradingLog[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class CopyTradingLogRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, CopyTradingLog::class);
    }
}
