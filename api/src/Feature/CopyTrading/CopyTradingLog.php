<?php

namespace App\Feature\CopyTrading;

use App\Feature\Order\Order;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass=CopyTradingLogRepository::class)
 *
 * @ORM\Table(name="copy_trading_logs")
 *
 * @ORM\HasLifecycleCallbacks
 */
class CopyTradingLog
{
    use TimestampableEntity;

    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     */
    private int $id;

    /**
     *  Родительский токен за которым следуем
     *
     * @ORM\ManyToOne(targetEntity="App\Feature\Order\Order")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id", nullable=false)
     */
    private Order $order;

    /**
     *  Токен который повторяет сделки за родительским
     *
     * @ORM\ManyToOne(targetEntity="App\Feature\CopyTrading\CopyTrading")
     * @ORM\JoinColumn(name="copy_trading_id", referencedColumnName="id", nullable=false)
     */
    private CopyTrading $copyTrading;

    /**
     * @ORM\Column(type="json")
     */
    private array $data;

    public function getId(): int
    {
        return $this->id;
    }

    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    public function getOrder(): Order
    {
        return $this->order;
    }

    public function setOrder(Order $order): self
    {
        $this->order = $order;

        return $this;
    }

    public function getCopyTrading(): CopyTrading
    {
        return $this->copyTrading;
    }

    public function setCopyTrading(CopyTrading $copyTrading): self
    {
        $this->copyTrading = $copyTrading;

        return $this;
    }

    public function getData(): array
    {
        return $this->data;
    }

    public function setData(array $data): self
    {
        $this->data = $data;

        return $this;
    }
}
