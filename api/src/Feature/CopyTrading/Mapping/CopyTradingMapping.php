<?php

namespace App\Feature\CopyTrading\Mapping;

use App\Feature\CopyTrading\CopyTrading;
use App\Feature\CopyTrading\DTO\CopyTradingDTO;
use App\Feature\StockExchangeToken\DTO\StockExchangeTokenPublicListDTO;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\StockExchangeToken\StockExchangeTokenRepository;
use AutoMapperPlus\AutoMapperInterface;
use AutoMapperPlus\AutoMapperPlusBundle\AutoMapperConfiguratorInterface;
use AutoMapperPlus\Configuration\AutoMapperConfigInterface;
use AutoMapperPlus\Exception\UnregisteredMappingException;
use Doctrine\ORM\EntityManagerInterface;

class CopyTradingMapping implements AutoMapperConfiguratorInterface
{
    public function __construct(
        private EntityManagerInterface $em,
        private AutoMapperInterface $mapper
    ) {
    }

    /**
     * @throws UnregisteredMappingException
     */
    public function configure(AutoMapperConfigInterface $config): void
    {
        /**
         * @var StockExchangeTokenRepository $stockExchangeTokenRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);
        $mapper = $this->mapper;

        $config->registerMapping(CopyTrading::class, CopyTradingDTO::class)
            ->forMember('childStockExchangeToken', static function (CopyTrading $entity) use ($stockExchangeTokenRepository, $mapper): StockExchangeTokenPublicListDTO {
                /** @var StockExchangeToken $token */
                $token = $stockExchangeTokenRepository->find($entity->getChildToken());

                /** @var StockExchangeTokenPublicListDTO $tokenDTO */
                $tokenDTO = $mapper->map($token, StockExchangeTokenPublicListDTO::class);

                return $tokenDTO;
            })
            ->forMember('parentStockExchangeToken', static function (CopyTrading $entity) use ($stockExchangeTokenRepository, $mapper): StockExchangeTokenPublicListDTO {
                /** @var StockExchangeToken $token */
                $token = $stockExchangeTokenRepository->find($entity->getParentToken());

                /** @var StockExchangeTokenPublicListDTO $tokenDTO */
                $tokenDTO = $mapper->map($token, StockExchangeTokenPublicListDTO::class);

                return $tokenDTO;
            });
    }
}
