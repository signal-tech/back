<?php

namespace App\Command;

use App\Feature\Portfolio\Message\CalculatePortfolioMessage;
use App\Feature\StockExchange\StockExchange;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\StockExchangeToken\StockExchangeTokenRepository;
use App\Service\Time\TimeServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
//use React\Socket\Connector;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use function time;

class CalculateTodayPortfolioCommand extends Command
{
    protected static $defaultName = 'calculate:today-portfolio';

    public function __construct(
        private EntityManagerInterface $em,
        private MessageBusInterface $messageBus,
        private TimeServiceInterface $timeService,
        ?string $name = null
    ) {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /**
         * @var StockExchangeTokenRepository $stockExchangeTokenRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);

        $tokens = $stockExchangeTokenRepository->getTokensByStockExchangeType(
            [StockExchange::FTX_TYPE, StockExchange::BINANCE_TYPE],
            false,
            true
        );

        $startAndTimeDTO = $this->timeService->getStartAndEndDayByTime(time());

        foreach ($tokens as $token) {
            $this->messageBus->dispatch(
                new CalculatePortfolioMessage(
                    $token->getId(),
                    $startAndTimeDTO->getStartTime() - (2 * TimeServiceInterface::DAY_SECONDS), //делаем лаг + 2 дня, что бы точно получить сформированные портфолио (в бинансе не всегда есть)
                    $startAndTimeDTO->getEndTime() - (2 * TimeServiceInterface::DAY_SECONDS)
                )
            );
        }

        return 0;
    }
}
