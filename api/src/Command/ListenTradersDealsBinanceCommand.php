<?php

namespace App\Command;

use App\Feature\Order\DTO\Request\OrderRequest;
use App\Feature\Order\Message\CreateOrderMessage;
use App\Feature\StockExchange\StockExchange;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\StockExchangeToken\StockExchangeTokenRepository;
use App\Service\Serializer\SerializerFactory;
use App\Service\StockExchange\Binance\BinanceStockExchangeServiceInterface;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
//use React\Socket\Connector;
use Psr\Log\LoggerInterface;
use Ratchet\Client\Connector;
use Ratchet\Client\WebSocket;
use React\EventLoop\Factory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\Serializer;
use Throwable;
use function hash_hmac;
use function json_decode;
use function json_encode;
use function memory_get_usage;
use function microtime;
use function round;
use function strtolower;
use function time;

class ListenTradersDealsBinanceCommand extends Command
{
    private const STATUS_MAPPING = [
        'NEW' => 'opened',
        'FILLED' => 'closed',
        'CANCELED' => 'cancelled',
    ];

    private EntityManagerInterface $em;

    private array $connectionsList = [];

    private MessageBusInterface $messageBus;

    private Serializer $serializer;

    private LoggerInterface $logger;

    protected static $defaultName = 'socket:listen-traders-deals-binance';

    public function __construct(
        EntityManagerInterface $em,
        MessageBusInterface $messageBus,
        LoggerInterface $logger,
        private BinanceStockExchangeServiceInterface $binanceStockExchangeService,
        ?string $name = null
    ) {
        $this->serializer = SerializerFactory::create();
        $this->em = $em;
        $this->messageBus = $messageBus;
        $this->logger = $logger;
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $loop = Factory::create();
        $connector = new Connector($loop);

        /**
         * @var StockExchangeTokenRepository $stockExchangeTokenRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);

        $tokens = $stockExchangeTokenRepository->getTokensByStockExchangeType([StockExchange::BINANCE_TYPE]);

        foreach ($tokens as $token) { //TODO оптимизировать по памяти
            try {
                $listenKey = $this->binanceStockExchangeService->getSocketListenToken($token);

                $token->setLastListeningChannelConnect(new DateTime());
                $this->em->persist($token);
                $this->em->flush();

                $this->createConnection(
                    $connector,
                    $token,
                    $this->connectionsList,
                    $this->em,
                    $this->messageBus,
                    $this->serializer,
                    $this->logger,
                    $listenKey
                );

                $loop->addPeriodicTimer(2400, function () use ($token, $listenKey): void {
                    $this->binanceStockExchangeService->updateSocketListenToken($token, $listenKey);
                });
            } catch (Throwable $throwable) {
            }
        }

        $loop->addPeriodicTimer(30, function () use ($stockExchangeTokenRepository, $connector, $loop): void {
            $tokens = $stockExchangeTokenRepository->getTokensByStockExchangeType([StockExchange::BINANCE_TYPE], true);

            foreach ($tokens as $token) { //TODO оптимизировать по памяти
                try {
                    $listenKey = $this->binanceStockExchangeService->getSocketListenToken($token);

                    $token->setLastListeningChannelConnect(new DateTime());
                    $this->em->persist($token);
                    $this->em->flush();

                    $this->createConnection(
                        $connector,
                        $token,
                        $this->connectionsList,
                        $this->em,
                        $this->messageBus,
                        $this->serializer,
                        $this->logger,
                        $listenKey
                    );

                    $loop->addPeriodicTimer(2400, function () use ($token, $listenKey): void {
                        $this->binanceStockExchangeService->updateSocketListenToken($token, $listenKey);
                    });
                } catch (Throwable $throwable) {
                }
            }
        });
        $loop->run();

        return 0;
    }

    private function createConnection(
        Connector $connector,
        StockExchangeToken $token,
        array &$connectionsList,
        EntityManagerInterface $em,
        MessageBusInterface $messageBus,
        Serializer $serializer,
        LoggerInterface $logger,
        string $listenKey
    ): void {
        try {
            $connectionsList[] = $connector('wss://stream.binance.com:9443/ws/' . $listenKey)->then(static function (WebSocket $conn) use (
                $token,
                $em,
                $logger,
                $serializer,
                $messageBus
            ): void {
//                $time = round(microtime(true) * 1000);
//
//                $conn->send(
//                    json_encode([
//                        'args' => [
//                            'key' => $token->getApiKey(),
//                            'sign' => hash_hmac('sha256', $time . 'websocket_login', $token->getSecret()),
//                            'time' => $time,
//                        ],
//                        'op' => 'login',
//                    ])
//                );
//
//                $loop->addPeriodicTimer(20, static function () use ($conn) { //В доке написано что нужно раз в 15 секунд, пробую чуть увеличить
//                    $conn->send(
//                        json_encode([
//                            'op' => 'ping',
//                        ])
//                    );
//                });
//                $conn->send(
//                    json_encode([
//                        "method"=> "SUBSCRIBE",
//                        "id" => 1,
//                        "streams" => ['executionReport']
//                    ])
//                );

                $conn->on('message', static function (\Ratchet\RFC6455\Messaging\MessageInterface $msg) use (
                    $token,
                    $logger,
                    $serializer,
                    $messageBus
                ): void {
                    $logger->info('BINANCE Received: ' . $token->getId() . " {$msg} memory: " . memory_get_usage());

                    try {
                        $data = json_decode((string) $msg, true);

                        if (isset($data['e']) && 'executionReport' === $data['e']) {
                            $orderData = [];
                            $orderData['data'] = $data;
                            $orderData['stockExchangeTokenId'] = $token->getId();
                            $orderData['time'] = (int) round(((float) $data['T']) / 1000);
                            $orderData['orderId'] = $data['i'];
                            $orderData['clientId'] = $data['c'];
                            $orderData['market'] = $data['s'];
                            $orderData['type'] = strtolower((string) $data['o']);
                            $orderData['side'] = strtolower((string) $data['S']);
                            $orderData['size'] = (float) ($data['q'] ?? 0.0);
                            $orderData['price'] = (float) ($data['p']);
                            $orderData['timeInForce'] = $data['f'];
                            $orderData['status'] = self::STATUS_MAPPING[$data['X']]; // "closed"

                            $orderRequest = $serializer->denormalize($orderData, OrderRequest::class);

                            $messageData = new CreateOrderMessage($orderRequest);
                            $messageBus->dispatch($messageData);
                        }
                    } catch (Throwable $throwable) {
                        $logger->critical('Binance error saving order: ' . $throwable->getMessage());
                    }
                    /*
                     * [2021-09-17T01:06:06.018855+03:00] app.INFO: BINANCE Received: 33 {"e":"executionReport","E":1631829965908,"s":"ADARUB","c":"web_cd14980ab6ea41c094fcfb6c56a7364d","S":"BUY","o":"LIMIT","f":"GTC","q":"1.00000000","p":"176.40000000","P":"0.00000000","F":"0.00000000","g":-1,"C":"","x":"NEW","X":"NEW","r":"NONE","i":12136711,"l":"0.00000000","z":"0.00000000","L":"0.00000000","n":"0","N":null,"T":1631829965907,"t":-1,"I":24740907,"w":true,"m":false,"M":false,"O":1631829965907,"Z":"0.00000000","Y":"0.00000000","Q":"0.00000000"} memory: 61861232 [] [].
                     * [2021-09-17T01:09:39.313219+03:00] app.INFO: BINANCE Received: 33 {"e":"outboundAccountPosition","E":1631830179260,"u":1631830179260,"B":[{"a":"BNB","f":"0.00000000","l":"0.00000000"},{"a":"ADA","f":"0.09900000","l":"0.00000000"},{"a":"RUB","f":"883.27860645","l":"0.00000000"}]} memory: 61876208 [] []
                     */
                });

                $conn->on('close', static function (int|float $code = null, ?string $reason = null) use ($token, $em, $logger): void {
                    $logger->error("Connection closed ({$code} - {$reason})\n");
                    $token->setLastListeningChannelConnect(null);
                    $em->persist($token);
                    $em->flush();
                });
            }, static function (Throwable $e) use ($logger): void {
                $logger->critical("Could not connect: {$e->getMessage()}");
            });
        } catch (Throwable $e) {
            $logger->critical("Error: {$e->getMessage()}");
        }
    }
}
