<?php

namespace App\Command;

use App\Feature\Order\DTO\Request\OrderRequest;
use App\Feature\Order\Message\CreateOrderMessage;
use App\Feature\StockExchange\StockExchange;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\StockExchangeToken\StockExchangeTokenRepository;
use App\Service\Serializer\SerializerFactory;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
//use React\Socket\Connector;
use Psr\Log\LoggerInterface;
use Ratchet\Client\Connector;
use Ratchet\Client\WebSocket;
use React\EventLoop\Factory;
use React\EventLoop\LoopInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Serializer\Serializer;
use Throwable;
use function hash_hmac;
use function json_decode;
use function json_encode;
use function memory_get_usage;
use function microtime;
use function round;

class ListenTradersDealsFtxCommand extends Command
{
    private EntityManagerInterface $em;

    private array $connectionsList = [];

    private MessageBusInterface $messageBus;

    private Serializer $serializer;

    private LoggerInterface $logger;

    protected static $defaultName = 'socket:listen-traders-deals-ftx';

    public function __construct(
        EntityManagerInterface $em,
        MessageBusInterface $messageBus,
        LoggerInterface $logger,
        ?string $name = null
    ) {
        $this->serializer = SerializerFactory::create();
        $this->em = $em;
        $this->messageBus = $messageBus;
        $this->logger = $logger;
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $loop = Factory::create();
        $connector = new Connector($loop);

        /**
         * @var StockExchangeTokenRepository $stockExchangeTokenRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);

        $tokens = $stockExchangeTokenRepository->getTokensByStockExchangeType([StockExchange::FTX_TYPE]);

        foreach ($tokens as $token) { //TODO оптимизировать по памяти
            $this->createConnection(
                $connector,
                $token,
                $this->connectionsList,
                $this->em,
                $loop,
                $this->messageBus,
                $this->serializer,
                $this->logger
            );
        }

        $loop->addPeriodicTimer(30, function () use ($stockExchangeTokenRepository, $connector, $loop): void {
            $tokens = $stockExchangeTokenRepository->getTokensByStockExchangeType([StockExchange::FTX_TYPE], true);

            foreach ($tokens as $token) { //TODO оптимизировать по памяти
                $this->createConnection(
                    $connector,
                    $token,
                    $this->connectionsList,
                    $this->em,
                    $loop,
                    $this->messageBus,
                    $this->serializer,
                    $this->logger
                );
            }
        });
        $loop->run();

        return 0;
    }

    private function createConnection(
        Connector $connector,
        StockExchangeToken $token,
        array &$connectionsList,
        EntityManagerInterface $em,
        LoopInterface $loop,
        MessageBusInterface $messageBus,
        Serializer $serializer,
        LoggerInterface $logger
    ): void {
        try {
            $connectionsList[] = $connector('wss://ftx.com/ws/')->then(static function (WebSocket $conn) use (
                $token,
                $em,
                $loop,
                $messageBus,
                $serializer,
                $logger
            ): void {
                $time = round(microtime(true) * 1000);

                $conn->send(
                    json_encode([
                        'args' => [
                            'key' => $token->getApiKey(),
                            'sign' => hash_hmac('sha256', $time . 'websocket_login', $token->getSecret()),
                            'time' => $time,
                        ],
                        'op' => 'login',
                    ])
                );

                $loop->addPeriodicTimer(20, static function () use ($conn): void { //В доке написано что нужно раз в 15 секунд, пробую чуть увеличить
                    $conn->send(
                        json_encode([
                            'op' => 'ping',
                        ])
                    );
                });
                $conn->send(
                    json_encode([
                        'op' => 'subscribe',
                        'channel' => 'orders',
                    ])
                );

                $conn->on('message', static function (\Ratchet\RFC6455\Messaging\MessageInterface $msg) use (
                    $token,
                    $em,
                    $messageBus,
                    $serializer,
                    $logger
                ): void {
                    $logger->info('FTX Received: ' . $token->getId() . " {$msg} memory: " . memory_get_usage());

                    try {
                        $data = json_decode((string) $msg, true);

                        if (isset($data['type']) && 'update' === $data['type']) {
                            $data = $data['data'];
                            $orderData = [];
                            $orderData['data'] = $data;
                            $orderData['stockExchangeTokenId'] = $token->getId();
                            $orderData['time'] = (new DateTime((string) $data['createdAt']))->getTimestamp(); //"createdAt": "2021-09-16T21:17:24.919852+00:00"
                            $orderData['orderId'] = $data['id'];
                            $orderData['clientId'] = $data['clientId'];
                            $orderData['market'] = $data['market'];
                            $orderData['type'] = $data['type'];
                            $orderData['side'] = $data['side'];
                            $orderData['size'] = 'opened' !== $data['status'] ? ($data['filledSize'] ?? 0.0) : ($data['size'] ?? 0.0);
                            $orderData['price'] = 'opened' !== $data['status'] ? ($data['avgFillPrice'] ?? 0.0) : ($data['price'] ?? 0.0);
                            $orderData['timeInForce'] = $data['ioc'] ? 'IOC' : null;
                            $orderData['status'] = $data['status']; // "closed"
                            //{"id": 79947216100, "clientId": null, "market": "SRN-PERP", "type": "market", "side": "sell", "price": null, "size": 30.0, "status": "closed", "filledSize": 30.0, "remainingSize": 0.0, "reduceOnly": true, "liquidation": false, "avgFillPrice": 0.012395, "postOnly": false, "ioc": true, "createdAt": "2021-09-16T21:17:24.919852+00:00"}
                            $orderRequest = $serializer->denormalize($orderData, OrderRequest::class);

                            $messageData = new CreateOrderMessage($orderRequest);
                            $messageBus->dispatch($messageData);
                        }

                        if (isset($data['type']) && 'subscribed' === $data['type']) {
                            $token->setLastListeningChannelConnect(new DateTime());
                            $em->persist($token);
                            $em->flush();
                        }
                    } catch (Throwable $throwable) {
                        $logger->critical('FTX error saving order: ' . $throwable->getMessage());
                    }

                    //echo memory_get_usage() . "\n";
                    //  $conn->close();
                });

                $conn->on('close', static function (float|int $code = null, ?string $reason = null) use ($token, $em, $logger): void {
                    $logger->error("FTX Connection closed ({$code} - {$reason})\n");
                    $token->setLastListeningChannelConnect(null);
                    $em->persist($token);
                    $em->flush();
                });
            }, static function (Throwable $e) use ($logger): void {
                $logger->critical("FTX Could not connect: {$e->getMessage()}");
            });
        } catch (Throwable $e) {
            $logger->critical("FTX Error: {$e->getMessage()}");
        }
    }
}
