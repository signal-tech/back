<?php

namespace App\Command;

use App\Feature\Portfolio\Message\CalculatePortfolioMessage;
use App\Feature\Statistics\Statistics;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Feature\Statistics\StatisticsServiceInterface;
use App\Service\StockExchange\Binance\BinanceStockExchangeService;
use App\Service\StockExchange\Binance\BinanceStockExchangeServiceInterface;
use App\Service\StockExchange\Bitmex\BitmexStockExchangeServiceInterface;
use App\Service\StockExchange\StockExchangeServiceInterface;
use App\Service\Time\TimeServiceInterface;
use Centrifugo\Centrifugo;
use Doctrine\ORM\EntityManagerInterface;
//use React\Socket\Connector;
use ParagonIE\Halite\KeyFactory;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use function json_encode;
use function time;

/**
 * @psalm-suppress UnevaluatedCode
 */
class FindFirstDepositCommand extends Command //Тестовая команда
{
    protected static $defaultName = 'find:first-deposit';

    public function __construct(
        private EntityManagerInterface $em,/** @phpstan-ignore-line */
        private StockExchangeServiceInterface $stockExchangeService,/** @phpstan-ignore-line */
        private TimeServiceInterface $timeService,/** @phpstan-ignore-line */
        private MessageBusInterface $messageBus,/** @phpstan-ignore-line */
        private StatisticsServiceInterface $statisticsService,/** @phpstan-ignore-line */
        private BitmexStockExchangeServiceInterface $bitmexStockExchangeService,/** @phpstan-ignore-line */
        private BinanceStockExchangeServiceInterface $binanceStockExchangeService,/** @phpstan-ignore-line */
        private Centrifugo $centrifugo,/* @phpstan-ignore-line */
        ?string $name = null
    ) {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $encKey = KeyFactory::generateEncryptionKey();
        KeyFactory::save($encKey, '/var/www/html/public/prod.txt');

        return 0;
//        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);
//        /** @var StockExchangeToken $token */
//        $token = $stockExchangeTokenRepository->find(63);
//
//        $data = $this->stockExchangeService->getFirstMoneyDepositDateTime($token);
//
//        var_dump($data);
//
//        exit;
//
//        $data = $this->statisticsService->calculateStatisticByPeriodAndGetEntity($tokenPart, 1636675200, 1636761600, Statistics::TYPE_DAY);
//        echo json_encode($data);

//        exit;
//        $data = $this->statisticsService->calculateStatisticByPeriodAndGetEntity(
//            $tokenPart,
//            1609459200,
//            1640995200,
//            Statistics::TYPE_YEAR
//        );
//        echo 1;
//
//        exit;
//        echo $data;
//        exit;
        //   $this->binanceStockExchangeService->getCoinsData($token);
        //$this->binanceStockExchangeService->getMarketHistoricalPrice($token, 'ETHUSD', 1598400000);

//        $this->messageBus->dispatch(new CalculatePortfolioMessage(25, 1598486400, 1598486400 + 86400));
//        die;
        //echo round(microtime(true) * 1000)

//        $data = $this->stockExchangeService->getFirstMoneyDepositDateTime($token);
//
//        echo $data;die;
//        die;
//        $data = $this->stockExchangeService->getPortfolioByPeriod($token, 1598486400 + 86400, 1598486400 + 86400 + 86400, null);
//        echo json_encode($data);
//        exit;
//        exit;
//        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);
//
//        /** @var StockExchangeToken $token */
//        $token = $stockExchangeTokenRepository->find(15);
//        // $data = $this->bitmexStockExchangeService->getData($token);
//
//        $data = $this->stockExchangeService->getFirstMoneyDepositDateTime($token);
//        //$data = $this->stockExchangeService->getPortfolioByPeriod($token, 1417392000, time(), null);
    }
}
