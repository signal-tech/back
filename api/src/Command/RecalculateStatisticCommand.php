<?php

namespace App\Command;

use App\Feature\Portfolio\Portfolio;
use App\Feature\Portfolio\PortfolioRepository;
use App\Feature\Statistics\Message\CalculateStatisticsDayMessage;
use Doctrine\ORM\EntityManagerInterface;
//use React\Socket\Connector;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Messenger\MessageBusInterface;

class RecalculateStatisticCommand extends Command //Тестовая команда
{
    protected static $defaultName = 'recalculate:statistics';

    public function __construct(
        private EntityManagerInterface $em,
        private MessageBusInterface $messageBus,
        ?string $name = null
    ) {
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        /**
         * @var PortfolioRepository $portfolioRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $portfolioRepository = $this->em->getRepository(Portfolio::class);

        $portfolios = $portfolioRepository->findBy(['stockExchangeTokenPart' => 33], ['id' => 'ASC']);

        foreach ($portfolios as $portfolio) {
            $this->messageBus->dispatch(
                new CalculateStatisticsDayMessage(
                    $portfolio->getId()
                )
            );
        }

        return 0;
    }
}
