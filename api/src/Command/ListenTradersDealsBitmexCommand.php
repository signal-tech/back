<?php

namespace App\Command;

use App\Feature\Order\DTO\Request\OrderRequest;
use App\Feature\Order\Message\CreateOrderMessage;
use App\Feature\StockExchange\StockExchange;
use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Feature\StockExchangeToken\StockExchangeTokenRepository;
use DateTime;
use Doctrine\ORM\EntityManagerInterface;
//use React\Socket\Connector;
use Psr\Log\LoggerInterface;
use Ratchet\Client\Connector;
use Ratchet\Client\WebSocket;
use React\EventLoop\Factory;
use React\EventLoop\LoopInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Throwable;
use function hash_hmac;
use function json_decode;
use function json_encode;
use function memory_get_usage;
use function microtime;
use function round;

class ListenTradersDealsBitmexCommand extends Command
{
    private EntityManagerInterface $em;

    private array $connectionsList = [];

    private LoggerInterface $logger;

    protected static $defaultName = 'socket:listen-traders-deals-bitmex';

    public function __construct(
        EntityManagerInterface $em,
        LoggerInterface $logger,
        ?string $name = null
    ) {
        $this->em = $em;
        $this->logger = $logger;
        parent::__construct($name);
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $loop = Factory::create();
        $connector = new Connector($loop);

        /**
         * @var StockExchangeTokenRepository $stockExchangeTokenRepository
         * @psalm-suppress UnnecessaryVarAnnotation
         */
        $stockExchangeTokenRepository = $this->em->getRepository(StockExchangeToken::class);

        $tokens = $stockExchangeTokenRepository->getTokensByStockExchangeType([StockExchange::BITMEX_TYPE]);

        foreach ($tokens as $token) { //TODO оптимизировать по памяти
            try {
                $token->setLastListeningChannelConnect(new DateTime());
                $this->em->persist($token);
                $this->em->flush();

                $this->createConnection(
                    $connector,
                    $token,
                    $this->connectionsList,
                    $this->em,
                    $loop,
                    $this->logger
                );
            } catch (Throwable $throwable) {
            }
        }

        $loop->addPeriodicTimer(30, function () use ($stockExchangeTokenRepository, $connector, $loop): void {
            $tokens = $stockExchangeTokenRepository->getTokensByStockExchangeType([StockExchange::BITMEX_TYPE], true);

            foreach ($tokens as $token) { //TODO оптимизировать по памяти
                try {
                    $token->setLastListeningChannelConnect(new DateTime());
                    $this->em->persist($token);
                    $this->em->flush();

                    $this->createConnection(
                        $connector,
                        $token,
                        $this->connectionsList,
                        $this->em,
                        $loop,
                        $this->logger
                    );
                } catch (Throwable $throwable) {
                }
            }
        });
        $loop->run();

        return 0;
    }

    private function createConnection(
        Connector $connector,
        StockExchangeToken $token,
        array &$connectionsList,
        EntityManagerInterface $em,
        LoopInterface $loop,
        LoggerInterface $logger
    ): void {
        try {
            $connectionsList[] = $connector('wss://www.bitmex.com/realtime')->then(static function (WebSocket $conn) use (
                $token,
                $em,
                $logger,
                $loop
            ): void {
                $time = round(microtime(true) * 1000) + 5;

                $conn->send(
                    json_encode([
                        'args' => [
                            $token->getApiKey(),
                            $time,
                            hash_hmac('sha256', 'GET/realtime' . $time, $token->getSecret()),
                        ],
                        'op' => 'authKeyExpires',
                    ])
                );

                $loop->addPeriodicTimer(20, static function () use ($conn): void { //В доке написано что нужно раз в 15 секунд, пробую чуть увеличить
                    $conn->send(
                        'ping'
                    );
                });
                $conn->send(
                    json_encode([
                        'op' => 'subscribe',
                        'args' => 'order',
                    ])
                );

                $conn->on('message', static function (\Ratchet\RFC6455\Messaging\MessageInterface $msg) use (
                    $token,
                    $em,
                    $logger
                ): void {
                    //echo $msg;
                    $data = json_decode((string) $msg, true);

                    $logger->info('BITMEX Received: ' . $token->getId() . " {$msg} memory: " . memory_get_usage());

                    if (
                        isset($data['op']) && 'authKeyExpires' === $data['op']
                        && isset($data['success']) && true === $data['success']
                    ) {
                        $token->setLastListeningChannelConnect(new DateTime());
                        $em->persist($token);
                        $em->flush();
                    }

                    //TODO
//                    if ( isset($data['op']) && 'authKeyExpires' !== $data['op']
//                        && 'executionReport' === $data) {
//                        $orderData['data'] = $data;
//                        $orderData['stockExchangeTokenPartId'] = $tokenPart->getId();
//                        $orderData['time'] = time();
//
//                        /** @var OrderRequest $orderRequest */
//                        $orderRequest = $serializer->denormalize($orderData, OrderRequest::class);
//
//                        $messageData = new CreateOrderMessage($orderRequest);
//                        $messageBus->dispatch($messageData);
//
//                        $orderRequest = null;
//                    }
                });

                $conn->on('close', static function (int|float $code = null, ?string $reason = null) use ($token, $em, $logger): void {
                    echo "Connection closed ({$code} - {$reason})\n";
                    $logger->error("Connection closed ({$code} - {$reason})\n");
                    $token->setLastListeningChannelConnect(null);
                    $em->persist($token);
                    $em->flush();
                });
            }, static function (Throwable $e) use ($logger): void {
                $logger->critical("Could not connect: {$e->getMessage()}");
            });
        } catch (Throwable $e) {
            $logger->critical("Error: {$e->getMessage()}");
        }
    }
}
