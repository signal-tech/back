<?php

namespace App\Infrastructure\Diagnostics\Check;

use ZendDiagnostics\Check\CheckInterface;
use ZendDiagnostics\Result\Failure;
use ZendDiagnostics\Result\Success;

class PhpExtensionsCheck implements CheckInterface
{
    public function check()
    {
        return new Success('Default timezone is UTC');
        //return new Failure('Default timezone is not UTC! It is actually ' . $tz);
    }

    public function getLabel()
    {
        return 'Check Php Extensions ';
    }
}
