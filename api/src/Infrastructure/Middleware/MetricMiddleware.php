<?php

namespace App\Infrastructure\Middleware;

use App\Service\InfluxDBService;
use Exception;
use InfluxDB\Point;
use League\Tactician\Middleware;
use function get_class;

class MetricMiddleware implements Middleware
{
    /**
     * @var InfluxDBService
     */
    private $influxDBService;

    public function __construct(
        InfluxDBService $influxDBService
    ) {
        $this->influxDBService = $influxDBService;
    }

    /**
     * @param object $command
     *
     * @throws \InfluxDB\Database\Exception
     * @throws \InfluxDB\Exception
     *
     * @return mixed
     */
    public function execute($command, callable $next)
    {
        try {
            $returnValue = $next($command);
            $this->influxDBService->getDataBase()->writePoints([new Point(
                get_class($command),
                1,
                ['status' => 'completed']
            )]);
        } catch (Exception $e) {
            $this->influxDBService->getDataBase()->writePoints([new Point(
                get_class($command),
                1,
                ['status' => 'failed']
            )]);

            throw $e;
        }

        return $returnValue;
    }
}
