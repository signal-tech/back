<?php

namespace App\Infrastructure\Middleware;

use League\Tactician\Middleware;
use Psr\Log\LoggerInterface;
use function get_class;

class LoggerMiddleware implements Middleware
{
    /**
     * @var LoggerInterface
     */
    private $logger;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param object $command
     *
     * @return mixed|void
     */
    public function execute($command, callable $next)
    {
        $this->logger->info('Комманда: ' . get_class($command));

        return $next($command);
    }
}
