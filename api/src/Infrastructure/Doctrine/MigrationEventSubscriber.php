<?php

namespace App\Infrastructure\Doctrine;

use Doctrine\Common\EventSubscriber;
use Doctrine\DBAL\Schema\SchemaException;
use Doctrine\ORM\Tools\Event\GenerateSchemaEventArgs;

class MigrationEventSubscriber implements EventSubscriber
{
    private const MIGRATION_NAMESPACE = 'public';

    public function getSubscribedEvents(): array
    {
        return [
            'postGenerateSchema',
        ];
    }

    /**
     * @throws SchemaException
     */
    public function postGenerateSchema(GenerateSchemaEventArgs $args): void
    {
        $schema = $args->getSchema();

        if (!$schema->hasNamespace(self::MIGRATION_NAMESPACE)) {
            $schema->createNamespace(self::MIGRATION_NAMESPACE);
        }
    }
}
