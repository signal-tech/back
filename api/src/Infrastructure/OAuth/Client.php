<?php

namespace App\Infrastructure\OAuth;

use Doctrine\ORM\Mapping as ORM;
use FOS\OAuthServerBundle\Entity\Client as BaseClient;

/**
 * @ORM\Entity
 *
 * @ORM\Table(name="oauth_client")
 */
class Client extends BaseClient
{
    /**
     * @var int
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

//    public function __construct()
//    {
//        parent::__construct();
//        // your own logic
//    }
}
