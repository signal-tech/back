<?php

namespace App\Infrastructure\OAuth\Service;

use App\Feature\User\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Symfony\Contracts\Translation\TranslatorInterface;
use function gettype;

/**
 * @todo: сделать интерфейс и это будет его реализацией
 */
class TokenService implements TokenServiceInterface
{
    private TokenStorageInterface $tokenStorage;

    private TranslatorInterface $translator;

    public function __construct(TokenStorageInterface $tokenStorage, TranslatorInterface $translator)
    {
        $this->tokenStorage = $tokenStorage;
        $this->translator = $translator;
    }

    /**
     * @psalm-suppress DocblockTypeContradiction
     */
    public function getUserByToken(): User
    {
        /** @var TokenInterface $token */
        $token = $this->tokenStorage->getToken();

        /** @var User $user */
        $user = $token->getUser();

        if ('string' === gettype($user)) {
            throw new AccessDeniedException($this->translator->trans('user.access_denied'));
        }

        return $user;
    }
}
