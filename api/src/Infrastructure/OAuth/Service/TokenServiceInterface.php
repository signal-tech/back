<?php

namespace App\Infrastructure\OAuth\Service;

use App\Feature\User\User;

interface TokenServiceInterface
{
    public function getUserByToken(): User;
}
