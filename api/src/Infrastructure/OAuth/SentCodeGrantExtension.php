<?php

namespace App\Infrastructure\OAuth;

use App\Feature\User\UserRepository;
use FOS\OAuthServerBundle\Storage\GrantExtensionInterface;
use OAuth2\Model\IOAuth2Client;
use RuntimeException;

class SentCodeGrantExtension implements GrantExtensionInterface
{
    private UserRepository $userRepository;

    public function __construct(
        UserRepository $userRepository
    ) {
        $this->userRepository = $userRepository;
    }

    public function checkGrantExtension(IOAuth2Client $client, array $inputData, array $authHeaders): array|bool
    {
        if (!isset($inputData['phone']) || !isset($inputData['code'])) {
            return false;
        }

        $user = $this->userRepository->findOneBy(['phone' => '+' . ((string) $inputData['phone']), 'verificationCode' => ((string) $inputData['code'])]);

        if (null !== $user) {
            return [
                'data' => $user,
            ];
        }

        throw new RuntimeException('Ошибка номера телефона или кода');
    }
}
