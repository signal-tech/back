<?php

namespace App\Controller\UserSubscription;

use App\Feature\User\DTO\RecommendedUsersListItemDTO;
use App\Feature\User\User;
use App\Feature\UserSubscription\Command\SubscribeCommand;
use App\Feature\UserSubscription\Command\UnsubscribeCommand;
use App\Feature\UserSubscription\DTO\Request\GetSubscribersListRequestFilter;
use App\Feature\UserSubscription\DTO\Request\GetSubscriptionsListRequestFilter;
use App\Feature\UserSubscription\Query\GetSubscribersListQuery;
use App\Feature\UserSubscription\Query\GetSubscriptionsListQuery;
use App\Service\CQRS;
use App\Service\ListPaginator\Pager;
use App\Service\Serializer\SerializerFactory;
use FOS\RestBundle\Controller\Annotations as Rest;
use InvalidArgumentException;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

class UserSubscriptionController extends AbstractController
{
    private Serializer $serializer;

    private CQRS $cqrs;

    public function __construct(CQRS $cqrs)
    {
        $this->serializer = SerializerFactory::create();
        $this->cqrs = $cqrs;
    }

    /**
     * Подписаться на пользователя.
     *
     * @SWG\Tag(name="Подписки")
     *
     * @Rest\Route(path="/users/{userId}/subscriptions", methods={"POST"})
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="success", type="boolean")
     *     )
     * )
     *
     * @throws ExceptionInterface
     */
    public function subscribeAction(int $userId): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user->getId() === $userId) {
            throw new InvalidArgumentException('Невозможно подписаться на самого себя');
        }

        $this->cqrs->execute(new SubscribeCommand($userId));

        return new JsonResponse(['success' => true]);
    }

    /**
     * Отписаться от пользователя.
     *
     * @SWG\Tag(name="Подписки")
     *
     * @Rest\Route(path="/users/{id}/subscriptions", methods={"DELETE"})
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="success", type="boolean")
     *     )
     * )
     *
     * @throws ExceptionInterface
     */
    public function unsubscribeAction(int $id): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        if ($user->getId() === $id) {
            throw new InvalidArgumentException('Невозможно отписаться от самого себя');
        }

        $this->cqrs->execute(new UnsubscribeCommand($id));

        return new JsonResponse(['success' => true]);
    }

    /**
     * Подписки пользователя.
     *
     * @SWG\Tag(name="Подписки")
     *
     * @Rest\Route(path="/users/{id}/subscriptions", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=GetSubscriptionsListRequestFilter::class))
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="pager", type="object", ref=@Model(type=Pager::class)),
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=RecommendedUsersListItemDTO::class))
     *         )
     *     )
     * )
     *
     * @throws ExceptionInterface
     */
    public function getSubscriptionsAction(int $id, GetSubscriptionsListRequestFilter $filter): JsonResponse
    {
        return new JsonResponse(
            $this->serializer->normalize($this->cqrs->get(new GetSubscriptionsListQuery($filter, $id)))
        );
    }

    /**
     * Подписчики пользователя.
     *
     * @SWG\Tag(name="Подписки")
     *
     * @Rest\Route(path="/users/{id}/subscribers", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=GetSubscribersListRequestFilter::class))
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="pager", type="object", ref=@Model(type=Pager::class)),
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=RecommendedUsersListItemDTO::class))
     *         )
     *     )
     * )
     *
     * @throws ExceptionInterface
     */
    public function getSubscribersAction(int $id, GetSubscribersListRequestFilter $filter): JsonResponse
    {
        return new JsonResponse(
            $this->serializer->normalize($this->cqrs->get(new GetSubscribersListQuery($filter, $id)))
        );
    }
}
