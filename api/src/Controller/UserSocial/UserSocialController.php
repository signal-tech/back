<?php

namespace App\Controller\UserSocial;

use App\Feature\UserSocial\Command\CreateUserSocialCommand;
use App\Feature\UserSocial\Command\DeleteUserSocialCommand;
use App\Feature\UserSocial\Command\UpdateUserSocialCommand;
use App\Feature\UserSocial\DTO\Request\CreateUserSocialRequest;
use App\Feature\UserSocial\DTO\Request\UpdateUserSocialRequest;
use App\Feature\UserSocial\DTO\UserSocialDTO;
use App\Feature\UserSocial\DTO\UserSocialListDTO;
use App\Feature\UserSocial\Query\GetUserSocialListQuery;
use App\Feature\UserSocial\Query\GetUserSocialQuery;
use App\Service\CQRS;
use App\Service\Serializer\SerializerFactory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

class UserSocialController extends AbstractController
{
    private Serializer $serializer;

    private CQRS $cqrs;

    public function __construct(CQRS $cqrs)
    {
        $this->serializer = SerializerFactory::create();
        $this->cqrs = $cqrs;
    }

    /**
     * Добавление социальной сети.
     *
     * @SWG\Tag(name="Социальные сети пользователя")
     *
     * @Rest\Route(path="/users/socials", methods={"POST"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=CreateUserSocialRequest::class))
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=UserSocialDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function createUserSocialAction(Request $request): JsonResponse
    {
        $data = $request->request->all();

        $createUserSocialRequest = $this->serializer->denormalize($data, CreateUserSocialRequest::class);

        /** @var int $tokenId */
        $tokenId = $this->cqrs->execute(new CreateUserSocialCommand($createUserSocialRequest));

        $profile = $this->cqrs->get(new GetUserSocialQuery($tokenId));

        return new JsonResponse($this->serializer->normalize($profile));
    }

    /**
     * Обновление социальной сети.
     *
     * @SWG\Tag(name="Социальные сети пользователя")
     *
     * @Rest\Route(path="/users/socials/{id}", methods={"PUT"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=UpdateUserSocialRequest::class))
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=UserSocialDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function putUserSocialAction(Request $request, int $id): JsonResponse
    {
        $data = $request->request->all();

        $createUserSocialRequest = $this->serializer->denormalize($data, UpdateUserSocialRequest::class);

        /** @var int $tokenId */
        $tokenId = $this->cqrs->execute(new UpdateUserSocialCommand($createUserSocialRequest, $id));

        $profile = $this->cqrs->get(new GetUserSocialQuery($tokenId));

        return new JsonResponse($this->serializer->normalize($profile));
    }

    /**
     * Получение списка социальных сетей.
     *
     * @SWG\Tag(name="Социальные сети пользователя")
     *
     * @Rest\Route(path="/users/{userId}/socials", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=UserSocialListDTO::class))
     *         )
     *     )
     * )
     */
    public function getUserSocialsAction(Request $request, int $userId): JsonResponse
    {
        $profile = $this->cqrs->get(new GetUserSocialListQuery($userId));

        return new JsonResponse(['data' => $this->serializer->normalize($profile)]);
    }

    /**
     * Удаление социальной сети.
     *
     * @SWG\Tag(name="Социальные сети пользователя")
     *
     * @Rest\Route(path="/users/socials/{id}", methods={"DELETE"})
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="success", type="boolean"),
     *         @SWG\Property(property="id", type="integer")
     *     )
     * )
     *
     * @throws ExceptionInterface
     */
    public function deleteUserSocialAction(int $id): JsonResponse
    {
        $id = $this->cqrs->execute(new DeleteUserSocialCommand($id));

        return new JsonResponse(['success' => (bool) $id, 'id' => $id]);
    }

    /**
     * Получение социальной сети.
     *
     * @SWG\Tag(name="Социальные сети пользователя")
     *
     * @Rest\Route(path="/users/socials/{id}", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=UserSocialDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function getUserSocialAction(int $id): JsonResponse
    {
        $profile = $this->cqrs->get(new GetUserSocialQuery($id));

        return new JsonResponse($this->serializer->normalize($profile));
    }
}
