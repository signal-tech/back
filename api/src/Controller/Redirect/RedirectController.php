<?php

namespace App\Controller\Redirect;

use App\Service\Redirect\RedirectServiceInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\RedirectResponse;

class RedirectController extends AbstractController
{
    private RedirectServiceInterface $redirectService;

    public function __construct(
        RedirectServiceInterface $redirectService
    ) {
        $this->redirectService = $redirectService;
    }

    /**
     * @Rest\Route(path="/r/{type}/{id}", methods={"GET"})
     */
    public function getCategoriesAction(string $type, int $id): RedirectResponse
    {
        return $this->redirect($this->redirectService->getRealUrl($type, $id));
    }
}
