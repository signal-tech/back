<?php

namespace App\Controller\Social;

use App\Feature\Social\DTO\SocialListDTO;
use App\Feature\Social\Query\GetSocialListQuery;
use App\Service\CQRS;
use App\Service\Serializer\SerializerFactory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class SocialController extends AbstractController
{
    private Serializer $serializer;

    private CQRS $cqrs;

    public function __construct(CQRS $cqrs)
    {
        $this->serializer = SerializerFactory::create();
        $this->cqrs = $cqrs;
    }

    /**
     * Получение списка социальных сетей.
     *
     * @SWG\Tag(name="Социальные сети")
     *
     * @Rest\Route(path="/socials", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=SocialListDTO::class))
     *         )
     *     )
     * )
     */
    public function getTokensAction(Request $request): JsonResponse
    {
        $profile = $this->cqrs->get(new GetSocialListQuery());

        return new JsonResponse(['data' => $this->serializer->normalize($profile)]);
    }
}
