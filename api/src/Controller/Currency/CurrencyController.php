<?php

namespace App\Controller\Currency;

use App\Feature\StockExchangeToken\StockExchangeToken;
use App\Service\Serializer\SerializerFactory;
use App\Service\StockExchange\Ftx\FtxStockExchangeServiceInterface;
use Doctrine\ORM\EntityManagerInterface;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;
use function time;

class CurrencyController extends AbstractController
{
    private Serializer $serializer;

    public function __construct(
        private FtxStockExchangeServiceInterface $ftxStockExchangeService,
        private EntityManagerInterface $em
    ) {
        $this->serializer = SerializerFactory::create();
    }

    /**
     * Получение списка курсов валют.
     *
     * @SWG\Tag(name="Курс валют")
     *
     * @Rest\Route(path="/currencies", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=CurrencyDTO::class))
     *         )
     *     )
     * )
     */
    public function getTokensAction(Request $request): JsonResponse
    {
        $currencies = [
            [
                'code' => 'BTC-PERP',
                'name' => 'BTC-PERP/USD',
            ],
            [
                'code' => 'ETH-PERP',
                'name' => 'ETH-PERP/USD',
            ],
            [
                'code' => 'AXS-PERP',
                'name' => 'AXS-PERP/USD',
            ],
            [
                'code' => 'DOGE-PERP',
                'name' => 'DOGE-PERP/USD',
            ],
        ];

        $stockExchangeToken = $this->em->getRepository(StockExchangeToken::class);

        /** @var StockExchangeToken $token */
        $token = $stockExchangeToken->findOneBy(['stockExchange' => 1]);

        $returnData = [];

        foreach ($currencies as $currency) {
            $price = $this->ftxStockExchangeService->getMarketHistoricalPrice($token, $currency['code'], time() - 15);
            $returnData[] = (new CurrencyDTO())->setName($currency['name'])->setPrice($price);
        }

        return new JsonResponse(['data' => $this->serializer->normalize($returnData)]);
    }
}
