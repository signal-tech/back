<?php

namespace App\Controller\OrderManual;

use App\Feature\Order\Order;
use App\Feature\OrderManual\Command\CreateOrderManualCommand;
use App\Feature\OrderManual\DTO\Request\CreateOrderManualRequest;
use App\Service\CQRS;
use App\Service\Serializer\SerializerFactory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

class OrderManualController extends AbstractController
{
    private Serializer $serializer;

    private CQRS $cqrs;

    public function __construct(CQRS $cqrs)
    {
        $this->serializer = SerializerFactory::create();
        $this->cqrs = $cqrs;
    }

    /**
     * Список заказов по подпискам пользователя.
     *
     * @SWG\Tag(name="Создать сделку на основе другой")
     *
     * @Rest\Route(path="orders/{id}/manuals", methods={"POST"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=CreateOrderManualRequest::class))
     *
     * @SWG\Response(response=Response::HTTP_OK, description="OK")
     *
     * @throws ExceptionInterface
     */
    public function postOrderAction(Request $request, Order $order): JsonResponse
    {
        $data = $request->request->all();

        $createRequest = $this->serializer->denormalize($data, CreateOrderManualRequest::class);

        $this->cqrs->execute(new CreateOrderManualCommand($createRequest, $order));

        return new JsonResponse(['success' => true]);
    }
}
