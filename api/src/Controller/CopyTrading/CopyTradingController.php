<?php

namespace App\Controller\CopyTrading;

use App\Feature\CopyTrading\Command\CreateCopyTradingCommand;
use App\Feature\CopyTrading\Command\DeleteCopyTradingCommand;
use App\Feature\CopyTrading\DTO\CopyTradingDTO;
use App\Feature\CopyTrading\DTO\Request\CreateCopyTradingRequest;
use App\Feature\CopyTrading\Query\GetCopyTradingQuery;
use App\Feature\UserSocial\Command\UpdateUserSocialCommand;
use App\Feature\UserSocial\DTO\Request\UpdateUserSocialRequest;
use App\Feature\UserSocial\DTO\UserSocialDTO;
use App\Feature\UserSocial\DTO\UserSocialListDTO;
use App\Feature\UserSocial\Query\GetUserSocialListQuery;
use App\Feature\UserSocial\Query\GetUserSocialQuery;
use App\Service\CQRS;
use App\Service\Serializer\SerializerFactory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;
use RuntimeException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

class CopyTradingController extends AbstractController
{
    private Serializer $serializer;

    private CQRS $cqrs;

    public function __construct(CQRS $cqrs)
    {
        $this->serializer = SerializerFactory::create();
        $this->cqrs = $cqrs;
    }

    /**
     * Добавление автокопирования.
     *
     * @SWG\Tag(name="Автокопирование сделок")
     *
     * @Rest\Route(path="/stock-exchange-tokens/{tokenId}/copy-tradings", methods={"POST"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=CreateCopyTradingRequest::class))
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=CopyTradingDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function createCopyTradingAction(Request $request, int $tokenId): JsonResponse
    {
        throw new RuntimeException('Отсутствует доступ'); //Пока не пригодится
//        $data = $request->request->all();
//
//        $createRequest = $this->serializer->denormalize($data, CreateCopyTradingRequest::class);
//
//        /** @var int $copyTradingId */
//        $copyTradingId = $this->cqrs->execute(new CreateCopyTradingCommand($createRequest, $tokenId));
//
//        $data = $this->cqrs->get(new GetCopyTradingQuery($copyTradingId));
//
//        return new JsonResponse($this->serializer->normalize($data), Response::HTTP_CREATED);
    }

//    /**
//     * Обновление социальной сети.
//     *
//     * @SWG\Tag(name="Социальные сети пользователя")
//     *
//     * @Rest\Route(path="/users/socials/{id}", methods={"PUT"})
//     *
//     * @SWG\Parameter(name="{}", in="query", @Model(type=UpdateUserSocialRequest::class))
//     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=UserSocialDTO::class))
//     *
//     * @throws ExceptionInterface
//     */
//    public function putUserSocialAction(Request $request, int $id): JsonResponse
//    {
//        $data = $request->request->all();
//
//        $createUserSocialRequest = $this->serializer->denormalize($data, UpdateUserSocialRequest::class);
//
//        /** @var int $tokenId */
//        $tokenId = $this->cqrs->execute(new UpdateUserSocialCommand($createUserSocialRequest, $id));
//
//        $profile = $this->cqrs->get(new GetUserSocialQuery($tokenId));
//
//        return new JsonResponse($this->serializer->normalize($profile));
//    }
//
//    /**
//     * Получение списка социальных сетей.
//     *
//     * @SWG\Tag(name="Социальные сети пользователя")
//     *
//     * @Rest\Route(path="/users/{userId}/socials", methods={"GET"})
//     *
//     * @SWG\Parameter(name="{}", in="query", @Model)
//     *
//     * @SWG\Response(
//     *     response=Response::HTTP_OK,
//     *     description="OK",
//     *     @SWG\JsonContent(
//     *         type="object",
//     *         @SWG\Property(property="data", type="array",
//     *             @SWG\Items(ref=@Model(type=UserSocialListDTO::class))
//     *         )
//     *     )
//     * )
//     */
//    public function getUserSocialsAction(Request $request, int $userId): JsonResponse
//    {
//        $profile = $this->cqrs->get(new GetUserSocialListQuery($userId));
//
//        return new JsonResponse(['data' => $this->serializer->normalize($profile)]);
//    }
//
    /**
     * Удаление автокопирования сделок.
     *
     * @SWG\Tag(name="Автокопирование сделок")
     *
     * @Rest\Route(path="/copy-tradings/{id}", methods={"DELETE"})
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="success", type="boolean"),
     *         @SWG\Property(property="id", type="integer")
     *     )
     * )
     *
     * @throws ExceptionInterface
     */
    public function deleteCopyTradingAction(int $id): JsonResponse
    {
        $id = $this->cqrs->execute(new DeleteCopyTradingCommand($id));

        return new JsonResponse(['success' => (bool) $id, 'id' => $id]);
    }

    /**
     * Получение детальной информации по автокопированию.
     *
     * @SWG\Tag(name="Автокопирование сделок")
     *
     * @Rest\Route(path="/copy-tradings/{id}", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=CopyTradingDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function getCopyTradingAction(int $id): JsonResponse
    {
        $data = $this->cqrs->get(new GetCopyTradingQuery($id));

        return new JsonResponse($this->serializer->normalize($data));
    }
}
