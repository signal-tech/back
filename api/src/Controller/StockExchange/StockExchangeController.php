<?php

namespace App\Controller\StockExchange;

use App\Feature\StockExchange\DTO\StockExchangeListDTO;
use App\Feature\StockExchange\Query\GetStockExchangeListQuery;
use App\Service\CQRS;
use App\Service\Serializer\SerializerFactory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class StockExchangeController extends AbstractController
{
    private Serializer $serializer;

    private CQRS $cqrs;

    public function __construct(CQRS $cqrs)
    {
        $this->serializer = SerializerFactory::create();
        $this->cqrs = $cqrs;
    }

    /**
     * Получение списка бирж.
     *
     * @SWG\Tag(name="Биржи")
     *
     * @Rest\Route(path="/stock-exchanges", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=StockExchangeListDTO::class))
     *         )
     *     )
     * )
     */
    public function getTokensAction(Request $request): JsonResponse
    {
        $profile = $this->cqrs->get(new GetStockExchangeListQuery());

        return new JsonResponse(['data' => $this->serializer->normalize($profile)]);
    }
}
