<?php

namespace App\Controller\User;

use App\Feature\User\Command\SendAuthCodeCommand;
use App\Feature\User\Command\UpdateProfileCommand;
use App\Feature\User\Command\UploadUserAvatarCommand;
use App\Feature\User\DTO\ProfileDTO;
use App\Feature\User\DTO\RecommendedUsersListItemDTO;
use App\Feature\User\DTO\Request\GetRecommendedUsersListRequestFilter;
use App\Feature\User\DTO\Request\UpdateProfileRequest;
use App\Feature\User\DTO\Request\UserRequest;
use App\Feature\User\DTO\TokenDTO;
use App\Feature\User\Query\GetProfileQuery;
use App\Feature\User\Query\GetRecommendedUsersListQuery;
use App\Feature\User\User;
use App\Service\CQRS;
use App\Service\ListPaginator\Pager;
use App\Service\Serializer\SerializerFactory;
use FOS\RestBundle\Controller\Annotations as Rest;
use InvalidArgumentException;
use Nelmio\ApiDocBundle\Annotation\Model;
use OAuth2\OAuth2;
use OAuth2\OAuth2ServerException;
use OpenApi\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

class UserController extends AbstractController
{
    private Serializer $serializer;

    private CQRS $cqrs;

    private OAuth2 $server;

    public function __construct(CQRS $cqrs, OAuth2 $server)
    {
        $this->serializer = SerializerFactory::create();
        $this->cqrs = $cqrs;
        $this->server = $server;
    }

    /**
     * Поиск пользователя и отправка на телефон кода авторизации.
     *
     * @SWG\Tag(name="Пользователь")
     *
     * @Rest\Route(path="/users/auth", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=UserRequest::class, groups={"getCode"}))
     * @SWG\Response(response=Response::HTTP_OK, description="OK")
     *
     * @throws ExceptionInterface
     */
    public function sendAuthCodeAction(Request $request): JsonResponse
    {
        $data = $request->query->all();

        $userRequest = $this->serializer->denormalize($data, UserRequest::class);

        $this->cqrs->execute(new SendAuthCodeCommand($userRequest));

        return new JsonResponse([]);
    }

    /**
     * Получение данных личного профиля.
     *
     * @SWG\Tag(name="Пользователь")
     *
     * @Rest\Route(path="/users/profile", methods={"GET"})
     *
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=ProfileDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function getProfileAction(): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $profile = $this->cqrs->get(new GetProfileQuery($user->getId()));

        return new JsonResponse($this->serializer->normalize($profile));
    }

    /**
     * Получение данных профиля пользователя.
     *
     * @SWG\Tag(name="Пользователь")
     *
     * @Rest\Route(path="/users/{id}/profile", methods={"GET"})
     *
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=ProfileDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function getProfileByIdAction(int $id): JsonResponse
    {
        $profile = $this->cqrs->get(new GetProfileQuery($id));

        return new JsonResponse($this->serializer->normalize($profile));
    }

    /**
     * Обновление данных личного профиля.
     *
     * @SWG\Tag(name="Пользователь")
     *
     * @Rest\Route(path="/users/profile", methods={"POST"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=UpdateProfileRequest::class))
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=ProfileDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function postProfileAction(Request $request): JsonResponse
    {
        $data = $request->request->all();

        $userProfileRequest = $this->serializer->denormalize($data, UpdateProfileRequest::class);

        $this->cqrs->execute(new UpdateProfileCommand($userProfileRequest));

        /** @var User $user */
        $user = $this->getUser();

        $profile = $this->cqrs->get(new GetProfileQuery($user->getId()));

        return new JsonResponse($this->serializer->normalize($profile));
    }

    /**
     * Авторизация.
     *
     * @SWG\Tag(name="Пользователь")
     *
     * @Rest\Route(path="/token", methods={"POST", "GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=UserRequest::class, groups={"auth"}))
     *
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=TokenDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function getTokenAction(Request $request): Response
    {
        try {
            return $this->server->grantAccessToken($request);
        } catch (OAuth2ServerException $e) {
            return $e->getHttpResponse();
        }
    }

    /**
     * Загрузка аватара пользователя (любого).
     *
     * @SWG\Tag(name="Пользователь", description="Загрузка аватара")
     *
     * @Rest\Route(path="/users/avatar-upload", methods={"POST"})
     *
     * @SWG\Parameter(name="avatar", in="query", @SWG\Schema(type="file"), description="Аватар", required=true)
     *
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=ProfileDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function uploadUserAvatarAction(Request $request): JsonResponse
    {
        /** @var File|null $avatar */
        $avatar = $request->files->get('avatar');

        if (empty($avatar)) {
            throw new InvalidArgumentException('Отсутствует аватар');
        }
        /** @var ProfileDTO $uploadUserAvatarDTO */
        $uploadUserAvatarDTO = $this->cqrs->execute(new UploadUserAvatarCommand($avatar));

        return new JsonResponse($this->serializer->normalize($uploadUserAvatarDTO));
    }

    /**
     * Получение рекомендованого списка пользователей.
     *
     * @SWG\Tag(name="Пользователь", description="Получение рекомендованого списка пользователей")
     *
     * @Rest\Route(path="/users/recommended", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=GetRecommendedUsersListRequestFilter::class))
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="pager", type="object", ref=@Model(type=Pager::class)),
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=RecommendedUsersListItemDTO::class))
     *         )
     *     )
     * )
     *
     * @throws ExceptionInterface
     */
    public function getUsersListAction(GetRecommendedUsersListRequestFilter $filter): JsonResponse
    {
        return new JsonResponse(
            $this->serializer->normalize($this->cqrs->get(new GetRecommendedUsersListQuery($filter)))
        );
    }
}
