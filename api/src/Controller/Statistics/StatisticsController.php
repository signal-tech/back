<?php

namespace App\Controller\Statistics;

use App\Feature\Statistics\DTO\StatisticsDayDTO;
use App\Feature\Statistics\DTO\StatisticsMonthDTO;
use App\Feature\Statistics\DTO\StatisticsYearDTO;
use App\Feature\Statistics\Query\GetStatisticsDaysListQuery;
use App\Feature\Statistics\Query\GetStatisticsMonthsListQuery;
use App\Feature\Statistics\Query\GetStatisticsYearsListQuery;
use App\Service\CQRS;
use App\Service\Serializer\SerializerFactory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class StatisticsController extends AbstractController
{
    private Serializer $serializer;

    private CQRS $cqrs;

    public function __construct(CQRS $cqrs)
    {
        $this->serializer = SerializerFactory::create();
        $this->cqrs = $cqrs;
    }

    /**
     * Получение статистик по годам.
     *
     * @SWG\Tag(name="Статистика")
     *
     * @Rest\Route(path="/users/stock-exchange-tokens/{tokenId}/statistics/years", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=StatisticsYearDTO::class))
     *         )
     *     )
     * )
     */
    public function getYearsListAction(int $tokenId): JsonResponse
    {
        $data = $this->cqrs->get(new GetStatisticsYearsListQuery($tokenId));

        return new JsonResponse(['data' => $this->serializer->normalize($data)]);
    }

    /**
     * Получение статистик по месяцам в году.
     *
     * @SWG\Tag(name="Статистика")
     *
     * @Rest\Route(path="/users/stock-exchange-tokens/{tokenId}/statistics/years/{year}/months", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=StatisticsMonthDTO::class))
     *         )
     *     )
     * )
     */
    public function getMonthsListAction(int $tokenId, int $year): JsonResponse
    {
        $data = $this->cqrs->get(new GetStatisticsMonthsListQuery($tokenId, $year));

        return new JsonResponse(['data' => $this->serializer->normalize($data)]);
    }

    /**
     * Получение статистик по дням в месяце.
     *
     * @SWG\Tag(name="Статистика")
     *
     * @Rest\Route(path="/users/stock-exchange-tokens/{tokenId}/statistics/years/{year}/months/{month}/days", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=StatisticsDayDTO::class))
     *         )
     *     )
     * )
     */
    public function getDaysListAction(int $tokenId, int $year, int $month): JsonResponse
    {
        $data = $this->cqrs->get(new GetStatisticsDaysListQuery($tokenId, $year, $month));

        return new JsonResponse(['data' => $this->serializer->normalize($data)]);
    }
}
