<?php

namespace App\Controller\Portfolio;

use App\Feature\Portfolio\DTO\PortfolioDTO;
use App\Feature\Portfolio\Query\GetPortfolioQuery;
use App\Service\CQRS;
use App\Service\Serializer\SerializerFactory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Serializer;

class PortfolioController extends AbstractController
{
    private Serializer $serializer;

    private CQRS $cqrs;

    public function __construct(CQRS $cqrs)
    {
        $this->serializer = SerializerFactory::create();
        $this->cqrs = $cqrs;
    }

    /**
     * Получение портфолио за определенный день.
     *
     * @SWG\Tag(name="Портфолио")
     *
     * @Rest\Route(path="/users/stock-exchange-tokens/{tokenId}/portfoilio/years/{year}/months/{month}/days/{day}", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="data", type="object", ref=@Model(type=PortfolioDTO::class)
     *         )
     *     )
     * )
     */
    public function getDaysListAction(int $tokenId, int $year, int $month, int $day): JsonResponse
    {
        $data = $this->cqrs->get(new GetPortfolioQuery($tokenId, $year, $month, $day));

        return new JsonResponse(['data' => $this->serializer->normalize($data)]);
    }
}
