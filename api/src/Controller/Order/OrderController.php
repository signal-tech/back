<?php

namespace App\Controller\Order;

use App\Feature\Order\DTO\OrderForManualDTO;
use App\Feature\Order\DTO\OrderListDTO;
use App\Feature\Order\DTO\Request\GetOrdersBySubscriptionsListRequestFilter;
use App\Feature\Order\DTO\Request\GetOrdersByUserListRequestFilter;
use App\Feature\Order\Query\GetOrderForManualQuery;
use App\Feature\Order\Query\GetOrdersBySubscriptionsListQuery;
use App\Feature\Order\Query\GetOrdersByUserListQuery;
use App\Service\CQRS;
use App\Service\ListPaginator\Pager;
use App\Service\Serializer\SerializerFactory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

class OrderController extends AbstractController
{
    private Serializer $serializer;

    private CQRS $cqrs;

    public function __construct(CQRS $cqrs)
    {
        $this->serializer = SerializerFactory::create();
        $this->cqrs = $cqrs;
    }

    /**
     * Список сделок по подпискам пользователя.
     *
     * @SWG\Tag(name="Сделки")
     *
     * @Rest\Route(path="users/subscriptions/orders", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=GetOrdersBySubscriptionsListRequestFilter::class))
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="pager", type="object", ref=@Model(type=Pager::class)),
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=OrderListDTO::class))
     *         )
     *     )
     * )
     *
     * @throws ExceptionInterface
     */
    public function getSubscriptionsAction(GetOrdersBySubscriptionsListRequestFilter $filter): JsonResponse
    {
        return new JsonResponse(
            $this->serializer->normalize($this->cqrs->get(new GetOrdersBySubscriptionsListQuery($filter)))
        );
    }

    /**
     * Список сделок пользователя.
     *
     * @SWG\Tag(name="Сделки")
     *
     * @Rest\Route(path="users/{userId}/orders", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=GetOrdersByUserListRequestFilter::class))
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="pager", type="object", ref=@Model(type=Pager::class)),
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=OrderListDTO::class))
     *         )
     *     )
     * )
     *
     * @throws ExceptionInterface
     */
    public function getOrdersByUserAction(GetOrdersByUserListRequestFilter $filter, int $userId): JsonResponse
    {
        return new JsonResponse(
            $this->serializer->normalize($this->cqrs->get(new GetOrdersByUserListQuery($filter, $userId)))
        );
    }

    /**
     * Детальная информация о сделке.
     *
     * @SWG\Tag(name="Сделки")
     *
     * @Rest\Route(path="orders/{orderId}/for-manual", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     *
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=OrderForManualDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function getOrderAction(int $orderId): JsonResponse
    {
        return new JsonResponse(
            [
                'data' => $this->serializer->normalize($this->cqrs->get(new GetOrderForManualQuery($orderId))),
            ]
        );
    }
}
