<?php

namespace App\Controller\StockExchangeToken;

use App\Feature\StockExchangeToken\Command\CreateStockExchangeTokenCommand;
use App\Feature\StockExchangeToken\Command\DeleteStockExchangeTokenCommand;
use App\Feature\StockExchangeToken\DTO\Request\CreateStockExchangeTokenRequest;
use App\Feature\StockExchangeToken\DTO\StockExchangeTokenDTO;
use App\Feature\StockExchangeToken\DTO\StockExchangeTokenListDTO;
use App\Feature\StockExchangeToken\DTO\StockExchangeTokenPublicListDTO;
use App\Feature\StockExchangeToken\Query\GetStockExchangeTokenListQuery;
use App\Feature\StockExchangeToken\Query\GetStockExchangeTokenPublicListQuery;
use App\Feature\StockExchangeToken\Query\GetStockExchangeTokenQuery;
use App\Service\CQRS;
use App\Service\Serializer\SerializerFactory;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Serializer\Exception\ExceptionInterface;
use Symfony\Component\Serializer\Serializer;

class StockExchangeTokenController extends AbstractController
{
    private Serializer $serializer;

    private CQRS $cqrs;

    public function __construct(CQRS $cqrs)
    {
        $this->serializer = SerializerFactory::create();
        $this->cqrs = $cqrs;
    }

    /**
     * Добавление интеграционного токена.
     *
     * @SWG\Tag(name="Интеграционный токен биржи пользователя")
     *
     * @Rest\Route(path="/users/stock-exchange-tokens", methods={"POST"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model(type=CreateStockExchangeTokenRequest::class))
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=StockExchangeTokenDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function createTokenAction(Request $request): JsonResponse
    {
        $data = $request->request->all();

        $createStockExchangeTokenRequest = $this->serializer->denormalize($data, CreateStockExchangeTokenRequest::class);

        /** @var int $tokenId */
        $tokenId = $this->cqrs->execute(new CreateStockExchangeTokenCommand($createStockExchangeTokenRequest));

        $profile = $this->cqrs->get(new GetStockExchangeTokenQuery($tokenId));

        return new JsonResponse($this->serializer->normalize($profile));
    }

    /**
     * Получение списка интеграционных токенов.
     *
     * @SWG\Tag(name="Интеграционный токен биржи пользователя")
     *
     * @Rest\Route(path="/users/stock-exchange-tokens", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=StockExchangeTokenListDTO::class))
     *         )
     *     )
     * )
     */
    public function getTokensAction(Request $request): JsonResponse
    {
        $profile = $this->cqrs->get(new GetStockExchangeTokenListQuery());

        return new JsonResponse(['data' => $this->serializer->normalize($profile)]);
    }

    /**
     * Получение списка интеграционных токенов конкретного пользователя.
     *
     * @SWG\Tag(name="Интеграционный токен биржи пользователя")
     *
     * @Rest\Route(path="/users/{userId}/stock-exchange-tokens", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="data", type="array",
     *             @SWG\Items(ref=@Model(type=StockExchangeTokenPublicListDTO::class))
     *         )
     *     )
     * )
     */
    public function getUserTokensAction(Request $request, int $userId): JsonResponse
    {
        $profile = $this->cqrs->get(new GetStockExchangeTokenPublicListQuery($userId));

        return new JsonResponse(['data' => $this->serializer->normalize($profile)]);
    }

    /**
     * Удаление интеграционного токена.
     *
     * @SWG\Tag(name="Интеграционный токен биржи пользователя")
     *
     * @Rest\Route(path="/users/stock-exchange-tokens/{id}", methods={"DELETE"})
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="success", type="boolean"),
     *         @SWG\Property(property="id", type="integer")
     *     )
     * )
     *
     * @throws ExceptionInterface
     */
    public function deleteTokenAction(int $id): JsonResponse
    {
        $id = $this->cqrs->execute(new DeleteStockExchangeTokenCommand($id));

        return new JsonResponse(['success' => (bool) $id, 'id' => $id]);
    }

    /**
     * Получение интеграционного токена.
     *
     * @SWG\Tag(name="Интеграционный токен биржи пользователя")
     *
     * @Rest\Route(path="/users/stock-exchange-tokens/{id}", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     * @SWG\Response(response=Response::HTTP_OK, description="OK", @Model(type=StockExchangeTokenDTO::class))
     *
     * @throws ExceptionInterface
     */
    public function getTokenAction(int $id): JsonResponse
    {
        $profile = $this->cqrs->get(new GetStockExchangeTokenQuery($id));

        return new JsonResponse($this->serializer->normalize($profile));
    }
}
