<?php

namespace App\Controller\Base;

use App\Service\Environment\EnvironmentService;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\Model;
use OpenApi\Annotations as SWG;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends AbstractController
{
    /**
     * Получение конфига приложения.
     *
     * @SWG\Tag(name="Конфиг")
     *
     * @Rest\Route(path="/config", methods={"GET"})
     *
     * @SWG\Parameter(name="{}", in="query", @Model)
     *
     * @SWG\Response(
     *     response=Response::HTTP_OK,
     *     description="OK",
     *     @SWG\JsonContent(
     *         type="object",
     *         @SWG\Property(property="version", type="string"),
     *         @SWG\Property(property="binanceOrderManualEnabled", type="boolean"),
     *         @SWG\Property(property="ftxOrderManualEnabled", type="boolean")
     *     )
     * )
     * )
     */
    public function getTokenPartsAction(): JsonResponse
    {
        return new JsonResponse([
            'version' => EnvironmentService::getApiVersion(),
            'binanceOrderManualEnabled' => true, //TODO потом куда нибудь перенести (скорее всего в енв)
            'ftxOrderManualEnabled' => false,
        ]);
    }
}
