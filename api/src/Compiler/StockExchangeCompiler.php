<?php

namespace App\Compiler;

use App\Service\StockExchange\StockExchangeServiceInterface;
use Symfony\Component\DependencyInjection\Compiler\CompilerPassInterface;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Reference;
use function array_keys;

class StockExchangeCompiler implements CompilerPassInterface
{
    public function process(ContainerBuilder $container): void
    {
        $definition = $container->findDefinition(StockExchangeServiceInterface::class);
        $services = $container->findTaggedServiceIds('stock_exchange_service');

        foreach (array_keys($services) as $service) {
            $definition->addMethodCall('addElement', [new Reference($service)]);
        }
    }
}
