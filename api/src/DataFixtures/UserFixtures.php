<?php

namespace App\DataFixtures;

use App\Feature\User\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class UserFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
        // create 20 products! Bam!
        for ($i = 10; $i < 99; ++$i) {
            $product = new User();
            $product->setPhone('+799915724' . $i);
            $product->setName('name' . $i);
            $product->setSurname('surname' . $i);
            $manager->persist($product);
        }

        $manager->flush();
    }
}
