<?php

namespace App\Exception\StockExchange;

use App\Exception\AbstractException;

class StockExchangeInvalidPermissionException extends AbstractException
{
}
