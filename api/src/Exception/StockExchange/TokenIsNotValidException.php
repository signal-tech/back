<?php

namespace App\Exception\StockExchange;

use App\Exception\AbstractException;

class TokenIsNotValidException extends AbstractException
{
}
