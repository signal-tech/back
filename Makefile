init: init_docker_compose start-containers api_post_init
update: api_post_update
start: start-containers

reset: api_reset
test: api_test

include ./make/docker-compose.make
include ./make/api.make

phpcs: api_phpcs
phpcsfix: api_phpcsfix
stan: api_stan
psalm: api_psalm

.PHONY: init
