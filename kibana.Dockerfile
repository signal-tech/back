FROM docker.elastic.co/kibana/kibana:5.2.2

RUN bin/kibana-plugin remove x-pack
